import frappe, datetime
from frappe.frappeclient import FrappeClient
from popupevents import send_emails
from apisync import connect_to_server

###############################FOR API CALLS###################################

@frappe.whitelist()
def get_discount_list():
    return frappe.db.sql("""SELECT name, discount_id from `tabSupplier Discounts`""")

@frappe.whitelist()
def get_updated_discounts(modified):
    return frappe.db.sql("""SELECT name, discount_id from `tabSupplier Discounts` where modified >=%s""", modified)

###############################FOR BENCH EXECUTE###############################

#set ids in CDO MAIN. DO NOT EXECUTE IN BRANCHES
@frappe.whitelist()
def set_discount_ids_main():
    discounts = frappe.db.sql("""SELECT name,discount_id from `tabSupplier Discounts`""")
    for i, discount in enumerate(discounts):
        frappe.db.sql("""UPDATE `tabSupplier Discounts` set discount_id = %s where name = %s""", ((i+1), discount[0]))
        #print i, discount[0], discount[1], (i+1 == discount[1])
    frappe.db.commit()

@frappe.whitelist()
def set_discount_ids():
    data = []
    try:
        # main_server = FrappeClient("http://202.137.117.24", "tailerp_api@gaisano.com", 't@1l3rpAPI')
        main_server = connect_to_server()
        discounts = main_server.get_api('gaisano.supplier_sync.get_discount_list')
    except:
        print "NO INTERNET."
    else:
        for discount in discounts:
            disc_in_local = frappe.db.sql("""SELECT count(*) from `tabSupplier Discounts` where name = %s""", discount[0])
            #print discount[0], disc_in_local[0][0]
            if disc_in_local[0][0] == 1:
                frappe.db.sql("""UPDATE `tabSupplier Discounts` set discount_id = %s where name =%s""", (discount[1], discount[0]))
            else:
                data.append({"discount":discount[0],"discount_id":discount[1]})
        frappe.db.commit()
    print "RENAMED OR NOT USED DISCOUNTS:"
    for item in data:
        print item["discount"], item['discount_id']

def get_local_discounts_not_used():
    data = []
    print "DISCOUNTS NOT USED"
    discounts = frappe.db.sql("""SELECT name from `tabSupplier Discounts` where (discount_id = 0)""")
    for disc in discounts:
        print disc[0]

########################################FOR API SYNC############################################
def minute_update_task():
    #ADD same as minute update task code, update based on last sync if server is not syncing.
    print "---------DISCOUNTS API SYNC--------"
    server_branch = frappe.db.get_value("Server Information", None, "branch")
    print "THIS SERVER BRANCH: ", server_branch, "| UPDATE?: ", ((server_branch!= "CDO Main") or (server_branch!= "Replication"))
    if (server_branch!= "CDO Main"):
        modified = datetime.datetime.now() - datetime.timedelta(minutes=6)
        update_local_discounts(modified)

def daily_update_task():
    print "---------DISCOUNTS DAILY SYNC--------"
    now_hour = datetime.datetime.now().hour
    server_branch = frappe.db.get_value("Server Information", None, "branch")
    print "THIS SERVER BRANCH: ", server_branch, "| UPDATE?: ", ((server_branch!= "CDO Main") or (server_branch!= "Replication"))
    if (server_branch!= "CDO Main"):
        modified = datetime.datetime.now() - datetime.timedelta(days=1)
        if now_hour==9:
            update_local_discounts(modified)

def force_update_discounts():
    print "---------DISCOUNTS WEEKLY SYNC--------"
    server_branch = frappe.db.get_value("Server Information", None, "branch")
    print "THIS SERVER BRANCH: ", server_branch, "| UPDATE?: ", ((server_branch!= "CDO Main") or (server_branch!= "Replication"))
    if (server_branch!= "CDO Main"):
        modified = datetime.datetime.strptime("2015-04-04", "%Y-%m-%d")
        print modified
        update_local_discounts(modified)

def update_local_discounts(modified):
    from apisync import get_latest_modified
    modified = modified if modified != None else (datetime.datetime.now() - datetime.timedelta(minutes=6))
    print modified
    last_modified = get_latest_modified()
    print "LAST MODIFIED", last_modified
    if last_modified < modified:
        print "not syncing since ", last_modified
        modified = last_modified
    try:
        # main_server = FrappeClient("http://202.137.117.24", "tailerp_api@gaisano.com", 't@1l3rpAPI')
        main_server = connect_to_server()
        discounts = main_server.get_api('gaisano.supplier_sync.get_updated_discounts', params={"modified": modified})
    except:
        print "NO INTERNET."
    else:
        errors_= []
        error_msg = "</b>DISCOUNTS with sync problems:</b><br>"
        if discounts and (len(discounts)>0):
            for discount in discounts:
                print discount[0], discount[1]
                item_in_local = get_discount_in_local(discount[1])
                print "Item in Local:", item_in_local
                try:
                    server_doc = main_server.get_doc("Supplier Discounts", discount[0])
                except:
                    print "ERROR on GET DOC"
                    errors_.append("<li>ERROR on get_doc: "+discount[0]+"</li>")
                    continue
                else:
                    if item_in_local is not None:
                        update_details(item_in_local, server_doc)
                        if item_in_local!=discount[0]:
                            frappe.rename_doc("Supplier Discounts", item_in_local, discount[0])
                        else:
                            print "--------------Update only--------------"
                    else:
                        print "--------------new discount--------------"
                        #try:
                        for_insert = frappe.get_doc(server_doc)
                        for_insert.discount_code = discount[0]
                        for_insert.discount_id = discount[1]
                        for_insert.items = {}
                        print for_insert.discount_code, for_insert.discount_id, for_insert.supplier_discount_name, for_insert.name
                        for_insert.insert()
                        #except:
                        #    errors_.append("<li>ERROR on insert: " + discount[0] + "</li>")
                        #    print "ERROR ON INSERT"
                        #    continue
                        #else:
                        #    print "insert this item"
                        frappe.db.sql("""UPDATE `tabSupplier Discounts` set discount_id = %s where name = %s""",(discount[1],discount[0]))
                        frappe.db.commit()
            for item in errors_:
                error_msg+=item
            if error_msg!="</b>DISCOUNTS with sync problems:</b><br>":
                server_branch = frappe.db.get_value("Server Information", None, "branch")
                error_msg += "<br><b>FOR BRANCH: "+server_branch+"</b>"
                send_emails(error_msg, "Supplier Discount Errors")
        else:
            print "No discounts to update"


def update_details(local_name, server_doc):
    update_discounts_and_other_details(local_name, server_doc)
    update_branch_availability(server_doc["all_branches"], server_doc["branch_table"], local_name, server_doc["disable"])
    update_branch_items(local_name, server_doc["items"])
    update_modified(local_name, server_doc['modified'], server_doc['modified_by'])

def update_discounts_and_other_details(local_name, server_doc):

    #print "update discounts"
    disc1, disc2, disc3, disc4 = server_doc["disc_1"], server_doc["disc_2"], server_doc["disc_3"], server_doc["disc_4"]
    bo1, bo2, bo3, bo4 = server_doc["bo_disc_1"], server_doc["bo_disc_2"], server_doc["bo_disc_3"], server_doc["bo_disc_4"]
    freight_cost_paid_by_supplier = server_doc['freight_cost_paid_by_supplier']
    discount_name = server_doc["supplier_discount_name"]
    supplier = server_doc["supplier_name"]
    #contact_email = None
    #try:
    #    contact_email = None if server_doc["contact_email"] is None else server_doc["contact_email"]
    #except:
    #    contact_email = ""
    #print supplier, "supplier name", contact_email

    frappe.db.sql("""UPDATE `tabSupplier Discounts` set disc_1 = %s, disc_2 = %s, disc_3 = %s, disc_4 = %s
                  where name = %s""", (disc1, disc2, disc3, disc4, local_name))
    frappe.db.sql("""UPDATE `tabSupplier Discounts` set bo_disc_1 = %s, bo_disc_2 = %s, bo_disc_3 = %s, bo_disc_4 = %s
                      where name = %s""", (bo1, bo2, bo3, bo4, local_name))
    frappe.db.sql("""UPDATE `tabSupplier Discounts` set supplier_discount_name = %s, supplier_name = %s where name = %s""", (discount_name, supplier, local_name))

    frappe.db.sql("""UPDATE `tabSupplier Discounts` set freight_cost_paid_by_supplier = %s where name = %s""",
                  (freight_cost_paid_by_supplier, local_name))
    frappe.db.commit()

def update_branch_items(local_name, discount_items):
    print "UPDATE ITEMS!"
    frappe.db.sql("""delete from `tabSupplier Discount Items` where parent = %s""", local_name)
    frappe.db.commit()
    for item in discount_items:
        try:
            #print item["items"]
            for_insert = frappe.get_doc(item)
            for_insert.parent = local_name
            for_insert.insert()
        except:
            continue

def update_branch_availability(all_branches, branch_table, discount, disable):
    #Copy disabled, all branches and branch_table:
    frappe.db.sql("""UPDATE `tabSupplier Discounts` set all_branches =%s, disable = %s where name = %s""",
                  (all_branches, disable, discount))
    frappe.db.sql("""DELETE from `tabItem Branch` where parent = %s""", discount)
    for branch in branch_table:
        row = frappe.get_doc(branch)
        row.parent = discount
        row.insert()
    disable_discount(all_branches, branch_table, discount, disable)

def disable_discount(all_branches, branch_table, discount, disable):
    if disable != 1:
        if all_branches == 1:
            frappe.db.sql("""UPDATE `tabSupplier Discounts` set disable = 0 where name = %s""", discount)
        else:
            enable_discount = in_branches(branch_table)
            print "enable_discount?", enable_discount
            if enable_discount:
                frappe.db.sql("""UPDATE `tabSupplier Discounts` set disable = 0 where name = %s""", discount)
            else:
                frappe.db.sql("""UPDATE `tabSupplier Discounts` set disable = 1 where name = %s""", discount)
        frappe.db.commit()


def in_branches(branch_table):
    enable_discount = False
    server_branch = frappe.db.get_value("Server Information", None, "branch")
    print "This server is:", server_branch
    for branch in branch_table:
        print "Compare with ", branch["branch"]
        if branch["branch"] == server_branch:
            print "Branch in table. Return True."
            enable_discount = True
            break
    return enable_discount


def get_discount_in_local(discount_id):
    local_name = frappe.db.sql("""SELECT name from `tabSupplier Discounts` where discount_id = %s""", discount_id)
    if local_name:
        return local_name[0][0]
    else:
        return None

def update_modified(discount, modified, modified_by):
    frappe.db.sql("""UPDATE `tabSupplier Discounts` set modified = %s, modified_by = %s where name = %s""",
                  (modified, modified_by, discount))
    #frappe.db.commit()