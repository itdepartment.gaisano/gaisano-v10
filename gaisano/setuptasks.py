import frappe
import datetime
import frappe.utils


def set_branch_offtake():
    suppliers = frappe.db.sql("""SELECT name, supplier_name from  `tabSupplier`""")
    for i in suppliers:
        supplier = frappe.get_doc("Supplier", i[0])
        copy_offtake_values(supplier)


def copy_offtake_values(supplier):
    cycle_time = supplier.cycle_time
    lead_time = supplier.lead_time
    payment_terms = supplier.payment_terms
    buffer = supplier.buffer
    parent = supplier.name

    if len(frappe.db.sql(
            """SELECT name, cycle_time from `tabBranch Offtake Details` where parent = %s and branch = 'CDO Main'""",
            parent)) < 1:
        offtake_setup = {
            "doctype": "Branch Offtake Details",
            "parent": parent,
            "parentfield": "branch_offtake_setup",
            "parenttype": "Supplier",
            "branch": "CDO Main",
            "cycle_time": cycle_time,
            "lead_time": lead_time,
            "payment_terms": payment_terms,
            "buffer": buffer
        }
        entry = frappe.get_doc(offtake_setup)
        entry.insert()


def set_barcode_packing():
    for item in frappe.db.sql("""SELECT name, item_code from `tabItem` where type !='Disabled'"""):
        item_doc = frappe.get_doc("Item", item[0])
        if len(item_doc.item_classification_1)>0:
            print item_doc.item_classification_1[0].classification
            temp = str(item_doc.item_classification_1[0].classification)+" | pck: "+ str(item_doc.packing)+ " | "+str(item_doc.barcode_retial)
            frappe.db.sql("UPDATE `tabItem` set classification_packing_barcode = %s where name = %s", (temp, item_doc.name))