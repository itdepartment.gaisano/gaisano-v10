import frappe
import datetime, decimal
import frappe.utils
from events import add_item_to_supplier_discount
from sletasks import delete_sle_range
from apisync import update_netcost_and_margins
from datetime import datetime, date, time, timedelta

@frappe.whitelist()
def fix_zero_item(from_date):
    from seevents import update_stock_ledger
    zero_item_list = check_zero_item(from_date)
    for transaction in zero_item_list:
        if transaction[1] == 'Stock Entry':
            se_doc = frappe.get_doc("Stock Entry", transaction[0])
            frappe.db.sql("""DELETE FROM `tabStock Ledger Entry` WHERE voucher_no=%s """,transaction[0])
            frappe.db.commit()
            update_stock_ledger(se_doc)
        else:
            rr_doc = frappe.get_doc("Purchase Receipt", transaction[0])
            frappe.db.sql("""DELETE FROM `tabStock Ledger Entry` WHERE voucher_no=%s """,transaction[0])
            frappe.db.commit()
            update_stock_ledger(rr_doc)

@frappe.whitelist()
def check_zero_item(from_date):
    zero_item = []
    next_item ={}

    to_date = datetime.now()
    warehouse = frappe.db.get_value("Auto PO", "Auto PO", 'auto_po_warehouse')
    sl_entries = get_stock_ledger_entries(from_date, to_date, warehouse)
    for i, sle in enumerate(sl_entries):
        next_index = i + 1
        current_item = sle['item_code']
        if current_item in zero_item:
            break

        try:
            next_item = sl_entries[next_index]
        except:
            break

            
        if current_item == next_item['item_code']:
            balance = sle['qty_after_transaction']
            x = next_item['actual_qty']
            next_balance = balance + x
            if next_balance != next_item['qty_after_transaction'] and next_item['qty_after_transaction'] == 0:
                zero_item.append([next_item['voucher_no'], next_item['voucher_type'], current_item])

    
        else:
            continue

    return zero_item

        


def get_stock_ledger_entries(from_date, to_date, warehouse):
    return frappe.db.sql("""SELECT item_code, actual_qty, qty_after_transaction,
                            voucher_no, voucher_type FROM `tabStock Ledger Entry` sle
                            WHERE posting_date BETWEEN %s AND %s AND warehouse = %s and voucher_type != 'Stock Reconciliation'
                            ORDER BY  item_code, posting_date ASC, posting_time ASC, sle.name ASC""",(from_date, to_date, warehouse), as_dict=1)

@frappe.whitelist()
def recreate_sle():
    branch = frappe.db.get_value("Server Information", None, "branch")
    to_time = datetime.now()
    from_time = to_time - timedelta(hours=1)
    print "From: ", from_time
    print "To:",to_time
    recreate_sle_rr(from_time, to_time, branch)
    time_completed_rr = datetime.now() - to_time
    se_time_start = datetime.now()
    recreate_sle_se(from_time, to_time, branch)
    time_complete_se = datetime.now() - se_time_start
    print "RR Time Completed: ", time_completed_rr
    print "SE Time Completed: ", time_complete_se
    print "Overall Time Completed: ", datetime.now() - to_time

@frappe.whitelist()
def recreate_sle_se(from_time,to_time,branch):
    from seevents import update_stock_ledger
    print "-----------SE without SLE---------------"
    ses = frappe.db.sql("""SELECT se.name FROM `tabStock Entry` se WHERE se.modified >= %s and se.modified <= %s and se.branch = %s
      and se.docstatus = 1 and NOT EXISTS (SELECT * FROM `tabStock Ledger Entry` sle where se.name = sle.voucher_no)""",(from_time,to_time,branch))
    for se in ses:
        print se[0]
        se_doc = frappe.get_doc("Stock Entry", se[0])
        update_stock_ledger(se_doc)

@frappe.whitelist()
def recreate_sle_rr(from_time,to_time,branch):
    from rrevents import update_stock_ledger
    print "-----------RR without SLE---------------"
    rrs = frappe.db.sql("""SELECT rr.name FROM `tabPurchase Receipt` rr WHERE rr.modified >= %s and rr.modified <= %s and rr.branch = %s
                                and rr.docstatus = 1 and NOT EXISTS (SELECT * FROM `tabStock Ledger Entry` sle where rr.name = sle.voucher_no)""",(from_time,to_time, branch))
    for rr in rrs:
        print rr[0]
        rr_no = frappe.get_doc("Purchase Receipt", rr[0])
        update_stock_ledger(rr_no)

@frappe.whitelist()
def create_so(date, name):
    doc = frappe.get_doc("Create SO", name)
    if not doc.docstatus:
        return {"status": "Requesting"}


# use this to remove duplicate supplier discounts in 1 supplier.
@frappe.whitelist()
def remove_duplicate_supplier_discounts():
    existing_discounts = frappe.db.sql(
        """SELECT name, item_code, supplier_discount FROM `tabItem` WHERE supplier_discount is NOT NULL or supplier_discount !=''""")
    print "=====================DISCOUNTS======================="
    for discount in existing_discounts:
        supplier_discounts = frappe.db.sql(
            """SELECT name, items FROM `tabSupplier Discount Items` where parent = %s and items = %s """,
            (discount[2], discount[0]))
        c = frappe.db.sql("""SELECT COUNT(items) FROM `tabSupplier Discount Items` where parent = %s and items = %s """,
                          (discount[2], discount[0]))
        if c[0][0] > 1:
            # print c[0][0]
            for i, duplicate in enumerate(supplier_discounts):
                if i == 0:
                    print 1
                    continue
                else:
                    print duplicate[0]
                    delete = frappe.db.sql("""DELETE FROM `tabSupplier Discount Items` where name = %s""", duplicate[0])
                    frappe.db.commit()


# Run set_offtake_items AFTER uploading CSV file via bench import-csv https://frappe.github.io/frappe/user/en/guides/data/import-large-csv-file
@frappe.whitelist()
def set_offtake_items():
    print "===========+SET OFFTAKE ITEMS+============="
    item_name = ""
    rows = frappe.db.sql("""SELECT barcode, item, name, branch from `tabUpload Offtake`""")
    for row in rows:
        # print "BARCODE: "+ str(row[0])+ " | ITEM: "+str(row[1])+" | name: "+str(row[2])
        try:
            id = "" + str(row[3]) + "-" + str(row[0])  # set id as branch-barcode format
            if id == row[2]:  # if name has been set, continue.
                continue
            # offtake_entry = frappe.get_doc("Upload Offtake", row[2])
            # find item with barcode = offtake records barcode entry
            item = frappe.db.sql("""SELECT name, item_code from `tabItem` where barcode_retial like %s""", ('%'+row[0]+'%'))

            if len(item) == 0:
                print "Item for " + str(row[0]) + "does not exist! Deleting item...."
                frappe.db.sql("""DELETE FROM `tabUpload Offtake` WHERE name = %s""", row[2])
                frappe.db.commit()
                # print "ITEM DELETED."
                continue
            else:
                for i in item:  # I think there should be only 1 item. But welp.
                    item_name = i[0]
                print "item name is : " + str(item_name)
                try:
                    id = "" + str(row[3]) + "-" + str(row[0])
                    delete_offtake_items(id)
                    print id
                    update = frappe.db.sql(
                        """UPDATE `tabUpload Offtake` SET item = %s, name = %s, offtake_id = %s where name= %s""",
                        (item_name, id, id, row[2]))
                    # update = frappe.db.sql("""UPDATE `tabUpload Offtake` SET item = %s, name = %s, offtake_id = %s where barcode = %s and branch = %s""", (item_name, id, id, row[0], row[3]))
                    frappe.db.commit()
                except:  # if it causes an error because of duplicate entries, etc, delete the item. LOL.
                    frappe.db.sql("""DELETE FROM `tabUpload Offtake` WHERE name = %s""", row[2])
                    frappe.db.commit()
        except:
            print "ERROR..."#error

# delete upload offtake items using this one
def delete_offtake_items(name):
    rows = frappe.db.sql("""SELECT barcode, item, name from `tabUpload Offtake` where name = %s""", name)
    for row in rows:
        print "DELETING OLD ENTRY FOR :" + row[2]
        frappe.db.sql("""DELETE FROM `tabUpload Offtake` WHERE name = %s""", row[2])
        frappe.db.commit()

@frappe.whitelist()
def update_po_dates(po = None):
    if "New" in po:
        return "no"
    else:
        docstatus = frappe.get_doc("Purchase Order", po).docstatus
        print docstatus

        if (docstatus == 1) or (docstatus == 0):
            rows = frappe.db.sql('''SELECT po.transaction_date, po.supplier, sup.lead_time, po.docstatus from `tabPurchase Order` po
                inner join `tabSupplier Discounts` sup on po.supplier_discount = sup.name where po.name = %s''', po)
            for po_data in rows:
                po_date = po_data[0]
                lead_time = po_data[2]
                po_date += datetime.timedelta(days=int(lead_time))
                cancellation_date = po_date + datetime.timedelta(days=1)
                print "Exepcted Delivery | Cancellation Date"
                print po_date, cancellation_date

                if str(po_data[0]) != po_date:
                    frappe.db.sql('''UPDATE `tabPurchase Order` set expected_delivery = %s, cancellation_date = %s where name = %s''',
                                  (po_date, cancellation_date, po))
                    frappe.db.commit()
        elif docstatus == 2:
            frappe.msgprint("The PO is already cancelled!")
            return 404
        else:
            frappe.msgprint("Oh god why.")

            return 404


@frappe.whitelist()
def freight_cost_fix():
    items =frappe.db.sql("""select item_code, item_cost, item_freight, item_freight_peso, item_cost_with_freight  from `tabItem` where item_cost_with_freight = '' """)
    for item in items:
        item_code, item_cost, item_freight_perc, item_freight_peso, item_cost_with_freight = item[0], item[1], item[2], item[3], item[4]
        new_cost_perc = float(item_cost)*(1+(float(item_freight_perc)/100))
        new_cost_peso = float(item_cost) + float(item_freight_peso)
        if (float(item_cost) != float(item_cost_with_freight)) and (item_freight_perc == 0) and (item_freight_peso==0):
            print "UPDATING: ", item_code, item_cost_with_freight, "WITH: ", item_cost
            frappe.db.sql("""UPDATE `tabItem` set item_cost_with_freight = %s where item_code = %s and item_cost_with_freight = '' """,
                          (item_cost, item_code))
            frappe.db.commit()
        elif (item_freight_perc > 0) and (item_freight_peso==0):
            print item_code, "item cost = with freight perc", new_cost_perc
        elif (item_freight_perc == 0) and (item_freight_peso > 0):
            print item_code, "item cost = with freight peso", new_cost_peso
        else:
            continue

def check_and_delete_rr(from_date, to_date):
    branch = frappe.db.get_value("Server Information", None, "branch")
    data = []
    from popupevents import delete_duplicate_rr
    from_time = datetime.now()
    print from_date, to_date, "Check and Delete RR...."
    rrs = frappe.db.sql("""SELECT rr.name, rr.delivered_to_warehouse, rr.posting_date, count(sle.name) from `tabPurchase Receipt` as rr
                                inner join `tabStock Ledger Entry` sle on sle.voucher_no = rr.name
                                where rr.docstatus = 1 and rr.posting_date >=%s and rr.posting_date <= %s and rr.branch = %s
                                GROUP BY rr.name""", (from_date, to_date, branch))
    for rr in rrs:
        items = frappe.db.sql("""SELECT count(*) from `tabPurchase Receipt Item` where parent = %s""", rr[0])
        no_sles = rr[3]
        no_items = items[0][0] if items[0][0] != None else 0
        print rr[0], rr[2], no_items, "|", no_sles
        if no_items != no_sles:
            print "UDPATE RR ----------------*********"
            data.append(rr[0])
    if len(data) > 0:
        print "RR's For Update:"
        data = list(set(data))
        for rr in data:
            print rr
            delete_duplicate_rr(rr)
    check_no_sle_rr(from_date, to_date, branch)
    print "TIME:", datetime.now() - from_time

def check_and_delete_se(from_date, to_date):
    branch = frappe.db.get_value("Server Information", None, "branch")
    current_voucher = None
    data = []
    sles = frappe.db.sql("""SELECT DISTINCT se.name, se.from_warehouse, se.posting_date, count(sle.name) from `tabStock Entry` as se
                            inner join `tabStock Ledger Entry` sle on sle.voucher_no = se.name and se.from_warehouse = sle.warehouse
                            where se.type != "Bad Order" and se.docstatus = 1 and se.posting_date >= %s and se.posting_date <= %s and se.branch = %s
                            GROUP BY se.name""", (from_date, to_date, branch))
    print from_date, to_date, "Check and Delete SE...."
    for sle in sles:
        current_voucher= sle[0]
        count_items = frappe.db.sql("""SELECT count(*) from `tabStock Entry Detail` where parent = %s""", sle[0])
        no_items = count_items[0][0] if count_items[0][0]!=None else 0
        no_sles = sle[3]
        print current_voucher, sle[1], sle[2], no_items, no_sles
        if no_items != no_sles:
            print "UPDATE.-------------------------***"
            data.append(current_voucher)
    data = list(set(data))
    print "\n\n********FOR UPDATE************"
    for se in data:
        print se
        from popupevents import delete_duplicate_se
        delete_duplicate_se(se)
    check_no_sle_se(from_date, to_date, branch)


def check_delete_rr_hourly(branch, from_time, to_time):
    from popupevents import delete_duplicate_rr
    rrs = frappe.db.sql("""SELECT distinct rr.name, count(sle.name) from `tabPurchase Receipt` rr join `tabStock Ledger Entry` sle
                        on sle.voucher_no = rr.name where rr.branch = %s and rr.modified>=%s 
                        and rr.modified<=%s group by rr.name""",(branch, from_time, to_time))
    for rr in rrs:
        item_count = frappe.db.sql("""SELECT COUNT(*) from `tabPurchase Receipt Item` where parent = %s""", rr[0])

        if rr[1] != item_count[0][0]:
            print rr[0], rr[1], item_count[0][0]
            delete_duplicate_rr(rr[0])

    recreate_sle_rr(from_time, to_time, branch)

def check_delete_se_hourly(branch, from_time, to_time):
    from popupevents import delete_duplicate_se
    ses = frappe.db.sql("""SELECT distinct se.name, count(sle.name) from `tabStock Entry` se join `tabStock Ledger Entry` sle
                        on sle.voucher_no = se.name where se.branch = %s and se.modified>=%s 
                        and se.modified<=%s group by se.name""",(branch, from_time, to_time))
    for se in ses:
        item_count = frappe.db.sql("""SELECT COUNT(*) from `tabStock Entry Detail` where parent = %s""", se[0])

        if se[1]!=(item_count[0][0]*2):
            print se[0], se[1], item_count[0][0]*2
            delete_duplicate_se(se[0])

    recreate_sle_se(from_time, to_time, branch)


#def test_rename():
#    item_name = "Hera Test"
#    new_name = "GORABELLS"
#    frappe.rename_doc("Item", item_name, new_name)

def check_no_sle_rr(from_date, to_date, branch):
    from popupevents import delete_duplicate_rr
    print branch,"98749081723098734*******8"
    data = []
    print "-----------NO SLES---------------"
    rrs = frappe.db.sql("""SELECT rr.name FROM `tabPurchase Receipt` rr WHERE rr.posting_date >= '{0}' and rr.posting_date <= '{1}' and rr.branch ='{2}'
                            and rr.docstatus = 1 and NOT EXISTS (SELECT * FROM `tabStock Ledger Entry` sle where rr.name = sle.voucher_no and
                            sle.posting_date = rr.posting_date and sle.posting_time = rr.posting_time) """.format(from_date, to_date, branch))
    for rr in rrs:
        print rr[0]
        delete_duplicate_rr(rr[0])

def check_no_sle_se(from_date, to_date, branch):
    from popupevents import delete_duplicate_se
    data = []
    print "-----------NO SLES---------------"
    ses = frappe.db.sql("""SELECT se.name FROM `tabStock Entry` se WHERE se.posting_date >= '{0}' and se.posting_date <= '{1}' and se.branch = '{2}'
                            and se.docstatus = 1 and NOT EXISTS (SELECT * FROM `tabStock Ledger Entry` sle where se.name = sle.voucher_no and
                            sle.posting_date >= '{0}' and sle.posting_date <= '{1}')""".format(from_date, to_date, branch))
    for se in ses:
        print se[0]
        delete_duplicate_se(se[0])

@frappe.whitelist()
def check_sles_rr(from_date, to_date):
    msg = "<h3>RR's With duplicate SLE's</h3><small>Please double check if problem persists by running gaisano.popupevents.check_and_delete_sles()</small>"
    data = []
    sles = frappe.db.sql("""SELECT DISTINCT name,delivered_to_warehouse from `tabPurchase Receipt` where docstatus = 1 and creation >= %s and creation<=%s""", (from_date, to_date))
    for sle in sles:
        items = frappe.db.sql("""SELECT count(*) from `tabPurchase Receipt Item` where parent = %s""", sle[0])
        print sle[0], items[0][0]
        count_sles = frappe.db.sql("""SELECT count(*) from `tabStock Ledger Entry` where voucher_no = %s and warehouse = %s""", (sle[0], sle[1]))
        print sle[0], "ITEMS: ", items[0][0], "SLE's: ",  count_sles[0][0]
        if count_sles[0][0] != items:
            print "----------------------------------*FOR UPDATE*"
            data.append(sle[1])
    if len(data)>0:
        print "RR's For Update:"
        data=list(set(data))
        for rr in data:
            print rr
            msg += "<li>" + rr + "</li>"
        #send_emails(msg, "RR's With duplicate SLE's")

@frappe.whitelist()
def check_sles_se(from_date, to_date):

    msg = "<h3>SE's With duplicate SLE's</h3>"
    data = []
    sles = frappe.db.sql("""SELECT DISTINCT name, from_warehouse, posting_date from `tabStock Entry`
                                where type != "Bad Order" and docstatus = 1 and creation >= %s and creation <= %s
                                order by posting_date asc""", (from_date, to_date))
    print from_date, to_date, "Check and Delete SE...."
    for sle in sles:
        current_voucher = sle[0]
        count_items = frappe.db.sql("""SELECT count(*) from `tabStock Entry Detail` where parent = %s""", sle[0])
        no_items = count_items[0][0] if count_items[0][0] != None else 0
        count_sles = frappe.db.sql(
            """SELECT count(*) from `tabStock Ledger Entry` where voucher_no = %s and warehouse = %s""",
            (sle[0], sle[1]))
        no_sles = count_sles[0][0] if count_sles[0][0] != None else 0
        #print current_voucher, sle[1], sle[2], no_items, no_sles
        print current_voucher, no_items, no_sles
        if no_items != no_sles:
            print "UPDATE.-------------------------***"
            data.append(current_voucher)
    data = list(set(data))
    if len(data)>0:
        print "\n\n********SE FOR UPDATE************"
        for se in data:
            print se
            msg += "<li>" + se + "</li>"
        #send_emails(msg, "SE's With duplicate SLE's")

@frappe.whitelist()
def send_emails(msg, subject):
    emails = ['hvillanueva.gaisano@gmail.com','rdvdeleonio@gmail.com']
    from frappe.desk.page.chat.chat import post
    for email in emails:
        print email
        post(**{"txt": msg, "contact": email, "subject": subject, "notify": 1})

@frappe.whitelist()
def get_branch():
    return frappe.db.get_value("Server Information", None, "branch")


def renumber_supplier_discount_items():
    discounts = frappe.db.sql("""SELECT name from `tabSupplier Discounts`""")
    for discount in discounts:
        print discount[0]
        discount_doc = frappe.get_doc("Supplier Discounts", discount[0])
        item_list = get_item_list(discount_doc.items)
        print "LEN ITEMS ORIGINAL", len(discount_doc.items)
        for item in discount_doc.items:
            frappe.get_doc("Supplier Discount Items", item.name).delete()
        frappe.db.commit()
        for item in item_list:
            add_item_to_supplier_discount(item['item_code'], discount[0])
        print "LEN NEW ITEMS", len(item_list)

def get_item_list(items):
    discount_items = []
    for item in items:
        discount_items.append({"item_code":item.items})
    return discount_items

def update_item_discount_table():
    discounts = frappe.db.sql("""SELECT name from `tabSupplier Discounts` where disable!=1 and discount_id>0 and discount_id<101""")
    for discount in discounts:
        print discount[0]
        discount_doc = frappe.get_doc("Supplier Discounts", discount[0])
        for item in discount_doc.items:
            print item.items, item.item_description
            if discount_doc.all_branches ==1:
                insert_to_all_branches(item.items, discount[0])
            else:
                insert_to_select_branches(item.items, discount[0], discount_doc.branch_table)

def insert_to_all_branches(item, discount):
    print "Insert to all branches!---------------------------$"
    for branch in frappe.db.sql("""SELECT name from `tabBranch`"""):
        print branch[0], discount
        insert_item_branch_discount(item, discount, branch[0])

def insert_to_select_branches(item, discount, branches):
    print "Insert to selected branches!-----------------------##"
    for branch in branches:
        print branch.branch, discount
        insert_item_branch_discount(item, discount, branch.branch)

def insert_item_branch_discount(item,discount,branch):
    count_rows = frappe.db.sql("""SELECT count(*) from `tabItem Branch Discount` where parent = %s and supplier_discount = %s
          and branch = %s""",(item, discount, branch))
    if count_rows[0][0]==0:
        discount_doc = frappe.get_doc("Supplier Discounts", discount)
        discount_row = {
            "doctype": "Item Branch Discount",
            "branch":branch,
            "supplier_discount":discount,
            "supplier_discount_name": discount_doc.supplier_discount_name,
            "parent": item,
            "parentfield":"item_branch_discounts",
            "parenttype": "Item"
        }

        row = frappe.get_doc(discount_row)
        row.insert()
        frappe.db.commit()

def rename_branch_upload_pos(trans_date=None, to_date=None):
    #select distinct filename, branch from `tabUpload POS` order by branch;
    count = 0
    if trans_date is None:
        query = frappe.db.sql("""select distinct filename, branch from `tabUpload POS` order by branch""")
    elif (trans_date is not None) and (to_date is None):
        print "1 trans date"
        query = frappe.db.sql("""select distinct filename, branch from `tabUpload POS` where trans_date = %s order by branch""", trans_date)
    else:
        print "range trans date"
        query = frappe.db.sql(
            """select distinct filename, branch from `tabUpload POS` where trans_date >= %s and trans_date <=%s order by branch""",
            (trans_date, to_date))
    for row in query:
        filename, branch = row[0], row[1]
        for i, pos_row in enumerate(frappe.db.sql("""SELECT name from `tabUpload POS` where filename = %s and branch = %s order by creation""",
                                     (filename, branch))):
            current = i + 1
            new_name = filename + "-" + branch + "-"
            if new_name not in pos_row[0]:
                new_name = filename + "-" + branch + "-" + str(current).zfill(7)
                count +=1
                print "FOR UPDATE:", filename, branch, pos_row[0]
                frappe.db.sql("""UPDATE `tabUpload POS` set name = %s where name = %s""",(new_name, pos_row[0]))
                frappe.db.commit()
        print "--------------END OF FILE (", filename, branch, ")-------------------"

def autofill_price_retail():
    rows = frappe.db.sql("""SELECT name from `tabItem` where item_price >1 and (item_price_retail_with_margin = 0 or item_price_retail_1 = 0)""")
    for item in rows:
        item_doc = frappe.get_doc("Item", item[0])
        print item_doc.name, item_doc.item_cost, item_doc.item_cost_with_freight, item_doc.item_price_retail_with_margin
        per_piece = float(item_doc.item_cost_with_freight) / float(item_doc.packing)
        frappe.db.sql("""UPDATE `tabItem` set item_price_retail_1 = %s, item_price_retail_with_margin = %s
                      where name = %s""",(per_piece, item_doc.item_price, item[0]))
        if (item_doc.item_cost_with_freight != item_doc.item_price):
            print "UPDATE margin/srp"
            item_margin_rate = ((item_doc.item_price-item_doc.item_cost_with_freight)/item_doc.item_cost_with_freight)*100 if item_doc.item_cost_with_freight > 0 else 0
            frappe.db.sql("""UPDATE `tabItem` set retail_margin_rate = %s where name = %s""",(item_margin_rate, item[0]))
        updated_item = frappe.get_doc("Item", item[0])
        update_netcost_and_margins(updated_item)
        frappe.db.commit()
    print len(rows), "NUMBER OF ROWS UPDATED"

def regenerate_sle(from_date, to_date):
    item_code = None
    current = 1
    sles = frappe.db.sql("""select name, voucher_no, item_code from `tabStock Ledger Entry` where name
                            not like %s and posting_date >=%s and posting_date <=%s order by
                            voucher_no asc, item_code asc""",('SLE/ITEM%',from_date, to_date))
    for sle in sles:
        if (item_code is None) or (item_code == sle[2]):
            item_code = sle[2]

        else:
            item_code = sle[2]
            current = 1
        update_sle_name_recursive(current, item_code, sle)
        #i = str(current).zfill(5)
        #new_name = "SLE/" + item_code + "-" + sle[1] +"-"+i
        #current += 1

        #print sle[1], item_code, new_name
        #try:
        #    frappe.db.sql("""UPDATE `tabStock Ledger Entry` set name = %s where name = %s""",(new_name, sle[0]))
        #    frappe.db.commit()
        #except:
        #    current += 1
        #    i = str(current).zfill(5)
        #    new_name = "SLE/" + item_code + "-" + sle[1] + "-" + i
        #    current += 1
        #    frappe.db.sql("""UPDATE `tabStock Ledger Entry` set name = %s where name = %s""", (new_name, sle[0]))
        #    frappe.db.commit()
        #else:
        #    continue
    delete_sle_range(from_date, to_date)

def update_sle_name_recursive(current, item_code, sle):
    i = str(current).zfill(5)
    new_name = "SLE/" + item_code + "-" + sle[1]+ "-" + i
    current += 1
    print sle[1], item_code, new_name
    try:
        frappe.db.sql("""UPDATE `tabStock Ledger Entry` set name = %s where name = %s""", (new_name, sle[0]))
        frappe.db.commit()
    except:
        current += 1
        update_sle_name_recursive(current, item_code, sle)
    else:
        pass

def update_item_retail_price():
    items = frappe.db.sql("""SELECT name from `tabItem` where ROUND(item_cost_with_freight/packing,2) != (item_price_retail_1) """)
    #print len(items)
    for item in items:
        item_doc = frappe.get_doc("Item", item[0])
        item_retail = decimal.Decimal(str(item_doc.item_price_retail_1)).quantize(decimal.Decimal('.01'),
                                                                                  rounding=decimal.ROUND_HALF_UP)
        print item, item_doc.item_cost_with_freight, item_doc.packing, item_doc.item_price_retail_1, item_retail
        item_margin_rate = ((item_doc.item_price - item_doc.item_cost_with_freight) / item_doc.item_cost_with_freight) * 100 if item_doc.item_cost_with_freight > 0 else 0
        frappe.db.sql("""UPDATE `tabItem` set retail_margin_rate = %s, item_price_retail_1 = %s where name = %s""", (item_margin_rate, item_retail, item[0]))
        updated_item = frappe.get_doc("Item", item[0])
        update_netcost_and_margins(updated_item)

def update_item_actual_rates():
    import time
    branch = frappe.db.get_value("Server Information", None, "branch")
    items = frappe.db.sql("""SELECT disc.parent, disc.supplier_discount, itm.item_cost_with_freight, itm.packing,
                              itm.item_price, itm.item_price_retail_with_margin from `tabItem Branch Discount` disc
                              inner join `tabItem` itm on itm.name = disc.parent where disc.branch = %s""", branch)
    start = time.time()
    for itm in items:
        item_code, discount = itm[0], itm[1]
        item_cost_with_freight, packing, item_price, item_price_retail_with_margin = itm[2], itm[3], itm[4], itm[5]
        disc = frappe.get_doc("Supplier Discounts", discount)

        net_cost = item_cost_with_freight * (1 - (float(disc.disc_1) / 100)) * (1 - (float(disc.disc_2) / 100)) * \
                   (1 - (float(disc.disc_3) / 100)) * (1 - (float(disc.disc_4) / 100))
        net_cost_retail = net_cost / packing
        actual_ws_margin = 0 if net_cost == 0 else ((item_price - net_cost) / net_cost) * 100
        actual_ret_margin = 0 if net_cost_retail == 0 else ((item_price_retail_with_margin - net_cost_retail) / net_cost_retail) * 100

        frappe.db.sql(
            """UPDATE `tabItem` set net_cost = %s, actual_retail_rate =%s, actual_wholesale_rate = %s where name = %s""",
            (net_cost, actual_ret_margin, actual_ws_margin, item_code))
        frappe.db.commit()
        #print item_code, item.item_cost_with_freight, net_cost, net_cost_retail, actual_ws_margin, actual_ret_margin
    end = time.time()
    time_spent = end - start
    print "No. of items: ", len(items), time_spent