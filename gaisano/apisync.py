import frappe, datetime
from frappe.frappeclient import FrappeClient
from events import get_rounded_price, add_item_to_supplier_discount
from autopoevents import update_item_stock_level
from frappe.utils.password import get_decrypted_password, check_password, encrypt, decrypt
#TODO: get_doc returns error (json not readable/parseable). Add try catch and continue
####################
## IGNORE FOR NOW ##
####################

@frappe.whitelist()
def connect_to_server():
	
	hostname = frappe.get_value("Advanced Settings", "Advanced Settings", "api_hostname")
	username = frappe.get_value("Advanced Settings", "Advanced Settings", "api_username")
	password = get_decrypted_password("Advanced Settings", "Advanced Settings", "api_password")
	print(hostname, username, password)
	
	try:
		# main_server = FrappeClient("http://202.137.117.24:8082", "tailerp_api@gaisano.com", 't@1l3rpAPI')
		main_server = FrappeClient(hostname, username, password)
	except:
		frappe.throw("ERROR ESTABLISHING TO "+hostname)
		return None
		
	else:
		# print(test)
		# main_server.logout()
		# frappe.msgprint("Server Connected")
		return main_server

	

def is_outbranch():
	outbranch = frappe.db.get_value("Server Information", None, "is_outbranch")
	if outbranch == "Yes":
		print "Is OUTBRANCH"
		return True
	else:
		print "Not an Outbranch"
		return False

def update_server_info_table(not_matching):
	update_items_not_syncing(not_matching)
	#update_html_table()

def is_in_branch(branch_list):
	server_branch = frappe.db.get_value("Server Information", None, "branch")
	return True if (server_branch in branch_list) else False

@frappe.whitelist()
def get_item_name(barcode):
    item = frappe.db.sql("""select name from `tabItem` where barcode_retial = %s limit 1""", barcode)
    if len(item) >0:
        return item
    else:
        return None

def get_item_doc(item_id):
	try:
		main_server = connect_to_server()
		# main_server = FrappeClient("http://202.137.117.24", "tailerp_api@gaisano.com", 't@1l3rpAPI')
		item = main_server.get_api('gaisano.itemsync.get_item_doc', params={"item_id": item_id})
	except:
		return "ERROR IN ESTABLISHING CONNECTION"
	else:
		if (item != None) and (item != "ERROR"):
			item_doc = frappe.get_doc(item)
			return item_doc
		else:
			return "ERROR IN GETTING ITEM"


#get newly updated items:
@frappe.whitelist()
def get_updated_items(date_modified):
    items = frappe.db.sql("""select name, item_cost, barcode_retial, item_freight_peso, item_cost_with_freight,item_cost_without_vat,modified from `tabItem` where modified >= %s ORDER by modified """, date_modified)
    if len(items) >0:
        return items

def get_latest_modified():
	#item = frappe.db.sql("""SELECT modified from `tabItem` order by modified desc limit 1""")
	date_time = datetime.datetime.strptime(frappe.db.get_value("Server Information", None, "last_sync"), "%Y-%m-%d %H:%M:%S.%f")
	return date_time

def first_update_task():
	print "----------***API SYNC FIRST UPDATE***-------------"
	now = datetime.datetime.now()
	now_time = now.time()
	now_hour = now_time.hour
	print now, now_time, now_hour
	branch = frappe.db.get_value("Server Information", None, "branch")
	ip_address = frappe.db.get_value("Server Information", None, "server_ip")
	print branch
	if (branch == "CDO Main"):
		print "CDO MAIN or REPLICATION"
		pass
	else:
		print "OKAY"
		if now_hour == 9:
			yesterday = now - datetime.timedelta(days=1)
			print "update items"
			update_modified_items(yesterday)
		else:
			pass

def minute_update_task():
	print "----------***API SYNC PER MINUTE TASK***-------------"
	now = datetime.datetime.now()
	now_time = now.time()
	now_hour = now_time.hour
	print now, now_time, now_hour
	branch = frappe.db.get_value("Server Information", None, "branch")
	ip_address = frappe.db.get_value("Server Information", None, "server_ip")
	print branch
	if (branch == "CDO Main"):
		print "CDO MAIN or REPLICATION"
		pass
	else:
		print "OKAY"
		if now_hour >= 9:
			print "----------***update items per minute***-------------"
			update_modified_items()
		else:
			pass

@frappe.whitelist()
def update_html_table():
	not_matching = frappe.db.sql(
		"""SELECT barcode, description, date_updated, sync_error from `tabItems Not Syncing`""")
	msg = "<center><h3>ITEM COSTS NOT SYNCING:</h3></center><br><table class='table table-bordered table-hover'>" \
		  "<tr><th>Barcode</th><th>Item Desc</th><th>Sync Attempted On</th><th>ERROR</th></tr>"
	print "--------------UPDATE SERVER INFO TABLE--------------------"

	print len(not_matching)
	for data in not_matching:
		print data[0], data[1], data[2], data[3]
		msg += "<tr><td>" + data[0] + "</td><td>" + data[1] + "</td><td> " + str(data[2]) + "</td><td>" + data[
			3] + "</td></tr>"
	msg += "</table>"
	print "\n", msg
	return msg
	#frappe.db.set_value("Server Information", None, "items_not_syncing", msg)
	#frappe.db.set_value("Server Information", None, "sync_status", "Errors on Sync")


def update_items_not_syncing(items):
	for data in items:
		existing = frappe.db.sql("""SELECT name from `tabItems Not Syncing` where barcode = %s""",
								 data["barcode"])
		if existing:
			continue

		item_dict = {
			"doctype": "Items Not Syncing",
			"parent": "Server Information",
			"parenttype": "Server Information",
			"parentfield": "items",
			"description": data["item"],
			"barcode": data["barcode"],
			"item_id": data["item_id"],
			"date_updated": datetime.datetime.now(),
			"sync_error": data["cost_in_local"] if ((data["cost_in_local"])) else "COST NOT MATCHING"
		}
		table_row = frappe.get_doc(item_dict)
		table_row.insert()

##############################
### UPDATED TO GET ITEM ID ###
##############################

def get_item_from_main(item_id):
	try:
		item = get_item_doc(item_id)
	except:
		print "SOMETHING WENT WRONG."
		return "ERROR"
	else:
		try:
			for_insert = frappe.get_doc(item)
			for_insert.item_branch_discounts = {}
			#item.supplier_discount = None
			for_insert.insert()
		except:
				print "ERROR"
				return "ERROR"
		else:
				frappe.db.sql("""UPDATE `tabItem` set item_id = %s where name = %s""",(item_id, item.name))
				update_item_branches(for_insert.name, item.all_branches, item.type, item.branches)
				update_item_discounts(for_insert.name, item.item_branch_discounts)
				print "OK"
				return "OK"

##################################################
##TASK for ALL task (update items every minute):##
##################################################

@frappe.whitelist()
def update_modified_items(date_time = None):
	date_now = date_time if date_time!=None else (datetime.datetime.now() - datetime.timedelta(minutes=6))
	print date_now
	last_modified = get_latest_modified()
	print "LAST MODIFIED", last_modified
	#datetime.datetime.
	#print last_modified - date_time
	if last_modified < date_now:
		print "not syncing since ", last_modified
		date_now = last_modified
		merge_rename_all(date_now)

	print str(date_now)
	print "*------------UPDATE RECENTLY MODIFIED-------------*"
	try:
		# todo : change URL; add port 8080
		replication = connect_to_server()
		# replication = FrappeClient("http://202.137.117.24", "tailerp_api@gaisano.com", 't@1l3rpAPI')
		# replication = FrappeClient("http://192.168.5.200", "tailerp_api@gaisano.com", 't@1l3rpAPI')
		last_item_replication = replication.get_api('gaisano.itemsync.get_updated_items',
													params={"date_modified": str(date_now)})
	except:
		print "Update status, sync failed. CDO Main Server Cannot be reached"
		#return {"status": "CDO Main Sever Cannot be Reached.", "last_sync": ""}
	else:
		if last_item_replication:
			not_matching = compare_updated_items(last_item_replication)
			frappe.db.set_value("Server Information", None, "last_sync", datetime.datetime.now())
			if len(not_matching) > 0: #Update Table Details to show items that have not synced or have errors
				update_server_info_table(not_matching)
			else:
				frappe.db.set_value("Server Information", None, "sync_status", "Syncing")
				now = datetime.datetime.now()
				#update_html_table()
				#msg = "Item Costs Last Update at " + str(now)
				#frappe.db.set_value("Server Information", None, "items_not_syncing", msg)


def compare_updated_items(item_list):
	print "UPDATE PRICE METHOD"
	not_matching = []
	for replication_item in item_list:
		# name, item_cost, barcode_retial, item_freight_peso, item_cost_with_freight
		item_name = frappe.db.sql("""SELECT name from `tabItem` where item_id = %s""", replication_item[2])
		if item_name:
			try:
				local_item = frappe.get_doc("Item", item_name[0][0])
			except:
				temp = {"item": replication_item[0], "barcode": replication_item[7], "cost_in_local": "ERROR ON GET DOC",
						"item_id": replication_item[2], "cost_in_main": replication_item[1]}
				not_matching.append(temp)
				continue
			else:
				if local_item.name != replication_item[0]:
					temp = update_item_doc(local_item, replication_item)
					if "ERROR" in temp:
						item_dict = {"item": replication_item[0], "barcode": replication_item[7], "cost_in_local": "ERROR ON UPDATE",
							"item_id":replication_item[2], "cost_in_main": replication_item[1]}
					else:
						#TODO: DEPRECATE RENAME.
						print "UPDATE ITEM DONE. **** RENAME ****", replication_item[1], local_item.item_name
						frappe.rename_doc("Item", local_item.name, replication_item[0])
						#item_dict = {"item": replication_item[0], "item id": replication_item[2],
						#		"cost_in_local": "DIFF. ITEM NAME",
						#		"cost_in_main": replication_item[1]}
						#not_matching.append(item_dict)
				else:
					temp = update_item_doc(local_item, replication_item)
					if "ERROR" in temp:
						not_matching.append({"item": replication_item[0], "barcode": replication_item[7], "cost_in_local": temp,
							"item_id":replication_item[2], "cost_in_main": replication_item[1]})
		else:
			print "Not in Local"
			try:
				item_insert = get_item_from_main(replication_item[2])
			except:
				temp = {"item": replication_item[0], "barcode": replication_item[7], "cost_in_local": "ERROR ON INSERT",
						"item_id":replication_item[2], "cost_in_main": replication_item[1]}
				not_matching.append(temp)
				continue
			else:
				if "ERROR" in str(item_insert):
					temp = {"item": replication_item[0], "barcode": replication_item[7],
							"cost_in_local": item_insert,
							"item_id":replication_item[2], "cost_in_main": replication_item[1]}
					not_matching.append(temp)
	return not_matching


def update_item_doc(item_in_local, replication_item):
	print "update item: call all the functions!"
	success = update_item_details(item_in_local.name, replication_item[2])
	update_item_prices(item_in_local.name, replication_item[1], replication_item[3], replication_item[4], replication_item[5]) # will also update srp + netcosts/etc
	if "ERROR" in success:
		return "ERROR ON PRICE UPDATE"
	else:
		return "OK"
def update_item_prices(item_name, updated_cost, freight, item_cost_with_freight, item_cost_without_vat):
	#TODO: if with rebates, do not update item cost!
	local_item_cost_with_freight = frappe.get_value('Item', item_name, 'item_cost_with_freight')
	cost_without_vat = item_cost_without_vat
	print "***********UPDATE ITEM PRICES AND COSTS************"
	try:
		main_server = connect_to_server()
		# main_server = FrappeClient("http://202.137.117.24", "tailerp_api@gaisano.com", 't@1l3rpAPI')
		replication_item = main_server.get_doc("Item",item_name)
	except:
		print "-----CDO MAIN CANNOT BE REACHED-----"
		print "ERROR on Get Replication Item for do not sync"
	else:
		#print replication_item
		do_not_sync_freight = replication_item['do_not_sync_freight']
		frappe.db.sql("""UPDATE `tabItem` set do_not_sync_freight = %s where name = %s""",(do_not_sync_freight,item_name))
		if not is_outbranch() and replication_item['do_not_sync_freight']!=1:
			print "--------Update Freight-------" #CDO Branches
			frappe.db.sql(
				"""UPDATE `tabItem` set item_cost = %s, item_cost_with_freight = %s, item_freight_peso=%s, item_cost_without_vat = %s, valuation_rate = %s where name = %s""",
				(updated_cost, item_cost_with_freight, freight, cost_without_vat, item_cost_with_freight, item_name,))
			frappe.db.sql("""Update `tabItem Price` set price_list_rate = %s where item_code = %s
									and price_list = 'Standard Buying'""", (item_cost_with_freight, item_name))
			#print item_name, "Cost with freight:", item_cost_with_freight, "item_cost", updated_cost
		else:
			print "------Do not update freight.----" #OUTBRANCHES. Use branch Freight.
			item_cost_with_freight, freight = get_freight(item_name, updated_cost) 
			frappe.db.sql(
				"""UPDATE `tabItem` set item_cost = %s, item_cost_with_freight = %s, item_freight_peso=%s, item_cost_without_vat = %s where name = %s""",
				(updated_cost, item_cost_with_freight, freight, cost_without_vat, item_name))
			frappe.db.sql("""Update `tabItem Price` set price_list_rate = %s where item_code = %s
											and price_list = 'Standard Buying'""", (item_cost_with_freight, item_name))
		frappe.db.commit()
		doc = frappe.get_doc('Item', item_name)
		if doc.allow_rebate == 1:
			check_previous_cost_item(doc, local_item_cost_with_freight, replication_item['barcode_retial'], replication_item['item_cost'], replication_item['retail_margin_rate'], replication_item['margin_rate'])

		update_srp(doc, local_item_cost_with_freight, replication_item['barcode_retial'], replication_item['item_cost'], replication_item['retail_margin_rate'], replication_item['margin_rate'])

def check_previous_cost_item(item, local_item_cost_with_freight, barcode, cost, retail_margin, wholesale_margin):
	print(item.previous_price , float(item.packing))
	price_per_piece = (item.previous_price) / float(item.packing)
	old_price_per_piece = (local_item_cost_with_freight) / float(item.packing)

	temp_price_retail = get_rounded_price(price_per_piece*(1+(item.retail_margin_rate/100))) # retail price with margin rate
	temp_price_wholesale = get_rounded_price(float(item.item_cost_with_freight)*(1+(item.margin_rate/100))) #wholesale price with margin rate
	
	#server side values
	temp_old_retail = get_rounded_price(old_price_per_piece*(1+(item.retail_margin_rate/100)))
	temp_old_wholesale = get_rounded_price(float(local_item_cost_with_freight)*(1+(item.margin_rate/100)))

	old_item_price_w = temp_old_wholesale if (item.use_retail_for_wholesale !=1) else (
	temp_old_wholesale * item.packing)
	old_item_price_r = temp_old_retail if (item.use_wholesale_price_for_retail_price !=1) else old_item_price_w

	new_item_price_w = temp_price_wholesale if (item.use_retail_for_wholesale != 1) else (
	temp_price_retail * item.packing)  # if use retail * packing is checked, use retail price with margin * packing as wholesale price
	new_item_price_r = temp_price_retail if (item.use_wholesale_price_for_retail_price !=1) else new_item_price_w # if use wholesale price for retail is checked,

	print(price_per_piece, old_price_per_piece , temp_price_retail, temp_old_wholesale)


def update_srp(item, local_item_cost_with_freight, barcode, cost, retail_margin, wholesale_margin):
	new_srp_is_less = []

	# TODO: Review code
	print item.retail_margin_rate, item.margin_rate, 'Local Margin', retail_margin, wholesale_margin, ' Server This is the margin'
	price_per_piece = (item.item_cost_with_freight) / float(item.packing)
	old_price_per_piece = (local_item_cost_with_freight) / float(item.packing)

	print "#############################"
	print item.item_price_retail_1, old_price_per_piece

	srp_r = (((float(item.item_price_retail_with_margin) - float(price_per_piece)) / float(
		price_per_piece)) * 100) if price_per_piece != 0 else 0

	temp_price_retail = get_rounded_price(
		price_per_piece * (1 + (item.retail_margin_rate / 100)))  # retail price with margin rate
	temp_price_wholesale = get_rounded_price(
		float(item.item_cost_with_freight) * (1 + (item.margin_rate / 100)))  # wholesale price with margin rate

	# server side values

	old_item_price_r = item.item_price_retail_with_margin
	old_item_price_w = item.item_price

	# temp_old_retail = get_rounded_price(old_price_per_piece * (1 + (item.retail_margin_rate / 100)))
	# temp_old_wholesale = get_rounded_price(float(local_item_cost_with_freight) * (1 + (item.margin_rate / 100)))

	# old_item_price_w = temp_old_wholesale if (item.use_retail_for_wholesale != 1) else (
	#     temp_old_wholesale * item.packing)
	# old_item_price_r = temp_old_retail if (item.use_wholesale_price_for_retail_price != 1) else old_item_price_w

	new_item_price_w = temp_price_wholesale if (item.use_retail_for_wholesale != 1) else (
	temp_price_retail * item.packing)  # if use retail * packing is checked, use retail price with margin * packing as wholesale price
	new_item_price_r = temp_price_retail if (
	item.use_wholesale_price_for_retail_price != 1) else new_item_price_w  # if use wholesale price for retail is checked, retail price should be = item wholesale price

	if ((new_item_price_r < old_item_price_r) or (new_item_price_w < old_item_price_w)) and item.ladlad != 1:
		new_srp_is_less.append({"item": item.item_name_dummy, "barcode": barcode,
								"cost_in_local": "Price Decrease/For Manual Updating/Rebates", "item_id": item.item_id,
								"cost_in_main": cost})
		frappe.db.sql(
			"""UPDATE `tabItem` set item_price_retail_1 = %s, previous_price = %s , allow_rebate = 1 where name = %s""",
			(price_per_piece, local_item_cost_with_freight, item.name))
		frappe.db.commit()
		update_netcost_and_margins(item)
	else:
		frappe.db.sql(
			"""UPDATE `tabItem` set item_price_retail_1 = %s, item_price=%s, item_price_retail_with_margin = %s, 
            previous_price = %s, allow_rebate = 0 where name = %s""",
			(price_per_piece, new_item_price_w, new_item_price_r, local_item_cost_with_freight, item.name))
		frappe.db.commit()
		item = frappe.get_doc("Item", item.name)
		update_netcost_and_margins(item)

	update_items_not_syncing(new_srp_is_less)


def update_netcost_and_margins(item):
	print "===========update srp margins=========="
	disc1, disc2, disc3, disc4 = 0, 0, 0, 0
	server_branch = frappe.db.get_value("Server Information", None, "branch")
	discount = frappe.db.sql("""SELECT supplier_discount from `tabItem Branch Discount` where parent = %s and branch =%s""", (item.name, server_branch))
	print discount
	if len(discount)>0:
		try:
			for i in discount:
				try:
					sup_disc = frappe.get_doc("Supplier Discounts", discount[0][0])
				except:
					continue
				else:
					disc1, disc2, disc3, disc4 = sup_disc.disc_1, sup_disc.disc_2, sup_disc.disc_3, sup_disc.disc_4
		except:
			pass

	net_cost = (((float(item.item_cost_with_freight) * (1 - (disc1 / 100))) * (1 - (disc2 / 100))) * (1 - (disc3 / 100))) * (1 - (disc4 / 100))
	#print "NET COST:",net_cost
	price_per_piece = net_cost / float(item.packing)
	#print "PRICE PER PIECE:", price_per_piece
	srp_w = (((float(item.item_price) - float(net_cost)) / float(net_cost)) * 100) if net_cost!= 0 else 0
	#print item.item_price, "SRP W:", srp_w
	srp_r = (((float(item.item_price_retail_with_margin) - float(price_per_piece)) / float(price_per_piece)) * 100) if price_per_piece!=0 else 0
	#print item.item_price_retail_with_margin, "SRP R:", srp_r

	frappe.db.sql(
		"""UPDATE `tabItem` set actual_retail_rate = %s, actual_wholesale_rate = %s, net_cost = %s where name = %s """,
		(srp_r, srp_w, net_cost, item.name))
	frappe.db.commit()

def get_freight(item, item_cost):
	new_item_cost = item_cost
	item_doc = frappe.get_doc("Item", item)
	freight_peso = item_doc.item_freight_peso if item_doc.item_freight_peso != None else 0
	freight_perc = item_doc.item_freight if item_doc.item_freight != None else 0
	if freight_peso!=0:
		new_item_cost = item_cost + freight_peso
	else:
		new_item_cost = item_cost+ (item_cost*(freight_perc/100))
	return  new_item_cost, freight_peso

def update_item_details(local_item_name, replication_item_id):
	print "UPDATE ITEM DETAILS", local_item_name
	replication_item = get_item_doc(replication_item_id)
	if replication_item!="ERROR":
		try:
			print "Update tab item, barcode:", replication_item.barcode_retial
			print "Use wholesale price for retial", replication_item.use_wholesale_price_for_retail_price
			frappe.db.sql("""UPDATE `tabItem` set barcode_retial=%s, item_short_name = %s , packing = %s ,
								dep_code = %s , points = %s , minimum = %s , uom = %s , stock_uom=%s, pos_uom = %s, barcode_pack = %s ,
								with_inventory_control = %s , senior = %s , special = %s, all_branches = %s, item_name_dummy=%s,
								item_name = %s, use_wholesale_price_for_retail_price=%s, valuation_rate= %s, ladlad = %s, sync_to_streetby=%s where name = %s""",
						  (replication_item.barcode_retial, replication_item.item_short_name, replication_item.packing,
						   replication_item.dep_code, replication_item.points, replication_item.minimum,
						   replication_item.uom, replication_item.stock_uom,
						   replication_item.pos_uom, replication_item.barcode_pack, replication_item.with_inventory_control,
						   replication_item.senior, replication_item.special, replication_item.all_branches,
						   replication_item.item_name_dummy,
						   replication_item.item_name, replication_item.use_wholesale_price_for_retail_price,
						   replication_item.valuation_rate,
						   replication_item.ladlad,replication_item.sync_to_streetby, 
						   local_item_name))
			frappe.db.sql("""UPDATE `tabItem` set classification_packing_barcode = %s where name = %s""",
						  (replication_item.classification_packing_barcode, local_item_name))
			if replication_item.type == "Promo Pack":
				print "UPDATE PROMO PACK's REGULAR ITEM"
				frappe.db.sql("""UPDATE `tabItem` set regular_item = %s, regular_item_details=%s where name = %s""",
							  (replication_item.regular_item, replication_item.regular_item_details, local_item_name))
			update_uoms(local_item_name, replication_item.uoms)
			update_supplier_items(local_item_name, replication_item.supplier_items)
			update_item_branches(local_item_name, replication_item.all_branches, replication_item.type,
								 replication_item.branches)
			update_item_classification(local_item_name, replication_item.item_classification_1)
			update_item_discounts(local_item_name, replication_item.item_branch_discounts)
			update_item_stock_level(local_item_name)
			frappe.db.commit()
		except:
			print "ERROR."
			return "ERROR ON ITEM UPDATE"
		else:
			frappe.db.sql("""UPDATE `tabItem` set modified = %s, modified_by = %s where name = %s""",
						  (replication_item.modified, replication_item.modified_by, local_item_name))
			frappe.db.commit()
		return "OK"
	else:
		return "ERROR ON ITEM UPDATE"

###############################
##UPDATE CHILD TABLES IN ITEM##
###############################

def update_uoms(item_name, replication_uoms):
	replication_uoms_list = []
	if replication_uoms:
		frappe.db.sql("""delete from `tabUOM Conversion Detail` where parent = %s""", item_name)
		frappe.db.commit()
	# for local_uom in local_uoms:
		for uom in replication_uoms:
			print uom.uom
			uom_temp = uom
			uom_doc = frappe.get_doc(uom_temp)
			uom_doc.parent = item_name
			uom_doc.insert()

def update_supplier_items(item_name, replication_supplier):
	if replication_supplier:
		frappe.db.sql("""delete from `tabItem Supplier` where parent = %s""", item_name)
		frappe.db.commit()
		for supplier in replication_supplier:
			#print supplier.supplier
			supplier_temp = supplier
			supplier_doc = frappe.get_doc(supplier_temp)
			supplier_doc.parent = item_name
			supplier_doc.insert()

def update_item_classification(item_name, replication_classification):
	if replication_classification:
		frappe.db.sql("""DELETE from `tabItem Class Each` where parent = %s""", item_name)
		frappe.db.commit()
		for classification in replication_classification:
			classification_doc = classification
			classification_doc = frappe.get_doc(classification_doc)
			classification_doc.parent = item_name
			classification_doc.insert()


def update_item_branches(item_name, all_branches, item_type, replication_branches):
	branch_list = []

	item_doc = frappe.get_doc("Item", item_name)
	override_type = "" if not item_doc.override_type else item_doc.override_type
	disable_in_branch = 0

	for branch in replication_branches:
		branch_list.append(branch.branch)

	if all_branches == 1:
		if item_type =='Disabled':
			disable_in_branch = 1
		else:
			if (override_type is None) or (override_type == ""):
				disable_in_branch = 0
			else:
				item_type = override_type
				if override_type == "Disabled":
					disable_in_branch =1
				else:
					disable_in_branch = 0

		frappe.db.sql("""UPDATE `tabItem` set type = %s, disabled = %s where name = %s """, (item_type, disable_in_branch, item_name))
	else:
		if replication_branches:
			frappe.db.sql("""DELETE from `tabItem Branch` where parent = %s""", item_name)
			for branch in replication_branches:
				branch_doc = branch
				branch_doc = frappe.get_doc(branch_doc)
				branch_doc.parent = item_name
				branch_doc.insert()

			if is_in_branch(branch_list):
				#print "in Branch."
				if (override_type is not None) and (override_type != ""):
					item_type = override_type
					if override_type == "Disabled":
						disable_in_branch = 1
					else:
						disable_in_branch = 0
				frappe.db.sql("""UPDATE `tabItem` set type =%s, disabled = %s where name = %s """,(item_type, disable_in_branch, item_name))
			else:
				frappe.db.sql("""UPDATE `tabItem` set type ='Disabled', disabled = 1 where name = %s """, item_name)

			frappe.db.commit()


def update_item_discounts(item_name, branch_discounts):
	print "UPDATE DISCOUNTS"
	check_if_discount_exists(branch_discounts)
	if branch_discounts:
		frappe.db.sql("""DELETE from `tabItem Branch Discount` where parent = %s""", item_name)
		frappe.db.commit()
		for branch_discount in branch_discounts:
			print branch_discount.parent, branch_discount.supplier_discount, "-----***"
			row_doc = branch_discount
			row_doc = frappe.get_doc(row_doc)
			row_doc.parent = item_name
			row_doc.insert()
			frappe.db.commit()
			add_item_to_supplier_discount(item_name,branch_discount.supplier_discount)

################################
### METHODS FOR FORCE UPDATE ###
################################
@frappe.whitelist()
def update_items(): #all items
	date_now = "2014-01-01"
	print str(date_now)
	merge_rename_all(datetime.datetime.strptime(date_now,"%Y-%m-%d"))
	try:
		# todo : change URL; add port 8080
		replication = connect_to_server()
		# replication = FrappeClient("http://202.137.117.24", "tailerp_api@gaisano.com", 't@1l3rpAPI')
		last_item_replication = replication.get_api('gaisano.itemsync.get_updated_items', params ={"date_modified": str(date_now)})
	except:
		#TODO: EMAIL.
		return {"status": "CDO Main Sever Cannot be Reached.", "last_sync": ""}
	else:
		print "GET NOT MATCHING"
		not_matching = compare_and_update_item_price(last_item_replication)
		if len(not_matching) > 0:
			frappe.db.sql("""delete from `tabItems Not Syncing` where sync_error != 'Price Decrease/For Manual Updating/Rebates'""")
			frappe.db.commit()
			update_server_info_table(not_matching)
		else:
			now = datetime.datetime.now()
			msg = "Item Costs Last Update at " + str(now)
			frappe.db.set_value("Server Information", None, "last_sync", datetime.datetime.now())
			#frappe.db.set_value("Server Information", None, "items_not_syncing", msg)

def compare_and_update_item_price(item_list):
	print "COMPARE AND UPDATE ITEM PRICE METHOD"
	not_matching = []
	for i, replication_item in enumerate(item_list):
		try:
			print "***********AT: ", i, "|", (len(item_list) - i), "LEFT***********"
			item_name = frappe.db.sql("""SELECT name from `tabItem` where item_id = %s""", replication_item[2])
			if item_name:
				local_item = frappe.get_doc("Item", item_name[0][0])
				print local_item.item_name_dummy,local_item.barcode_retial
				temp = update_item_doc(local_item, replication_item)
				if "ERROR" in temp:
					not_matching.append({"item": local_item.item_name_dummy, "barcode": replication_item[7], "cost_in_local": temp,
										 "item_id":replication_item[2], "cost_in_main": replication_item[1]})
				if local_item.name != replication_item[0]: #TODO: deprecate.
					try:
						print "RENAME DOC!"
						frappe.rename_doc("Item", local_item.name, replication_item[0])
					except:
						temp = {"item": local_item.item_name_dummy, "barcode": replication_item[7], "cost_in_local": "ERROR ON RENAME",
								"item_id":replication_item[2], "cost_in_main": replication_item[1]}
						not_matching.append(temp)
			else:
				print "Not in Local. New item insert."
				try:
					item_insert = get_item_from_main(replication_item[2])
				except:
					temp = {"item": replication_item[0], "barcode": replication_item[7], "cost_in_local": "ERROR IN COPY",
							"item_id":replication_item[2], "cost_in_main": replication_item[1]}
					not_matching.append(temp)
				else:
					if "ERROR" in item_insert:
						temp = {"item": replication_item[0], "barcode": replication_item[7],
								"cost_in_local": item_insert, "item_id":replication_item[2],
								"cost_in_main": replication_item[1]}
						not_matching.append(temp)
		except:
			print "Unforeseen error."
			continue
	return not_matching

##############TEST###################
def update_all_items_uom():
	items = frappe.db.sql("""select item.name, item_id, count(uom.uom) as uom from `tabItem` item left join `tabUOM Conversion Detail` uom on uom.parent = item.name where item.type != 'Disabled' group by item.name having uom = 0""")
	for i,item in enumerate(items):
		print i, " | left: ", (len(items)-i)
		try:
			replication_item = get_item_doc(item[1])
			update_uoms(item[0], replication_item.uoms)
		except:
			print "ERROR on Replication UOM retrieval"
		else:
			print "OK", item[0]

def check_if_discount_exists(branch_discounts):
	for discount in branch_discounts:
		discount_exists = frappe.db.sql("""SELECT count(*) from `tabSupplier Discounts` where name = %s""",
										discount.supplier_discount)
		#print "DOES DISCOUNT EXIST?", discount_exists[0][0], (discount_exists[0][0] > 0)
		if discount_exists[0][0] < 1:
			try:
				main_server = connect_to_server()
				# main_server = FrappeClient("http://202.137.117.24", "tailerp_api@gaisano.com", 't@1l3rpAPI')
				server_doc = main_server.get_doc("Supplier Discounts", discount.supplier_discount)
			except:
				print "-----CDO MAIN CANNOT BE REACHED-----"
				break
			else:
				disc_for_insert = frappe.get_doc(server_doc)
				disc_for_insert.items = {}
				disc_for_insert.branch_offtake = {}
				disc_for_insert.insert()
		else:
			continue

@frappe.whitelist()
def merge_rename_hourly_task():
	server_branch = frappe.db.get_value("Server Information", None, "branch")
	if server_branch != "CDO Main":
		to_ = datetime.datetime.now()
		from_ = to_ - datetime.timedelta(hours=1)
		print from_, to_, server_branch
		try:
			main_server = connect_to_server()
			# main_server = FrappeClient("http://202.137.117.24", "tailerp_api@gaisano.com", 't@1l3rpAPI')
			data = main_server.get_api('gaisano.mergeevents.get_data_for_rename', params ={"from_":from_, "to_":to_})
		except:
			print "-----CDO MAIN CANNOT BE REACHED-----"
		else:
			if data:
				for item in data:
					print item['name'], item['source_doctype'], item['old_name'], item['new_name'], item['is_merge']
					if (item['source_doctype'] == "Item") and item['is_merge'] == True:
						try:
							old_doc = frappe.get_doc("Item", item['old_name'])
							new_doc = frappe.get_doc("Item", item['new_name'])
							print "OLD DOC:",old_doc.name, old_doc.uom, old_doc.stock_uom
							print "NEW DOC:",new_doc.name, new_doc.uom, new_doc.stock_uom
							if (old_doc.uom != new_doc.uom) or (old_doc.stock_uom != new_doc.stock_uom):
								frappe.db.sql("""Update `tabItem` set uom = %s, stock_uom = %s where item_code = %s""",
											  (new_doc.uom, new_doc.uom, old_doc.name))
								frappe.db.commit()
						except:
							pass
					frappe.rename_doc(item['source_doctype'], item['old_name'], item['new_name'],
									  merge=item['is_merge'])


@frappe.whitelist()
def merge_rename_all(from_date):
	server_branch = frappe.db.get_value("Server Information", None, "branch")
	if server_branch != "CDO Main":
		to_ = datetime.datetime.now()
		from_ = from_date
		print from_, to_, server_branch
		try:
			main_server = connect_to_server()
			# main_server = FrappeClient("http://202.137.117.24", "tailerp_api@gaisano.com", 't@1l3rpAPI')
			data = main_server.get_api('gaisano.mergeevents.get_data_for_rename', params ={"from_":from_, "to_":to_})
		except:
			print "-----CDO MAIN CANNOT BE REACHED-----"
		else:
			if data:
				for item in data:
					print item['name'], item['source_doctype'], item['old_name'], item['new_name'], item['is_merge']
					if (item['source_doctype'] == "Item") and item['is_merge'] == True:
						try:
							old_doc = frappe.get_doc("Item", item['old_name'])
							new_doc = frappe.get_doc("Item", item['new_name'])
							print "OLD DOC:",old_doc.name, old_doc.uom, old_doc.stock_uom
							print "NEW DOC:",new_doc.name, new_doc.uom, new_doc.stock_uom
							if (old_doc.uom != new_doc.uom) or (old_doc.stock_uom != new_doc.stock_uom):
								frappe.db.sql("""Update `tabItem` set uom = %s, stock_uom = %s where item_code = %s""",
											  (new_doc.uom, new_doc.uom, old_doc.name))
								frappe.db.commit()
						except:
							pass
					try:
						frappe.rename_doc(item['source_doctype'], item['old_name'], item['new_name'],
									  merge=item['is_merge'])
					except:
						print "Error on:"+item['source_doctype'] +" "+ item['old_name'] +" "+ item['new_name']


def recalculate_prices():
    items = frappe.db.sql(
        """select name, item_cost_with_freight, packing, margin_rate, retail_margin_rate from `tabItem` where type !='Disabled' """)

    for item in items:
        item_name = item[0]
        cost = float(item[1])
        packing = float(item[2])
        w_margin = float(item[3]) / 100
        r_margin = float(item[4]) / 100

        cost_per_piece = cost / packing
        price_w = get_rounded_price((cost * (1 + w_margin)))
        price_r = get_rounded_price((cost_per_piece * (1 + r_margin)))

        print item_name, price_w, price_r

        frappe.db.sql("""UPDATE `tabItem` set item_price_retail_1 =%s, item_price = %s, item_price_retail_with_margin = %s
                          where name = %s""", (cost_per_piece, price_w, price_r, item_name))
        frappe.db.commit
