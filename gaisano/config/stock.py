from __future__ import unicode_literals
from frappe import _


def get_data():
    return [
        {
            "label": _("Items and Pricing"),
            "icon": "octicon octicon-cloud-upload",
            "items": [
                {
                    "type": "doctype",
                    "name": "Item Classification",
                    "label": _("Item Classification"),
                    "description": _("Item Classification"),
                    "hide_count": True
                }
            ]
        },
        {
            "label": _("Setup"),
            "icon": "octicon octicon-cloud-upload",
            "items": [
                {
                    "type": "doctype",
                    "name": "Payment Terms",
                    "label": _("Payment Terms"),
                    "description": _("Payment Terms"),
                    "hide_count": True
                }
            ]
        }
    ]