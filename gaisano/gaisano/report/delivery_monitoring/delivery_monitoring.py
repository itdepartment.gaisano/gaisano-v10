# Copyright (c) 2013, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe


def execute(filters=None):
    columns = [
        {"label": "Supplier", 'width': 200 , "fieldname":"supp_name"},
        {"label": "PO #", 'width': 90, "fieldname": "po_name"},
        {"label": "PO Date", 'width': 90, "fieldname": "po_date"},
        {"label": "RR #", 'width': 90, "fieldname": "rr_name"},
        {"label": "RR Date", 'width': 90, "fieldname": "rr_date"},
        {"label": "Days", 'width': 50, "fieldname": "rday"}
        ]
    data = []
    from_date = filters.get("from_date")
    to_date = filters.get("to_date")
    supplier = filters.get("Supplier")
    branch = filters.get("Branch")

    po = frappe.db.sql("""Select supplier from `tabPurchase Order` where
						  transaction_date >= %s and transaction_date <= %s and supplier = %s and branch = %s""",
                       (from_date, to_date, supplier, branch))
    for s in po:
        supplier = str(s[0])

        break

    doc = frappe.db.sql(
        """SELECT a.supplier, a.name, a.transaction_date, b.name, b.posting_date, datediff(b.posting_date,a.transaction_date), a.status FROM `tabPurchase Order` AS a, `tabPurchase Receipt` AS b WHERE a.name = b.purchase_order and a.supplier = %s AND a.status != 'Cancelled' AND b.status != 'Cancelled' and a.transaction_date >= %s and a.transaction_date <= %s and a.branch = %s""",
        (supplier, from_date, to_date, branch))

    for d in doc:
        supp_name = str(d[0]).center(50)
        po_name = str(d[1])
        po_date = str(d[2])
        rr_name = str(d[3])
        rr_date = str(d[4])
        rday = str(d[5])

        data.append({"supp_name": supp_name, "po_name": po_name.center(90), "po_date": po_date.center(90),
                     "rr_name": rr_name.center(90), "rr_date": rr_date.center(90), "rday": rday.center(90)})
    return columns, data

    # hi try ni
