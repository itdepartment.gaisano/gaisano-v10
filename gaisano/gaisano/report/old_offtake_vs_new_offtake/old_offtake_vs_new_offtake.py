# Copyright (c) 2013, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt


from __future__ import unicode_literals
import frappe
from dateutil.relativedelta import relativedelta
import datetime


def execute(filters=None):
    columns = get_columns(filters)
    data = get_results(filters)
    return columns, data


def get_columns(filters):

    if (filters.get('show_new_breakdown')==1) and (filters.get('show_old_breakdown')==1):
        columns = [
            {"label": "Item Code", 'width': 280, "fieldname": "item_code"},
            {"label": "Packing", 'width': 100, "fieldname": "packing"},
            {"label": "POS|WS|ST", 'width': 150, "fieldname": "b1"},
            {"label": "1st 30 Days", 'width': 80, "fieldname": "m1"},
            {"label": "POS|WS|ST", 'width': 150, "fieldname": "b2"},
            {"label": "2nd 30 Days", 'width': 80, "fieldname": "m2"},
            {"label": "POS|WS|ST", 'width': 150, "fieldname": "b3"},
            {"label": "Last 30 Days", 'width': 80, "fieldname": "m3"},
            #{"label": "CISL", 'width': 50, "fieldname": "cisl"},
            {"label": "Cycle Days", 'width': 50, "fieldname": "cycle_days"},
            {"label": "Offtake", 'width': 50, "fieldname": "offtake"},
            {"label": "Old m1", 'width': 80, "fieldname": "old_m1"},
            {"label": "Old m2", 'width': 80, "fieldname": "old_m2"},
            {"label": "Old m3", 'width': 80, "fieldname": "old_m3"},
            {"label": "Old Offtake", 'width': 50, "fieldname": "old_offtake"}
        ]
    elif (filters.get('show_new_breakdown')==1) and (filters.get('show_old_breakdown')==None):
        columns = [
            {"label": "Item Code", 'width': 280, "fieldname": "item_code"},
            {"label": "Packing", 'width': 100, "fieldname": "packing"},
            {"label": "POS|WS|ST", 'width': 150, "fieldname": "b1"},
            {"label": "1st 30 Days", 'width': 80, "fieldname": "m1"},
            {"label": "POS|WS|ST", 'width': 150, "fieldname": "b2"},
            {"label": "2nd 30 Days", 'width': 80, "fieldname": "m2"},
            {"label": "POS|WS|ST", 'width': 150, "fieldname": "b3"},
            {"label": "Last 30 Days", 'width': 80, "fieldname": "m3"},
            #{"label": "CISL", 'width': 50, "fieldname": "cisl"},
            {"label": "Cycle Days", 'width': 50, "fieldname": "cycle_days"},
            {"label": "Offtake", 'width': 50, "fieldname": "offtake"},
            {"label": "Old Offtake", 'width': 50, "fieldname": "old_offtake"}
        ]
    elif (filters.get('show_new_breakdown')==None) and (filters.get('show_old_breakdown')==1):
        columns = [
            {"label": "Item Code", 'width': 280, "fieldname": "item_code"},
            {"label": "Packing", 'width': 100, "fieldname": "packing"},
            #{"label": "POS|WS|ST", 'width': 150, "fieldname": "b1"},
            {"label": "1st 30 Days", 'width': 80, "fieldname": "m1"},
            #{"label": "POS|WS|ST", 'width': 150, "fieldname": "b2"},
            {"label": "2nd 30 Days", 'width': 80, "fieldname": "m2"},
            #{"label": "POS|WS|ST", 'width': 150, "fieldname": "b3"},
            {"label": "Last 30 Days", 'width': 80, "fieldname": "m3"},
            #{"label": "CISL", 'width': 50, "fieldname": "cisl"},
            {"label": "Cycle Days", 'width': 50, "fieldname": "cycle_days"},
            {"label": "Offtake", 'width': 50, "fieldname": "offtake"},
            {"label": "Old m1", 'width': 80, "fieldname": "old_m1"},
            {"label": "Old m2", 'width': 80, "fieldname": "old_m2"},
            {"label": "Old m3", 'width': 80, "fieldname": "old_m3"},
            {"label": "Old Offtake", 'width': 50, "fieldname": "old_offtake"}
        ]
    else:
        columns = [
            {"label": "Item Code", 'width': 280, "fieldname": "item_code"},
            {"label": "Packing", 'width': 100, "fieldname": "packing"},
            #{"label": "POS|WS|ST", 'width': 150, "fieldname": "b1"},
            {"label": "1st 30 Days", 'width': 80, "fieldname": "m1"},
            #{"label": "POS|WS|ST", 'width': 150, "fieldname": "b2"},
            {"label": "2nd 30 Days", 'width': 80, "fieldname": "m2"},
            #{"label": "POS|WS|ST", 'width': 150, "fieldname": "b3"},
            {"label": "Last 30 Days", 'width': 80, "fieldname": "m3"},
            #{"label": "CISL", 'width': 50, "fieldname": "cisl"},
            {"label": "Cycle Days", 'width': 50, "fieldname": "cycle_days"},
            {"label": "Offtake", 'width': 50, "fieldname": "offtake"},
            {"label": "Old Offtake", 'width': 50, "fieldname": "old_offtake"}
        ]
    return columns


def get_results(filters):
    data = []
    to_date = filters.get("to_date")
    sup_disc = filters.get('supplier')
    m1_range = get_dates(to_date, 60)[2]
    m2_range = get_dates(to_date, 30)[2]
    m3_range = get_dates(to_date, 0)[2]

    dates = {"b1": m1_range, "b2": m2_range, "b3": m3_range}
    data.append(dates)
    if (sup_disc == None) or (sup_disc==''):
        data = get_all_items(filters, data)
    else:
        data = get_items_from_supplier_discount(filters, data)
    return data


def get_item_details(filters, data):
    item_code = filters.get("item_name")
    branch = filters.get('branch')
    supplier = filters.get('supplier')
    to_date = filters.get('to_date')
    from_date = get_dates(to_date, 60)[1]

    item_doc = frappe.get_doc("Item", item_code)

    offtake = get_qty_sold(branch, item_code, supplier, from_date, to_date)[4]
    cisl = get_qty_sold(branch, item_code, supplier, from_date, to_date)[5]
    cycle_days = get_qty_sold(branch, item_code, supplier, from_date, to_date)[6]
    m1, b1 = get_m1(filters, item_code)
    m2, b2 = get_m2(filters, item_code)
    m3, b3 = get_m3(filters, item_code)
    packing = get_uom(item_code, supplier)
    temp = {"item_code": item_doc.item_name_dummy, "b1": b1, "b2": b2, "b3": b3, "m1": m1, "m2": m2, "m3": m3,
            "cisl": cisl, "cycle_days": cycle_days, "offtake": offtake}
    data.append(temp)
    return data


def get_dates(to_date, days):
    now = datetime.datetime.strptime(to_date, '%Y-%m-%d').date()
    now -= datetime.timedelta(days=days)
    date = now - datetime.timedelta(days=30)
    dates = "" + str(date) + " to " + str(now)
    return now, date, dates


def get_m1(filters, item_code, supplier):
    branch = filters.get('branch')
    #supplier = filters.get('supplier')
    to_date = filters.get('to_date')

    total = 0
    eh = 0

    now, date, dates = get_dates(to_date, 60)

    s1, s2, s3 = 0, 0, 0
    total = get_qty_sold(branch, item_code, supplier, date, now)[0]
    s1, s2, s3 = get_qty_sold(branch, item_code, supplier, date, now)[1], \
                 get_qty_sold(branch, item_code, supplier, date, now)[2], \
                 get_qty_sold(branch, item_code, supplier, date, now)[3]
    eh = get_uom(item_code, supplier)
    total /= eh
    breakdown = str(int(s1 / eh)) + " | " + str(int(s2 / eh)) + " | " + str(int(s3 / eh))

    return int(total), breakdown


def get_m2(filters, item_code, supplier):
    branch = filters.get('branch')
    #supplier = filters.get('supplier')
    to_date = filters.get('to_date')
    total = 0
    eh = 0
    now, date, dates = get_dates(to_date, 30)
    s1, s2, s3 = 0, 0, 0
    total = get_qty_sold(branch, item_code, supplier, date, now)[0]
    s1, s2, s3 = get_qty_sold(branch, item_code, supplier, date, now)[1], \
                 get_qty_sold(branch, item_code, supplier, date, now)[2], \
                 get_qty_sold(branch, item_code, supplier, date, now)[3]
    eh = get_uom(item_code, supplier)
    total /= eh
    breakdown = str(int(s1 / eh)) + " | " + str(int(s2 / eh)) + " | " + str(int(s3 / eh))
    return int(total), breakdown


def get_m3(filters, item_code, supplier):
    branch = filters.get('branch')
    #supplier = filters.get('supplier')
    to_date = filters.get('to_date')
    total = 0
    eh = 0
    now, date, dates = get_dates(to_date, 0)
    s1, s2, s3 = 0, 0, 0
    total = get_qty_sold(branch, item_code, supplier, date, now)[0]
    s1, s2, s3 = get_qty_sold(branch, item_code, supplier, date, now)[1], \
                 get_qty_sold(branch, item_code, supplier, date, now)[2], \
                 get_qty_sold(branch, item_code, supplier, date, now)[3]
    eh = get_uom(item_code, supplier)
    total /= eh
    breakdown = str(int(s1 / eh)) + " | " + str(int(s2 / eh)) + " | " + str(int(s3 / eh))
    return int(total), breakdown


def get_all_items(filters, data):
    supplier_discounts = frappe.db.sql("""SELECT name from `tabSupplier Discounts`""")
    to_date = filters.get('to_date')
    from_date = get_dates(to_date, 60)[1]
    for discount in supplier_discounts:
        items = frappe.db.sql("""Select name, item_cost, uom, item_code, item_name_dummy from tabItem where name in
                                      (Select items from `tabSupplier Discount Items` where parent = %s order by items asc)""",
                                  discount[0])
        for item in items:
            item_code = item[3]
            item_desc = item[4]
            offtake = get_qty_sold(filters.get('branch'), item[3], discount[0], from_date, to_date)[4]
            cisl = get_qty_sold(filters.get('branch'), item[3], discount[0], from_date, to_date)[5]
            cycle_days = get_qty_sold(filters.get('branch'), item[3], discount[0], from_date, to_date)[6]
            m1, b1 = get_m1(filters, item_code, discount[0])
            m2, b2 = get_m2(filters, item_code, discount[0])
            m3, b3 = get_m3(filters, item_code, discount[0])
            packing = get_uom(item_code, discount[0])
            old_m1, old_m2, old_m3, old_offtake = get_old_system_offtake(to_date, item_code, filters.get('branch'),
                                                                         get_uom(item_code, discount[0]))
            old_offtake = (old_offtake * float(cycle_days))/90

            if abs(old_offtake - offtake) > filters.get('diff'):
                temp = {"item_code": item_desc, "packing": packing, "b1": b1, "b2": b2, "b3": b3, "m1": m1, "m2": m2,
                        "m3": m3, "cisl": cisl,
                        "cycle_days": cycle_days, "offtake": offtake, "old_m1": old_m1, "old_m2": old_m2,
                        "old_m3": old_m3,
                        "old_offtake": old_offtake}
                data.append(temp)
            else:
                continue
    return data


def get_items_from_supplier_discount(filters, data):
    sup_disc = filters.get('supplier')
    to_date = filters.get('to_date')
    from_date = get_dates(to_date, 60)[1]

    items = None
    #print "=============+SUP DISC+==============="
    #print sup_disc

    items = frappe.db.sql("""Select name, item_cost, uom, item_code, item_name_dummy from tabItem where name in
                          (Select items from `tabSupplier Discount Items` where parent = %s order by items asc)""",
                          sup_disc)
    for item in items:
        item_code = item[3]
        item_desc = item[4]
        #item_doc = frappe.get_doc("Item", item_code)
        offtake = get_qty_sold(filters.get('branch'), item[3], sup_disc, from_date, to_date)[4]
        cisl = get_qty_sold(filters.get('branch'), item[3], sup_disc, from_date, to_date)[5]
        cycle_days = get_qty_sold(filters.get('branch'), item[3], sup_disc, from_date, to_date)[6]
        m1, b1 = get_m1(filters, item_code, filters.get('supplier'))
        m2, b2 = get_m2(filters, item_code, filters.get('supplier'))
        m3, b3 = get_m3(filters, item_code, filters.get('supplier'))
        packing = get_uom(item_code, sup_disc)
        old_m1, old_m2, old_m3, old_offtake = get_old_system_offtake(to_date, item_code, filters.get('branch'),
                                                                     get_uom(item_code, sup_disc))
        old_offtake = (old_offtake* float(cycle_days))/90

        if abs(old_offtake - offtake) > filters.get('diff'):
            temp = {"item_code": item_desc, "packing": packing, "b1": b1, "b2": b2, "b3": b3, "m1": m1, "m2": m2,
                    "m3": m3, "cisl": cisl,
                    "cycle_days": cycle_days, "offtake": offtake, "old_m1": old_m1, "old_m2": old_m2, "old_m3": old_m3,
                    "old_offtake": old_offtake}
            data.append(temp)
        else:
            continue
    return data


def get_cisl(filters):
    cisl = 0
    cisl2 = 0
    branch = filters.get('branch')
    item_code = filters.get('item_name')
    supplier = filters.get('supplier')
    to_date = filters.get('to_date')

    total = 0
    eh = 0

    now = datetime.datetime.strptime(to_date, '%Y-%m-%d').date()
    date = now - datetime.timedelta(days=90)

    s1, s2, s3 = 0, 0, 0
    cisl = get_qty_sold(branch, item_code, supplier, date, now)[4]
    return cisl


def get_qty_sold(branch, item_code, supplier_disc, date, now):
    cisl, cisl2 = 0, 0
    con_f = 1
    s1_sum, s1_qty = 0, 0
    s2_sum, s2_qty = 0, 0
    s3_sum, s3_qty = 0, 0
    new_avg = 0
    uom_conv = 0
    sum = 0
    supplier = ""

    #supplier_temp = frappe.db.sql("""SELECT supplier_name, name from `tabSupplier Discounts` where name = %s""",
    #                              supplier_disc)
    #for sup in supplier_temp:
        #print sup
    #    supplier = sup[0]

    frequency = frappe.db.sql("""Select offtake_calculation_frequency from `tabBranch` where branch = %s""", (branch))
    freq = str(frequency[0][0])
    fac_calc = frappe.db.sql("""Select (lead_time + cycle_time + buffer) from `tabSupplier Discounts` where name = %s""",
                             (supplier_disc))  # factor calculation
    for f in fac_calc:

        factor = str(f[0])
        #print "FACTOR = " + factor
        con_factor = frappe.db.sql(
            """Select packing from `tabItem` where item_code = %s""", (item_code))

        for f in con_factor:
            con_f = str(f[0])
        if str(con_f) == 'None':
            uom_conv = 1
        else:
            uom_conv = float(con_f)

            # ave qty: per piece/smallest UOM
            # count : # of records/days

    #print "========================compute======================"

    s1 = frappe.db.sql("""Select count(qty), sum(qty), uom, avg(qty) from `tabUpload POS`
                                                              where trans_date >= %s and trans_date <= %s and qty !=0 and
                                                              barcode = (Select barcode_retial from `tabItem` where item_code = %s)
                                                             and branch = %s""",
                       (date, now, item_code, branch))
    s2 = frappe.db.sql("""SELECT count(`tabStock Entry Detail`.qty), sum(`tabStock Entry Detail`.qty), `tabStock Entry Detail`.stock_uom, `tabStock Entry Detail`.uom,  avg(`tabStock Entry Detail`.qty) from `tabStock Entry Detail` LEFT JOIN `tabStock Entry`
                                 ON `tabStock Entry Detail`.parent = `tabStock Entry`.name WHERE `tabStock Entry`.posting_date >= %s
                                 and `tabStock Entry`.posting_date <= %s and `tabStock Entry Detail`.qty >0
                                 and `tabStock Entry Detail`.item_code = %s
                                 AND `tabStock Entry`.branch = %s AND `tabStock Entry`.type = 'Wholesale' AND `tabStock Entry`.docstatus = 1""",
                       (date, now, item_code, branch))
    s3 = frappe.db.sql("""SELECT count(`tabStock Entry Detail`.qty), sum(`tabStock Entry Detail`.qty), `tabStock Entry Detail`.stock_uom, `tabStock Entry Detail`.uom   from `tabStock Entry Detail` LEFT JOIN `tabStock Entry`
                            ON `tabStock Entry Detail`.parent = `tabStock Entry`.name WHERE `tabStock Entry`.posting_date >= %s
                            and `tabStock Entry`.posting_date <= %s and `tabStock Entry Detail`.qty >0
                            and `tabStock Entry Detail`.item_code = %s
                            AND `tabStock Entry`.branch = %s AND  `tabStock Entry`.type ='Stock Transfer' AND `tabStock Entry Detail`.s_warehouse = 'CDO Warehouse - GG' AND
                            `tabStock Entry Detail`.t_warehouse != 'CDO Main Display Area - GG' AND `tabStock Entry`.docstatus = 1""",
                       (date, now, item_code, branch))

    if str(s1[0][1]) == 'None':
        s1_sum, s1_qty = 0, 0
    else:
        s1_sum = s1[0][1]
        s1_qty = s1[0][0]

    if str(s2[0][1]) == 'None':
        s2_sum, s2_qty = 0, 0
    else:
        s2_sum = (s2[0][1] * uom_conv)
        s2_qty = s2[0][0]

    if str(s3[0][1]) == 'None':
        s3_sum, s3_qty = 0, 0
    else:
        s3_sum = (s3[0][1] * uom_conv)
        s3_qty = s1[0][0]
    divisor = s2_qty+s1_qty
    if divisor== 0:
        divisor= 1
        # if (s1_qty+s2_qty+s3_qty)!=0:
        # new_avg = (s1_sum+s2_sum+s3_sum)/(s1_qty+s2_qty+s3_qty)
    sum = s1_sum + s2_sum  # + s3_sum
    new_avg = sum / divisor#90                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        "+str(s2[0][4])
    # new_avg = (s1_sum + s2_sum + s3_sum) / 90
    # else:
    #    new_avg = 0

    cisl2 = round((float(new_avg) / (float(con_f)) * float(factor)), 0)
    cisl_temp = round(new_avg / float(con_f), 2)
    print
    return sum, s1_sum, s2_sum, s3_sum, cisl2, cisl_temp, factor


def get_uom(item_code, supplier_disc):
    uom_conv = 1
    con_f = 1

    supplier_temp = frappe.db.sql("""SELECT supplier_name, name from `tabSupplier Discounts` where name = %s""",
                                  supplier_disc)
    for sup in supplier_temp:
        print sup
        supplier = sup[0]

    fac_calc = frappe.db.sql("""Select (lead_time + cycle_time + buffer) from `tabSupplier` where name = %s""",
                             (supplier))  # factor calculation
    for f in fac_calc:

        factor = str(f[0])

        con_factor = frappe.db.sql(
            """Select packing from `tabItem` where item_code = %s""", (item_code))

        for f in con_factor:
            con_f = str(f[0])
        # print "==========+CONF+==========="
        # print "conf " + con_f

        if str(con_f) == 'None':
            uom_conv = 1
        else:
            uom_conv = float(con_f)
    return uom_conv


def get_old_system_offtake(now, item_code, branch, packing):
    #print "===========OFFTAKE OLD==========="
    #print "itemcode"
    #print item_code
    barcode = ""
    barcodes = frappe.db.sql("""Select barcode, wholesale from `tabUpload Offtake` where item = %s""", item_code)
    offtake = 0
    for row in barcodes:
        barcode = row[0]
    #print "barcode"
    #print barcode

    m1, m2, m3 = 0, 0, 0

    rows = frappe.db.sql(
        """SELECT m1_qty, m2_qty, m3_qty from `tabUpload Offtake` where barcode = %s and branch = %s""",
        (barcode, branch))

    if len(rows) == 0:
        offtake = 0
    for data in rows:
        print data, data[0], data[1], data[2]
        if (data[0] == None) or (data[1] == None) or (data[2] == None):
            continue
        else:
            m1 = data[0]
            m2 = data[1]
            m3 = data[2]
            offtake = (int(data[0]) + int(data[1]) + int(data[2]))
            #offtake = (sum / 90)
            # offtake = ((data[0] + data[1] + data[2]) / 90)/packing*cycletime
            #print offtake

    return m1, m2, m3, offtake
