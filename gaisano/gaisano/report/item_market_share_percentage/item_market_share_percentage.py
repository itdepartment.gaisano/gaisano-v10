# Copyright (c) 2013, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from operator import itemgetter
from gaisano.popupevents import get_report_password


def execute(filters=None):
    if filters.get('get_totals') == 1:
        columns = [
            {"label": "Sub Class", 'width': 200, "fieldname": "sub_class"},
            {"label": "Supplier", 'width': 200, "fieldname": "supplier"},
            # {"label": "Volume Sales", 'width': 150, "fieldname": "item_sales", "fieldtype": "float"},
            # {"label": "Category Volume", 'width': 170, "fieldname": "total_sales", "fieldtype": "float"},
            {"label": "Volume Percentage", 'width': 150, "fieldname": "percentage", "fieldtype": "Percent"},
            # {"label": "Vol Perc", 'width': 100, "fieldname": "end_vol_perc", "fieldtype": "Percent"},
            # {"label": "Peso Value", 'width': 150, "fieldname": "item_peso", "fieldtype": "Currency"},
            # {"label": "Category Peso Value", 'width': 170, "fieldname": "total_peso", "fieldtype": "Currency"},
            {"label": "Peso Percentage", 'width': 150, "fieldname": "peso_percentage", "fieldtype": "Percent"},
            # {"label": "Peso Perc", 'width': 100, "fieldname": "end_peso_perc", "fieldtype": "Percent"},
            # {"label": "Profit Value", 'width': 170, "fieldname": "item_profit", "fieldtype": "Currency"},
            # {"label": "Category Profit", 'width': 170, "fieldname": "whole_profit", "fieldtype": "Currency"},
            {"label": "Profit Percentage", 'width': 150, "fieldname": "profit_perc", "fieldtype": "Percent"},
            # {"label": "Profit Perc", 'width': 100, "fieldname": "end_profit_perc", "fieldtype": "Percent"},
            {"label": "Total Perc", 'width': 100, "fieldname": "overall", "fieldtype": "Percent"},

            # {"label": "Factor (Cycle Time + Lead Time + Buffer", 'width': 100, "fieldname": "factor"},
            # {"label": "Offtake", 'width': 100, "fieldname": "offtake"},
        ]
    else:
        columns = [
            {"label": "Sub Class", 'width': 200, "fieldname": "sub_class"},
            {"label": "Supplier", 'width': 200, "fieldname": "supplier"},
            {"label": "Item Code", 'width': 300, "fieldname": "item_code"},
            {"label": "Barcode", 'width': 300, "fieldname": "barcode"},
            # {"label": "Volume Sales", 'width': 150, "fieldname": "item_sales", "fieldtype": "float"},
            # {"label": "Category Volume", 'width': 170, "fieldname": "total_sales", "fieldtype": "float"},
            {"label": "Volume Percentage", 'width': 150, "fieldname": "percentage", "fieldtype": "Percent"},
            # {"label": "Vol Perc", 'width': 100, "fieldname": "end_vol_perc", "fieldtype": "Percent"},
            #{"label": "Peso Value", 'width': 150, "fieldname": "item_peso", "fieldtype": "Currency"},
            # {"label": "Category Peso Value", 'width': 170, "fieldname": "total_peso", "fieldtype": "Currency"},
            {"label": "Peso Percentage", 'width': 150, "fieldname": "peso_percentage", "fieldtype": "Percent"},
            # {"label": "Peso Perc", 'width': 100, "fieldname": "end_peso_perc", "fieldtype": "Percent"},
            # {"label": "Profit Value", 'width': 170, "fieldname": "item_profit", "fieldtype": "Currency"},
            # {"label": "Category Profit", 'width': 170, "fieldname": "whole_profit", "fieldtype": "Currency"},
            {"label": "Profit Percentage", 'width': 150, "fieldname": "profit_perc", "fieldtype": "Percent"},
            # {"label": "Profit Perc", 'width': 100, "fieldname": "end_profit_perc", "fieldtype": "Percent"},
            {"label": "Total Perc", 'width': 100, "fieldname": "overall", "fieldtype": "Percent"},

            # {"label": "Factor (Cycle Time + Lead Time + Buffer", 'width': 100, "fieldname": "factor"},
            # {"label": "Offtake", 'width': 100, "fieldname": "offtake"},
        ]
    data = []
    from_date = filters.get("from_date")
    to_date = filters.get("to_date")
    category = filters.get("category")
    branch = filters.get("branch")
    subclass = filters.get("subclass")
    show_peso_value = filters.get("show_peso_value")

    password = filters.get("report_password")

    if show_peso_value == 1:
        if not get_report_password(password):
            report_msg = "Please Enter the Report Password" if password is None else "WRONG REPORT PASSWORD ENTERED."
            frappe.throw(report_msg)
        else:
            columns.append({"label": "Peso Value", 'width': 150, "fieldname": "item_peso", "fieldtype": "Currency"})

    print branch
    if branch == None:
        branch = ""
    print subclass

    # is_summary = filters.get("summary")

    v_perc = float(filters.get("volumne")) / 100
    p_perc = float(filters.get("peso_value")) / 100
    pr_perc = float(filters.get("profit")) / 100

    if (subclass != "") and (subclass!=None):
        itm_cat = frappe.db.sql("""Select parent, sub_class from `tabItem Class Each` where classification = %s and sub_class = %s""", (category, subclass))
        #total_sales = frappe.db.sql("""select sum(qty), sum(amount), ((sum(amount))-(sum(qty*cost)))
        #                                      from `tabUpload POS` where branch LIKE %s and trans_date >= %s and trans_date <= %s
        #                                      AND barcode IN (Select barcode_retial from `tabItem` where item_code in
        #                                      (Select parent from `tabItem Class Each` where classification = %s and sub_class = %s))""",
        #                            ('%' + branch + '%', str(from_date), str(to_date), category, subclass))
        print "With Subclass"
    else:
        itm_cat = frappe.db.sql("""Select parent, sub_class from `tabItem Class Each` where classification = %s""", (category))
        #total_sales = frappe.db.sql("""select sum(qty), sum(amount), ((sum(amount))-(sum(qty*cost)))
        #                                              from `tabUpload POS` where branch LIKE %s and trans_date >= %s and trans_date <= %s
        #                                              AND barcode IN (Select barcode_retial from `tabItem` where item_code in
        #                                              (Select parent from `tabItem Class Each` where classification = %s))""",
        #                            ('%' + branch + '%', str(from_date), str(to_date), category))

    #whole_sale = str(total_sales[0][0])
    #whole_sale_peso = str(total_sales[0][1])
    whole_sale, whole_sale_peso, whole_profit = calc_discount(from_date, to_date, category, branch, subclass)

    if (whole_sale!='None') and (whole_sale!=0):
        for itm in itm_cat:
            itm_code = str(itm[0])
            sub_class = str(itm[1])
            idv_sales = frappe.db.sql("""select sum(qty), sum(amount), ((sum(amount))-(sum((qty*cost) * (1-(Select item_discount from `tabItem` where item_code = %s and item_discount is not null))))),
                                              (sum((qty*cost) * (Select item_discount from `tabItem` where item_code = %s and item_discount is not null))) as total_discount
                                              from `tabUpload POS` where branch LIKE %s and trans_date >= %s and trans_date <= %s
                                              AND barcode = (Select barcode_retial from `tabItem` where item_code = %s)""",
                                      (itm_code, itm_code, '%'+branch+'%', str(from_date), str(to_date), itm_code))
            idv_sales = [""]
            for i in idv_sales:
                #itm_sales = str(i[0])
                #itm_sales_peso = str(i[1])
                #itm_profit = str(i[2])
                itm_sales, itm_sales_peso, itm_profit = get_idv_sales(branch, from_date, to_date, itm_code)
                item_doc = frappe.get_doc("Item", itm_code)
                barcode = item_doc.barcode_retial
                if (itm_sales != 'None'):
                    # itm_profit_f = abs(float(itm_profit))
                    itm_profit_f = (float(itm_profit))
                    vol_perc = (float(itm_sales) / float(whole_sale)) * 100
                    peso_perc = (float(itm_sales_peso) / float(whole_sale_peso)) * 100
                    prof_perc = (float(itm_profit_f) / float(whole_profit)) * 100
                    end_vol_percent = ((float(vol_perc)) * v_perc)
                    end_peso_percent = ((float(peso_perc)) * p_perc)
                    end_prof_percent = ((float(prof_perc)) * pr_perc)
                    overall = end_vol_percent + end_peso_percent + end_prof_percent

                    supplier = frappe.db.sql("""Select supplier from `tabItem Supplier` where parent = %s""", (itm_code))
                    for s in supplier:
                        sup = s[0]
                        data.append({"sub_class": sub_class, "supplier": sup, "total_sales": whole_sale,
                                         "item_sales": itm_sales,"barcode":barcode,
                                         "item_code": item_doc.item_name_dummy, "total_peso": whole_sale_peso,
                                         "item_peso": itm_sales_peso, "percentage": vol_perc,
                                         "peso_percentage": peso_perc, "end_vol_perc": end_vol_percent,
                                         "end_peso_perc": end_peso_percent, "whole_profit": whole_profit,
                                         "item_profit": itm_profit_f, "profit_perc": prof_perc,
                                         "end_profit_perc": end_prof_percent, "overall": overall
                                         })
        data = show_subclass_totals(data, filters.get('get_totals'))
        data = reorder_list(data, 'overall', True)
        #data = check_totals(data)
    else:
        frappe.msgprint("Classification has no sales/Is not being used.")
    return columns, data


def calc_discount(from_date, to_date, category, branch, subclass):
    if (subclass != "") and (subclass != None):
        itm_cat = frappe.db.sql("""Select parent from `tabItem Class Each` where classification = %s and sub_class = %s""", (category, subclass))
    else:
        itm_cat = frappe.db.sql("""Select parent, sub_class from `tabItem Class Each` where classification = %s""",
                                (category))
    volume, peso, profit = 0,0,0
    for itm in itm_cat:
        itm_code = str(itm[0])
        #idv_sales = frappe.db.sql("""select ((sum(amount))-(sum((qty*cost) * (1-(Select item_discount from `tabItem` where item_code = %s and item_discount is not null)))))
        #                                          from `tabUpload POS` where branch LIKE %s and trans_date >= %s and trans_date <= %s
        #                                          AND barcode = (Select barcode_retial from `tabItem` where item_code = %s)""",
        #                          (itm_code,'%'+branch+'%', str(from_date), str(to_date), itm_code))
        #for i in idv_sales:
        #    itm_profit = str(i[0])
        #    if itm_profit != 'None' :
        #        profit += float(itm_profit)
        #    else:
        #        itm_profit = 0
        #        profit += float(itm_profit)
        temp = get_idv_sales(branch, from_date, to_date, itm_code)
        volume += temp[0]
        peso += temp[1]
        profit += temp[2]

    return volume, peso, profit



def show_subclass_totals(data, get_totals):
    new_data = []

    temp_data = {}

    current_subclass = None
    percentage = 0
    peso_percentage = 0
    profit_perc = 0
    overall = 0

    total_perc = 0
    total_peso = 0
    total_profit = 0
    total_overall = 0

    print "=================DATA====================="
    newlist = sorted(data, key=lambda k: k['sub_class'])
    if get_totals == 1:
        print "GET TOTALS SAD LIFE"
        new_data = get_supplier_totals(newlist)
    else:
        print "Don't get totals"
        for i, item in enumerate(newlist):
            #print "Current item: "+str(item)
            total_perc += item['percentage']
            total_peso += item['peso_percentage']
            total_profit += item['profit_perc']
            total_overall += item['overall']

            if current_subclass == None:
                current_subclass = item['sub_class']
                new_data.append(item)
                percentage += item['percentage']
                peso_percentage += item['peso_percentage']
                profit_perc += item['profit_perc']
                overall += item['overall']

            else:

                if current_subclass != item['sub_class']:
                    temp_data = {"sub_class": "<b>SUBCLASS TOTAL FOR "+current_subclass+"</b>", "percentage": percentage,
                                 "peso_percentage": peso_percentage, "profit_perc": profit_perc,
                                 "overall": overall}
                    new_data.append(temp_data)

                    current_subclass = item['sub_class']
                    percentage = item['percentage']
                    peso_percentage = item['peso_percentage']
                    profit_perc = item['profit_perc']
                    overall = item['overall']
                    new_data.append(item)

                else:
                    new_data.append(item)
                    percentage += item['percentage']
                    peso_percentage += item['peso_percentage']
                    profit_perc += item['profit_perc']
                    overall += item['overall']

                if (i + 1) == len(newlist):
                    temp_data = {"sub_class": "<b>TOTAL FOR "+current_subclass+"</b>", "percentage": percentage,
                                 "peso_percentage": peso_percentage, "profit_perc": profit_perc,
                                 "overall": overall}
                    new_data.append(temp_data)

    return new_data


def check_totals(new_data):
    temp1, temp2, temp3, temp4, = 0, 0, 0, 0
    for i, item in enumerate(new_data):
        temp1 += item['percentage']
        temp2 += item['peso_percentage']
        temp3 += item['profit_perc']
        temp4 += item['overall']

    temp1 -= 100
    temp2 -= 100
    temp3 -= 100
    temp4 -= 100
    new_data.append({"supplier": "<B>TOTAL based on Totals</B>", "percentage": temp1,
                     "peso_percentage": temp2, "profit_perc": temp3,
                     "overall": temp4
                     })
    return new_data

def get_supplier_totals(newlist):
    print "=====================-GET SUPPLIER TOTALS!-==================="
    sup_perc, total_perc = 0,0
    sup_peso, total_peso = 0,0
    sup_profit, total_profit = 0,0
    sup_total, total_overall = 0,0
    sup_peso_value, total_peso_value = 0,0
    percentage = 0
    peso_percentage = 0
    profit_perc = 0
    overall = 0
    peso_value = 0
    new_data = []
    current_supplier = ""
    current_subclass = ""
    newlist = reorder_list(newlist, 'supplier', False)
    for i, item in enumerate(newlist):
        total_peso_value += item['item_peso']
        total_perc += item['percentage']
        total_peso += item['peso_percentage']
        total_profit += item['profit_perc']
        total_overall += item['overall']

        if i == 0:
            #print "NEW SUPPLIER : "+ item['supplier']
            #print "NEW SUBCLASS : "+ item['sub_class']
            current_subclass = item['sub_class']
            current_supplier = item['supplier']
            sup_perc, percentage = item['percentage'], item['percentage']
            sup_peso, peso_percentage = item['peso_percentage'], item['peso_percentage']
            sup_profit, profit_perc = item['profit_perc'], item['profit_perc']
            sup_total, overall = item['overall'], item['overall']
            sup_peso_value, peso_value = item['item_peso'], item['item_peso']

        else:

            if current_subclass != item['sub_class']:
                #append current supplier data and totals

                temp_sup = {"sub_class": current_subclass, "supplier": current_supplier, "percentage": sup_perc,
                             "peso_percentage": sup_peso, "profit_perc": sup_profit, "item_peso":sup_peso_value,
                             "overall": sup_total}
                new_data.append(temp_sup)
                temp_data = {"sub_class": "<b>TOTAL FOR " + current_subclass + "</b>", "percentage": percentage,
                             "peso_percentage": peso_percentage, "profit_perc": profit_perc, "item_peso": peso_value,
                             "overall": overall}
                #print "NEW SUPPLIER : " + item['supplier']
                #print "NEW SUBCLASS : " + item['sub_class']
                new_data.append(temp_data)
                current_supplier = item['supplier']
                current_subclass = item['sub_class']
                sup_perc, percentage = item['percentage'], item['percentage']
                sup_peso, peso_percentage = item['peso_percentage'], item['peso_percentage']
                sup_profit, profit_perc = item['profit_perc'], item['profit_perc']
                sup_total, overall = item['overall'], item['overall']
                sup_peso_value, peso_value = item['item_peso'], item['item_peso']


            else:
                #new_data.append(item)
                percentage += item['percentage']
                peso_percentage += item['peso_percentage']
                profit_perc += item['profit_perc']
                overall += item['overall']
                peso_value += item['item_peso']

                if current_supplier != item['supplier']:
                    temp_data = {"sub_class": current_subclass,"supplier":current_supplier, "percentage": sup_perc,
                                 "peso_percentage": sup_peso, "profit_perc": sup_profit, "item_peso": sup_peso_value,
                                 "overall": sup_total}
                    new_data.append(temp_data)
                    #print "NEW SUPPLIER : " + item['supplier']
                    #print "NEW SUBCLASS : " + item['sub_class']
                    current_supplier = item['supplier']
                    current_subclass = item['sub_class']
                    sup_perc = item['percentage']
                    sup_peso = item['peso_percentage']
                    sup_profit = item['profit_perc']
                    sup_total = item['overall']
                    sup_peso_value = item['item_peso']
                else:
                    sup_perc += item['percentage']
                    sup_peso += item['peso_percentage']
                    sup_profit += item['profit_perc']
                    sup_total += item['overall']
                    sup_peso_value +=item['item_peso']


            if (i + 1) == len(newlist):
                temp_sup = {"sub_class": current_subclass, "supplier": current_supplier, "percentage": sup_perc,
                             "peso_percentage": sup_peso, "profit_perc": sup_profit,  "item_peso": sup_peso_value,
                             "overall": sup_total}
                new_data.append(temp_sup)
                temp_data = {"sub_class": "<b>TOTAL FOR " + current_subclass + "</b>", "percentage": percentage,
                             "peso_percentage": peso_percentage, "profit_perc": profit_perc,  "item_peso": peso_value,
                             "overall": overall}
                new_data.append(temp_data)
    new_data = reorder_list(new_data, 'overall', True)

    return new_data
    #return new_data


def reorder_list(newlist, filter, is_reverse):
    temp_list = []
    current_subclass = ""
    new_data = []

    for i, item in enumerate(newlist):

        #if filter == "supplier":
            #print item["supplier"], item['sub_class']
        #print "i is " + str(i) + "| len is " + str(len(newlist))
        if i == 0:
            #if filter == "supplier":
                #print item["supplier"], item['sub_class']
            current_subclass = item['sub_class']
            temp_list.append(item)
        else:
            if ((i+1) != len(newlist)) and (current_subclass != item['sub_class']):
               # print "sort previous. Start new list."
                #if filter == "supplier":
                    #print item["supplier"], item['sub_class']
                temp_list = sort_by_supplier(temp_list, filter, is_reverse)
                i = len(new_data)
                j = i + len(temp_list)
                new_data[i:j] = temp_list
                temp_list = []
                temp_list.append(item)
                current_subclass = item['sub_class']

                #new_data.append(temp_data)
            elif ((i+1) != len(newlist)) and (current_subclass == item['sub_class']):
                temp_list.append(item)
                #if filter == "supplier":
                    #print item["supplier"], item['sub_class']
                #print "Just add to temp list"
            elif (i + 1) == len(newlist):
                #print "===============LAST ITEM!==================="
                if current_subclass != item['sub_class']:
                    #print "sort previous. Start new list."
                    #if filter == "supplier":
                       # print item["supplier"], item['sub_class']
                    temp_list = sort_by_supplier(temp_list, filter, is_reverse)
                    i = len(new_data)
                    j = i + len(temp_list)
                    new_data[i:j] = temp_list
                    temp_list = []
                    new_data.append(item)
                else:
                    temp_list.append(item)
                    temp_list = sort_by_supplier(temp_list, filter, is_reverse)
                    i = len(new_data)
                    j = i + len(temp_list)
                    new_data[i:j] = temp_list

    #return new_data
    return new_data

def sort_by_supplier(temp_list, filter, is_reverse):
    #print "unsorted"
    #print temp_list
    #print "=============SORTED==============="
    #print sorted(temp_list, key=lambda k: k['supplier'])
    #for item in sorted(temp_list, key=lambda k: k['supplier']):
    #    print item
    if filter == 'overall':
        #print "overall"
        return sorted(temp_list, key=itemgetter('overall'), reverse=is_reverse)
    else:
        #print "supplier"
        return sorted(temp_list, key=lambda k: k[filter], reverse=is_reverse)

def get_idv_sales(branch, from_date, to_date, item_code):
    print "GET IDV SALES FUNCTION"
    volume , peso_value, profit = 0, 0, 0
    d1, d2, d3, d4 = get_supplier_discounts(item_code, branch)
    try:
        item_doc = frappe.get_doc("Item", item_code)
    except:
        print "ERROR"
    else:
        idv_sales = frappe.db.sql("""select sum(qty), sum(amount), sum(qty*cost) from `tabUpload POS` where branch LIKE %s and trans_date >= %s and
                              trans_date <= %s AND barcode = %s""",
                                  ('%' + branch + '%', str(from_date), str(to_date), str(item_doc.barcode_retial)))
        if len(idv_sales) > 0:
            for sales in idv_sales:
                volume = (sales[0] if sales[0] != None else 0)
                peso_value = (sales[1] if sales[1] != None else 0)
                total_cost = (sales[2] if sales[2] != None else 0)
                net_cost = ((((float(total_cost) * (1 - d1)) * (1 - d2)) * (1 - d3)) * (1 - d4))
                profit = peso_value - (net_cost)
    print volume, peso_value, profit
    return volume, peso_value, profit


def get_supplier_discounts(item_code, branch):
    discounts = frappe.db.sql("""Select supplier_discount from `tabItem Branch Discount` where parent = %s and branch = %s""", (item_code, branch))
    disc1, disc2, disc3, disc4 = 0, 0, 0, 0
    if len(discounts) > 0:
        for discount in discounts:
            supplier_discount = frappe.get_doc("Supplier Discounts", discount[0])
            disc1 = (supplier_discount.disc_1 if supplier_discount.disc_1 != None else 0)
            disc2 = (supplier_discount.disc_2 if supplier_discount.disc_2 != None else 0)
            disc3 = (supplier_discount.disc_3 if supplier_discount.disc_3 != None else 0)
            disc4 = (supplier_discount.disc_4 if supplier_discount.disc_4 != None else 0)
    return float(disc1/100), float(disc2/100), float(disc3/100), float(disc4/100)
