# Copyright (c) 2013, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe


def execute(filters=None):
	print "-----------------execute------------------"
	select_query, count_query, query_filters = get_item_query(filters)

	from_date = filters.get('from_date')
	to_date = filters.get('to_date')
	branch = filters.get('branch')
	show_values = filters.get('show_values')
	volume_perc = float(filters.get("volume_perc"))
	peso_perc = float(filters.get("peso_perc"))
	profit_perc =float(filters.get('profit_perc'))

	columns = get_columns(show_values)

	count_items = frappe.db.sql(count_query)
	top80 = int(count_items[0][0] * .8)
	all_items = get_items(select_query, from_date, to_date, branch)
	data = get_item_percentages(all_items, volume_perc, peso_perc, profit_perc)
	new_list = sorted(data, key=lambda k: k['overall'], reverse=True)
	total_volume = sum(item['volume'] for item in all_items)
	total_peso = sum(item['peso_value'] for item in all_items)
	total_profit = sum(item['profit'] for item in all_items)
	total_volume_perc = sum(float(item['volume_perc']) for item in all_items)
	total_peso_perc = sum(float(item['peso_perc']) for item in all_items)
	total_profit_perc = sum(float(item['profit_perc']) for item in all_items)
	total_overall = sum(float(item['overall']) for item in all_items)
	data = new_list[0:top80]
	data.append({"item_description":"TOTAL SKU's VS TOP 80%", "barcode":str(count_items[0][0])+" | "+ str(top80), "volume":total_volume,
				 "total_peso":total_peso, "total_profit": total_profit, "volume_perc":total_volume_perc,
				 "peso_perc":total_peso_perc, "profit_perc":total_profit_perc, "overall":total_overall})

	print "TOTAL ITEMS:", count_items[0][0], "TOP 80:", top80

	return columns, data


def get_columns(show_values):
    columns = [
        {"label": "Item Description", "fieldname": "item_description", "fieldtype": "Data", "width": 180},
        {"label": "Barcode", "fieldname": "barcode", "fieldtype": "Data", "width": 180}
    ]
    if show_values == 1:
        columns.append({"label": "Volume ", 'width': 150, "fieldname": "volume", "fieldtype": "Float", "Precision": 2})
        columns.append({"label": "Peso Value", 'width': 150, "fieldname": "peso_value", "fieldtype": "Float", "Precision": 2})
        columns.append({"label": "Profit", 'width': 150, "fieldname": "profit", "fieldtype": "Float", "Precision": 2})
    columns.append({"label": "Volume Percentage", 'width': 150, "fieldname": "volume_perc", "fieldtype": "Percent","precision": 2})
    columns.append({"label": "Peso Percentage", 'width': 150, "fieldname": "peso_perc", "fieldtype": "Percent", "Precision": 2})
    columns.append({"label": "Profit Percentage", 'width': 150, "fieldname": "profit_perc", "fieldtype": "Percent","Precision": 2})
    columns.append({"label": "Total Perc", 'width': 100, "fieldname": "overall", "fieldtype": "Percent", "Precision": 2})
    return columns


def get_item_query(filters):
    query_filters = "where itm.type !='Disabled' and setup.classification = '{0}' and sup.supplier = '{1}'".format(
        filters.get('classification'), filters.get('supplier'))
    if filters.get('category'):
        query_filters += " and setup.category = '{0}'".format(filters.get('category'))
    if filters.get('subclass'):
        query_filters += " and setup.sub_class = '{0}'".format(filters.get('subclass'))
    select_query = """SELECT itm.name, itm.item_name_dummy, itm.barcode_retial from `tabItem` itm inner join `tabItem Class Each` setup on setup.parent = itm.name inner join `tabItem Supplier` sup on sup.parent = itm.name {}""".format(
        query_filters)
    count_query = """SELECT count(*) from `tabItem` itm inner join `tabItem Class Each` setup on setup.parent = itm.name inner join `tabItem Supplier` sup on sup.parent = itm.name {}""".format(
        query_filters)
    return select_query, count_query, query_filters


def get_items(item_query, from_date, to_date, branch):
    data = []
    items = frappe.db.sql(item_query)
    for item in items:
		volume, peso_value, profit = get_idv_sales(branch, from_date, to_date, item[0])
		print item[0], volume, peso_value, profit
		data.append({"item_description": item[1], "barcode": item[2], "volume":volume, "peso_value":peso_value, "profit":profit})
    return data

def get_item_percentages(all_items, volume_perc, peso_perc, profit_perc):
	total_volume = sum(item['volume'] for item in all_items)
	total_peso = sum(item['peso_value'] for item in all_items)
	total_profit = sum(item['profit'] for item in all_items)
	for item in all_items:
		item['volume_perc'] = None if total_volume == 0 else ((item['volume']/total_volume)*100)
		item['peso_perc'] = None if total_peso == 0 else ((item['peso_value'] / total_peso) * 100)
		item['profit_perc'] = None if total_profit == 0 else ((item['profit'] / total_profit) * 100)

		volume_temp = float(item['volume_perc'])*(volume_perc/100)
		peso_temp = float(item['peso_perc']) * (peso_perc / 100)
		profit_temp = float(item['profit_perc']) * (profit_perc / 100)

		item['overall'] = volume_temp+peso_temp+profit_temp
	return all_items

def get_idv_sales(branch, from_date, to_date, item_code):
	print "GET IDV SALES FUNCTION"
	volume, peso_value, profit = 0, 0, 0
	d1, d2, d3, d4 = get_supplier_discounts(item_code, branch)
	try:
		item_doc = frappe.get_doc("Item", item_code)
	except:
		print "ERROR"
	else:
		idv_sales = frappe.db.sql("""select sum(qty), sum(amount), sum(qty*cost) from `tabUpload POS` where branch LIKE %s and trans_date >= %s and
								  trans_date <= %s AND barcode = %s""",
								  ('%' + branch + '%', str(from_date), str(to_date), str(item_doc.barcode_retial)))
		if len(idv_sales) > 0:
			for sales in idv_sales:
				volume = (sales[0] if sales[0] != None else 0)
				peso_value = (sales[1] if sales[1] != None else 0)
				total_cost = (sales[2] if sales[2] != None else 0)
				net_cost = ((((float(total_cost) * (1 - d1)) * (1 - d2)) * (1 - d3)) * (1 - d4))
				profit = peso_value - (net_cost)
	return volume, peso_value, profit


def get_supplier_discounts(item_code, branch):
	discounts = frappe.db.sql(
		"""Select supplier_discount from `tabItem Branch Discount` where parent =%s and branch= %s""",
		(item_code, branch))
	disc1, disc2, disc3, disc4 = 0, 0, 0, 0
	if len(discounts) > 0:
		for discount in discounts:
			supplier_discount = frappe.get_doc("Supplier Discounts", discount[0])
			disc1 = (supplier_discount.disc_1 if supplier_discount.disc_1 != None else 0)
			disc2 = (supplier_discount.disc_2 if supplier_discount.disc_2 != None else 0)
			disc3 = (supplier_discount.disc_3 if supplier_discount.disc_3 != None else 0)
			disc4 = (supplier_discount.disc_4 if supplier_discount.disc_4 != None else 0)
	return float(disc1 / 100), float(disc2 / 100), float(disc3 / 100), float(disc4 / 100)
