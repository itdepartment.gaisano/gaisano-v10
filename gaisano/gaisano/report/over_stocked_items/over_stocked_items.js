// Copyright (c) 2016, Bai Mobile and Web Labs and contributors
// For license information, please see license.txt

frappe.query_reports["Over Stocked Items"] = {
	"filters": [
		{
			"fieldname":"branch",
			"label":__("Branch"),
			"fieldtype": "Link",
			"options": "Branch",
			"reqd":1
		},
		{
			"fieldname":"warehouse",
			"label": __("Warehouse"),
			"fieldtype": "Link",
			"options": "Warehouse",
			"reqd":1,
			"get_query": function(){
				var branch = frappe.query_report_filters_by_name.branch.get_value();
				console.log(branch);
				var filters = get_warehouse_filters(branch);
				return{
					"filters":filters
        		}
    		},
			"on_change": function(query_report){
                var warehouse = frappe.query_report_filters_by_name.warehouse.get_value();
                if (warehouse){
                    return frappe.call({
                        method : "frappe.client.get",
                        args :{
                            "doctype" : "Warehouse",
                            "name" : warehouse
                        },
                        callback: function(r){
                            frappe.query_report.refresh();
                        }
                    })
                }
                else{
                    frappe.query_report_filters_by_name.warehouse.set_value("");
                    frappe.query_report.refresh();
                }
            }
		},
		{
			"fieldname":"date",
			"label":__("Date"),
			"fieldtype":"Date",
			"default":frappe.datetime.get_today(),
			"reqd":1
		}
	]
}



function get_warehouse_filters(branch) {
        var filters = {"branch": branch};

    return filters
}