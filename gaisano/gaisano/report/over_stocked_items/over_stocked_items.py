# Copyright (c) 2013, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from gaisano.poevents import get_balance
import datetime


def execute(filters=None):
	columns, data = [], []
	columns = [
		{"label": "Item Name", "width": 300, "fieldname": "item_name"},
		{"label": "Barcode", "width": 180, "fieldname": "item_barcode"},
		{"label": "Cisl in 30 days", "width": 150, "fieldname": "ideal_stock"},
		{"label": "Inventory", "width": 100, "fieldname": "inv"}
	]
	data = get_items(filters)
	return  columns, data

def get_items(filters):
	data =[]
	branch = filters.get("branch")
	warehouse = filters.get("warehouse")
	date = filters.get("date")
	date = datetime.datetime.strptime(date, '%Y-%m-%d').date()
	from_date = date - datetime.timedelta(days=90)
	items = frappe.db.sql("""select name, barcode_retial, packing, supplier_discount, item_name_dummy, item_price, item_cost from `tabItem` where  supplier_discount is not null""", as_dict = True)
	for item in items:
		print item['name']
		s1 = frappe.db.sql("""Select count(qty), sum(qty) from `tabUpload POS`
														 where trans_date >= %s and trans_date <= %s and qty !=0 and
														 barcode = %s
														and branch = %s""",
						   (from_date, date, item['barcode_retial'], branch))
		s2 = frappe.db.sql("""SELECT count(`tabStock Entry Detail`.qty), sum(`tabStock Entry Detail`.qty), `tabStock Entry Detail`.stock_uom, `tabStock Entry Detail`.uom from `tabStock Entry Detail` LEFT JOIN `tabStock Entry`
														ON `tabStock Entry Detail`.parent = `tabStock Entry`.name WHERE `tabStock Entry`.posting_date >= %s
														and `tabStock Entry`.posting_date <= %s and `tabStock Entry Detail`.qty >0
														and `tabStock Entry Detail`.item_code = %s
														AND `tabStock Entry`.branch = %s AND `tabStock Entry`.type = 'Wholesale' AND `tabStock Entry`.docstatus = 1""",
						   (from_date, date, item['name'], branch))
		s3 = frappe.db.sql("""SELECT count(`tabStock Entry Detail`.qty), sum(`tabStock Entry Detail`.qty), `tabStock Entry Detail`.stock_uom, `tabStock Entry Detail`.uom   from `tabStock Entry Detail` LEFT JOIN `tabStock Entry`
													ON `tabStock Entry Detail`.parent = `tabStock Entry`.name WHERE `tabStock Entry`.posting_date >= %s
													and `tabStock Entry`.posting_date <= %s and `tabStock Entry Detail`.qty >0
													and `tabStock Entry Detail`.item_code = %s
													AND `tabStock Entry`.branch = %s AND `tabStock Entry`.type ='Stock Transfer' AND `tabStock Entry Detail`.s_warehouse = %s AND
													`tabStock Entry`.docstatus = 1""",
						   (from_date, date, item['name'], branch, warehouse))
		if item['packing'] == None:
			packing == 1
		else:
			packing = item['packing']
		if str(s1[0][1]) == 'None':
			s1_sum, s1_qty = 0, 0
		else:
			s1_sum = s1[0][1]

		if str(s2[0][1]) == 'None':
			s2_sum, s2_qty = 0, 0
		else:
			s2_sum = (s2[0][1] * packing)

		if str(s3[0][1]) == 'None':
			s3_sum, s3_qty = 0, 0
		else:
			s3_sum = (s3[0][1] * packing)
		sum = s1_sum + s2_sum + s3_sum
		item_packing = sum / packing
		offtake = item_packing / 90
		# cycle_time, buffer, lead_time = get_supplier_details(item['supplier_discount'])
		ideal_stock = offtake * 30 
		ideal_stock = round(ideal_stock,2)
		if ideal_stock < 0: ideal_stock = 0
		inv = get_balance(warehouse, item["name"], date)
		# print "inv",inv, "overstocked",overstocked, sum, "sum", packing, "packing", offtake, "offtake", cisl, "cisl"
		if inv > ideal_stock and ideal_stock != 0:
			print item['name'], "Item Is overstocked"
			data.append({"item_name": item['item_name_dummy'], "item_barcode": item['barcode_retial'], "ideal_stock": ideal_stock, "inv": inv})
	return data


# def get_supplier_details(item_discount):
# 	cycle_time, buffer, lead_time = 0,0,0
# 	suppliers = frappe.db.sql("""select cycle_time, buffer, lead_time from `tabSupplier Discounts` where name = %s """, item_discount, as_dict = True)
# 	for calc in suppliers:
# 		try:
# 			cycle_time = calc['cycle_time']
# 			buffer = calc['buffer']
# 			lead_time = calc['lead_time']
# 		except:
# 			cycle_time = 0
# 			buffer = 0
# 			lead_time = 0
# 	return cycle_time, buffer, lead_time
