// Copyright (c) 2016, Bai Mobile and Web Labs and contributors
// For license information, please see license.txt

frappe.query_reports["Inventory Monitoring Report"] = {
	"filters": [

        {
            "fieldname": "branch",
            "label": __("Branch"),
            "fieldtype": "Link",
            "options": "Branch",
            "reqd": 1
        },

        {
            "fieldname": "warehouse",
            "label": __("Display Area"),
            "fieldtype": "Link",
            "options": "Warehouse",
            "reqd": 1
        },

        {
            "fieldname": "date",
            "label": __("Date"),
            "fieldtype": "Date",
            "reqd": 1
        },
		{
            "fieldname": "supplier",
            "label": __("Supplier"),
            "fieldtype": "Link",
			"options": "Supplier",
            "reqd": 1
        }

	]
}