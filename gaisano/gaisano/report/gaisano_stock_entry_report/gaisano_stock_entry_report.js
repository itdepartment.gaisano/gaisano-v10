// Copyright (c) 2016, Bai Mobile and Web Labs and contributors
// For license information, please see license.txt

frappe.query_reports["Gaisano Stock Entry Report"] = {
	"filters": [
		{
			"fieldname":"branch",
			"label":"Branch",
			"fieldtype":"Link",
			"options":"Branch",
			"reqd":1
		},
		{
			"fieldname":"se_type",
			"label": __("Stock Entry Type"),
			"fieldtype": "Select",
			"options": ['','Bad Order', 'Stock Transfer', 'Wholesale', 'Withdrawals'],
			"reqd":1
		},
		{
			"fieldname":"from_date",
			"label": __("From Date"),
			"fieldtype": "Date",
			"reqd":1
		},
		{
			"fieldname":"to_date",
			"label": __("To Date"),
			"fieldtype": "Date",
			"reqd":1
		},
		{
			"fieldname":"show_memo",
			"label": __("Show Memo?"),
			"fieldtype": "Check",
			"reqd":0
		},
				{
			"fieldname":"show_dm",
			"label": __("Show DM#'s?"),
			"fieldtype": "Check",
			"reqd":0
		}
	]
};
