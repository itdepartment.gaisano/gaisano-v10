// Copyright (c) 2016, Bai Mobile and Web Labs and contributors
// For license information, please see license.txt

frappe.query_reports["Daily Service Level"] = {
	"filters": [
		{
            "fieldname": "date",
            "label": __("Date"),
            "fieldtype": "Date",
            "reqd": 1
        },
		{
            "fieldname": "report_type",
            "label": __("Report Type"),
            "fieldtype": "Select",
            "options": ["Service Level per Supplier", "Service Level per Supplier Discount", "Service Level Scheduled Delivery"],
            "reqd": 1
        }
	]
}
