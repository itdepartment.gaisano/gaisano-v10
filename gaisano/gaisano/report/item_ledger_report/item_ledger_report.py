# Copyright (c) 2015, Frappe Technologies Pvt. Ltd. and Contributors
# License: GNU General Public License v3. See license.txt

from __future__ import unicode_literals
import frappe
from frappe import _


def execute(filters=None):
    filters['company'] = frappe.db.get_value("Global Defaults", None, "default_company")
    columns = get_columns()
    sl_entries = get_stock_ledger_entries(filters)
    item_details = get_item_details(filters)
    #opening_row = get_opening_balance(filters, columns)
    data = []

    #if opening_row:
    #    data.append(opening_row)

    for sle in sl_entries:
        print sle.item_code, sle.supplier_discount
        item_detail = item_details[sle.item_code]
        item_doc = frappe.get_doc("Item", sle.item_code)
        voucher_type = sle.voucher_type
        print voucher_type
        discount = get_supplier_discount(sle.item_code)
        if discount != None:
            discount_doc = frappe.get_doc("Supplier Discounts", discount)
            discount_name = discount_doc.supplier_discount_name
        else:
            discount_name = "None"
        voucher_type = voucher_type.replace(" ", "%20")
        sle_link = "<a href=%s target='_blank'>" % ("/desk#Form/"+voucher_type+"/" + (sle.voucher_no)) + (sle.voucher_no + "</a>")
        if filters.get("supplier_discount"):
            if discount!= filters.get("supplier_discount"):
                continue
        temp = [sle.date, item_doc.item_name_dummy, sle.warehouse,
                     item_detail.stock_uom, sle.actual_qty, sle.qty_after_transaction, discount_name ,sle_link
                    ]
        data.append(temp)
    return columns, data


def get_columns():
    return [_("Date") + ":Datetime:90", _("Item") + ":Data:280", #_("Item Name") + "::100",
            _("Warehouse") + ":Link/Warehouse:180",
            _("UOM") + ":Link/UOM:60",
            _("Qty") + ":Float:50",
            _("Balance Qty") + ":Float:100",
            #_("Supplier") + ":Link/Supplier:170",
            _("Supplier Discount") + ":Data:200",
            #_("Incoming Rate") + ":Currency:110", _("Valuation Rate") + ":Currency:110",
            #_("Balance Value") + ":Currency:110",#_("Voucher Type") + "::110",
            _("Voucher #") + ":Data:200" #+ _("Voucher Type") + ":100",
            #_("Batch") + ":Link/Batch:100",
            #_("Serial #") + ":Link/Serial No:100", _("Company") + ":Link/Company:100"
            ]


def get_stock_ledger_entries(filters):
    return frappe.db.sql("""select concat_ws(" ", posting_date, posting_time) as date,
			item_code, warehouse, actual_qty, qty_after_transaction, incoming_rate, valuation_rate,
			stock_value, voucher_type, voucher_no, batch_no, serial_no, company
		from `tabStock Ledger Entry` sle left join `tabItem Supplier` ts on sle.item_code = ts.parent
		 where company = %(company)s and posting_date between %(from_date)s and %(to_date)s
			{sle_conditions}
			order by posting_date asc, posting_time asc, sle.name asc""" \
                         .format(sle_conditions=get_sle_conditions(filters)), filters, as_dict=1)


def get_item_details(filters):
    item_details = {}
    for item in frappe.db.sql("""select name, item_name, description, item_group,
			brand, stock_uom from `tabItem` {item_conditions}""" \
                                      .format(item_conditions=get_item_conditions(filters)), filters, as_dict=1):
        item_details.setdefault(item.name, item)

    return item_details


def get_item_conditions(filters):
    conditions = []
    if filters.get("item_code"):
        conditions.append("name=%(item_code)s")
    if filters.get("brand"):
        conditions.append("brand=%(brand)s")

    return "where {}".format(" and ".join(conditions)) if conditions else ""


def get_sle_conditions(filters):
    conditions = []
    item_conditions = get_item_conditions(filters)
    if item_conditions:
        conditions.append("""sle.item_code in (select name from tabItem
			{item_conditions})""".format(item_conditions=item_conditions))
    if filters.get("warehouse"):
        conditions.append(get_warehouse_condition(filters.get("warehouse")))
    if filters.get("voucher_no"):
        conditions.append("voucher_no=%(voucher_no)s")
    if filters.get("supplier"):
        conditions.append("ts.supplier=%(supplier)s")
    return "and {}".format(" and ".join(conditions)) if conditions else ""


def get_warehouse_condition(warehouse):
    warehouse_details = frappe.db.get_value("Warehouse", warehouse, ["lft", "rgt"], as_dict=1)
    if warehouse_details:
        return " exists (select name from `tabWarehouse` wh \
			where wh.lft >= %s and wh.rgt <= %s and sle.warehouse = wh.name)" % (warehouse_details.lft,
                                                                                 warehouse_details.rgt)

    return ''

def get_supplier_discount(item_code):
    branch = frappe.db.get_value("Server Information", None, "branch")
    print branch, item_code
    rows = frappe.db.sql("""SELECT supplier_discount from `tabItem Branch Discount` where branch = %s and parent = %s limit 1""",(branch, item_code))
    return rows[0][0] if len(rows)>0 else None