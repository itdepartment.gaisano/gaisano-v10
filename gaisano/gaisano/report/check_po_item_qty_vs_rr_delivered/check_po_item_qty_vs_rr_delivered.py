# Copyright (c) 2013, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe

def execute(filters=None):
	columns = [
		{"label": "PO ", 'width': 100, "fieldname": "po"},
		{"label": "RR #", 'width': 100, "fieldname": "pr"},
		{"label": "RR QTY", 'width': 100, "fieldname": "qty_received"},
		{"label": "PO QTY", 'width': 100, "fieldname": "qty_ordered"},
		{"label": "PO Item", 'width': 280, "fieldname": "po_item"},
		{"label": "RR Amount", 'width': 200, "fieldname": "pr_amount", "fieldtype":"Currency"},
		{"label": "PO Amount", 'width': 200, "fieldname": "po_amount", "fieldtype":"Currency"},
		{"label": "RR Status", 'width': 150, "fieldname": "status"},
		{"label": "Date Created", 'width': 110, "fieldname": "date"},
		{"label": "Expected Delivery", 'width': 110, "fieldname": "delivery_date"},
	]
	data = []
	print "----------------HELLO-------------------"
	po = filters.get("po")
	branch = filters.get("branch")
	selected_po = frappe.get_doc("Purchase Order", po)
	items = selected_po.items

	prs = frappe.db.sql("""SELECT name, invoice_amount FROM `tabPurchase Receipt` WHERE purchase_order = %s and branch = %s """, (selected_po.name,branch))
	for pr in prs:
		data = get_pr_data(selected_po, items, pr, data)

	return columns, data

#Start With PR. Then check Items. crie

def get_pr_data(po, po_items, pr, data):
	po_ref = "<a href=%s target='_blank'>" % ("/desk#Form/Purchase%20Order/" + (po.name)) + (po.name + "</a>")
	pr_ref = "<a href=%s target='_blank'>" % ("/desk#Form/Purchase%20Receipt/" + (pr[0])) + (pr[0] + "</a>")

	pr_selected = frappe.get_doc("Purchase Receipt", pr[0])
	pr_items = frappe.get_doc("Purchase Receipt", pr[0]).items
	temp_sum = 0
	status = ""

	if pr_selected.docstatus == 0:
		status = "Draft"
	elif pr_selected.docstatus == 1:
		status = "Submitted"
	else:
		status = "Cancelled"

	for po_item in po_items:
		no_pr_items = frappe.db.sql(
			"""SELECT name, item_code FROM `tabPurchase Receipt Item` WHERE purchase_order_item = %s and parent = %s""",
			(po_item.name, pr[0]))
		if len(no_pr_items)==0:
			temp = {"po": po_ref, "pr": pr_ref, "qty_ordered": po_item.qty, "qty_received": 0,
					"po_item": po_item.item_description, "pr_amount":0, "po_amount":po_item.amount, "status":status, "date": po.creation, "delivery_date": po.expected_delivery}
			data.append(temp)
			continue
		for pr_item in pr_items:
			if po_item.item_code == pr_item.item_code:
				temp_sum += pr_item.amount
				temp = {"po": po_ref, "pr": pr_ref, "qty_ordered": po_item.qty,  "qty_received": pr_item.qty, "po_item": po_item.item_description, "pr_amount":pr_item.amount, "po_amount":po_item.amount, "status":status, "date": po.creation, "delivery_date": po.expected_delivery}
				data.append(temp)
	temp = {"po": po_ref, "pr": pr_ref,
					"po_item": "<b>TOTAL AMOUNTS</b>", "pr_amount":temp_sum, "po_amount":po.grand_total}
	data.append(temp)
	return data