// Copyright (c) 2016, Bai Mobile and Web Labs and contributors
// For license information, please see license.txt

frappe.query_reports["Check PO Item Qty VS RR Delivered"] = {
	"filters": [
        {
            "fieldname": "branch",
            "label":__("Branch"),
            "fieldtype": "Link",
            "options": "Branch",
            "reqd": 1
        },
		        {
            "fieldname": "po",
            "label": __("Purchase Order"),
            "fieldtype": "Link",
			"options":"Purchase Order",
            "reqd": 1
        }


	]
}
// For license information, please see license.txt
