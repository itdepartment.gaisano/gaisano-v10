# Copyright (c) 2013, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
import datetime


def execute(filters=None):
    discount_name = ""
    if filters.get('discount_name')!=None:
        discount_doc = frappe.get_doc("Supplier Discounts", filters.get('discount_name'))
        discount_name = discount_doc.supplier_discount_name
        print discount_name

    columns = get_columns(filters)
    data = []
    data = get_supplier_pos(filters.get('from_date'), filters.get('to_date'), data, filters.get('ref_service_level'),
                            filters)
    return columns, data


def get_columns(filters):
    columns = []
    supplier = filters.get("supplier_name")
    discount = filters.get("discount_name")

    if supplier == None:
        supplier = ""
    if discount == None:
        discount = ""

    if get_report_type(filters) == 0:
        columns = [
            {"label": "Supplier", 'width': 200, "fieldname": "supplier"},
            {"label": "PO Peso Value", 'width': 150, "fieldname": "peso_value", "fieldtype": "Currency",
             "precision": 2},
            {"label": "PO QTY (Cases)", 'width': 150, "fieldname": "po_qty", "fieldtype": "float",
             "precision": 2},
            {"label": "Service Level (Peso Value)", 'width': 200, "fieldname": "service_level_peso", "fieldtype":"Percent"},
            {"label": "Service Level (QTY)", 'width': 200, "fieldname": "service_level_qty", "fieldtype":"Percent"},
            {"label": "RR Peso Value", 'width': 150, "fieldname": "rr_peso_value", "fieldtype": "Currency",
             "precision": 2},
            {"label": "RR QTY (Cases)", 'width': 150, "fieldname": "rr_qty", "fieldtype": "float",
             "precision": 2}

        ]
    elif get_report_type(filters) == 1:
        columns = [
            {"label": "Supplier", 'width': 200, "fieldname": "supplier"},
            {"label": "Supplier Discount", 'width': 200, "fieldname": "supplier_disc"},
            {"label": "PO Peso Value", 'width': 150, "fieldname": "peso_value", "fieldtype": "Currency",
             "precision": 2},
            {"label": "PO QTY (Cases)", 'width': 150, "fieldname": "po_qty", "fieldtype": "float",
             "precision": 2},
            {"label": "Service Level (Peso Value)", 'width': 200, "fieldname": "service_level_peso", "fieldtype":"Percent"},
            {"label": "Service Level (QTY)", 'width': 200, "fieldname": "service_level_qty","fieldtype":"Percent"},
            {"label": "RR Peso Value", 'width': 150, "fieldname": "rr_peso_value", "fieldtype": "Currency",
             "precision": 2},
            {"label": "RR QTY (Cases)", 'width': 150, "fieldname": "rr_qty", "fieldtype": "float",
             "precision": 2}
        ]

    elif get_report_type(filters) == 2:
        if (supplier == "") and (discount == ""):
            columns = [
                {"label": "Supplier", 'width': 200, "fieldname": "supplier"},
                {"label": "Supplier Discount", 'width': 200, "fieldname": "supplier_disc"},
                {"label": "Purchase Order", 'width': 150, "fieldname": "po"},
                {"label": "PO Peso Value", 'width': 150, "fieldname": "peso_value", "fieldtype": "Currency",
                 "precision": 2},
                {"label": "PO QTY (Cases)", 'width': 150, "fieldname": "po_qty", "fieldtype": "float",
                 "precision": 2},
                {"label": "Service Level (Peso Value)", 'width': 200, "fieldname": "service_level_peso", "fieldtype":"Percent"},
                {"label": "Service Level (QTY)", 'width': 200, "fieldname": "service_level_qty", "fieldtype":"Percent"},
                {"label": "RR Peso Value", 'width': 150, "fieldname": "rr_peso_value", "fieldtype": "Currency",
                 "precision": 2},
                {"label": "RR QTY (Cases)", 'width': 150, "fieldname": "rr_qty", "fieldtype": "float",
                 "precision": 2}
            ]
        else:
            if supplier != "":
                columns = [
                    {"label": "Supplier Discount", 'width': 200, "fieldname": "supplier_disc"},
                    {"label": "Purchase Order", 'width': 150, "fieldname": "po"},
                    {"label": "PO Peso Value", 'width': 150, "fieldname": "peso_value", "fieldtype": "Currency",
                     "precision": 2},
                    {"label": "PO QTY (Cases)", 'width': 150, "fieldname": "po_qty", "fieldtype": "float",
                     "precision": 2},
                    {"label": "Service Level (Peso Value)", 'width': 200, "fieldname": "service_level_peso",
                     "fieldtype": "Percent"},
                    {"label": "Service Level (QTY)", 'width': 200, "fieldname": "service_level_qty",
                     "fieldtype": "Percent"},
                    {"label": "RR Peso Value", 'width': 150, "fieldname": "rr_peso_value", "fieldtype": "Currency",
                     "precision": 2},
                    {"label": "RR QTY (Cases)", 'width': 150, "fieldname": "rr_qty", "fieldtype": "float",
                     "precision": 2}
                ]
            else:
                columns = [
                    {"label": "Purchase Order", 'width': 150, "fieldname": "po"},
                    {"label": "PO Peso Value", 'width': 150, "fieldname": "peso_value", "fieldtype":"Currency", "precision":2},
                    {"label": "PO QTY (Cases)", 'width': 150, "fieldname": "po_qty", "fieldtype": "float",
                     "precision": 2},
                    {"label": "Service Level (Peso Value)", 'width': 200, "fieldname": "service_level_peso",
                     "fieldtype": "Percent"},
                    {"label": "Service Level (QTY)", 'width': 200, "fieldname": "service_level_qty",
                     "fieldtype": "Percent"},
                    {"label": "RR Peso Value", 'width': 150, "fieldname": "rr_peso_value", "fieldtype": "Currency",
                     "precision": 2},
                    {"label": "RR QTY (Cases)", 'width': 150, "fieldname": "rr_qty", "fieldtype": "float",
                     "precision": 2}
                ]
    elif get_report_type(filters) == 3:
        if (supplier == "") and (discount == ""):
            columns = [
                {"label": "Supplier", 'width': 200, "fieldname": "supplier"},
                {"label": "Purchase Order", 'width': 150, "fieldname": "po"},
                {"label": "PO Peso Value", 'width': 150, "fieldname": "peso_value", "fieldtype": "Currency",
                 "precision": 2},
                {"label": "PO QTY (Cases)", 'width': 150, "fieldname": "po_qty", "fieldtype": "float",
                 "precision": 2},
                {"label": "Service Level (Peso Value)", 'width': 200, "fieldname": "service_level_peso", "fieldtype":"Percent"},
                {"label": "Service Level (QTY)", 'width': 200, "fieldname": "service_level_qty", "fieldtype":"Percent"},
                {"label": "RR Peso Value", 'width': 150, "fieldname": "rr_peso_value", "fieldtype": "Currency",
                 "precision": 2},
                {"label": "RR QTY (Cases)", 'width': 150, "fieldname": "rr_qty", "fieldtype": "float",
                 "precision": 2}
            ]
        else:
            if supplier != "":
                columns = [
                    {"label": "Purchase Order", 'width': 150, "fieldname": "po"},
                    {"label": "PO Peso Value", 'width': 150, "fieldname": "peso_value", "fieldtype": "Currency",
                     "precision": 2},
                    {"label": "PO QTY (Cases)", 'width': 150, "fieldname": "po_qty", "fieldtype": "float",
                     "precision": 2},
                    {"label": "Service Level (Peso Value)", 'width': 200, "fieldname": "service_level_peso",
                     "fieldtype": "Percent"},
                    {"label": "Service Level (QTY)", 'width': 200, "fieldname": "service_level_qty",
                     "fieldtype": "Percent"},
                    {"label": "RR Peso Value", 'width': 150, "fieldname": "rr_peso_value", "fieldtype": "Currency",
                     "precision": 2},
                    {"label": "RR QTY (Cases)", 'width': 150, "fieldname": "rr_qty", "fieldtype": "float",
                     "precision": 2}
                ]
            else:
                columns = [
                    {"label": "Purchase Order", 'width': 150, "fieldname": "po"},
                    {"label": "PO Peso Value", 'width': 150, "fieldname": "peso_value", "fieldtype": "Currency",
                     "precision": 2},
                    {"label": "PO QTY (Cases)", 'width': 150, "fieldname": "po_qty", "fieldtype": "float",
                     "precision": 2},
                    {"label": "Service Level (Peso Value)", 'width': 200, "fieldname": "service_level_peso",
                     "fieldtype": "Percent"},
                    {"label": "Service Level (QTY)", 'width': 200, "fieldname": "service_level_qty",
                     "fieldtype": "Percent"},
                    {"label": "RR Peso Value", 'width': 150, "fieldname": "rr_peso_value", "fieldtype": "Currency",
                     "precision": 2},
                    {"label": "RR QTY (Cases)", 'width': 150, "fieldname": "rr_qty", "fieldtype": "float",
                     "precision": 2}
                ]
    if filters.get('show_rr') == 1:
        columns.append({"label": "Linked RR's", 'width': 200, "fieldname": "rr"})
    return columns


def get_report_type(filters):
    # print "============get report type==========="
    # print filters.get('supplier_discount')
    # print filters.get('show_po')

    if (filters.get('supplier_discount') == None) and (
        filters.get('supplier') == 1):  # sort by supplier and don't show po
        # print 0
        return 0
    elif (filters.get('supplier_discount') == 1) or ((filters.get('supplier_discount') == 1) and (filters.get('supplier') == 1)):  # sort by supplier discount; don't show po
        # print 1
        return 1
    elif (filters.get('supplier_discount') == None) and (filters.get('show_po') == None) and (
        filters.get('supplier') == None):  # sort by rr and po's
        # print 2
        return 2
    else:  # sort by po
        # print 3
        return 3


def get_supplier_pos(from_date=None, to_date=None, data=None, service_level=None, filters=None):
    all_pos = []
    rows = []
    branch = filters.get("branch")
    supplier_name = filters.get("supplier_name")
    discount_name = filters.get("discount_name")

    print from_date, to_date, supplier_name, discount_name

    if (supplier_name!=None) and (discount_name==None):
        rows = frappe.db.sql(
            """SELECT name, supplier from `tabPurchase Order` where transaction_date >= %s and transaction_date <= %s
            and docstatus = 1 and supplier = %s and branch =%s ORDER BY supplier asc""",
            (from_date, to_date, supplier_name, branch))
    elif (supplier_name==None) and (discount_name!=None):
        rows = frappe.db.sql(
            """SELECT name, supplier from `tabPurchase Order` where transaction_date >= %s and transaction_date <= %s
            and docstatus = 1 and supplier_discount = %s and branch =%s ORDER BY supplier asc""",
            (from_date, to_date, discount_name, branch))
    elif (supplier_name != None) and (discount_name != None):
        rows = frappe.db.sql(
            """SELECT name, supplier from `tabPurchase Order` where transaction_date >= %s and transaction_date <= %s
            and docstatus = 1 and supplier = %s and supplier_discount = %s and branch =%s ORDER BY supplier asc""",
            (from_date, to_date, supplier_name, discount_name, branch)) #sad life

    for row in rows:
        po = frappe.get_doc("Purchase Order", row[0])
        # print po.name, po.transaction_date
        data = get_rrs(po, data, service_level, filters)
    data = filter_data_only(filters, data)
    if get_report_type(filters) == 2:
        data = add_totals_modified(data, service_level)
    else:
        data = add_total_row(data)
    return data


def get_rrs(po, data, service_level, filters):
    print "=============GET RR============="
    print po.name
    rr_list = ""
    peso_lvl, qty_lvl, peso_value = 0, 0 ,0
    data_for_filtering = []  # save all data here and the remove the stuff that don't need to be displayed.
    rows = frappe.db.sql(
        """SELECT name, grand_total from  `tabPurchase Receipt` where purchase_order = %s and docstatus = 1""", po.name)
    # print rows
    if len(rows)>0:
        for row in rows:
            # print row
            this_rr = frappe.get_doc("Purchase Receipt", row[0])
            rr_link = "<a href=%s target='_blank'>" % ("/desk#Form/Purchase%20Receipt/" + (this_rr.name)) + (
            this_rr.name + "</a>")
            po_cases = po.total_case
            rr_cases = this_rr.total_case
            po_link = "<a href=%s target='_blank'>" % ("/desk#Form/Purchase%20Order/" + (po.name)) + (po.name + "</a>")
            peso_lvl, qty_lvl, peso_value = compute_service_level(this_rr)
            if (peso_lvl <= service_level) or (qty_lvl <= service_level):
                temp = {"supplier": po.supplier, "supplier_disc": po.supplier_discount_name, "po": po_link, "po_name": po.name,
                        "peso_value":peso_value, "service_level_peso": peso_lvl, "service_level_qty": qty_lvl, "rr": rr_link, "rr_name":this_rr.name,
                        "po_qty":po_cases, "rr_qty": rr_cases, "rr_peso_value":this_rr.grand_total}
                data.append(temp)
    else:
        print "-------NO RR's-------"
        po_link = "<a href=%s target='_blank'>" % ("/desk#Form/Purchase%20Order/" + (po.name)) + (po.name + "</a>")
        peso_lvl, qty_lvl, peso_value = 0, 0, po.grand_total
        print peso_lvl, qty_lvl, peso_value
        if (peso_lvl <= service_level) or (qty_lvl <= service_level):
            temp = {"supplier": po.supplier, "supplier_disc": po.supplier_discount_name, "po": po_link, "po_name": po.name,
                    "peso_value": peso_value, "service_level_peso": peso_lvl, "service_level_qty": qty_lvl,
                    "rr": "", "rr_name": "", "po_qty":po.total_case, "rr_qty": 0, "rr_peso_value":0}
            data.append(temp)
            # print temp

    return data


def compute_service_level(doc):
    print "=============SERVICE LEVEL==========="
    peso_value = 0.00
    qty_based = 0.00

    linked_po = frappe.get_doc("Purchase Order", str(doc.purchase_order))
    po_grand_total = linked_po.grand_total
    pr_grand_total = doc.total_amount

    peso_value = pr_grand_total / po_grand_total * 100

    po_items = get_items_total_qty(linked_po.items)
    pr_items = get_items_total_qty(doc.items)

    qty_based = pr_items / po_items * 100

    print peso_value
    print qty_based

    return round(peso_value,2), round(qty_based,2), po_grand_total


def get_items_total_qty(items):
    total_qty = 0
    # print "=============items=============="
    for item in items:
        #    print item.item_code
        #    print item.qty
        total_qty += item.qty
    return total_qty


def filter_data_only(filters, data):
    current_filter = None
    new_data = None
    service_level = filters.get('ref_service_level')
    if get_report_type(filters) == 2:
        newlist = sorted(data, key=lambda k: k['po'])
        return newlist

    else:
        new_data = filter_by_po(data, service_level)
        if get_report_type(filters) == 3:
            print "PO only"
            return new_data
        else:
            if get_report_type(filters) == 0:  # sort by supplier
                current_filter = 'supplier'
                print "supplier"
            elif get_report_type(filters) == 1:  # sort by supplier discount
                current_filter = 'supplier_disc'
                print "supplier_discount"
            new_data = sorted(new_data, key=lambda k: k[current_filter])
            new_data = get_SL_averages(current_filter, new_data, service_level)
            return new_data


def get_po_totals(po):
    rr_qty = 0
    rr_peso_value = 0
    po_name = po
    total_percent = 0
    total_qty = 0
    total_peso_value = frappe.get_doc("Purchase Order",po).grand_total
    rrs = frappe.db.sql("""SELECT name from `tabPurchase Receipt` where purchase_order = %s and docstatus = 1""",
                        po_name)
    rr_links = ""
    rr_names = ""
    for i, rr in enumerate(rrs):
        rr_doc = frappe.get_doc("Purchase Receipt", rr[0])
        rr_service_level = compute_service_level(rr_doc)
        total_percent += rr_service_level[0]
        total_qty += rr_service_level[1]
        rr_qty += (float(rr_doc.total_case) if rr_doc.total_case is not None else 0)
        rr_peso_value += rr_doc.grand_total
        #total_peso_value += compute_service_level(rr_doc)[2]
        rr_links += "<a href=%s target='_blank'>" % ("/desk#Form/Purchase%20Receipt/" + (rr[0])) + (rr[0] + "</a>")
        rr_names += ""+rr[0]
        if (i + 1) != len(rrs):
            rr_links += ", "
            rr_names += ", "

    return total_percent, total_qty, rr_links, rr_names, total_peso_value, rr_qty, rr_peso_value


def filter_by_po(data, servicelvl):

    newlist = sorted(data, key=lambda k: k["po_name"])

    new_data = []
    current_po = ""
    peso_lvl = 0
    qty_lvl = 0
    po_qty = 0
    rr_qty = 0
    peso_value = 0
    supplier = ""
    sup_disc = ""
    rr_link = ""
    po_link = ""
    rr_links = ""
    rr_names = ""

    for i, item in enumerate(newlist):

        if i == 0:
            print "i is 0"
            current_po = item["po_name"]  # change to supplier
            print current_po
            peso_lvl, qty_lvl, rr_links, rr_names, peso_value, rr_qty, rr_peso_value = get_po_totals(current_po)
            po_qty = item['po_qty']
            supplier = item['supplier']
            sup_disc = item['supplier_disc']

        if current_po != item["po_name"]:
            po_link = "<a href=%s target='_blank'>" % ("/desk#Form/Purchase%20Order/" + (current_po)) + (current_po + "</a>")

            temp_data = {"supplier": supplier, "supplier_disc": sup_disc, "po": po_link, "po_name": current_po,
                         "service_level_peso": peso_lvl, "peso_value": peso_value,
                         "service_level_qty": qty_lvl, "rr": rr_links, "rr_name":rr_names,
                         "po_qty": po_qty, "rr_qty": rr_qty, "rr_peso_value": rr_peso_value
                         }
            print "different PO"
            print temp_data

            if (qty_lvl<=servicelvl) or (peso_lvl<=servicelvl):
                new_data.append(temp_data)

            current_po = item['po_name']
            supplier = item['supplier']
            sup_disc = item['supplier_disc']
            peso_lvl, qty_lvl, rr_links, rr_names, peso_value, rr_qty, rr_peso_value = get_po_totals(current_po)
            po_qty = item['po_qty']



        elif current_po== item['po_name'] and (i+1)<len(newlist):
            print "same po"
            print item['po_name']
            continue

        if (i + 1) == len(newlist):
            po_link = "<a href=%s target='_blank'>" % ("/desk#Form/Purchase%20Order/" + (current_po)) + (
                current_po + "</a>")
            temp_data = {"supplier": supplier, "supplier_disc": sup_disc, "po": po_link, "po_name": current_po,
                         "service_level_peso": peso_lvl, "peso_value": peso_value,
                         "service_level_qty": qty_lvl, "rr": rr_links, "rr_name":rr_names,
                         "po_qty": po_qty, "rr_qty": rr_qty, "rr_peso_value": rr_peso_value}
            if (qty_lvl<=servicelvl) or (peso_lvl<=servicelvl):
                new_data.append(temp_data)
    return new_data

    #return data


def get_SL_averages(setfilter, newlist, servicelvl):
    counter = 1
    current_data = ""
    peso_total = 0
    qty_total, rr_qty, po_qty, rr_peso_value = 0, 0, 0, 0
    peso_value = 0
    supplier = ""
    po_links = ""
    po_names = ""
    sup_disc = ""
    rr_links = ""
    rr_names = ""
    new_data = []
    print "============get_sl!=============="
    print setfilter
    for i, item in enumerate(newlist):

        if i == 0:
            current_data = item[setfilter]  # change to supplier
            supplier, sup_disc, po_links, po_names = item['supplier'], item['supplier_disc'], item['po'], item['po_name']
            peso_total, qty_total, rr_links, rr_names = item['service_level_peso'], item['service_level_qty'], item['rr'], item['rr_name']
            po_qty, rr_qty, rr_peso_value = float(item['po_qty']), float(item['rr_qty']), float(item['rr_peso_value'])
            peso_value = item['peso_value']
            counter = 1

        if current_data != item[setfilter]:
            qty_total = rr_qty/po_qty * 100
            peso_total = rr_peso_value/peso_value * 100
            temp_data = {"supplier": supplier, "supplier_disc": sup_disc, "po": po_links, "po_name": po_names,
                         "peso_value":peso_value,"service_level_peso": peso_total,
                         "service_level_qty": qty_total, "rr": rr_links, "rr_name":rr_names,
                         "po_qty": po_qty, "rr_qty":rr_qty, "rr_peso_value":rr_peso_value}
            if (peso_total<=servicelvl) or (qty_total<=servicelvl):
                new_data.append(temp_data)

            current_data = item[setfilter]  # change to supplier
            peso_total, qty_total, rr_links, rr_names = item['service_level_peso'], item['service_level_qty'], item['rr'], item['rr_name']
            po_qty, rr_qty, rr_peso_value = float(item['po_qty']), float(item['rr_qty']), float(item['rr_peso_value'])
            supplier = item['supplier']
            sup_disc = item['supplier_disc']
            po_links = item['po']
            po_names = item['po_name']
            peso_value = item['peso_value']
            counter = 1

        else:
            if i!=0:
                print item['po_qty'], item['rr_qty'], item['rr_peso_value'], "***************DATA##"
                po_qty += float(item['po_qty'])
                rr_qty += float(item['rr_qty'])
                rr_peso_value += float(item['rr_peso_value'])
                peso_total += item['service_level_peso']
                qty_total += item['service_level_qty']
                rr_links += (", " + item['rr'])
                rr_names += (", " + item['rr_name'])
                po_names += (", " + item['po_name'])
                peso_value += item['peso_value']
                counter += 1

        if (i + 1) == len(newlist):
            qty_total = rr_qty / po_qty * 100
            peso_total = rr_peso_value / peso_value * 100
            temp_data = {"supplier": supplier, "supplier_disc": sup_disc, "po": po_links, "po_name":po_names,
                         "peso_value":peso_value, "service_level_peso": peso_total,
                         "service_level_qty": qty_total, "rr": rr_links, "rr_name":rr_names,
                         "po_qty": po_qty, "rr_qty":rr_qty, "rr_peso_value":rr_peso_value}
            if (peso_total<=servicelvl) or (qty_total<=servicelvl):
                new_data.append(temp_data)

            # new_data = check_totals(new_data)

    return new_data


def add_total_row(data):
    total_peso_value = 0
    average_qty=0
    average_peso_value=0
    po_qty = 0
    rr_qty = 0
    rr_grand_total = 0
    counter = 0
    for i,row in enumerate(data):
        total_peso_value +=row['peso_value']
        po_qty += (int(row['po_qty']) if row['po_qty'] is not None else 0)
        rr_qty += row['rr_qty']
        rr_grand_total += row['rr_peso_value']
    average_peso_value =rr_grand_total/total_peso_value*100
    average_qty = rr_qty/po_qty*100
    data.append({"po": "TOTAL PESO VALUE", "peso_value":total_peso_value, "service_level_peso": average_peso_value,
                         "service_level_qty": average_qty, "po_qty": po_qty, "rr_qty": rr_qty, "rr_peso_value": rr_grand_total})
    return data

def add_totals_modified(data, service_level):
    new_data = filter_by_po(data, service_level)
    total_peso_value = 0
    average_qty=0
    average_peso_value=0
    rr_grand_total = 0
    po_qty = 0
    rr_qty = 0
    i=0
    for i,row in enumerate(new_data):
        total_peso_value +=row['peso_value']
        po_qty += (int(row['po_qty']) if row['po_qty'] is not None else 0)
        rr_qty += row['rr_qty']
        rr_grand_total += row['rr_peso_value']
    average_peso_value = rr_grand_total/total_peso_value * 100
    average_qty = rr_qty/po_qty * 100
    data.append({"po": "TOTAL PESO VALUE", "peso_value": total_peso_value, "service_level_peso": average_peso_value,
                 "service_level_qty": average_qty, "po_qty": po_qty, "rr_qty": rr_qty, "rr_peso_value": rr_grand_total})
    return data 