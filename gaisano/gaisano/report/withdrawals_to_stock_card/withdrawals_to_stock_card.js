// Copyright (c) 2016, Bai Mobile and Web Labs and contributors
// For license information, please see license.txt

frappe.query_reports["Withdrawals to Stock Card"] = {
	"filters": [
		{
			"fieldname": "from_date",
			"label": __("From Date"),
			"fieldtype": "Date",
			"reqd": 1
		},

		{
			"fieldname": "to_date",
			"label": __("To Date"),
			"fieldtype": "Date",
			"reqd": 1
		},

		{
			"fieldname": "branch",
            "label": __("Branch"),
            "fieldtype": "Link",
            "options": "Branch",
            "reqd": 1
		},
		{
			"fieldname": "item",
            "label": __("Item"),
            "fieldtype": "Link",
            "options": "Item",

			"get_query": function() {
			return {
			"doctype": "Item",
			"filters": {
			"stock_card": 1,
						}
					}
			}
		}


	]
}
