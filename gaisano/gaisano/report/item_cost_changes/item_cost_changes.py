# Copyright (c) 2013, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe, datetime
from frappe.frappeclient import FrappeClient
from gaisano.apisync import connect_to_server

def execute(filters=None):
	columns, data = [], []
	branch = filters.get("branch")
	reference_date = filters.get("reference_date")
	supplier = filters.get("supplier")
	supplier_discount = filters.get("supplier_discount")
	category = filters.get("category")
	columns = [
		{"fieldname": "item_description", "label": "Item Name", "fieldtype":"Data", "width":400},
		{"fieldname": "barcode", "label": "Barcode", "fieldtype": "Data", "width":150},
		{"fieldname": "prev_price", "label": "Previous Cost", "fieldtype": "Currency", "width":120},
		{"fieldname": "new_price", "label": "New Cost", "fieldtype": "Currency", "width":120},
		{"fieldname": "srp_wholesale", "label": "Wholesale SRP", "fieldtype": "Currency", "width": 120},
		{"fieldname": "srp_retail", "label": "Retail SRP", "fieldtype": "Currency", "width": 120},
		{"fieldname": "modified_by", "label": "Modified By", "fieldtype": "Data", "width":200}
	]
	data = get_data(reference_date, supplier, supplier_discount, branch, category)


	return columns, data


def get_data(reference_date, supplier, supplier_discount, branch, category):
	temp = []
	category = category if category is not None else ""
	ref_date_obj = datetime.datetime.strptime(reference_date, "%Y-%m-%d")
	tomorrow = ref_date_obj + datetime.timedelta(days=1)
	if branch == "CDO Main":
		# return frappe.db.sql("""SELECT hist.*, itm.item_name_dummy as item_description, itm.item_price as srp_wholesale,
		# 					itm.item_price_retail_with_margin as srp_retail from `tabItem Price History` hist inner join `tabItem` itm on itm.name = hist.item_code where hist.date_modified = %s""", reference_date, as_dict = True)
		if (supplier == None) and (supplier_discount == None):
			items = frappe.db.sql("""SELECT hist.modified_by, hist.prev_price, hist.new_price, hist.barcode,  itm.item_name_dummy, itm.item_price,
		    					itm.item_price_retail_with_margin from `tabItem Price History` hist inner join `tabItem` itm on itm.name = hist.item_code inner join `tabItem Class Each` ice on itm.name = ice.parent where hist.date_modified = %s and ice.category like %s""", (reference_date, '%'+category+'%'))
		elif (supplier != None) and (supplier_discount == None):
			items = frappe.db.sql("""SELECT hist.modified_by, hist.prev_price, hist.new_price, hist.barcode,  itm.item_name_dummy, itm.item_price,
		    					itm.item_price_retail_with_margin from `tabItem Price History` hist inner join `tabItem` itm on itm.name = hist.item_code left join `tabItem Supplier` sup on sup.parent = itm.name inner join `tabItem Class Each` ice on itm.name = ice.parent where hist.date_modified = %s and sup.supplier = %s and ice.category like %s""",(reference_date, supplier, '%'+category+'%'))
		elif(supplier != None) and (supplier_discount != None):
			items = frappe.db.sql("""SELECT hist.modified_by, hist.prev_price, hist.new_price, hist.barcode,  itm.item_name_dummy, itm.item_price,
								itm.item_price_retail_with_margin from `tabItem Price History` hist inner join `tabItem` itm on itm.name = hist.item_code inner join `tabItem Supplier` sup on sup.parent = itm.name inner join `tabItem Branch Discount` disc on disc.parent = itm.name inner join `tabItem Class Each` ice on itm.name = ice.parent where hist.date_modified = %s and sup.supplier = %s and disc.supplier_discount = %s and disc.branch = %s and ice.category like %s """, (reference_date, supplier, supplier_discount, branch, '%'+category+'%'))
		for item in items:
			modified_by = item[0]
			prev_price = item[1]
			new_price = item[2]
			barcode = item[3]
			item_name_dummy = item[4]
			item_price_retail_with_margin = item[6]
			item_price = item[5]

			temp.append({"item_description": item_name_dummy, "barcode": barcode, "prev_price": prev_price,
							 "new_price": new_price, "srp_wholesale": item_price,
							 "srp_retail": item_price_retail_with_margin, "modified_by": modified_by})


	else:
		temp = []
		# main_server = FrappeClient("http://202.137.117.24", "tailerp_api@gaisano.com", 't@1l3rpAPI')
		main_server = connect_to_server()
		# main_server = FrappeClient("http://192.168.5.125", "tailerp_api@gaisano.com", 't@1l3rpAPI')
		data = main_server.get_api('gaisano.gaisano.report.item_cost_changes.item_cost_changes.get_data_from_server',params={"reference_date": str(reference_date)})
		for item in data:
			modified_by = item[0]
			prev_price = item[1]
			new_price = item[2]
			barcode = item[3]
			item_code = item[4]

			if (supplier == None) and (supplier_discount == None):
				local_items = frappe.db.sql("""SELECT itm.item_price, itm.item_price_retail_with_margin, itm.item_name_dummy from `tabItem` itm inner join `tabItem Class Each` ice on itm.name = ice.parent where itm.name = %s and  ice.category like %s""", (item_code,'%'+category+'%'))
			elif (supplier != None) and (supplier_discount == None):
				local_items = frappe.db.sql("""SELECT itm.item_price, itm.item_price_retail_with_margin, itm.item_name_dummy from `tabItem` itm left join `tabItem Supplier` sup on sup.parent = itm.name inner join `tabItem Class Each` ice on itm.name = ice.parent where itm.name = %s and sup.supplier = %s and ice.category like %s""",(item_code, supplier, '%'+category+'%'))
			elif(supplier != None) and (supplier_discount != None):
				local_items = frappe.db.sql("""SELECT itm.item_price, itm.item_price_retail_with_margin, itm.item_name_dummy from `tabItem` itm inner join `tabItem Supplier` sup on sup.parent = itm.name inner join `tabItem Branch Discount` disc on disc.parent = itm.name inner join `tabItem Class Each` ice on itm.name = ice.parent where itm.name = %s and sup.supplier = %s and disc.supplier_discount = %s and disc.branch = %s and ice.category like %s """, (item_code, supplier, supplier_discount, branch, '%'+category+'%'))
			for local in local_items:

				temp.append({"item_description": local[3], "barcode": barcode, "prev_price": prev_price,
							 "new_price": new_price, "srp_wholesale": local[0],
							 "srp_retail": local[1], "modified_by": modified_by})

	return temp

@frappe.whitelist()
def get_data_from_server(reference_date):
	items = frappe.db.sql("""SELECT hist.modified_by, hist.prev_price, hist.new_price, hist.barcode, hist.item_code from `tabItem Price History` hist where hist.date_modified = %s""",(reference_date))

	return items