# Copyright (c) 2013, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe

def execute(filters=None):
	columns = [
		{"label": "STT Number", "width": 200, "fieldname": "stt_number"},
		{"label": "Posting Date", "width": 150, "fieldname": "posting_date"},
		{"label": "Status", "width": 150, "fieldname": "status"},
		{"label": "Source Warehouse", "width": 200, "fieldname": "source_warehouse"},
		{"label": "Target Warehouse", "width": 200, "fieldname": "target_warehouse"},
		{"label": "Service Level", "width": 150, "fieldname": "service_level"}
	]

	branch = filters.get("branch")
	from_date = filters.get("from_date")
	to_date = filters.get("to_date")
	data = stt_report(from_date, to_date,branch)

	return columns, data

def stt_report(from_date, to_date, branch):
	data = []

	stt = frappe.db.sql("""select name, posting_date, from_warehouse, to_warehouse, total_case from `tabStock Entry` where type = 'Stock Transfer' and posting_date >= %s and posting_date <= %s and branch = %s""", (from_date, to_date, branch))

	for stts in stt:
		from_warehouse = frappe.db.sql("""select branch from `tabWarehouse` where name = %s""", (stts[2]))
		to_warehouse = frappe.db.sql("""select branch from `tabWarehouse` where name = %s""", (stts[3]))

		if from_warehouse[0] != to_warehouse[0]:
			rr_total_case = frappe.db.sql("""select sum(total_case) from `tabPurchase Receipt` where rr_type = "Get STT from Other Branch" and docstatus = 1 and stt_from_branches = %s group by purchase_order""", (stts[0]))
			if (rr_total_case):
				service_level = (float(rr_total_case[0][0] / float(stts[4])) * 100)
				if service_level == 100:
					status = "Fully Served"
				elif service_level > 100:
					status = "Overserved"
				else:
					status = "Partially Served"
			else:
				service_level = 0
				status = "In-Transit"

			data.append({"stt_number": stts[0], "posting_date": stts[1], "status": status, "source_warehouse": stts[2], "target_warehouse": stts[3] ,"service_level": service_level})

	return data