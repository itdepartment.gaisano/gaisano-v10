# Copyright (c) 2013, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
from dateutil.relativedelta import relativedelta
import frappe, datetime

def execute(filters=None):
	columns = [
        {"label": "Supplier Name", 'width': 300, "fieldname": "supplier"},
		{"label": "Period Covered", 'width': 300, "fieldname": "period_covered"},
        {"label": "Peso Value", 'width': 150, "fieldname": "peso_value", "fieldtype":"Currency"},
		{"label": "Volume (in pcs)", 'width': 150, "fieldname": "volume", "fieldtype": "float", "precision":2},
		{"label": "Percent Growth (Peso Value)", 'width': 100, "fieldname": "growth_peso", "fieldtype": "Float", "precision":2},
		{"label": "Percent Growth (Volume)", 'width': 100, "fieldname": "growth_volume", "fieldtype": "Float","precision": 2},



    ]
	data = []
	supplier = filters.get("supplier")
	from_date = filters.get("from_date")
	to_date = filters.get("to_date")
	branch = filters.get("branch")
	supplier_discount = filters.get("supplier_discount")
	breakdown_by_discount = filters.get("breakdown_by_discount")

	if breakdown_by_discount != 1:
		data,prev_pos_volume, next_pos_volume, prev_pos_sale, next_pos_sale = get_sales_single(data, from_date, to_date,  branch, supplier, supplier_discount)
		data, prev_wholesale_volume, next_wholesale_volume, prev_wholesale_sale, next_wholesale_sale = get_wholesale_sales(data, from_date, to_date,  branch, supplier, supplier_discount)
		data = get_totals(data, from_date, to_date, branch, supplier, supplier_discount)
	else:
		if supplier_discount is not None:
			data = break_down_sales(data, from_date, to_date, branch, supplier, supplier_discount)
			data = break_down_wholesale(data, from_date, to_date, branch, supplier, supplier_discount)
		else:
			for discount in frappe.db.sql("""SELECT name from `tabSupplier Discounts` where supplier_name = %s""", supplier):
				data = break_down_sales(data, from_date, to_date, branch, supplier, discount[0])
				data = break_down_wholesale(data, from_date, to_date, branch, supplier, discount[0])

	return columns, data

def get_item_discount(item_code, supplier_discount):
	branch = frappe.db.get_value("Server Information", None, "branch")
	discount = frappe.db.sql("""SELECT count(*) from `tabItem Branch Discount` where parent = %s and branch = %s and supplier_discount = %s""",
				  (item_code, branch,supplier_discount))
	# print item_code, supplier_discount, discount[0][0]
	if (discount[0][0]) == 0:
		return False
	else:
		return True

def get_sales_single(data, from_date, to_date,  branch, supplier, supplier_discount):
	discount_doc = None if (supplier_discount is None) else frappe.get_doc("Supplier Discounts", supplier_discount)
	date_str = str(from_date) + " TO " + str(to_date)
	# print supplier, from_date, to_date

	items = frappe.db.sql(
		"""select itm.barcode_retial, itm.name from `tabItem` itm inner join `tabItem Supplier` sup on sup.parent = itm.name where sup.supplier = %s""",
		supplier)

	prev_from_date = datetime.datetime.strptime(from_date, "%Y-%m-%d") - relativedelta(years=1)
	prev_to_date = datetime.datetime.strptime(to_date, "%Y-%m-%d") - relativedelta(years=1)
	prev_date_str = str(prev_from_date.strftime("%Y-%m-%d")) + " TO " + str(prev_to_date.strftime("%Y-%m-%d"))
	# print "PREV DATE", from_date, to_date
	prev_total_sales = 0.0
	prev_volume = 0.0
	for item in items:
		if (supplier_discount != None) and (supplier_discount != ""):
			if get_item_discount(item[1], supplier_discount) is False:
				continue
		barcode_retial = item[0]
		idv_sales = frappe.db.sql("""select sum(amount), sum(qty) from `tabUpload POS` where branch LIKE %s and trans_date >= %s and
    		                          trans_date <= %s AND barcode = %s""",
								  ('%' + branch + '%', str(prev_from_date), str(prev_to_date), str(barcode_retial)))
		for row in idv_sales:
			# print peso_value[0]
			peso_value = row[0] if row[0] is not None else 0
			volume = row[1]if row[1] is not None else 0
			prev_total_sales += peso_value
			prev_volume += volume
	data.append({"supplier":discount_doc.supplier_discount_name + " (Based on POS Sales)" if supplier_discount is not None else supplier + " (Based on POS Sales)",
				 "period_covered": prev_date_str, "peso_value": prev_total_sales, "volume":prev_volume})

	total_sales = 0.0
	total_volume = 0.0
	for item in items:
		if (supplier_discount != None) and (supplier_discount != ""):
			if get_item_discount(item[1], supplier_discount) is False:
				continue
		barcode_retial = item[0]
		idv_sales = frappe.db.sql("""select sum(amount), sum(qty) from `tabUpload POS` where branch LIKE %s and trans_date >= %s and
    		                          trans_date <= %s AND barcode = %s""",
								  ('%' + branch + '%', str(from_date), str(to_date), str(barcode_retial)))
		for row in idv_sales:
			peso_value = row[0] if row[0] is not None else 0
			volume = row[1] if row[1] is not None else 0
			total_sales += peso_value
			total_volume += volume
	growth_peso = (((total_sales - prev_total_sales) / prev_total_sales) * 100) if prev_total_sales > 0 else None
	growth_volume = (((total_volume - prev_volume) / prev_volume) * 100) if prev_volume > 0 else None
	data.append({"supplier": discount_doc.supplier_discount_name + " (Based on POS Sales)" if supplier_discount is not None else supplier + " (Based on POS Sales)",
				 "period_covered": date_str, "peso_value": total_sales, "volume":total_volume})
	data.append({"period_covered": "% GROWTH", "growth_peso": growth_peso,"growth_volume":growth_volume})
	return data, prev_volume, total_volume, prev_total_sales, total_sales

def get_wholesale_sales(data, from_date, to_date, branch, supplier, supplier_discount):
	discount_doc = None if (supplier_discount is None) else frappe.get_doc("Supplier Discounts", supplier_discount)
	date_str = str(from_date) + " TO " + str(to_date)
	# print supplier, from_date, to_date
	items = frappe.db.sql(
		"""select itm.barcode_retial, itm.name from `tabItem` itm inner join `tabItem Supplier` sup on sup.parent = itm.name where sup.supplier = %s""",
		supplier)

	prev_from_date = datetime.datetime.strptime(from_date, "%Y-%m-%d") - relativedelta(years=1)
	prev_to_date = datetime.datetime.strptime(to_date, "%Y-%m-%d") - relativedelta(years=1)
	prev_date_str = str(prev_from_date.strftime("%Y-%m-%d")) + " TO " + str(prev_to_date.strftime("%Y-%m-%d"))
	# print "PREV DATE", from_date, to_date
	prev_wholesale_sale = 0.0
	prev_wholesale_volume = 0.0
	for item in items:
		if (supplier_discount != None) and (supplier_discount != ""):
			if get_item_discount(item[1], supplier_discount) is False:
				continue
		barcode_retial = item[0]
		prev_wholesale_sales = frappe.db.sql("""select sum(sed.amount), sum(sed.qty), sed.packing from `tabStock Entry` se inner join `tabStock Entry Detail` sed on se.name = sed.parent where type = 'Wholesale' and posting_date >= %s and posting_date <= %s and branch = %s and barcode_retial = %s  """,(str(prev_from_date), str(prev_to_date), branch, str(barcode_retial)))
		for row in prev_wholesale_sales:
			peso_value = row[0] if row[0] is not None else 0
			volume = row[1]*row[2] if row[1] is not None else 0
			prev_wholesale_sale += peso_value
			prev_wholesale_volume += volume
	data.append({"supplier": discount_doc.supplier_discount_name + " (Based on Wholesale Sales)" if supplier_discount is not None else supplier + " (Based on Wholesale Sales)",
				 "period_covered": prev_date_str, "peso_value":prev_wholesale_sale, "volume":prev_wholesale_volume})

	wholesale_sale = 0.0
	wholesale_volume = 0.0
	for item in items:
		if (supplier_discount != None) and (supplier_discount != ""):
				if get_item_discount(item[1], supplier_discount) is False:
					continue
		barcode_retial = item[0]
		wholesale_sales = frappe.db.sql(
			"""select sum(sed.amount), sum(sed.qty), sed.packing from `tabStock Entry` se inner join `tabStock Entry Detail` sed on se.name = sed.parent where type = 'Wholesale' and posting_date >= %s and posting_date <= %s and branch = %s and barcode_retial = %s  """,(from_date, to_date, branch, str(barcode_retial)))
		for row in wholesale_sales:
			peso_value = row[0] if row[0] is not None else 0
			volume = row[1]*row[2] if row[1] is not None else 0
			wholesale_sale += peso_value
			wholesale_volume += volume
	wholesale_growth_peso = (((wholesale_sale - prev_wholesale_sale) / prev_wholesale_sale) * 100) if prev_wholesale_sale > 0 else None
	wholesale_growth_volume = (((wholesale_volume - prev_wholesale_volume) / prev_wholesale_volume) * 100) if prev_wholesale_volume > 0 else None
	data.append({"supplier": discount_doc.supplier_discount_name + " (Based on Wholesale Sales)" if supplier_discount is not None else supplier + " (Based on Wholesale Sales)",
				 "period_covered": date_str, "peso_value": wholesale_sale, "volume": wholesale_volume})
	data.append({"period_covered": "% GROWTH", "growth_peso": wholesale_growth_peso, "growth_volume": wholesale_growth_volume})
	return data, prev_wholesale_volume, wholesale_volume, prev_wholesale_sale, wholesale_sale

def get_totals(data, from_date, to_date, branch, supplier, supplier_discount):
	discount_doc = None if (supplier_discount is None) else frappe.get_doc("Supplier Discounts", supplier_discount)
	date_str = str(from_date) + " TO " + str(to_date)
	prev_from_date = datetime.datetime.strptime(from_date, "%Y-%m-%d") - relativedelta(years=1)
	prev_to_date = datetime.datetime.strptime(to_date, "%Y-%m-%d") - relativedelta(years=1)
	prev_date_str = str(prev_from_date.strftime("%Y-%m-%d")) + " TO " + str(prev_to_date.strftime("%Y-%m-%d"))
	data = []
	blank, prev_pos_volume, next_pos_volume, prev_pos_sale, next_pos_sale = get_sales_single(data, from_date, to_date, branch, supplier,supplier_discount)
	blank, prev_wholesale_volume, next_wholesale_volume, prev_wholesale_sale, next_wholesale_sale = get_wholesale_sales(data, from_date, to_date, branch, supplier,supplier_discount)

	prev_pos_wholesale_total_sale = prev_pos_sale + prev_wholesale_sale
	next_pos_wholesale_total_sale = next_pos_sale + next_wholesale_sale

	prev_pos_wholesale_total_volume =  prev_pos_volume + prev_wholesale_volume
	next_pos_wholesale_total_volume = next_pos_volume + next_wholesale_volume

	growth_peso = (((next_pos_wholesale_total_sale - prev_pos_wholesale_total_sale) / prev_pos_wholesale_total_sale) * 100) if prev_wholesale_sale > 0 else None
	growth_volume = (((next_pos_wholesale_total_volume - prev_pos_wholesale_total_volume) / prev_pos_wholesale_total_volume) * 100) if prev_pos_wholesale_total_volume > 0 else None

	data.append({"supplier": discount_doc.supplier_discount_name + " (Wholesale Sales and POS Sales)" if supplier_discount is not None else supplier + " (Wholesale Sales and POS Sales)",
				 "period_covered": prev_date_str, "peso_value": prev_pos_wholesale_total_sale, "volume": prev_pos_wholesale_total_volume})

	data.append({"supplier": discount_doc.supplier_discount_name + " (Wholesale Sales and POS Sales)" if supplier_discount is not None else supplier + " (Wholesale Sales and POS Sales)",
				 "period_covered": date_str, "peso_value": next_pos_wholesale_total_sale, "volume": next_pos_wholesale_total_volume})

	data.append({"period_covered": "% GROWTH", "growth_peso": growth_peso, "growth_volume": growth_volume})

	return data

def break_down_sales(data, from_date, to_date,  branch, supplier, supplier_discount):
	discount_doc = None if (supplier_discount is None) else frappe.get_doc("Supplier Discounts", supplier_discount)
	date_str = str(from_date) + " TO " + str(to_date)
	print supplier, from_date, to_date

	items = frappe.db.sql(
		"""select itm.barcode_retial, itm.name from `tabItem` itm inner join `tabItem Supplier` sup on sup.parent = itm.name where sup.supplier = %s""",
		supplier)

	prev_from_date = datetime.datetime.strptime(from_date, "%Y-%m-%d") - relativedelta(years=1)
	prev_to_date = datetime.datetime.strptime(to_date, "%Y-%m-%d") - relativedelta(years=1)
	prev_date_str = str(prev_from_date.strftime("%Y-%m-%d")) + " TO " + str(prev_to_date.strftime("%Y-%m-%d"))
	print "PREV DATE", from_date, to_date
	prev_total_sales = 0.0
	prev_volume = 0.0
	for item in items:
		if (supplier_discount != None) and (supplier_discount != ""):
			if get_item_discount(item[1], supplier_discount) is False:
				continue
		barcode_retial = item[0]
		idv_sales = frappe.db.sql("""select sum(amount), sum(qty) from `tabUpload POS` where branch LIKE %s and trans_date >= %s and
    		                          trans_date <= %s AND barcode = %s""",
								  ('%' + branch + '%', str(prev_from_date), str(prev_to_date), str(barcode_retial)))
		for row in idv_sales:
			# print peso_value[0]
			peso_value = row[0] if row[0] is not None else 0
			volume = row[1]if row[1] is not None else 0
			prev_total_sales += peso_value
			prev_volume += volume
	data.append({"supplier":discount_doc.supplier_discount_name + " (Based on POS Sales)" if supplier_discount is not None else supplier + " (Based on POS Sales)",
				 "period_covered": prev_date_str, "peso_value": prev_total_sales, "volume":prev_volume})

	total_sales = 0.0
	total_volume = 0.0
	for item in items:
		if (supplier_discount != None) and (supplier_discount != ""):
			if get_item_discount(item[1], supplier_discount) is False:
				continue
		barcode_retial = item[0]
		idv_sales = frappe.db.sql("""select sum(amount), sum(qty) from `tabUpload POS` where branch LIKE %s and trans_date >= %s and
    		                          trans_date <= %s AND barcode = %s""",
								  ('%' + branch + '%', str(from_date), str(to_date), str(barcode_retial)))
		for row in idv_sales:
			peso_value = row[0] if row[0] is not None else 0
			volume = row[1] if row[1] is not None else 0
			total_sales += peso_value
			total_volume += volume
	growth_peso = (((total_sales - prev_total_sales) / prev_total_sales) * 100) if prev_total_sales > 0 else None
	growth_volume = (((total_volume - prev_volume) / prev_volume) * 100) if prev_volume > 0 else None
	data.append({"supplier": discount_doc.supplier_discount_name + " (Based on POS Sales)" if supplier_discount is not None else supplier + " (Based on POS Sales)",
				 "period_covered": date_str, "peso_value": total_sales, "volume":total_volume})
	data.append({"period_covered": "% GROWTH", "growth_peso": growth_peso,"growth_volume":growth_volume})
	return data

def break_down_wholesale(data, from_date, to_date, branch, supplier, supplier_discount):
	discount_doc = None if (supplier_discount is None) else frappe.get_doc("Supplier Discounts", supplier_discount)
	date_str = str(from_date) + " TO " + str(to_date)
	print supplier, from_date, to_date
	items = frappe.db.sql(
		"""select itm.barcode_retial, itm.name from `tabItem` itm inner join `tabItem Supplier` sup on sup.parent = itm.name where sup.supplier = %s""",
		supplier)

	prev_from_date = datetime.datetime.strptime(from_date, "%Y-%m-%d") - relativedelta(years=1)
	prev_to_date = datetime.datetime.strptime(to_date, "%Y-%m-%d") - relativedelta(years=1)
	prev_date_str = str(prev_from_date.strftime("%Y-%m-%d")) + " TO " + str(prev_to_date.strftime("%Y-%m-%d"))
	print "PREV DATE", from_date, to_date
	prev_wholesale_sale = 0.0
	prev_wholesale_volume = 0.0
	for item in items:
		if (supplier_discount != None) and (supplier_discount != ""):
			if get_item_discount(item[1], supplier_discount) is False:
				continue
		barcode_retial = item[0]
		prev_wholesale_sales = frappe.db.sql("""select sum(sed.amount), sum(sed.qty), sed.packing from `tabStock Entry` se inner join `tabStock Entry Detail` sed on se.name = sed.parent where type = 'Wholesale' and posting_date >= %s and posting_date < %s and branch = %s and barcode_retial = %s  """,(str(prev_from_date), str(prev_to_date), branch, str(barcode_retial)))
		for row in prev_wholesale_sales:
			peso_value = row[0] if row[0] is not None else 0
			volume = row[1]*row[2] if row[1] is not None else 0
			prev_wholesale_sale += peso_value
			prev_wholesale_volume += volume
			print prev_wholesale_sale + prev_wholesale_volume
	data.append({"supplier": discount_doc.supplier_discount_name + " (Based on Wholesale Sales)" if supplier_discount is not None else supplier + " (Based on Wholesale Sales)",
				 "period_covered": prev_date_str, "peso_value":prev_wholesale_sale, "volume":prev_wholesale_volume})

	wholesale_sale = 0.0
	wholesale_volume = 0.0
	for item in items:
		if (supplier_discount != None) and (supplier_discount != ""):
				if get_item_discount(item[1], supplier_discount) is False:
					continue
		barcode_retial = item[0]
		wholesale_sales = frappe.db.sql(
			"""select sum(sed.amount), sum(sed.qty), sed.packing from `tabStock Entry` se inner join `tabStock Entry Detail` sed on se.name = sed.parent where type = 'Wholesale' and posting_date >= %s and posting_date < %s and branch = %s and barcode_retial = %s  """,(from_date, to_date, branch, str(barcode_retial)))
		for row in wholesale_sales:
			peso_value = row[0] if row[0] is not None else 0
			volume = row[1]*row[2] if row[1] is not None else 0
			wholesale_sale += peso_value
			wholesale_volume += volume
	wholesale_growth_peso = (((wholesale_sale - prev_wholesale_sale) / prev_wholesale_sale) * 100) if prev_wholesale_sale > 0 else None
	wholesale_growth_volume = (((wholesale_volume - prev_wholesale_volume) / prev_wholesale_volume) * 100) if prev_wholesale_volume > 0 else None
	data.append({"supplier": discount_doc.supplier_discount_name + " (Based on Wholesale Sales)" if supplier_discount is not None else supplier + " (Based on Wholesale Sales)",
				 "period_covered": date_str, "peso_value": wholesale_sale, "volume": wholesale_volume})
	data.append({"period_covered": "% GROWTH", "growth_peso": wholesale_growth_peso, "growth_volume": wholesale_growth_volume})
	return data

