# Copyright (c) 2013, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
import datetime, calendar

def execute(filters=None):
    columns = [
        {"label": "PO #", 'width': 150, "fieldname": "po_name"},
        {"label": "Branch", 'width': 150, "fieldname": "branch"},
        {"label": "PO Date", 'width': 100, "fieldname": "po_date"},
        {"label": "Expected Delivery", 'width': 100, "fieldname": "expected_delivery"},
        {"label": "Status", 'width': 100, "fieldname": "status"},
        {"label": "Delivered?", 'width': 100, "fieldname": "stat"},
        {"label": "On Schedule?", 'width': 100, "fieldname": "on_schedule"}
    ]
    data = []
    from_date = filters.get("from_date")
    to_date = filters.get("to_date")
    supplier = filters.get("Supplier")
    branch = filters.get("Branch")

    po = frappe.db.sql("""Select supplier from `tabPurchase Order` where
                              transaction_date >= %s and transaction_date <= %s and supplier = %s and branch = %s """,
                       (from_date, to_date, supplier, branch))
    for s in po:
        supplier = str(s[0])

        break

    doc = frappe.db.sql("""SELECT `tabPurchase Order`.name, `tabPurchase Order`.branch, `tabPurchase Order`.transaction_date, `tabPurchase Order`.expected_delivery, CASE WHEN `tabPurchase Order`.docstatus = '1' THEN 'Submitted' WHEN `tabPurchase Order`.docstatus = '0' THEN 'Draft' WHEN `tabPurchase Order`.docstatus = '2' THEN 'Cancelled' ELSE 'cancelled' END AS awwww, if(`tabPurchase Order`.name = `tabPurchase Receipt`.purchase_order, 'Yes', 'No') AS whip FROM `tabPurchase Order` LEFT JOIN `tabPurchase Receipt` ON `tabPurchase Order`.name = `tabPurchase Receipt`.purchase_order WHERE `tabPurchase Order`.branch = %s and `tabPurchase Order`.supplier = %s and transaction_date >= %s and transaction_date <= %s """, (branch, supplier, from_date, to_date))


    for d in doc:
        po_name = str(d[0]).center(50)
        branch = str(d[1])
        po_date = str(d[2])
        expected_delivery = str(d[3])
        status = str(d[4])
        stat = str(d[5])

        on_schedule = get_po_sched(po_date,d[0])
        data.append({"po_name": po_name.center(200), "branch": branch.center(90), "po_date": po_date.center(90), "expected_delivery": expected_delivery.center(90), "status": status.center(300), "stat": stat.center(90), "on_schedule":on_schedule})
    return columns, data

def get_po_sched(transaction_date, po):

    weekdays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
    po_doc = frappe.get_doc("Purchase Order",po)
    supplier = po_doc.supplier_discount
    po_date = datetime.datetime.strptime(transaction_date, "%Y-%m-%d")

    booking_status = ""

    po_details = frappe.db.sql("""select po_schedule from `tabSupplier Discounts` where name = %s""", supplier)#Change to Supplier Discount
    po_schedule = po_details[0][0]

    # date_diff = po_date.weekday() - weekdays.index(po_schedule)
    # print po_doc.name, po_date.weekday(), weekdays[po_date.weekday()], weekdays.index(
    #     po_schedule), po_schedule, date_diff
    # if date_diff == 0:
    #     booking_status = "On Schedule"
    # elif date_diff < 0:
    #     booking_status = "Advanced Booking"
    # else:
    #     booking_status = "Late Booking"

    if calendar.day_name[po_date.weekday()] != po_schedule:
        return "No"
    else:
        return "Yes"

    return booking_status

