# Copyright (c) 2013, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe, datetime
from dateutil.relativedelta import relativedelta
from gaisano.popupevents import get_report_password

def execute(filters=None):
	columns, data = [], []
	from_date = filters.get("from_date")
	to_date = filters.get("to_date")
	branch = filters.get("branch")
	password = filters.get("report_password")
	show_peso_value = filters.get("show_peso_value")



	if show_peso_value ==1:
		#print "***********GET REPORT PASSSWOOOOORDD*************"
		#print get_report_password(password)
		if not get_report_password(password):
			report_msg = "Please Enter the Report Password" if password is None else "WRONG REPORT PASSWORD ENTERED."
			frappe.throw(report_msg)

	data = get_data(from_date, to_date, branch)
	columns = get_columns(show_peso_value)
	return columns, data


def get_columns(show_peso_value):
	if show_peso_value == 1:
		columns = [
			{"label": "Supplier", "fieldname": "supplier", "fieldtype": "Data", "width": 150},
			{"label": "Supplier Market Share", "fieldname": "market_share", "fieldtype": "Percent", "precision": 2, "width": 150},
			{"label": "Sales Peso Value", "fieldname": "peso_value", "fieldtype": "Currency", "precision": 2, "width": 150},
			{"label": "Last Year Sales", "fieldname": "prev_peso_value", "fieldtype": "Currency", "precision": 2, "width": 150},
			{"label": "Growth(vs Last Year Sales)", "fieldname": "growth_peso", "fieldtype": "Percent", "precision":2,"width": 150}
		]

	else:
		columns = [
			{"label": "Supplier", "fieldname": "supplier", "fieldtype": "Data", "width": 150},
			{"label": "Supplier Market Share", "fieldname": "market_share", "fieldtype": "Percent", "precision": 2,
			 "width": 150},
			{"label": "Growth(vs Last Year Sales)", "fieldname": "growth_peso", "fieldtype": "Percent", "precision": 2,
			 "width": 150}
		]
	return columns

def get_data(from_date, to_date, branch):
	data = []
	current_supplier = None
	supplier_total = 0
	previous_total = 0
	supplier_marketshare = 0
	supplier_marketshare_total = 0
	item_previous_total_sales = 0

	grand_total = get_total_sales(from_date, to_date, branch)
	prev_from_date = datetime.datetime.strptime(from_date, "%Y-%m-%d") - relativedelta(years=1)
	prev_to_date = datetime.datetime.strptime(to_date, "%Y-%m-%d") - relativedelta(years=1)
	# print prev_from_date, prev_to_date


	items = frappe.db.sql("""SELECT itm.barcode_retial, sup.supplier from `tabItem` itm
							inner join `tabItem Supplier` sup on sup.parent = itm.name where sup.supplier != "" order by sup.supplier""")
	print(items)
	for row in items:
		item_supplier = row[1]
		item_barcode = row[0]
		wholesale_sales = get_wholesale_sales(from_date, to_date, item_barcode, branch)
		prev_wholesale_sales = get_wholesale_sales(prev_from_date, prev_to_date, item_barcode, branch) 
		item_sales = get_item_sales(from_date, to_date, item_barcode, branch)
		item_previous_sales = get_item_sales(prev_from_date, prev_to_date, item_barcode, branch)
		item_previous_total_sales += get_item_sales(prev_from_date, prev_to_date, item_barcode, branch)

		if current_supplier != item_supplier:
			growth = ((supplier_total - previous_total) / previous_total)*100 if previous_total > 0 else 100
			supplier_marketshare = (supplier_total/grand_total) * 100
			supplier_marketshare_total += (supplier_total/grand_total) * 100
			data.append({ "supplier": current_supplier, "market_share":supplier_marketshare,"peso_value": supplier_total, "prev_peso_value": previous_total, "growth_peso":growth})
			supplier_total = item_sales
			current_supplier = item_supplier
			previous_total = item_previous_sales
		else:
			supplier_total += item_sales + wholesale_sales
			previous_total += item_previous_sales + prev_wholesale_sales
	growth = ((supplier_total - previous_total) / previous_total)*100 if previous_total > 0 else 100
	total_growth = ((grand_total - item_previous_total_sales) / item_previous_total_sales)* 100 if item_previous_sales > 0 else 100
	supplier_marketshare = (supplier_total / grand_total) * 100
	data.append({"supplier": current_supplier,  "market_share":supplier_marketshare, "peso_value": supplier_total, "prev_peso_value": previous_total, "growth_peso":growth})
	data = sorted(data, key=lambda k: k['market_share'], reverse=True)
	data.append({"supplier": "TOTAL", "market_share": supplier_marketshare_total, "peso_value": grand_total, "prev_peso_value": item_previous_total_sales, "growth_peso": total_growth })
	return data


def get_item_sales(from_date, to_date, barcode, branch):
	sales = frappe.db.sql("""SELECT sum(amount) from `tabUpload POS` where barcode = %s and branch = %s and
							trans_date >=%s and trans_date <=%s""",(barcode, branch, from_date, to_date))
	return sales[0][0] if sales[0][0] is not None else 0

def get_total_sales(from_date, to_date, branch):
	#print "!!!!!!!!!!!!1get_total_sales!!!!!!!!!!!!!!!!!"
	total=0
	items = frappe.db.sql("""SELECT itm.barcode_retial, sup.supplier from `tabItem` itm
							inner join `tabItem Supplier` sup on sup.parent = itm.name where sup.supplier != "" order by sup.supplier""")
	for item in items:
		total += get_item_sales(from_date, to_date, item[0], branch)
		# print total
	print "FINAL TOTAL:", total
	return total

def get_wholesale_sales(from_date, to_date, barcode, branch):
	sales = frappe.db.sql("""SELECT SUM(sed.amount) FROM `tabStock Entry` se INNER JOIN `tabStock Entry Detail` sed ON se.name = sed.parent WHERE se.type = 'Wholesale' AND barcode_retial =%s AND branch=%s AND  posting_date >= %s AND posting_date <= %s""",(barcode, branch, from_date, to_date))
	return sales[0][0] if sales[0][0] is not None else 0