// Copyright (c) 2016, Bai Mobile and Web Labs and contributors
// For license information, please see license.txt

frappe.query_reports["Supplier Ranking"] = {
	"filters": [
		{
			"fieldname":"from_date",
			"label": __("From Date"),
			"fieldtype": "Date",
			"reqd": 1
		},
		{
			"fieldname":"to_date",
			"label": __("To Date"),
			"fieldtype": "Date",
			"reqd": 1
		},
		{
			"fieldname":"branch",
			"label": __("Branch"),
			"fieldtype": "Link",
			"options":"Branch",
			"reqd": 1
		},
		{
			"fieldname" : "report_password",
			"fieldtype" : "Password",
			"label" : "Password for Show Peso Value",
			"reqd" : 0
		},
		{
			"fieldname":"show_peso_value",
			"label": __("Show Peso Value?"),
			"fieldtype": "Check"
		}

	]
};
