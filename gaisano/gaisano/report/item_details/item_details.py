# Copyright (c) 2013, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from gaisano.tasks import item_in_old, get_list


def execute(filters=None):
	columns = get_columns(filters)
	data = []

	if filters.get("report_type") == "Cost, SRP, Margin, Price":
		data = get_item_cost_srp_margin(filters)
	elif filters.get("report_type") == "Old System Cost vs New System Cost":
		data = get_cost_comparison(filters)
	elif filters.get("report_type") == "Old System SRP vs New System SRP":
		data = compare_srp(filters)
	elif filters.get("report_type") == "Check if Old Barcodes in New System":
		data = check_old_barcodes()
	elif filters.get("report_type") == "Item SRP vs Calculated SRP":
		data = get_item_SRP_vs_calculated_SRP(filters)
	elif filters.get("report_type") == "No SKUs in Category":
		data = get_sku_count(filters)
	elif filters.get("report_type") == "Check Items for Manual Update":
		data = get_items_for_manual_update(filters)
	elif filters.get("report_type") == "Check Old System vs New System Department Code":
		data = get_items_for_depcode_report()


	return columns, data


def get_columns(filters):
	if filters.get("report_type") == "Cost, SRP, Margin, Price":
		columns = [
			{"label": "Item Code", 'width': 230, "fieldname": "item_code"},
			{"label": "Packing", 'width': 50, "fieldname": "packing"},
			{"label": "Unit", 'width': 50, "fieldname": "uom"},
			{"label": "Barcode", 'width': 120, "fieldname": "barcode"},
			{"label": "Cost", 'width': 90, "fieldname": "item_cost", "fieldtype": "Currency"},
			{"label": "Cost with Freight", 'width': 90, "fieldname": "item_cost_with_freight", "fieldtype": "Currency"},
			{"label": "SRP (Wholesale)", 'width': 90, "fieldname": "wholesale_srp", "fieldtype": "Currency"},
			{"label": "Margin (Wholesale)", 'width': 90, "fieldname": "wholesale_margin", "fieldtype": "Percent"},
			{"label": "Price (Retail)", 'width': 90, "fieldname": "retail_srp", "fieldtype": "Currency"},
			{"label": "Margin (Retail)", 'width': 90, "fieldname": "retail_margin", "fieldtype": "Percent"},
			{"label": "Markup(wholesale)",'width': 90, "fieldname": "wholesale_markup", "fieldtype": "Percent"},
			{"label": "Markup(retail)", 'width': 90, "fieldname": "retail_markup", "fieldtype": "Percent"},
			{"label": "ACTUAL WHOLESALE MARKUP", 'width': 90, "fieldname": "actual_ws_margin", "fieldtype": "Percent"},
			{"label": "ACTUAL RETAIL MARKUP", 'width': 90, "fieldname": "actual_ret_margin", "fieldtype": "Percent"}
		]
	elif (filters.get("report_type") == "Old System Cost vs New System Cost"):
		columns = [
			{"label": "Barcode", 'width': 230, "fieldname": "barcode"},
			{"label": "Item Code", 'width': 230, "fieldname": "item_code"},
			{"label": "Packing Old", 'width': 50, "fieldname": "packing_old"},
			{"label": "Packing New", 'width': 50, "fieldname": "packing_new"},
			{"label": "Cost in Old System", 'width': 90, "fieldname": "item_cost_old", "fieldtype": "Currency"},
			{"label": "Cost in New System", 'width': 90, "fieldname": "item_cost_new", "fieldtype": "Currency"},
			{"label": "Status", 'width': 100, "fieldname": "status"}
		]
	elif (filters.get("report_type") == "Old System SRP vs New System SRP") :
		columns = [
			{"label": "Barcode", 'width': 230, "fieldname": "barcode"},
			{"label": "Item Code", 'width': 230, "fieldname": "item_code"},
			{"label": "Packing Old", 'width': 50, "fieldname": "packing_old"},
			{"label": "Packing New", 'width': 50, "fieldname": "packing_new"},
			{"label": "SRP Wholesale (Old System)", 'width': 90, "fieldname": "price_w_old", "fieldtype": "Currency"},
			{"label": "SRP Wholesale (New System)", 'width': 90, "fieldname": "price_w_new", "fieldtype": "Currency"},
			{"label": "SRP Retail (Old System)", 'width': 90, "fieldname": "price_r_old", "fieldtype": "Currency"},
			{"label": "SRP Retail (New System)", 'width': 90, "fieldname": "price_r_new", "fieldtype": "Currency"},
			{"label": "Status", 'width': 100, "fieldname": "status"}
		]
	elif (filters.get("report_type")=="Check if Old Barcodes in New System"):
		columns = [
			{"label": "Item Code", 'width': 230, "fieldname": "item_code"},
			{"label": "Barcode in Old System", 'width': 110, "fieldname": "barcode_old"},
			{"label": "Supplier in Old System", 'width': 200, "fieldname": "supplier"},
			{"label": "Status", 'width': 150, "fieldname": "status"}
		]
	elif (filters.get("report_type")=="Item SRP vs Calculated SRP"):
		columns = [
			{"label": "Barcode", 'width': 150, "fieldname": "barcode"},
			{"label": "Item Cost", 'width':100, "fieldname": "item_cost", "fieldtype": "Currency"},
			{"label": "Margin Rate", 'width':100, "fieldname":"margin_rate", "fieldtype": "Percent"},
			{"label": "Retial Margin Rate", 'width':150, "fieldname":"retail_margin_rate", "fieldtype":"Percent"},
			{"label": "Rounded Calculated Item Price", 'width': 150, "fieldname": "calculated_item_price", "fieldtype": "Currency"},
			{"label": "Item Price With Margin", 'width':150, "fieldname":"item_price_retail_with_margin" , "fieldtype":"Currency"},
			{"label": "Calculated Retail Price", 'width':150, "fieldname":"calculated_retail_price", "fieldtype":"Currency"}

		]
	elif (filters.get("report_type")=="Check Items for Manual Update"):
		columns = [
			{"label": "Item description", 'width': 300, 'fieldname': 'item_description'},
			{"label": "Barcode", 'width': 200, 'fieldname': 'item_barcode'},
			{"label": "Date Updated", "fieldname": 'date_updated'}
		]
	elif filters.get("report_type") == "Check Old System vs New System Department Code":
		columns=[
			{"label": "Item description", 'width': 300, 'fieldname': 'item_desc'},
			{"label": "Barcode", 'width': 200, 'fieldname': 'barcode'},
			{"label": "Status", 'width': 200, "fieldname": 'status'},
			{"label": "Depcode in Old", 'width': 100, 'fieldname': 'depcode_in_old'},
			{"label": "Depcode in New", 'width': 100, 'fieldname': 'depcode_in_new'},
		]
		# ({'barcode': item_doc.barcode_retial, 'item_desc': item_doc.item_name_dummy,
		#   'status': status, 'depcode_in_new': str(item_doc.dep_code),
		#   'depcode_in_old': table[i].dep_code})
	else:
		columns = [
			{"label": "Category/Classification", 'width': 230, "fieldname": "category"},
			{"label": "No SKU's", 'width': 110, "fieldname": "no_skus", "fieldtype":"Int"},
		]
	return columns

def get_items_for_manual_update(filters):
	data = []
	items = frappe.db.sql("""SELECT sync.date_updated, sync.description, sync.barcode FROM `tabItems Not Syncing` sync INNER JOIN `tabItem` itm ON sync.barcode = itm.barcode_retial WHERE sync_error = 'Price Decrease/For Manual Updating/Rebates' and itm.type != 'Disabled'""", as_dict=True)
	for item in items:
		data.append({'item_description': item['description'], 'item_barcode':item['barcode'], 'date_updated':item['date_updated']})
	return data

def get_item_cost_srp_margin(filters):
	data = []
	item_details = get_item_details(filters)
	branch = filters.get('branch')
	for detail in item_details:
		disc1, disc2, disc3, disc4 = get_supplier_discounts(detail[0],branch)
		item_doc = frappe.get_doc("Item", detail[0])

		cost = float(detail[4])
		cost_piece = float(detail[4] / detail[1])
		net_cost = ((((float(cost) * (1 - disc1)) * (1 - disc2)) * (1 - disc3)) * (1 - disc4))
		net_cost_ret = ((((float(cost_piece) * (1 - disc1)) * (1 - disc2)) * (1 - disc3)) * (1 - disc4))

		if detail[7] == 0:
			ws_margin = -100
			actual_ws_margin = -100
		else:
			ws_margin = float((detail[7] - cost) / detail[7]) * 100
			actual_ws_margin = 0 if (net_cost == 0) else (float(detail[7]/ net_cost) - 1) * 100
		if detail[9] == 0:
			ret_margin = -100
			actual_ret_margin = -100
		else:
			ret_margin = float((detail[9] - cost_piece) / detail[9]) * 100
			actual_ret_margin = 0 if net_cost_ret == 0 else (float(detail[9] / net_cost_ret)-1)* 100
		print item_doc.barcode_retial, ws_margin, ret_margin
		data.append(
			{"item_code": item_doc.item_name_dummy, "packing": detail[1], "uom": detail[2], "barcode": detail[3],
			 "item_cost": detail[4], "item_cost_with_freight": detail[5], "wholesale_markup": detail[6],
			 "wholesale_srp": detail[7], "retail_markup": detail[8], "retail_srp": detail[9],
			 "wholesale_margin": ws_margin, "retail_margin": ret_margin,
			 "actual_ret_margin":actual_ret_margin, "actual_ws_margin": actual_ws_margin})
	return data
def get_item_details(filters):
	items = []
	supplier = filters.get("supplier") if (filters.get("supplier") and (filters.get("supplier")!="")) else None
	supplier_discount = filters.get("supplier_discount") if (filters.get("supplier_discount") and (filters.get("supplier_discount") != "")) else None
	if (supplier != None) and (supplier_discount == None):
		items = frappe.db.sql("""SELECT itm.item_code, itm.packing, itm.uom, itm.barcode_retial, itm.item_cost, itm.item_cost_with_freight,
				itm.margin_rate, itm.item_price, itm.retail_margin_rate, itm.item_price_retail_with_margin from
				`tabItem` itm left join `tabItem Supplier` sup on sup.parent = itm.name where sup.supplier = %s and itm.type!='Disabled'""", supplier)
	elif (supplier == None) and (supplier_discount != None):
		items = frappe.db.sql("""SELECT itm.item_code, itm.packing, itm.uom, itm.barcode_retial, itm.item_cost, itm.item_cost_with_freight,
				itm.margin_rate, itm.item_price, itm.retail_margin_rate, itm.item_price_retail_with_margin from
				`tabItem` itm left join `tabSupplier Discount Items` disc on disc.items = itm.name where disc.parent = %s and itm.type!='Disabled'""", supplier_discount)
	elif (supplier != None) and (supplier_discount != None):
		items = frappe.db.sql("""SELECT itm.item_code, itm.packing, itm.uom, itm.barcode_retial, itm.item_cost, itm.item_cost_with_freight,
				itm.margin_rate, itm.item_price, itm.retail_margin_rate, itm.item_price_retail_with_margin from
				`tabItem` itm left join `tabItem Supplier` sup on sup.parent = itm.name
				inner join `tabSupplier Discount Items` disc on disc.items = itm.name where sup.supplier = %s and disc.parent = %s and itm.type!='Disabled'""", (supplier, supplier_discount))
	else:
		items = frappe.db.sql("""SELECT itm.item_code, itm.packing, itm.uom, itm.barcode_retial, itm.item_cost, itm.item_cost_with_freight,
	    				itm.margin_rate, itm.item_price, itm.retail_margin_rate, itm.item_price_retail_with_margin from
	    				`tabItem` itm left join `tabItem Supplier` sup on sup.parent = itm.name
	    				inner join `tabSupplier Discount Items` disc on disc.items = itm.name and itm.type!='Disabled'""")
	return items

def get_cost_comparison(filters):
	list = get_list()
	supplier = filters.get("supplier") if (filters.get("supplier") and (filters.get("supplier") != "")) else None
	supplier_discount = filters.get("supplier_discount") if (
	filters.get("supplier_discount") and (filters.get("supplier_discount") != "")) else None

	data = []

	if (supplier != None) and (supplier_discount == None):
		print "===============>supplier only"
		items = frappe.db.sql("""SELECT itm.barcode_retial, itm.item_code, itm.item_cost_with_freight, itm.item_name_dummy, itm.packing from `tabItem` itm left join `tabItem Supplier` sup on sup.parent = itm.name where sup.supplier = %s and itm.type!='Disabled'""", supplier)
	elif (supplier == None) and (supplier_discount != None):
		print "===============>discount only"
		items = frappe.db.sql("""SELECT itm.barcode_retial, itm.item_code, itm.item_cost_with_freight, itm.item_name_dummy, itm.packing from `tabItem` itm left join `tabSupplier Discount Items` disc on disc.items = itm.name where disc.parent = %s and itm.type!='Disabled'""", supplier_discount)
	elif (supplier != None) and (supplier_discount != None):
		print "===============>discount + supplier"
		items = frappe.db.sql("""SELECT itm.barcode_retial, itm.item_code, itm.item_cost_with_freight, itm.item_name_dummy, itm.packing from `tabItem` itm left join `tabItem Supplier` sup on sup.parent = itm.name inner join `tabSupplier Discount Items` disc on disc.items = itm.name where sup.supplier = %s and disc.parent = %s and itm.type!='Disabled'""", (supplier, supplier_discount))
	else:
		print "===============>all items"
		items = frappe.db.sql("""select barcode_retial, item_code, item_cost_with_freight, item_name_dummy, packing from `tabItem` where type != 'Disabled'""")

	for i,item in enumerate(items):
		#try:
		# print "-----------ITEM IN NEW------------"
		# print item[0], item[1], item[2]
		old_item = item_in_old(list, item[0])
		if old_item:
			# print "OLD ITEM packing vs new packing:", old_item['packing'], item[4], item[4] == old_item['packing']
			# print "--------------OLD COST-----------------"
			# print old_item['cost_W']
			if (round(float(item[2]), 2) != round(float(old_item['cost_w']), 2)) or (item[4] != old_item['packing']):
				print "OLD ITEM packing vs new packing:", old_item['packing'], item[4], item[4] == old_item[
					'packing']
				status = "Cost Does Not Match"
				if (round(float(item[2]), 2) != round(float(old_item['cost_w']), 2)) and (
					item[4] != old_item['packing']):
					status = "Cost and Packing do Not Match"
				elif (round(float(item[2]), 2) == round(float(old_item['cost_w']), 2)) and (
					item[4] != old_item['packing']):
					status = "Packing Does Not Match"
				else:
					status = "Cost Does Not Match"
				data.append({"barcode": item[0],
							 "item_code": item[3],
							 "packing_new": item[4],
							 "packing_old": old_item['packing'],
							 "item_cost_old": old_item['cost_w'],
							 "item_cost_new": item[2],
							 "status": status
							 })
		#except:
		#	print "ERROR on "+item[0]
		#	continue

	return data

def compare_srp(filters):
	list = get_list()
	supplier = filters.get("supplier") if (filters.get("supplier") and (filters.get("supplier") != "")) else None
	supplier_discount = filters.get("supplier_discount") if (
	filters.get("supplier_discount") and (filters.get("supplier_discount") != "")) else None

	data = []

	if (supplier != None) and (supplier_discount == None):
		print "===============>supplier only"
		items = frappe.db.sql("""SELECT itm.barcode_retial, itm.item_code, itm.item_price, itm.item_price_retail_with_margin, itm.item_name_dummy, itm.packing from `tabItem` itm left join `tabItem Supplier` sup on sup.parent = itm.name where sup.supplier = %s and itm.type !='Disabled'""", supplier)
	elif (supplier == None) and (supplier_discount != None):
		print "===============>discount only"
		items = frappe.db.sql("""SELECT itm.barcode_retial, itm.item_code, itm.item_price, itm.item_price_retail_with_margin,itm.item_name_dummy,itm.packing from `tabItem` itm left join `tabSupplier Discount Items` disc on disc.items = itm.name where disc.parent = %s and itm.type !='Disabled'""", supplier_discount)
	elif (supplier != None) and (supplier_discount != None):
		print "===============>discount + supplier"
		items = frappe.db.sql("""SELECT itm.barcode_retial, itm.item_code, itm.item_price, itm.item_price_retail_with_margin,itm.item_name_dummy, itm.packing from `tabItem` itm left join `tabItem Supplier` sup on sup.parent = itm.name inner join `tabSupplier Discount Items` disc on disc.items = itm.name where sup.supplier = %s and disc.parent = %s and itm.type !='Disabled'""", (supplier, supplier_discount))
	else:
		print "===============>all items"
		items = frappe.db.sql("""select barcode_retial, item_code, item_price, item_price_retail_with_margin, item_name_dummy, packing from `tabItem` where type != 'Disabled'""")

	for item in items:

		old_cost = get_srp_from_old(list, item[0])
		if old_cost:
			status = ""
			if (round(float(item[2]), 2) != round(float(old_cost[0]["price_w"]), 2)) or (
				round(float(item[3]), 2) != round(float(old_cost[0]["price_r"]), 2)) or (
				item[5]!=old_cost[0]['packing']):
				print "Does not match!", item[0]
				if (round(float(item[2]), 2) != round(float(old_cost[0]["price_w"]), 2)) and (
					round(float(item[3]), 2) != round(float(old_cost[0]["price_r"]), 2)):
					if old_cost[0]["price_r"] == 0:
						status = "Wholesale doesn't match."
					else:
						status = "Wholesale & Retail don't match."
				elif (round(float(item[2]), 2) != round(float(old_cost[0]["price_w"]), 2)) and (
					round(float(item[3]), 2) == round(float(old_cost[0]["price_r"]), 2)):
					status = "Wholesale doesn't match."
				elif (round(float(item[2]), 2) == round(float(old_cost[0]["price_w"]), 2)) and (
					round(float(item[3]), 2) != round(float(old_cost[0]["price_r"]), 2)):
					if old_cost[0]["price_r"] == 0:
						continue
					else:
						status = "Retail doesn't match"
				if old_cost[0]['packing']!= item[5]:
					status += " | Packing doesn't Match"
				data.append({"barcode": item[0],
							 "item_code": item[4],
							 "packing_old": old_cost[0]['packing'],
							 "packing_new": item[5],
							 "price_w_old": round(float(old_cost[0]["price_w"]), 2),
							 "price_w_new": round(float(item[2]), 2),
							 "price_r_old": round(float(old_cost[0]["price_r"]), 2),
							 "price_r_new": round(float(item[3]), 2),
							 "status": status
							 })

	return data

def get_sku_count(filters):
	data = []
	#supplier = filters.get("supplier")
	#supplier_discount = filters.get("supplier_discount")

	classifications = frappe.db.sql("""select c.name, c.category, count(item.parent) from `tabItem Classification` c
		  left join `tabItem Class Each` item on item.classification = c.name GROUP BY c.name""")
	for classification in classifications:
		if classification[1] != None:
			continue
		else:
			print classification[0], classification[2]
			data.append({"category":classification[0], "no_skus":classification[2]})
	categories = frappe.db.sql("""select c.name, count(item.parent) from `tabItem Category` c
			  left join `tabItem Class Each` item on item.category = c.name GROUP BY c.name""")
	for category in categories:
		print category[0], category[1]
		data.append({"category":category[0], "no_skus":category[1]})
	return sorted(data, key=lambda k: k['no_skus'])



	return data

def get_srp_from_old(list, barcode):
	from gaisano.tasks import loop_search
	return loop_search(list, barcode)


def check_old_barcodes():
	data = []
	table = get_list()
	for item in table:
		try:
			in_old = get_item_in_new(item["barcode"])
			if not in_old:
				continue
			else:
				print item["barcode"]
				if item['item_code'][0]=='+':
					print item["item_code"], "Phased OUT"
					continue
				supplier = get_supplier(item['supplier'])
				temp = {"barcode_old": item["barcode"], "item_code": item["item_code"], "status": in_old, "supplier":supplier}
				print temp
				data.append(temp)
		except:
			continue
	return data


def get_item_in_new(barcode):
	items = frappe.db.sql("""SELECT name from `tabItem` where barcode_retial = %s""",(barcode))
	if len(items) > 1:
		return "Multiple Items"
	elif len(items) == 0:
		return "Not in New/ Wrong Barcode in New"

def get_supplier(supp_code):
	suppliers = frappe.db.sql("""SELECT name from `tabSupplier` where supp_code = %s""", supp_code)
	if len(suppliers) == 0:
		return "SUPPLIER/SUPPLIER CODE NOT ENCODED IN NEW."
	else:
		return suppliers[0][0]

def get_supplier_discounts(item_code, branch):
    discounts = frappe.db.sql("""Select supplier_discount from `tabItem Branch Discount` where parent = %s and branch = %s""", (item_code, branch))
    disc1, disc2, disc3, disc4 = 0, 0, 0, 0
    if len(discounts) > 0:
        for discount in discounts:
            supplier_discount = frappe.get_doc("Supplier Discounts", discount[0])
            disc1 = (supplier_discount.disc_1 if supplier_discount.disc_1 != None else 0)
            disc2 = (supplier_discount.disc_2 if supplier_discount.disc_2 != None else 0)
            disc3 = (supplier_discount.disc_3 if supplier_discount.disc_3 != None else 0)
            disc4 = (supplier_discount.disc_4 if supplier_discount.disc_4 != None else 0)
    return float(disc1/100), float(disc2/100), float(disc3/100), float(disc4/100)

def get_item_SRP_vs_calculated_SRP(filters):

	supplier = filters.get("supplier") if (filters.get("supplier") and (filters.get("supplier") != "")) else None
	supplier_discount = filters.get("supplier_discount") if (
		filters.get("supplier_discount") and (filters.get("supplier_discount") != "")) else None

	data = []

	if (supplier != None) and (supplier_discount == None):
		print "===============>supplier only"
		items = frappe.db.sql("""SELECT itm.barcode_retial, itm.item_cost, itm.margin_rate, itm.item_price, itm.retail_margin_rate, itm.item_price_retail_with_margin, itm.item_cost_with_freight, itm.packing from `tabItem` itm left join `tabItem Supplier` sup on sup.parent = itm.name where sup.supplier = %s and itm.type !='Disabled'""", supplier)
	elif (supplier == None) and (supplier_discount != None):
		print "===============>discount only"
		items = frappe.db.sql("""SELECT itm.barcode_retial, itm.item_cost, itm.margin_rate, itm.item_price, itm.retail_margin_rate, itm.item_price_retail_with_margin, itm.item_cost_with_freight, itm.packing from `tabItem` itm left join `tabSupplier Discount Items` disc on disc.items = itm.name where disc.parent = %s and itm.type !='Disabled'""", supplier_discount)
	elif (supplier != None) and (supplier_discount != None):
		print "===============>discount + supplier"
		items = frappe.db.sql("""SELECT itm.barcode_retial, itm.item_cost, itm.margin_rate, itm.item_price, itm.retail_margin_rate, itm.item_price_retail_with_margin, itm.item_cost_with_freight, itm.packing from `tabItem` itm left join `tabItem Supplier` sup on sup.parent = itm.name inner join `tabSupplier Discount Items` disc on disc.items = itm.name where sup.supplier = %s and disc.parent = %s and itm.type !='Disabled'""", (supplier, supplier_discount))
	else:
		print "===============>all items"
		items = frappe.db.sql("""SELECT itm.barcode_retial, itm.item_cost, itm.margin_rate, itm.item_price, itm.retail_margin_rate, itm.item_price_retail_with_margin, itm.item_cost_with_freight, itm.packing from
				    				`tabItem` itm where type != 'Disabled'""")

	for item in items:
		item_retial_price_with_margin = item[5]
		calculated_retail_price = item[6] * (1 + item[4] / 100)
		caculated_retail_price_in_pieces = calculated_retail_price / item[7]
		srp = float(caculated_retail_price_in_pieces)
		price_calculcated_srp = round(srp, 1)
		truncate_calculated_srp = round(srp, 2)
		round_off_calculated_price = round(float(price_calculcated_srp - truncate_calculated_srp),2)

		if ((round_off_calculated_price == 0) or (round_off_calculated_price == 0.05) or (round_off_calculated_price == -0.05)):
			round_off = truncate_calculated_srp
			if item_retial_price_with_margin != round_off:
				print "if" , "calculated" , round_off, item_retial_price_with_margin
				data.append({"barcode": item[0],
							 "item_cost": item[1],
							 "margin_rate": item[2],
							 "calculated_item_price": round_off,
							 "retail_margin_rate": item[4],
							 "item_price_retail_with_margin": item[5],
							 "calculated_retail_price":srp})

		elif ((round_off_calculated_price >= 0.01) and (round_off_calculated_price <= 0.04)):
			round_off = price_calculcated_srp
			if item_retial_price_with_margin != round_off:
				print "elif" , "calculated" , round_off,  item_retial_price_with_margin
				data.append({"barcode": item[0],
							 "item_cost": item[1],
							 "margin_rate": item[2],
							 "calculated_item_price": round_off,
							 "retail_margin_rate": item[4],
							 "item_price_retail_with_margin": item[5],
							 "calculated_retail_price":srp})
        #
		# else:
		# 	round_off = (price_calculcated_wholesale_price + 0.05)
		# 	if wholesale_price != round_off:
		# 		print "else" , "calculated" , round_off, "wholsale" , wholesale_price, item[3]
		# 		data.append({"barcode": item[0], "item_cost": item[1], "margin_rate": item[2], "item_price": wholesale_price,
		# 					 "calculated_item_price": round_off, "retail_margin_rate": item[4],
		# 					 "item_price_retail_with_margin": item[5], "calculated_retail_price":caculated_retail_price_in_pieces})
	return data

def get_items_for_depcode_report():
	from gaisano.itemevents import check_item_dep_codes
	items = check_item_dep_codes()
	return items