# Copyright (c) 2013, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import json
import frappe
import os
import dbf

def execute(filters=None):
	data = []
	columns = get_columns(filters)
	if filters.get('report_type') == 'New to old':
		data = get_erp_upload_for_prices()

	elif filters.get('report_type') == 'Grocery to POS':
		data = get_erp_upload_for_pos()

	return columns, data


def get_columns(filters):
	columns = []
	if filters.get('report_type') == 'New to old':
		columns = [
			{"fieldname": "date", "label": "Date #", "fieldtype": "date", "width": 150},
			{"fieldname": "time", "label": "time", "fieldtype": "Data", "width": 150},
		]

	elif filters.get('report_type') == 'Grocery to POS':
		columns = [
			{'fieldname': 'date', 'label': 'Date #', 'fieldtype':'date', 'width':150},
			{'fieldname': 'time', 'label':'Time', 'fieldtype':'date','width':150},
			{'fieldname': 'user', 'label':'User', 'fieldtype':'Data', 'width':200},
		]
	return columns


def get_erp_upload_for_prices():
	data = []
	dbf_path = frappe.db.get_value("Server Information", None, "grocery_dbf_dir")
	os.system("mv /media/GROCERY_SERVER/" + dbf_path + "/POS_LOG.DBF /media/GROCERY_SERVER/" + dbf_path + "/POS_LOG.dbf")
	# os.system("mv /home/gaisano1/gaisano_chaka/POS_LOG.DBF /home/gaisano1/gaisano_chaka/POS_LOG.dbf")
	table = dbf.Table('/media/GROCERY_SERVER/' + dbf_path + '/POS_LOG', codepage='cp437')
	# table = dbf.Table('/home/gaisano1/gaisano_chaka/POS_LOG', codepage='cp437')
	table.open()
	for record in reversed(table):
		date =  json.dumps(record[0], sort_keys=True, default=str)
		time = json.dumps(record[1], sort_keys=True, default=str)
		date = date.replace('"', "")
		time = time.replace('"', "")
		data.append({"date": date, "time":time})
	# os.system("mv /home/gaisano1/gaisano_chaka/POS_LOG.dbf /home/gaisano1/gaisano_chaka/POS_LOG.DBF")
		os.system("mv /media/GROCERY_SERVER/" + dbf_path + "/POS_LOG.dbf /media/GROCERY_SERVER/" + dbf_path + "/POS_LOG.DBF")
	return data

def get_erp_upload_for_pos():
	data = []
	dbf_path = frappe.db.get_value("Server Information", None, "grocery_dbf_dir")
	# os.system("mv /home/gaisano1/gaisano_chaka/POS_LOG.DBF /home/gaisano1/gaisano_chaka/POS_LOG.dbf")
	os.system("mv /media/GROCERY_SERVER/" + dbf_path + "/LOG_POS.DBF /media/GROCERY_SERVER/" + dbf_path + "/LOG_POS.dbf")
	# table = dbf.Table('/home/baban/testing/LOG_POS', codepage='cp437')
	table = dbf.Table('/media/GROCERY_SERVER/' + dbf_path + '/LOG_POS', codepage='cp437')
	table.open()
	for record in reversed(table):
		date =  json.dumps(record[1], sort_keys=True, default=str)
		time = json.dumps(record[2], sort_keys=True, default=str)
		user = json.dumps(record[3], sort_keys=True, default=str)
		date = date.replace('"', "")
		time = time.replace('"', "")
		user = user.replace('"', "")
		data.append({'date':date, 'time':time, 'user':user})
		os.system("mv /media/GROCERY_SERVER/" + dbf_path + "/LOG_POS.dbf /media/GROCERY_SERVER/" + dbf_path + "/LOG_POS.DBF")

	return data