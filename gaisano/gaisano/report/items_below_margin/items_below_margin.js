// Copyright (c) 2016, Bai Mobile and Web Labs and contributors
// For license information, please see license.txt

frappe.query_reports["Items below Margin"] = {
	"filters": [
		{
			"fieldname": "branch",
			"label": __("Branch"),
			"fieldtype": "Link",
			"options" :"Branch",
			"reqd": 1

		},
		{
			"fieldname":"margin_rate",
			"label":__("Margin Rate"),
			"fieldtype":"Float",
			"reqd":1
		},
		{
			"fieldname":"supplier",
			"label":__("Supplier"),
			"fieldtype":"Link",
			"options":"Supplier",
			"reqd":0
		}
	]
};
