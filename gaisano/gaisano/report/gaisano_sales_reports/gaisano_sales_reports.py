# Copyright (c) 2013, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from gaisano.popupevents import get_report_password

def execute(filters=None):
	columns, data = [], []
	report_type = filters.get("type")
	branch = filters.get("branch")
	from_date = filters.get("from_date")
	to_date = filters.get("to_date")
	branch = branch if (branch is not None) and (branch!="") else ""
	supplier = filters.get("supplier")
	password = filters.get("report_password")
	show_peso_value = filters.get("show_peso_value")

	if show_peso_value == 1:
		if not get_report_password(password):
			error_msg = "Please Enter Password" if password is None else "WRONG REPORT PASSWORD!"
			frappe.throw(error_msg)

	columns = get_columns(report_type, show_peso_value)

	if report_type == "Branch Item Sales and Profit":
		data = get_item_sales_data(branch, from_date, to_date)
		data = get_total_row(data)
	elif report_type == "Item Sales, Volume, Classification by Supplier":
		if supplier is None:
			frappe.throw("Please specify a supplier!")
		data = get_item_sales_volume_classifcation(supplier, from_date, to_date, branch)
	elif report_type == "Item Sales, Volume, Classification by Supplier Exclude Company Bundling":
		if supplier is None:
			frappe.throw("Please specify a supplier!")
		data = get_item_sales_volume_classifcation_excluding_company_bundling_items(supplier, from_date, to_date, branch)
	elif report_type == "Branch POS and Wholesale Sales":
		data = get_branch_sales(branch, from_date, to_date)
	elif report_type == "Daily Sales Report":
		data = get_daily_sales(from_date, to_date, branch)
	elif report_type == "Sales per Department":
		data = get_sales_per_department(from_date, to_date, branch)
	else:
		data = get_item_sales_volume(from_date, to_date, branch)
	return columns, data

def get_item_sales_data(branch, from_date, to_date):
	data = []
	items = frappe.db.sql("""SELECT barcode_retial, name, item_name_dummy from `tabItem` where type!='Disabled' and item_cost>0""")
	for item in items:
		print item[0], item[1], item[2]
		qty, cost, selling, total_sales, total_profit, net_cost = get_item_branch_sales(branch, item[0], item[1], from_date, to_date)
		ws_sales, ws_profit=get_item_branch_wholesale_data(branch, item[1], from_date, to_date)
		margin_rate = None if net_cost == 0 else (selling-net_cost)/net_cost * 100
		data.append({"barcode":item[0], "item_desc":item[2], "qty":qty, "cost":cost, "srp" : selling, "margin_rate":margin_rate,
					 "net_cost":net_cost,"sales":total_sales, "profit":total_profit, "ws_sales":ws_sales, "ws_profit":ws_profit})
	return data


def get_item_branch_sales(branch, barcode, item_code, from_date, to_date):
	total_qty, cost, selling, total_sales, total_profit = 0, 0, 0, 0, 0
	pos_data = frappe.db.sql("""SELECT sum(qty), sum(amount), avg(cost), avg(price) from `tabUpload POS` where trans_date >= %s and trans_date <= %s
	    							and branch = %s and barcode = %s""", (from_date, to_date, branch, barcode))
	if (str(pos_data[0][0]) == "None"):
		total_qty = 0
	else:
		total_qty = float(pos_data[0][0])
		total_sales = float(pos_data[0][1])
		cost = float(pos_data[0][2])
		selling = float(pos_data[0][3])

	disc1, disc2, disc3, disc4 = get_item_discount(branch, item_code)
	net_cost = cost * (1 - disc1 / 100) * (1 - disc2 / 100) * (1 - disc3 / 100) * (1 - disc4 / 100)
	total_profit = total_sales -(net_cost * total_qty)

	return total_qty, cost, selling, total_sales, total_profit, net_cost

def get_item_branch_wholesale_data(branch, item_code, from_date, to_date):
	print "WHOLESALE QTY:"
	total_cost, total_qty, total_wholesale_amount = 0, 0, 0
	data = frappe.db.sql("""SELECT sum(item.qty_dummy), sum(item.price), sum(item.amount) from `tabStock Entry Detail` item inner join `tabStock Entry` se
								on se.name = item.parent where se.posting_date >= %s and se.posting_date<=%s
								and se.branch =%s and item.item_code = %s and se.docstatus = 1 and se.type = 'Wholesale'""",
						 (from_date, to_date, branch, item_code))
	print data[0][0], data[0][1]
	total_qty = data[0][0] if (data[0][0] is not None) else 0
	item_details = frappe.db.sql("""SELECT margin_rate, retail_margin_rate from `tabItem` where
										name = %s """, item_code)
	disc1, disc2, disc3, disc4 = get_item_discount(branch, item_code)
	cost = float(item_details[0][0])
	net_cost = cost * (1 - disc1 / 100) * (1 - disc2 / 100) * (1 - disc3 / 100) * (1 - disc4 / 100)
	total_profit = float(total_qty)*(float(item_details[0][1])-float(net_cost))
	total_wholesale_amount = float(total_qty)*(float(item_details[0][1]))
	return total_wholesale_amount, total_profit

def get_total_row(data):
	total_sales, total_profit, ws_sales, ws_profit = 0,0,0,0
	average_margin = 0
	total_margin = 0
	for row in data:
		total_profit += row['profit']
		total_sales += row['sales']
		ws_profit += row['ws_profit']
		ws_sales += row['ws_sales']
		total_margin+= 0 if row['margin_rate'] is None else row['margin_rate']
	average_margin = total_margin/(len(data))
	print "LEN OF ITEMS", len(data)
	data.append(
		{"item_desc": "TOTAL:", "sales": total_sales, "profit": total_profit, "ws_profit":ws_profit, "ws_sales":ws_sales, "margin_rate":average_margin})
	return data

def get_item_sales_volume_classifcation(supplier, from_date, to_date, branch):
	print supplier, from_date, to_date
	data = []
	total =0
	items = frappe.db.sql("""SELECT itm.barcode_retial, itm.item_name_dummy, cat.category, cat.classification,
							  cat.sub_class from `tabItem` itm inner join `tabItem Supplier` sup on itm.name = sup.parent
							inner join `tabItem Class Each` cat on cat.parent = itm.name where sup.supplier = %s and itm.type!='Disabled'""",supplier)
	for item in items:
		item_sales = get_item_qty_and_sales(item[0], from_date, to_date, branch)
		qty, amount = (0 if item_sales[0] is None else item_sales[0]), (0 if item_sales[1] is None else item_sales[1])
		total += 0 if amount is None else amount
		data.append({
			"barcode": item[0], "item_desc": item[1], "supplier":supplier, "qty":qty, "peso_value" : amount,
			"category":item[2], "classification":item[3], "subclass":item[4]
		})
	data = update_item_market_share(data, total)
	return data

def get_item_sales_volume_classifcation_excluding_company_bundling_items(supplier, from_date, to_date, branch):
	print supplier, from_date, to_date
	data = []
	total =0
	items = frappe.db.sql("""SELECT itm.barcode_retial, itm.item_name_dummy, cat.category, cat.classification,
							  cat.sub_class from `tabItem` itm inner join `tabItem Supplier` sup on itm.name = sup.parent
							inner join `tabItem Class Each` cat on cat.parent = itm.name where sup.supplier = %s and itm.type!='Disabled' and itm.type  != 'Company Bundling'""",supplier)
	for item in items:
		item_sales = get_item_qty_and_sales(item[0], from_date, to_date, branch)
		qty, amount = (0 if item_sales[0] is None else item_sales[0]), (0 if item_sales[1] is None else item_sales[1])
		total += 0 if amount is None else amount
		data.append({
			"barcode": item[0], "item_desc": item[1], "supplier":supplier, "qty":qty, "peso_value" : amount,
			"category":item[2], "classification":item[3], "subclass":item[4]
		})
	data = update_item_market_share(data, total)
	return data

def update_item_market_share(data, total):
	for row in data:
		market_share = None if total == 0 else (float(row['peso_value'])/total)*100
		row['market_share'] = market_share
	return data

def get_item_qty_and_sales(barcode, from_date, to_date, branch):
	data = frappe.db.sql("""SELECT sum(qty), sum(amount) from `tabUpload POS` where barcode = %s and trans_date >=%s
							and trans_date <=%s and branch = %s""",(barcode, from_date, to_date, branch))
	if len(data)>0:
		return data[0][0], data[0][1]
	else:
		return 0,0

def get_branch_sales(branch, from_date, to_date):
	#data = []
	sales = frappe.db.sql("""select sum(amount) from `tabUpload POS` where branch = %s and trans_date >=%s
							  and trans_date <=%s and barcode in (select barcode_retial from `tabItem` where type != 'Disabled')""",(branch, from_date, to_date))
	wholesale_total = frappe.db.sql("""SELECT sum(total_amount) from `tabStock Entry` where type ='Wholesale'
							  and docstatus = 1 and branch =%s and posting_date >=%s and posting_date <=%s""",
									(branch,from_date,to_date))
	return [{"branch":branch, "pos_sales":sales[0][0], "ws_sales":wholesale_total[0][0]}]

def get_item_sales_volume(from_date, to_date, branch):
	print "NEW REPORT FOR SGV", from_date, to_date
	data = []
	total =0
	items = frappe.db.sql("""SELECT DISTINCT barcode_retial, item_name_dummy, name from `tabItem` where type!='Disabled'""")
	for item in items:
		current_item = item[0]
		#print previous_item, current_item
		item_sales = get_item_qty_and_sales(item[0], from_date, to_date, branch)
		qty, amount = (0 if item_sales[0] is None else item_sales[0]), (0 if item_sales[1] is None else item_sales[1])
		total += 0 if amount is None else amount
		data.append({
			"barcode": item[0], "item_desc": item[1], "qty":qty, "peso_value" : amount,
		})
		#print item[0],qty#, supplier
	return data

def get_daily_sales(from_date, to_date, branch):
	data = []
	sales = frappe.db.sql("""select date(trans_date), sum(amount) from `tabUpload POS` where trans_date >= %s and trans_date <= %s and branch = %s group by date(trans_date)""", (from_date, to_date, branch))

	for row in sales:
		pos_date, pos_total = row[0], row[1]
		wholesale_sales = frappe.db.sql("""select sum(total_amount) from `tabStock Entry` where type = 'Wholesale' and branch = %s and posting_date = %s """,(branch, pos_date))
		data.append({"pos_date":pos_date, "pos_total":pos_total, "wholesale_sales": wholesale_sales[0]})
	return data

def get_sales_per_department(from_date, to_date, branch):
	data = []
	sales = frappe.db.sql("""SELECT itm.dep_code, dep.department, sum(pos.amount) from `tabUpload POS` pos join `tabItem` itm on
			itm.barcode_retial = pos.barcode join `tabDepartment Code` dep on dep.name = itm.dep_code
			where pos.branch = %s and pos.trans_date>=%s and pos.trans_date <=%s group by itm.dep_code""",(branch, from_date, to_date))
	for item in sales:
		data.append({"depcode":item[0],"department":item[1],"sales":item[2]})
	return data

def get_columns(type, show_peso_value):
	columns = []
	if type == "Branch Item Sales and Profit":
		if show_peso_value ==1:
			columns = [{"label":"Barcode", "width":100,"fieldname":"barcode", "fieldtype":"Data"},
					   {"label": "Item Description", "width": 250, "fieldname": "item_desc", "fieldtype": "Data"},
					   {"label": "QTY Sold", "width": 90, "fieldname": "qty", "fieldtype": "float", "precision": 2},
					   {"label": "Average Cost (per piece)", "width": 90, "fieldname": "cost", "fieldtype": "Currency", "precision": 2},
					   {"label": "Net Cost (per piece)", "width": 90, "fieldname": "net_cost", "fieldtype": "Currency",
						"precision": 2},
					   {"label": "Average SRP", "width": 90, "fieldname": "srp", "fieldtype": "Currency", "precision": 2},
					   {"label": "Retail Sales", "width": 100, "fieldname": "sales", "fieldtype": "Currency", "precision":2},
					   {"label": "Retail Profit", "width": 100, "fieldname": "profit", "fieldtype": "Currency", "precision":2},
					   {"label": "Margin Rate", "width": 100, "fieldname": "margin_rate", "fieldtype": "Percent",
						"precision": 2}
					   #{"label": "Wholesale Sales", "width": 100, "fieldname": "ws_sales", "fieldtype": "Currency",
						#"precision": 2},
					   #{"label": "Wholesale Profit", "width": 100, "fieldname": "ws_profit", "fieldtype": "Currency",
						#"precision": 2}
					   ]
		else:
			columns = [{"label": "Barcode", "width": 100, "fieldname": "barcode", "fieldtype": "Data"},
					   {"label": "Item Description", "width": 250, "fieldname": "item_desc", "fieldtype": "Data"},
					   {"label": "Average Cost (per piece)", "width": 90, "fieldname": "cost", "fieldtype": "Currency",
						"precision": 2},
					   {"label": "Net Cost (per piece)", "width": 90, "fieldname": "net_cost", "fieldtype": "Currency",
						"precision": 2},
					   {"label": "Average SRP", "width": 90, "fieldname": "srp", "fieldtype": "Currency",
						"precision": 2},
					   {"label": "Margin Rate", "width": 100, "fieldname": "margin_rate", "fieldtype": "Percent",
						"precision": 2}
					   ]
	elif type == "Item Sales, Volume, Classification by Supplier":
		if show_peso_value == 1:
			columns = [{"label": "Barcode", "width": 100, "fieldname": "barcode", "fieldtype": "Data"},
					   {"label": "Item Description", "width": 250, "fieldname": "item_desc", "fieldtype": "Data"},
					   {"label": "QTY/Volume Sold", "width": 90, "fieldname": "qty", "fieldtype": "float", "precision": 2},
					   {"label": "Peso Value", "width": 90, "fieldname": "peso_value", "fieldtype": "Currency",
						"precision": 2},
					   {"label": "Market Share", "width": 90, "fieldname": "market_share", "fieldtype": "Percent",
						"precision": 2},
					   {"label": "Supplier", "width": 100, "fieldname": "supplier", "fieldtype": "Data"},
					   {"label": "Category", "width": 100, "fieldname": "category", "fieldtype": "Data"},
					   {"label": "Classification", "width": 100, "fieldname": "classification", "fieldtype": "Data"},
					   {"label": "Subclass", "width": 100, "fieldname": "subclass", "fieldtype": "Data"}
					   ]
		else:
			columns = [{"label": "Barcode", "width": 100, "fieldname": "barcode", "fieldtype": "Data"},
					   {"label": "Item Description", "width": 250, "fieldname": "item_desc", "fieldtype": "Data"},
					   {"label": "Market Share", "width": 90, "fieldname": "market_share", "fieldtype": "Percent",
						"precision": 2},
					   {"label": "Supplier", "width": 100, "fieldname": "supplier", "fieldtype": "Data"},
					   {"label": "Category", "width": 100, "fieldname": "category", "fieldtype": "Data"},
					   {"label": "Classification", "width": 100, "fieldname": "classification", "fieldtype": "Data"},
					   {"label": "Subclass", "width": 100, "fieldname": "subclass", "fieldtype": "Data"}
					   ]
	elif type == "Item Sales, Volume, Classification by Supplier Exclude Company Bundling":
		if show_peso_value == 1:
			columns = [{"label": "Barcode", "width": 100, "fieldname": "barcode", "fieldtype": "Data"},
					   {"label": "Item Description", "width": 250, "fieldname": "item_desc", "fieldtype": "Data"},
					   {"label": "QTY/Volume Sold", "width": 90, "fieldname": "qty", "fieldtype": "float", "precision": 2},
					   {"label": "Peso Value", "width": 90, "fieldname": "peso_value", "fieldtype": "Currency",
						"precision": 2},
					   {"label": "Market Share", "width": 90, "fieldname": "market_share", "fieldtype": "Percent",
						"precision": 2}
					   ]
		else:
			columns = [{"label": "Barcode", "width": 100, "fieldname": "barcode", "fieldtype": "Data"},
					   {"label": "Item Description", "width": 250, "fieldname": "item_desc", "fieldtype": "Data"},
					   {"label": "Market Share", "width": 90, "fieldname": "market_share", "fieldtype": "Percent",
						"precision": 2}
					   ]
	elif type=="Branch POS and Wholesale Sales":
		if show_peso_value == 1:
			columns = [
						{"label": "POS Sales", "width": 250, "fieldname": "pos_sales", "fieldtype": "Currency",
							"precision": 2},
						{"label": "Wholesale Sales", "width": 250, "fieldname": "ws_sales", "fieldtype": "Currency",
							"precision": 2}
					   ]
		else:
			frappe.throw("Please Enter Report Password and check the 'Show Peso Value' checkbox.")

	elif type=="Daily Sales Report":
		columns = [
			{"label": "Date", "width":250, "fieldname": "pos_date", "fieldtype": "Data", "percision": 2},
			{"label": "POS Sales", "width": 250, "fieldname": "pos_total", "fieldtype": "Currency", "percision": 2},
			{"label": "Wholesale", "width": 250, "fieldname": "wholesale_sales", "fieldtype": "Currency", "percision": 2}
		]
	elif type=="Sales per Department":
		columns = [
			{"label": "Depcode", "width":250, "fieldname": "depcode", "fieldtype": "Data", "percision": 2},
			{"label": "Department", "width": 250, "fieldname": "department", "fieldtype": "Data", "percision": 2},
			{"label": "POS Sales", "width": 250, "fieldname": "sales", "fieldtype": "Currency", "percision": 2}#,
			#{"label": "Wholesale", "width": 250, "fieldname": "wholesale_sales", "fieldtype": "Currency", "percision": 2}
		]
	else:
		columns = [{"label": "Barcode", "width": 100, "fieldname": "barcode", "fieldtype": "Data"},
				   {"label": "Item Description", "width": 250, "fieldname": "item_desc", "fieldtype": "Data"},
				   {"label": "POS QTY Sold", "width": 90, "fieldname": "qty", "fieldtype": "float", "precision": 2}#,
				   #{"label": "Supplier", "width": 100, "fieldname": "supplier", "fieldtype": "Data"}
				   ]
		if show_peso_value == 1:
			columns.append({"label": "Peso Value", "width": 90, "fieldname": "peso_value", "fieldtype": "Currency",
						"precision": 2})
	return  columns


def get_item_discount(branch, item_code):
	disc1, disc2, disc3, disc4 = 0,0,0,0
	discounts = frappe.db.sql("""SELECT supplier_discount from `tabItem Branch Discount` where branch = %s and parent =%s""",(branch, item_code))
	for discount in discounts:
		discount_doc = frappe.get_doc("Supplier Discounts",discount[0])
		disc1, disc2, disc3, disc4 = discount_doc.disc_1, discount_doc.disc_2, discount_doc.disc_3, discount_doc.disc_4
	return disc1, disc2, disc3, disc4