// Copyright (c) 2016, Bai Mobile and Web Labs and contributors
// For license information, please see license.txt

frappe.query_reports["Gaisano Sales Reports"] = {
	"filters": [
		{
			"fieldname":"from_date",
			"label": __("From Date"),
			"fieldtype": "Date",
			"reqd": 1
		},
		{
			"fieldname":"to_date",
			"label": __("To Date"),
			"fieldtype": "Date",
			"reqd": 1
		},
		{
			"fieldname":"branch",
			"label": __("Branch"),
			"fieldtype": "Link",
			"options":"Branch",
			"reqd": 1
		},
		{
			"fieldname":"supplier",
			"label": __("Supplier"),
			"fieldtype": "Link",
			"options":"Supplier",
			"reqd": 0
		},
		{
			"fieldname":"type",
			"label": __("Type"),
			"fieldtype": "Select",
			"options":["","Branch POS and Wholesale Sales","Branch Item Sales and Profit", "Item Sales, Volume, Classification by Supplier", "Item Sales, Volume, Classification by Supplier Exclude Company Bundling", "Gaisano Item Sales Volume", "Daily Sales Report", "Sales per Department"],
			"reqd": 1
		},
		{
			"fieldname" : "report_password",
			"fieldtype" : "Password",
			"label" : "Password for Show Peso Value",
			"reqd" : 0
		},
        {
            "fieldname":"show_peso_value",
            "label": "Show Peso Value",
            "fieldtype": "Check"
        }
	]
};
