# Copyright (c) 2013, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe


def execute(filters=None):
    columns, data = [],[],


    from_date = filters.get("from_date")
    to_date = filters.get("to_date")
    branch = filters.get("Branch")


    columns = get_columns()
    data = get_zero_sales(branch, from_date, to_date)

    return columns,data



def get_columns():
    columns = []
    columns = [
        {"label": "Item Name", "fieldname": "item_name_dummy", "type":"data", "width": 200},
        {"label": "Barcode", "fieldname": "barcode_retial", "type": "data", "width": 150},
     ]

    return columns


def get_zero_sales(branch,from_date, to_date):
    data = []
    items = frappe.db.sql(
        """SELECT item_name_dummy,barcode_retial from
        `tabItem` where type != 'Disabled' and barcode_retial not in (select DISTINCT barcode from `tabUpload POS` where branch = %s and trans_date >=%s
		  and trans_date <=%s)""",(branch, from_date, to_date))
    for item in items:
        data.append({"item_name_dummy":item[0],"barcode_retial":item[1]})

    return data








