// Copyright (c) 2016, Bai Mobile and Web Labs and contributors
// For license information, please see license.txt

frappe.query_reports["Category Market Share"] = {
	"filters": [
    {
            "fieldname": "from_date",
            "label": __("From Date"),
            "fieldtype": "Date",
            "reqd": 1
        },
        {
            "fieldname": "to_date",
            "label": __("To Date"),
            "fieldtype": "Date",
            "reqd": 1
        },
        {
            "fieldname": "report_type",
            "label": __("Report Type"),
            "fieldtype": "Select",
			"options": ["Rank Categories", "Rank Classifications", "View all Category items","Market Share with Suppliers (By Classification)", "Market Share with Suppliers (Entire Category)", "Market Share By Category (Hide Subclass)", "Market Share by Category with Subclass"],
            "reqd": 1
        },
        {
            "fieldname": "category",
            "label": __("Item Category"),
            "fieldtype": "Link",
			"options": "Item Category",
            "reqd": 0
        },
        {
            "fieldname": "volume",
            "label": __("Volume Percentage"),
            "fieldtype": "Int",
            "reqd": 1,
            "default":40
        },
        {
            "fieldname": "peso_value",
            "label": __("Peso Value Percentage"),
            "fieldtype": "Int",
            "reqd": 1,
            "default":40
        },
        {
            "fieldname": "profit",
            "label": __("Profit Percentage"),
            "fieldtype": "Int",
            "reqd": 1,
            "default":20
        },
        {
            "fieldname": "branch",
            "label": __("Branch"),
            "fieldtype": "Link",
            "options": "Branch",
            "reqd": 1
        },
        {
        "fieldname" : "report_password",
        "fieldtype" : "Password",
        "label" : "Password for Show Peso Value",
        "reqd" : 0
        },
        {
            "fieldname":"show_peso_value",
            "label": "Show Peso Value (for Rank Categories only)",
            "fieldtype": "Check"
        }

	]
};
