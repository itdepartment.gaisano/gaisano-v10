// Copyright (c) 2016, Bai Mobile and Web Labs and contributors
// For license information, please see license.txt

frappe.query_reports["Category Growth"] = {
	"filters": [
		{
            "fieldname": "from_date",
            "label": __("From Date"),
            "fieldtype": "Date",
            "reqd": 1
        },
		{
            "fieldname": "to_date",
            "label": __("To Date"),
            "fieldtype": "Date",
            "reqd": 1
        },
		{
            "fieldname": "branch",
            "label": __("Branch"),
            "fieldtype": "Link",
			"options": "Branch",
            "reqd": 1
        },
		{
            "fieldname": "report_type",
            "label": __("Report Type"),
            "fieldtype": "Select",
			"options": ["Monthly", "Quarterly", "Annual"],
            "reqd": 1
        },
		{
            "fieldname": "category",
            "label": __("Category"),
            "fieldtype": "Link",
			"options": "Item Category",
            "reqd": 0
        },
        {
            "fieldname": "classification",
            "label": __("Classification"),
            "fieldtype": "Link",
			"options": "Item Classification",
            "reqd": 0
        }
	]
};
