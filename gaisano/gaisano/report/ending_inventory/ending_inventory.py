# Copyright (c) 2013, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe

def execute(filters=None):
	branch = filters.get("branch")
	warehouse = filters.get("warehouse")
	date = filters.get("reference_date")
	per_supplier = filters.get("per_supplier")

	if per_supplier == 1:
		columns = [
			{"label": "Supplier", 'width': 300, "fieldname": "supplier"},
			{"label": "Peso Value", 'width': 150, "fieldname": "peso_value", "fieldtype": "Currency"}
		]
	else:
		columns = [
			{"label": "Supplier", 'width': 300, "fieldname": "supplier"},
			{"label": "Item Name", 'width': 200, "fieldname": "item_name", "fieldtype": "Data"},
			{"label": "Barcode", 'width': 150 ,"fieldname": "barcode", "fieldtype": "Data"},
			{"label": "Quantity", "fieldname" : "qty", "fieldtype": "Data"},
			{"label": "Peso Value", 'width': 150, "fieldname": "peso_value", "fieldtype": "Currency"},
		]
	data = []



	if per_supplier == 1:
		counter = 0
		supplier_total = 0.0
		grand_total = 0.0
		supplier_type_total = 0.0
		prev_supplier_type = None
		suppliers = frappe.db.sql("""select name, supplier_type from `tabSupplier` order by field (supplier_type, "Imported", "Volume", "Local", "Distributor")""")
		for supplier in suppliers:

			items = frappe.db.sql("""select disc.name, itm.barcode_retial, itm.item_cost, item_name_dummy, itm.name, disc.supplier_name from `tabItem` itm inner join `tabSupplier Discounts` disc on itm.supplier_discount = disc.name where disc.supplier_name = %s and itm.type != 'Disabled'""",(supplier[0]))
			for item in items:
				qty = get_balance(warehouse, item[4], date)
				if qty is None or qty <= 0:
					qty = 0
				supplier_total += item[2] * qty
				supplier_type_total += item[2] * qty
			grand_total += supplier_total

			if prev_supplier_type != supplier[1] and counter != 0:
				supplier_type_total = supplier_type_total - supplier_total
				data.append({"supplier": "Supplier Type Total", "peso_value": supplier_type_total})
				supplier_type_total = supplier_total
			data.append({"supplier": supplier[0], "peso_value": supplier_total})
			counter =+ 1
			prev_supplier_type = supplier[1]
			supplier_total = 0.0
		data.append({"supplier": "Grand Total: ", "peso_value": grand_total })


	else:
		supplier_total = 0.0
		item_total = 0.0
		total = 0.0
		suppliers = frappe.db.sql("""select name, supplier_type from `tabSupplier` order by field (supplier_type, "Imported", "Volume", "Local", "Distributor")""")
		for supplier in suppliers:
			items = frappe.db.sql("""select disc.name, itm.barcode_retial, itm.item_cost, item_name_dummy, itm.name, disc.supplier_name from `tabItem` itm inner join `tabSupplier Discounts` disc on itm.supplier_discount = disc.name where disc.supplier_name = %s and itm.type != 'Disabled'  """,supplier[0])
			for item in items:
				qty = get_balance(warehouse, item[4], date)
				if qty is None or qty <= 0:
					qty = 0
				if supplier[0] == item[5]:
					supplier_total += item[2] * qty
				item_total += item[2] * qty
				data.append({"item_name": item[3], "barcode": item[1],  "qty": qty, "peso_value": item_total})
				item_total = 0.0
			total += supplier_total
			data.append({"supplier" : supplier[0], "Total" : "TOTAL", "peso_value": supplier_total, "qty" : "TOTAL"})
			supplier_total = 0.0
		data.append({"peso_value": total, "qty": "Grand Total"})

	return columns, data

def get_balance(warehouse, item_code, date):
	recon = frappe.db.sql("""select posting_date, qty_after_transaction from `tabStock Ledger Entry`
					where warehouse = %s and
					item_code = %s and
					posting_date <= %s
					ORDER BY posting_date DESC, posting_time DESC limit 1""", (warehouse, item_code, date))
	try:
		print recon[0][1]
	except:
		balance = 0
	else:
		balance = recon[0][1]

		return balance

