# Copyright (c) 2013, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe

def execute(filters=None):
	data = get_ladlad_items()
	columns = get_columns()
	branch = filters.get("branch")
	return columns, data

def get_columns():
	columns = [
		{"label": "PLU Code", 'width': 50, "fieldname": "plu_code"},
		{"label": "Barcode", 'width': 100, "fieldname": "barcode"},
		{"label": "Item Name", 'width': 100, "fieldname": "item_name"},
		{"label": "SRP", 'width': 150, "fieldname": "srp"}
	]
	return columns

def get_ladlad_items():

	data = []
	eleven = slice(11)
	items = frappe.db.sql("""SELECT barcode_retial,item_short_name, plu_code,item_price_retail_with_margin from `tabItem` where ladlad = 1 and plu_code <> '' or plu_code <> Null""")
	for item in items:
		item_name = (item[1][eleven])

		data.append({"plu_code": item[2], "item_name": item_name, "barcode": item[0], "srp": item[3]})

	return data


