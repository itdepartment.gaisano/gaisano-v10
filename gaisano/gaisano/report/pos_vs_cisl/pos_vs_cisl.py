# Copyright (c) 2013, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe, datetime


def execute(filters=None):
    columns = [


        {"label": "Item Code", 'width': 150, "fieldname": "item_code"},
        {"label": "Barcode", 'width': 150, "fieldname": "barcode", "fieldtype": "Data"},
        {"label": "Packing", 'width': 50, "fieldname": "packing", "fieldtype": "Int"},
        {"label": "Offtake", 'width': 50, "fieldname": "cisl", "fieldtype": "Float", "precision": 2},# Nabali ang offtake ug CISL hahaha
        {"label": "Cycle Time", 'width': 50, "fieldname": "cycletime", "fieldtype": "Int"},
        {"label": "POS QTY", "width": 50, "fieldname": "qty_pos", "fieldtype": "Int"},
        {"label": "WS Qty", 'width': 50, "fieldname": "qty_ws"},
        {"label": "WD Qty", 'width': 50, "fieldname": "qty_wd"},
        {"label": "Selling area Stocks", 'width': 150, "fieldname": "selling_area_stocks"},
        {"label": "Inventory", 'width': 100, "fieldname": "inventory", "fieldtype": "Data"},
        {"label": "CISL POS", 'width': 100, "fieldname": "offtake", "fieldtype": "Float", "precision": 2},
        {"label": "CISL POS + WS", 'width': 100, "fieldname": "cisl_pos_ws"},
        {"label": "CISL WD + WS", 'width': 150, "fieldname": "cisl_wd_ws"}
    ]

    data = []
    supplier_discount = filters.get("supplier_discount")\
        if filters.get("supplier_discount") != "" \
        else ""

    to_date = filters.get("to_date")
    branch = filters.get("branch")
    warehouse = filters.get("warehouse")

    now = datetime.datetime.strptime(to_date, '%Y-%m-%d').date()
    from_date = now - datetime.timedelta(days=90)

    if supplier_discount != "":
        itm_det = frappe.db.sql("""Select item.name, item.barcode_retial, item.packing, item.stock_uom, item.barcode_pack,disc.parent
        from `tabItem` item left join `tabSupplier Discount Items` disc on disc.items = item.item_code
        where disc.parent = %s ORDER BY item.type DESC, item.name ASC""", (supplier_discount))
    else:
        itm_det = frappe.db.sql("""Select item.name, item.barcode_retial, item.packing, item.stock_uom, item.barcode_pack, disc.parent
        from `tabItem` item left join `tabSupplier Discount Items` disc on disc.items = item.item_code ORDER BY item.type DESC, item.name ASC""")

    for item in itm_det:
        item_code, barcode, packing, discount = item[0], item[1], item[2], item[5]
        item_doc = frappe.get_doc("Item", item_code)
        get_qty_sold_temp = get_qty_sold(branch, item_code, supplier_discount, from_date, to_date)
        print "===========get qty sold temp value=========="
        print get_qty_sold_temp
        offtake = get_qty_sold_temp[4]
        cisl = get_qty_sold_temp[5]
        cycle_days = get_qty_sold_temp[6]
        pos_qty = get_qty_sold_temp[1]
        ws_qty = get_qty_sold_temp[2]
        wd_qty = get_qty_sold_temp[7]
        selling_area_stocks = wd_qty- pos_qty
        cisl_pos_ws = get_qty_sold_temp[8]
        cisl_wd_ws = get_qty_sold_temp[9]
        print "for dict"
        print item_code, barcode, offtake, cisl, cycle_days
        inventory = get_balance(warehouse, item_code, to_date)
        temp = {"item_code": item_doc.item_name_dummy, "barcode":barcode, "packing": packing, "cisl": cisl,
                "cycletime": cycle_days, "offtake": offtake, "inventory":inventory, "qty_pos": pos_qty, "qty_ws":ws_qty,
                "qty_wd":wd_qty,"selling_area_stocks":selling_area_stocks, "cisl_pos_ws":cisl_pos_ws,"cisl_wd_ws":cisl_wd_ws}
        data.append(temp)

    return columns, data

def get_qty_sold(branch, item_code, supplier_disc, date, now):
    cisl, cisl2 = 0, 0
    con_f = 1
    s1_sum, s1_qty = 0, 0
    s2_sum, s2_qty = 0, 0
    s3_sum, s3_qty = 0, 0
    new_avg = 0
    uom_conv = 0
    sum = 0
    supplier = ""

    frequency = frappe.db.sql("""Select offtake_calculation_frequency from `tabBranch` where branch = %s""", (branch))
    freq = str(frequency[0][0])
    fac_calc = frappe.db.sql("""Select (lead_time + cycle_time + buffer) from `tabSupplier Discounts` where name = %s""",(supplier_disc))  # factor calculation
    for f in fac_calc:

        factor = str(f[0])
        print "FACTOR = "+ factor
        con_factor = frappe.db.sql(
            """Select packing from `tabItem` where item_code = %s""", (item_code))

        for f in con_factor:
            con_f = str(f[0])
        if (str(con_f) == 'None') or (str(con_f) == ''):
            uom_conv = 1
        else:
            uom_conv = float(con_f)

        # ave qty: per piece/smallest UOM
        # count : # of records/days

    print "========================compute======================"

    s1 = frappe.db.sql("""Select count(qty), sum(qty), uom from `tabUpload POS`
                                                              where trans_date >= %s and trans_date <= %s and qty !=0 and
                                                              barcode = (Select barcode_retial from `tabItem` where item_code = %s)
                                                             and branch = %s""",
                           (date, now, item_code, branch))
    s2 = frappe.db.sql("""SELECT count(`tabStock Entry Detail`.qty), sum(`tabStock Entry Detail`.qty), `tabStock Entry Detail`.stock_uom, `tabStock Entry Detail`.uom from `tabStock Entry Detail` LEFT JOIN `tabStock Entry`
                                 ON `tabStock Entry Detail`.parent = `tabStock Entry`.name WHERE `tabStock Entry`.posting_date >= %s
                                 and `tabStock Entry`.posting_date <= %s and `tabStock Entry Detail`.qty >0
                                 and `tabStock Entry Detail`.item_code = %s
                                 AND `tabStock Entry`.branch = %s AND `tabStock Entry`.type = 'Wholesale' AND `tabStock Entry`.docstatus = 1""",
                           (date, now, item_code, branch))
    s3 = frappe.db.sql("""SELECT count(`tabStock Entry Detail`.qty), sum(`tabStock Entry Detail`.qty), `tabStock Entry Detail`.stock_uom, `tabStock Entry Detail`.uom   from `tabStock Entry Detail` LEFT JOIN `tabStock Entry`
                            ON `tabStock Entry Detail`.parent = `tabStock Entry`.name WHERE `tabStock Entry`.posting_date >= %s
                            and `tabStock Entry`.posting_date <= %s and `tabStock Entry Detail`.qty >0
                            and `tabStock Entry Detail`.item_code = %s
                            AND `tabStock Entry`.branch = %s AND  `tabStock Entry`.type ='Stock Transfer' AND `tabStock Entry Detail`.s_warehouse = 'CDO Warehouse - GG' AND
                            `tabStock Entry Detail`.t_warehouse != 'CDO Main Display Area - GG' AND `tabStock Entry`.docstatus = 1""",
                            (date, now, item_code, branch))

    s4 = frappe.db.sql("""SELECT count(`tabStock Entry Detail`.qty), sum(`tabStock Entry Detail`.qty), `tabStock Entry Detail`.stock_uom, `tabStock Entry Detail`.uom from `tabStock Entry Detail` LEFT JOIN `tabStock Entry`
                                     ON `tabStock Entry Detail`.parent = `tabStock Entry`.name WHERE `tabStock Entry`.posting_date >= %s
                                     and `tabStock Entry`.posting_date <= %s and `tabStock Entry Detail`.qty >0
                                     and `tabStock Entry Detail`.item_code = %s
                                     AND `tabStock Entry`.branch = %s AND `tabStock Entry`.type = 'Withdrawals' AND `tabStock Entry`.docstatus = 1""",
                       (date, now, item_code, branch))

    if str(s1[0][1]) == 'None':
        s1_sum, s1_qty = 0, 0
    else:
        s1_sum = s1[0][1] # pos_qty
        s1_qty = s1[0][0] # pos_number of days naay halin

    if str(s2[0][1]) == 'None':
        s2_sum, s2_qty = 0, 0
    else:
        s2_sum = (s2[0][1] * uom_conv)
        s2_qty = s2[0][0]

    if str(s3[0][1]) == 'None':
        s3_sum, s3_qty = 0, 0
    else:
        s3_sum = (s3[0][1] * uom_conv)
        s3_qty = s3[0][0]

    if str(s4[0][1]) == 'None':
        s4_sum, s4_qty = 0, 0
    else:
        s4_sum = (s4[0][1] * uom_conv)



    sum = s1_sum + s2_sum  # + s3_sum
    new_avg = sum / 90
    sum_pos_ws = (s1_sum + s2_sum) / 90
    sum_wd_ws =  (s2_sum + s4_sum) / 90

    cisl2 = round((float(new_avg) / (float(con_f)) * float(factor)), 0)
    cisl_temp = round(new_avg/float(con_f),2)
    cisl_pos_ws = round((float(sum_pos_ws) / (float(con_f)) * float(factor)), 2)
    cisl_wd_ws = round((float(sum_wd_ws) / (float(con_f)) * float(factor)), 2)


    return sum, s1_sum, s2_sum, s3_sum, cisl2, cisl_temp, factor, s4_sum, cisl_pos_ws, cisl_wd_ws

def get_balance(warehouse, item_code, trans_date):
    balance = 0
    recon = frappe.db.sql("""select posting_date, qty_after_transaction from `tabStock Ledger Entry`
                where warehouse = %s and
                item_code = %s and
                posting_date <= %s
                ORDER BY posting_date DESC, posting_time DESC, name DESC limit 1""",(warehouse, item_code, trans_date))
    try:
        print "============RECON==========="
        print recon[0][1]
    except:
        balance = 0
    else:
        balance = recon[0][1]

    return balance