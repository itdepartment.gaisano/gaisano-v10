// Copyright (c) 2016, Bai Mobile and Web Labs and contributors
// For license information, please see license.txt

frappe.ui.form.on('Supplier Discounts', 'onload', function(frm){
	if(frm.doc.__islocal)
		set_discount_id(frm);
    if (frm.doc.frequency == null)
    {
        frm.doc.frequency = "Weekly";
    }
    if (frm.doc.frequency == "Daily")
	{
		frm.set_df_property("monday", "hidden", 0);
		frm.set_df_property("tuesday", "hidden", 0);
		frm.set_df_property("wednesday", "hidden", 0);
		frm.set_df_property("thursday", "hidden", 0);
		frm.set_df_property("friday", "hidden", 0);
		frm.set_df_property("saturday", "hidden", 0);
		frm.set_df_property("po_schedule", "hidden", 0);
		frm.set_df_property("po_date", "hidden", 1);
	}
    frappe.call({
    	method: "gaisano.usevents.get_branch",
		args:{},
		callback: function(r){
    		if(r.message){
    			var branch = r.message;
    			if (branch=="CDO Main") {
                    frm.set_df_property("disable", "label", "Disabled in ALL BRANCHES");
                    frm.set_df_property("disabled_in_main", "hidden", 0);
                    frm.set_df_property("update_item_supplier", "hidden", 0);
                }
                else{
    				frm.set_df_property("disable", "label", "Disabled in Branch");
    				frm.set_df_property("disabled_in_main", "hidden", 1);
    				frm.set_df_property("update_item_supplier", "hidden", 1);
				}
			}


		}
	});
	var read_only;
    frappe.user.has_role('Support Team')==true ? read_only=0 : read_only=1;
    cur_frm.set_df_property("items", "read_only", read_only);

});

frappe.ui.form.on("Supplier Discounts", "supplier_name", function(frm){
    frm.set_value("supplier_discount_name", (frm.doc.supplier_name + " " + frm.doc.disc_name_dummy));
});

frappe.ui.form.on("Supplier Discounts", "disc_name_dummy", function(frm){
	frm.set_value("supplier_discount_name", (frm.doc.supplier_name + " " + frm.doc.disc_name_dummy));
});

frappe.ui.form.on('Supplier Discounts', {
	refresh: function(frm) {
	var same_as_regular_discount = frm.doc.same_as_regular_discount;
	frm.set_df_property("bo_disc_1", "read_only", same_as_regular_discount);
	frm.set_df_property("bo_disc_2", "read_only", same_as_regular_discount);
	frm.set_df_property("bo_disc_3", "read_only", same_as_regular_discount);
	frm.set_df_property("bo_disc_4", "read_only", same_as_regular_discount);
	}
});

frappe.ui.form.on("Supplier Discounts", 'same_as_regular_discount', function(frm){
	var same_as_regular_discount = frm.doc.same_as_regular_discount;
	frm.set_df_property("bo_disc_1", "read_only", same_as_regular_discount);
	frm.set_df_property("bo_disc_2", "read_only", same_as_regular_discount);
	frm.set_df_property("bo_disc_3", "read_only", same_as_regular_discount);
	frm.set_df_property("bo_disc_4", "read_only", same_as_regular_discount);
	if (same_as_regular_discount == 1){
		frm.set_value("bo_disc_1", frm.doc.disc_1);
		frm.set_value("bo_disc_2", frm.doc.disc_2);
		frm.set_value("bo_disc_3", frm.doc.disc_3);
		frm.set_value("bo_disc_4", frm.doc.disc_4);
	}
	else{
		frm.set_value("bo_disc_1", 0);
		frm.set_value("bo_disc_2", 0);
		frm.set_value("bo_disc_3", 0);
		frm.set_value("bo_disc_4", 0);
	}

});
/*
frappe.ui.form.on("Supplier Discounts", "update_item_supplier_discounts", function(frm){
	var progress_bar = '<span id="progress_bar_text">Updating Supplier Discount in Each Item...</span><div class="progress">' +
        '<div id = "progress_bar" class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="01" aria-valuemin="0" aria-valuemax="100" style="width:100%">' +
        '</div></div>';
    var progress = new frappe.ui.Dialog({
        title: "Loading...",
        fields: [
            {"fieldtype": "HTML", "fieldname": "progress"}]
    });
    progress.fields_dict.progress.$wrapper.html(progress_bar);
    progress.show();
	console.log(frm.doc.name);
	frappe.call({
		method: "gaisano.gaisano.doctype.supplier_discounts.supplier_discounts.update_supplier_discount_items",
		args: {
			"discount": frm.doc.name
		},
		callback: function (){
			progress.hide();
			msgprint(r.message, "DONE.");
		}
	});
});*/

frappe.ui.form.on("Supplier Discounts", "update_item_supplier", function(frm){
	var discount = frm.doc.name;
	var supplier = frm.doc.supplier_name;
	console.log(discount+"|"+supplier);
	frappe.call({
		method:"gaisano.gaisano.doctype.supplier_discounts.supplier_discounts.update_item_supplier",
		args:{
			"discount":discount,
			"supplier":supplier
		},
		callback:function(r){
			frappe.msgprint("DONE.");
		}
	});
});

frappe.ui.form.on("Supplier Discounts", "po_date", date_calculation);
frappe.ui.form.on("Supplier Discounts", "frequency", date_calculation);

function date_calculation(frm){
	if (frm.doc.frequency == "Daily"){
		frm.set_df_property("po_schedule", "hidden", 1);
		frm.set_df_property("po_date", "hidden", 1);
		frm.set_df_property("next_po_date", "hidden", 1);
		frm.set_df_property("rr_date", "hidden", 1);
		frm.set_df_property("next_rr_date", "hidden", 1);

		frm.set_df_property("monday", "hidden", 0);
		frm.set_df_property("tuesday", "hidden", 0);
		frm.set_df_property("wednesday", "hidden", 0);
		frm.set_df_property("thursday", "hidden", 0);
		frm.set_df_property("friday", "hidden", 0);
		frm.set_df_property("saturday", "hidden", 0);

		frm.set_value("po_schedule", null);
		frm.set_value("po_date", null);
		frm.set_value("next_po_date", null);
		frm.set_value("rr_date", null);
		frm.set_value("next_rr_date", null);
		console.log("first")
	}
	else if (frm.doc.frequency != "Daily" && frm.doc.po_date == null) {
		frm.set_df_property("po_schedule", "hidden", 0);
		frm.set_df_property("po_date", "hidden", 0);
		frm.set_df_property("next_po_date", "hidden", 0);
		frm.set_df_property("rr_date", "hidden", 0);
		frm.set_df_property("next_rr_date", "hidden", 0);

		frm.set_df_property("monday", "hidden", 1);
		frm.set_df_property("tuesday", "hidden", 1);
		frm.set_df_property("wednesday", "hidden", 1);
		frm.set_df_property("thursday", "hidden", 1);
		frm.set_df_property("friday", "hidden", 1);
		frm.set_df_property("saturday", "hidden", 1);
		console.log("Second")
	}

	else if ((frm.doc.po_date != null || frm.doc.frequency != "Daily")) {
		frm.set_df_property("po_schedule", "hidden", 0);
		frm.set_df_property("po_date", "hidden", 0);
		frm.set_df_property("next_po_date", "hidden", 0);
		frm.set_df_property("rr_date", "hidden", 0);
		frm.set_df_property("next_rr_date", "hidden", 0);

		frm.set_df_property("monday", "hidden", 1);
		frm.set_df_property("tuesday", "hidden", 1);
		frm.set_df_property("wednesday", "hidden", 1);
		frm.set_df_property("thursday", "hidden", 1);
		frm.set_df_property("friday", "hidden", 1);
		frm.set_df_property("saturday", "hidden", 1);
		console.log("Third")

		frappe.call({
		method:"gaisano.gaisano.doctype.supplier_discounts.supplier_discounts.next_po_date",
		args: {
			"po_date": frm.doc.po_date,
            "frequency": frm.doc.frequency,
			"lead_time": frm.doc.lead_time
			},
		callback: function(r){
			if (r.message){
				console.log(r.message);
				frm.set_value("next_po_date", r.message.next_po_date);
				frm.set_value("po_schedule", r.message.po_schedule);
				frm.set_value("rr_date", r.message.rr_date);
				frm.set_value("next_rr_date", r.message.next_rr_date);
			}
		}
	});
	}
}

frappe.ui.form.on("Supplier Discounts", "validate", function(frm){
	if ((frm.doc.monday == 0 && frm.doc.tuesday == 0 && frm.doc.wednesday == 0 && frm.doc.thursday == 0 && frm.doc.friday == 0  && frm.doc.saturday == 0) && frm.doc.frequency == "Daily") {
		frappe.msgprint("Please select at least 1 Day");
		return false;
	}
	else if (frm.doc.frequency != "Daily") {
		frm.set_value("monday", 0);
		frm.set_value("tuesday", 0);
		frm.set_value("wednesday", 0);
		frm.set_value("thursday", 0);
		frm.set_value("friday", 0);
		frm.set_value("saturday", 0);
	}
});

frappe.ui.form.on("Supplier Discount Items", "items", function(frm, cdt, cdn){
	var item = locals[cdt][cdn];
	frappe.call({
		method: "frappe.client.get",
		args:{
			"doctype":"Item",
			"name":item.items
		},
		callback:function(r){
			if(r.message){
				frappe.model.set_value(cdt, cdn, "item_description",r.message['item_name_dummy']);
				frm.refresh_field("items");
			}
		}
	});
});

function set_discount_id(frm){
	frappe.call({
		method: "gaisano.gaisano.doctype.supplier_discounts.supplier_discounts.get_last_id",
		args:{},
		callback:function(r){
		if((frm.doc.item_id==0)||(!frm.doc.item_id)){
		    frm.set_value("discount_id", r.message);
			frm.set_value("discount_code", "DISC-"+("0000000" + r.message).slice(-6))
        }
		}
	});
}