from frappe import _

def get_data():
	return {
		'fieldname': 'supplier_discount',
		'transactions': [
			{
				'label': _('Edit History'),
				'items': ['Supplier Discount Edit History']
			},
			{
				'label': _('Purchases'),
				'items': ['Purchase Order', 'Purchase Receipt']
			},
			{
				'label': _('Withdrawals, Bad Orders, etc.'),
				'items': ['Stock Entry']
			}
		]
	}
