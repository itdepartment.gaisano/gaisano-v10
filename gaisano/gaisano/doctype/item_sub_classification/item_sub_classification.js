// Copyright (c) 2016, Bai Mobile and Web Labs and contributors
// For license information, please see license.txt

frappe.ui.form.on('Item Sub Classification', {
	refresh: function(frm) {

	}
});

frappe.ui.form.on("Item Sub Classification","update_items", function(frm){
	var progress_bar = '<span id="progress_bar_text">Updating Item Classification</span><div class="progress">' +
        '<div id = "progress_bar" class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="01" aria-valuemin="0" aria-valuemax="100" style="width:100%">' +
        '</div></div>';
    var progress = new frappe.ui.Dialog({
        title: "Loading...",
        fields: [
            {"fieldtype": "HTML", "fieldname": "progress"}]
    });
    progress.fields_dict.progress.$wrapper.html(progress_bar);
    progress.show();
	frappe.call({
		method:"gaisano.gaisano.doctype.item_sub_classification.item_sub_classification.update_item_classification",
		args:{
			"category": frm.doc.category,
			"classification":frm.doc.classification,
			"subclass":frm.doc.class_name
		},
		callback:function(r){
			if(r.message){
				progress.hide();
				msgprint("Finished Updating Item Classifications.", "Done.");
			}
		}
	});
});

frappe.ui.form.on("Item Sub Classification","transfer_btn", function(frm){
	var progress_bar = '<span id="progress_bar_text">Updating Item Classification</span><div class="progress">' +
        '<div id = "progress_bar" class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="01" aria-valuemin="0" aria-valuemax="100" style="width:100%">' +
        '</div></div>';
    var progress = new frappe.ui.Dialog({
        title: "Loading...",
        fields: [
            {"fieldtype": "HTML", "fieldname": "progress"}]
    });
    progress.fields_dict.progress.$wrapper.html(progress_bar);
    progress.show();
	frappe.call({
		method:"gaisano.gaisano.doctype.item_sub_classification.item_sub_classification.update_item_subclass",
		args:{
			"newsubclass":frm.doc.transfer_to,
			"subclass":frm.doc.class_name
		},
		callback:function(r){
			if(r.message){
				progress.hide();
				msgprint("Finished Updating Item Classifications.", "Done.");
			}
		}
	});
});
