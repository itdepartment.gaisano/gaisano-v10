# -*- coding: utf-8 -*-
# Copyright (c) 2015, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe, datetime
from frappe.model.document import Document
from json import loads


class MergeRename(Document):
    pass

@frappe.whitelist()
def rename_and_submit(docname, item_list):
    json_doc = loads(item_list)
    for item in json_doc:
        #print item
        print item['source_doctype'], item['old_name'], item['new_name'], item['is_merge'], item['parent'], item['name']
        is_merge = True if item['is_merge'] == 1 else False
        frappe.db.sql("""delete from `tabMerge Item` where parent = %s""", item['parent'])
        frappe.db.sql("""delete from `tabRename Item` where parent = %s""", item['parent'])
        frappe.db.sql("""UPDATE `tabMerge Rename Item` set docstatus = 1 where parent = %s and name =%s""",(item['parent'],item['name']))
        frappe.db.commit()
        frappe.rename_doc(item['source_doctype'], item['old_name'], item['new_name'], merge=is_merge)
    #print datetime.datetime.now(), frappe.get_user().name
    frappe.db.sql("""UPDATE `tabMerge Rename` set docstatus = 1, modified =%s, modified_by = %s where name = %s""",
                  (datetime.datetime.now(), frappe.get_user().name,docname))
    return "DONE."