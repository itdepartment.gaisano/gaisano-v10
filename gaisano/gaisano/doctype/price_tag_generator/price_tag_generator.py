# -*- coding: utf-8 -*-
# Copyright (c) 2015, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe, datetime
from frappe.model.document import Document
from frappe.frappeclient import FrappeClient
from gaisano.apisync import connect_to_server
class PriceTagGenerator(Document):
	pass

@frappe.whitelist()
def get_items_with_srp_changes(modified):
	data = []
	if modified:

		branch = frappe.db.get_value("Server Information", None, "Branch")
		if (branch == "CDO Main"):
			data = item_generate_process(modified)
			print "renzo==========="

		else:
			main_server = connect_to_server()
			# data = main_server.get_api('gaisano.gaisano.doctype.price_tag_generator.price_tag_generator.item_generate_process',
			# 						   params={"modified": modified})
			items = main_server.get_api('gaisano.gaisano.doctype.price_tag_generator.price_tag_generator.get_items_with_updated_cost',
									   params={"modified": modified})
			for item in items:
				local_item = frappe.db.sql("""SELECT item_name_dummy, item_price_retail_with_margin, packing FROM `tabItem` WHERE name = %s """,(item[0]))
				data.append({'item':item[0], 'srp':local_item[0][1], 'item_description':local_item[0][0], 'barcode':item[1], 'user_modified':item[2]})

		return data

	else:
		frappe.throw("Please Select a date")

@frappe.whitelist()
def item_generate_process(modified):
	data = []
	date_today = datetime.datetime.strptime(modified, '%Y-%m-%d').strftime('%Y-%m-%d')
	date_tomorrow = datetime.datetime.strptime(date_today, '%Y-%m-%d') + datetime.timedelta(days=1)
	tomorrow = date_tomorrow.strftime('%Y-%m-%d')
	items = frappe.db.sql(
		"""SELECT item_code, MAX(creation) from `tabItem Price History` where creation >= %s and creation <= %s GROUP BY item_code""",
		(date_today, tomorrow))
	for item in items:
		price_history = frappe.db.sql(
			"""SELECT item_code, new_retail, barcode ,modified_by from `tabItem Price History` WHERE item_code = %s and creation = %s""",
			(items[0], items[1]))
		item_db = frappe.db.sql("""SELECT item_name_dummy from `tabItem` where name = %s""", (items[0]))
		data.append({"item": price_history[0][0], "srp": price_history[0][1], "item_description": item_db[0][0],
					 "barcode": price_history[0][2],
					 "user_modified": price_history[0][3]})
	return data

@frappe.whitelist()
def get_items_with_updated_cost(modified):
	data = []
	date_today = datetime.datetime.strptime(modified, '%Y-%m-%d').strftime('%Y-%m-%d')
	date_tomorrow = datetime.datetime.strptime(date_today, '%Y-%m-%d') + datetime.timedelta(days=1)
	tomorrow = date_tomorrow.strftime('%Y-%m-%d')
	items = frappe.db.sql(
		"""SELECT item_code, barcode, modified_by,  MAX(creation) from `tabItem Price History` where creation >= %s and creation <= %s GROUP BY item_code""",
		(date_today, tomorrow))
	return items

@frappe.whitelist()
def get_items(modified,item_supplier=None, modified_by=None):
	modified = modified +'T00:00:00.000'
	date_today = datetime.datetime.strptime(modified, '%Y-%m-%dT%H:%M:%S.%f')
	date_tomorrow = date_today + datetime.timedelta(days=1)
	tomorrow = date_tomorrow.strftime('%Y-%m-%dT%H:%M:%S.%f')
	modified_by = modified_by if modified_by is not None else ""
	item_supplier = item_supplier if item_supplier is not None else ""
	# print "----------------------------------" + "Date modified " + modified + " Date Tomorrow " + tomorrow + "--------------------------------------"
	data = []

	items = frappe.db.sql("""SELECT itm.name, itm.item_price_retail_with_margin, itm.item_name_dummy, itm.barcode_retial, itm.modified_by from `tabItem` itm inner join
							`tabItem Supplier` sup on sup.parent = itm.name where sup.supplier like %s and itm.modified>=%s and itm.modified <%s and itm.modified_by like %s and item_price_retail_with_margin != 0 and itm.type != 'Disabled'
							""",('%'+item_supplier+'%', modified, tomorrow, '%'+modified_by+'%'))
	for item in items:
		# print item[0]
		data.append({"item":item[0], "srp":item[1], "item_description":item[2], "barcode":item[3], "user_modified":item[4]})
	# print len(items), "<-----------Total-----------"
	return data

#@frappe.whitelist()
#def generate_printout(doctype, docname):
	#printout = frappe.get_print("Price Tag Generator")
	#printout = frappe.get_print(doctype, docname)
	#print printout
	#frappe.local.response.filename = "{name}.pdf".format(name=name.replace(" ", "-").replace("/", "-"))
	#frappe.local.response.filecontent = get_pdf(html)
	#frappe.local.response.type = "download"
