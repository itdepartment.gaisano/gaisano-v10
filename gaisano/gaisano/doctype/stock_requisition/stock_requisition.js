// Copyright (c) 2016, Bai Mobile and Web Labs and contributors
// For license information, please see license.txt

frappe.ui.form.on('Stock Requisition', {
	refresh: function(frm) {

	}
});

cur_frm.cscript.item_code = function (doc, cdt, cdn) {
	var d = locals[cdt][cdn];
	return frappe.call({
		method: "gaisano.gaisano.doctype.stock_requisition.stock_requisition.fill_details",
		args: {
			'item_code': d.item_code,
			'warehouse': doc.warehouse,
		},
		callback: function(r) {
			// frappe.throw("Error");
			frappe.model.set_value(cdt, cdn, "description", r.message["description"]);
            frappe.model.set_value(cdt, cdn, "inv", r.message["inv"]);
            if (r.message["msg"]) {
                frappe.msgprint(r.message["msg"]);
            }
		}
	});
}
