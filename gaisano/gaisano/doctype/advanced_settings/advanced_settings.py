# -*- coding: utf-8 -*-
# Copyright (c) 2015, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
import json
from gaisano.rrevents import update_stock_ledger
from gaisano.apisync import update_netcost_and_margins
from gaisano.itemevents import update_margin_rates
from gaisano.events import get_rounded_price


class AdvancedSettings(Document):

	pass

@frappe.whitelist()
def	save_all_items():
	msg = ""
	error = None
	items = frappe.db.sql("""SELECT name, supplier_discount from `tabItem` where type !='Disabled'""")
	for item in items:
		try:
			item_doc =frappe.get_doc("Item", item[0])
			item_doc.save()
			print item[0]
		except:
			print "ERROR @" + str(item[0])
			error = 1
		else:
			continue
	if error == 1:
		msg = "Some Items were not saved due to errors!"
	else:
		msg = "ALL ITEMS SAVED!"
	return msg



@frappe.whitelist()
def delete_price_history():
	current_entry = None
	all_items = frappe.db.sql("""SELECT name, item_code from `tabItem` where type !='Disabled'""")
	for active_item in all_items:
		item_name = active_item[0]
		history_entries = frappe.db.sql(
			"""SELECT new_price, item, name from `tabItem Price History` where item = %s ORDER BY date_modified asc, time_modified asc""",
			item_name)
		for item in history_entries:
			history = str(item [0]) + " "+ str(item[1])
			print "==========CURRENT ENTRY VS HISTORY==========="
			print current_entry, history
			if current_entry == None:
				current_entry = history
			else:
				if current_entry == history:
					print "************DELETE************"
					frappe.db.sql("""DELETE FROM `tabItem Price History` where name = %s""", item[2])#delete duplicate
					frappe.db.commit()
				else:
					current_entry = history
	return "Done Removing Duplicate Item Price Histories"

@frappe.whitelist()
def get_cancelled_rrs(limit):
	if limit == 0:
		rrs = frappe.db.sql("""SELECT name, supplier_discount, modified, owner from `tabPurchase Receipt` where docstatus = 2""")
	elif limit < 0:
		frappe.msgprint("INVALID NUMBER", "ERROR")
		pos = []
	else:
		rrs = frappe.db.sql("""SELECT name, supplier_discount, modified, owner from `tabPurchase Receipt` where docstatus = 2 LIMIT {0}""".format(limit))
	return {"rrs":rrs}

@frappe.whitelist()
def get_cancelled_pos(limit):
	if limit == 0:
		pos = frappe.db.sql("""SELECT name, supplier_discount, modified, owner from `tabPurchase Order` where docstatus = 2""")
	elif limit < 0:
		frappe.msgprint("INVALID NUMBER", "ERROR")
		pos = []
	else:
		pos = frappe.db.sql(
			"""SELECT name, supplier_discount, modified, owner from `tabPurchase Order` where docstatus = 2 LIMIT {0}""".format(limit))
	#rrs = frappe.db.sql("""SELECT name, supplier_discount, modified, owner from `tabPurchase Receipt` where docstatus = 2""")
	return {"pos":pos}

@frappe.whitelist()
def delete_document(doctype, document):
	error = None
	try:
		document = json.loads(document)
		if "RR" in str(doctype):
			print "Document is an RR"
			doc_id = document["rr"]
			frappe.db.sql("""DELETE from `tabPurchase Receipt` where name = %s""", doc_id)
			frappe.db.sql("""DELETE from `tabPurchase Receipt Item` where parent = %s""", doc_id)
			frappe.db.commit()
			#delete RR
		else:
			print "Document is a PO"
			doc_id = document["po"]
			frappe.db.sql("""DELETE from `tabPurchase Order` where name = %s""", doc_id)
			frappe.db.sql("""DELETE from `tabPurchase Order Item` where parent = %s""", doc_id)
			frappe.db.commit()
			#delete po
		print "Add Code Here!"
	except:
		print "Something went wrong somewhere."
		error = "eh"
	return error

@frappe.whitelist()
def cancel_doc_with_sle(r_doctype, r_id):
	print "-----------------REVER TO DRAFT--------------"
	table_name = '`tab'+str(r_doctype)+'`'

	child_table =""
	count_sle = frappe.db.sql("""select count(*) from `tabStock Ledger Entry` where voucher_no = %s""", r_id)

	if r_doctype == "Purchase Order":
		child_table = '`tabPurchase Order Item`'
		childquery = """UPDATE {0} set docstatus = 2 where parent = '{1}'""".format(child_table, r_id)
		parentquery = """UPDATE {0} set status = 'Cancelled', docstatus = 2, workflow_state = 'Cancel' where name = '{1}'""".format(
			table_name, r_id)
	elif r_doctype == "Purchase Receipt":
		child_table = '`tabPurchase Receipt Item`'
		childquery = """UPDATE {0} set docstatus = 2 where parent = '{1}'""".format(child_table, r_id)
		parentquery = """UPDATE {0} set status = 'Cancelled', docstatus = 2 where name = '{1}'""".format(
			table_name, r_id)
		frappe.db.sql(childquery)
		frappe.db.sql(parentquery)
		if count_sle[0][0]>0:
			from gaisano.rrevents import update_stock_ledger
			rr_doc = frappe.get_doc("Purchase Receipt", r_id)
			rr_doc.docstatus = 2
			rr_doc.update_stock_ledger()

	elif r_doctype == "Stock Entry":
		child_table = '`tabStock Entry Detail`'
		childquery = """UPDATE {0} set docstatus = 2 where parent = '{1}'""".format(child_table, r_id)
		parentquery = """UPDATE {0} set docstatus = 2, workflow_state = 'Cancel' where name = '{1}'""".format(
			table_name, r_id)
		frappe.db.sql(childquery)
		frappe.db.sql(parentquery)
		if count_sle[0][0] > 0:
			print "SE.... attempting to cancel and adjust sle's"
			from gaisano.seevents import update_stock_ledger
			se_doc = frappe.get_doc("Stock Entry", r_id)
			se_doc.docstatus = 2
			update_stock_ledger(se_doc)

	frappe.db.commit()
	return "Cancelled Document and Items."


@frappe.whitelist()
def revert_to_draft(r_doctype, r_id):
	print "-----------------REVER TO DRAFT--------------"
	table_name = '`tab'+str(r_doctype)+'`'

	child_table =""

	if r_doctype == "Purchase Order":
		child_table = '`tabPurchase Order Item`'
		childquery = """UPDATE {0} set docstatus = 2 where parent = '{1}'""".format(child_table, r_id)
		parentquery = """UPDATE {0} set status = 'Cancelled', docstatus = 2, workflow_state = 'Cancel' where name = '{1}'""".format(
			table_name, r_id)
	elif r_doctype == "Purchase Receipt":
		child_table = '`tabPurchase Receipt Item`'
		childquery = """UPDATE {0} set docstatus = 2 where parent = '{1}'""".format(child_table, r_id)
		parentquery = """UPDATE {0} set status = 'Cancelled', docstatus = 2 where name = '{1}'""".format(
			table_name, r_id)
	elif r_doctype == "Stock Entry":

		child_table = '`tabStock Entry Detail`'
		childquery = """UPDATE {0} set docstatus = 2 where parent = '{1}'""".format(child_table, r_id)
		parentquery = """UPDATE {0} set docstatus = 2, workflow_state = 'Cancel' where name = '{1}'""".format(
			table_name, r_id)
		from gaisano.seevents import update_stock_ledger
		se_doc = frappe.get_doc("Stock Entry", r_id)
		update_stock_ledger(se_doc)

	frappe.db.sql(childquery)
	frappe.db.sql(parentquery)
	frappe.db.commit()
	return "Cancelled Document and Items."

@frappe.whitelist()
def delete_discount(discount):
	try:
		frappe.db.sql("""DELETE from `tabSupplier Discounts` where name = %s""", discount)
		frappe.db.sql("""DELETE from `tabSupplier Discount Items` where parent = %s""", discount)
		frappe.db.commit()
	except:
		return "There were errors."
	else:
		return "Deleted Discount: '" + discount + "'."

@frappe.whitelist()
def delete_and_rename(doctype, old_doc, new_doc):
	print "DOCTYPE :"+ doctype
	print "OLD: "+ old_doc
	print "NEW: "+ new_doc

	modify = modified(doctype, old_doc)

	##Cancel and delete Old
	try:
		cancel_doc_with_sle(doctype, old_doc)
		#revert_to_draft(doctype, old_doc)
	except:
		frappe.throw("ERROR occured on cancel.")
	else:
		print "DELETING"
		delete_before_rename(doctype, old_doc)

	##Rename and Submit new
	rename_doc(doctype, old_doc, new_doc,modify)
	submit_doc(doctype, old_doc)

def modified(doctype, old_name):
	if doctype == "Purchase Order":
		result = frappe.db.sql("""SELECT creation,modified,modified_by,owner from `tabPurchase Order` where name = %s""", old_name)

	elif doctype == "Purchase Receipt":
		result = frappe.db.sql("""SELECT creation,modified,modified_by,owner from `tabPurchase Receipt` where name = %s""", old_name)

	elif doctype == "Stock Entry":
		result = frappe.db.sql("""SELECT creation,modified,modified_by,owner from `tabStock Entry` where name = %s""", old_name)

	return result

def rename_doc(doctype, new_name, new_doc, modify):

	print "==========+RENAMING+==========="
	table_name = '`tab' + str(doctype) + '`'
	child_table = ('`tab' + str(doctype) + ' Item`') if doctype!="Stock Entry" else ('`tab' + str(doctype) + ' Detail`')

	childquery = """UPDATE {0} set parent = '{1}' where parent = '{2}'""".format(child_table, new_name, new_doc)
	parentquery = """UPDATE {0} set name = '{1}', creation = '{3}', modified = '{4}', modified_by = '{5}', owner = '{6}' where name = '{2}'""".format(table_name, new_name, new_doc, modify[0][0], modify[0][1], modify[0][2], modify[0][3])
	frappe.db.sql(childquery)
	frappe.db.sql(parentquery)
	frappe.db.commit()


def submit_doc(doctype, doc):
	print "============SUBMITTING==========="
	if doctype == "Purchase Order":
		childquery = """UPDATE `tabPurchase Order Item` set docstatus = 1 where parent = '{0}'""".format(doc)
		parentquery = """UPDATE `tabPurchase Order` set status = 'To Receive and Bill', docstatus = 1, workflow_state = 'Submitted' where name = '{0}'""".format(doc)
		print childquery
		print parentquery
		frappe.db.sql(childquery)
		frappe.db.sql(parentquery)
		frappe.db.commit()
	elif doctype == "Purchase Receipt":
		child_table = '`tabPurchase Receipt Item`'
		childquery = """UPDATE `tabPurchase Receipt Item` set docstatus = 1 where parent = '{0}'""".format(doc)
		parentquery = """UPDATE `tabPurchase Receipt` set status = 'To Bill', docstatus = 1 where name = '{0}'""".format(doc)
		doc = frappe.get_doc("Purchase Receipt", doc)
		update_stock_ledger(doc)
		print childquery
		print parentquery
		frappe.db.sql(childquery)
		frappe.db.sql(parentquery)
		frappe.db.commit()
	else:
		from gaisano.seevents import force_submit
		force_submit(doc)



def delete_before_rename(doctype, name):
	print "==========+RENAMING+==========="
	table_name = '`tab' + str(doctype) + '`'
	child_table = ('`tab' + str(doctype) + ' Item`') if doctype!="Stock Entry" else ('`tab' + str(doctype) + ' Detail`')

	childquery = """DELETE from {0} where parent = '{1}'""".format(child_table, name)
	parentquery = """DELETE from {0} where name = '{1}'""".format(table_name, name)
	frappe.db.sql(childquery)
	frappe.db.sql(parentquery)
	frappe.db.commit()

@frappe.whitelist()
def markup_item_prices(markup):
	print "---------MARKUP ITEM PRICES--------"
	markup = float(markup)
	items = frappe.db.sql("""SELECT name from `tabItem`""")
	for item in items:
		item_doc =frappe.get_doc("Item", item[0])
		if item_doc.type == 'Promo Pack':
			continue
		item_price = float(item_doc.item_price)
		item_price_retail = float(item_doc.item_price_retail_with_margin)

		if (item_price==0) and (item_price_retail == 0):
			continue

		new_ws_price = get_rounded_price(item_price*(1+(markup/100)))
		new_rt_price = get_rounded_price(item_price_retail*(1+(markup/100)))

		print item_price, item_price_retail, "OLD PRICES"
		print new_ws_price, new_rt_price, "NEW PRICES"

		frappe.db.sql("""UPDATE `tabItem` set item_price = %s, item_price_retail_with_margin = %s where name = %s""",
					  (new_ws_price, new_rt_price, item_doc.name))

		item_doc_updated = frappe.get_doc("Item", item[0])
		update_margin_rates(item[0], item_doc_updated.item_price_retail_1, item_doc_updated.item_cost_with_freight,
							item_doc_updated.item_price_retail_with_margin, item_doc_updated.item_price)
		update_netcost_and_margins(item_doc_updated)
		frappe.db.commit()

	return "ITEMS UPDATED"