// Copyright (c) 2016, Bai Mobile and Web Labs and contributors
// For license information, please see license.txt

frappe.ui.form.on('Server Information', {
	refresh: function(frm) {
        get_last_sync(frm);
        get_server_ip(frm);
        change_button_text(frm);
        var sync_html = document.querySelector('[data-fieldname="sync_html"]');
        frappe.call({
            method: "gaisano.apisync.update_html_table",
            args:{},
            callback:function(r){
                sync_html.innerHTML = r.message;
                    if (frappe.user.has_role("Support Team")) {
                    if(frm.doc.docstatus==0)
                        frm.set_df_property("grocery_ip_address", "read_only", 0);
                        frm.set_df_property("grocery_server_drive", "read_only", 0);
                        frm.set_df_property("grocery_dbf_dir", "read_only", 0);
                        frm.set_df_property("sync_folder_dir", "read_only", 0);
                        frm.set_df_property("grocery_credentials_file", "read_only", 0);

                        frm.set_df_property("pos_ip_address", "read_only", 0);
                        frm.set_df_property("pos_server_drive", "read_only", 0);
                        frm.set_df_property("pos_dbf_dir", "read_only", 0);
                        frm.set_df_property("pos_ntx_dir", "read_only", 0);
                        frm.set_df_property("pos_credentials_file", "read_only", 0);

        }
            }
        });

	}
});

frappe.ui.form.on("Server Information", "run_force_update", function(frm){
	var progress_bar = '<span id="progress_bar_text">Updating</span><div class="progress">' +
        '<div id = "progress_bar" class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="01" aria-valuemin="0" aria-valuemax="100" style="width:100%">' +
        '</div></div>';
    var progress = new frappe.ui.Dialog({
        title: "Loading...",
        fields: [
            {"fieldtype": "HTML", "fieldname": "progress"}]
    });
    // CLEAR CHILD TABLE
    progress.fields_dict.progress.$wrapper.html(progress_bar);
    progress.show();
	frappe.call({
        method: "gaisano.gaisano.doctype.server_information.server_information.force_bench_update",
        args: {},
        callback: function (r) {
            if (r.message) {
                progress.hide();
                msgprint("Version Update Complete!", "Done.");
                frm.set_value("last_update",r.message);
                frm.save();
            }
        }
    });
});

frappe.ui.form.on("Server Information", "check_item_costs", function(frm){
    var progress_bar = '<span id="progress_bar_text">Checking Costs</span><div class="progress">' +
        '<div id = "progress_bar" class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="01" aria-valuemin="0" aria-valuemax="100" style="width:100%">' +
        '</div></div>';
    var progress = new frappe.ui.Dialog({
        title: "Loading...",
        fields: [
            {"fieldtype": "HTML", "fieldname": "progress"}]
    });
    frm.clear_table("items");
    progress.fields_dict.progress.$wrapper.html(progress_bar);
    progress.show();
    frappe.call({
        method: "gaisano.gaisano.doctype.server_information.server_information.check_item_cost_sync",
        args: {},
        callback:function(r){
            progress.hide();
            frappe.msgprint("Updating Finished.", "Done.");
            var sync_html = document.querySelector('[data-fieldname="sync_html"]');
            sync_html.innerHTML = frm.doc.items_not_syncing;
        }
    });
});

frappe.ui.form.on("Server Information", "update_costs_and_freight", function(frm){
    var progress_bar = '<span id="progress_bar_text">Updating Costs</span><div class="progress">' +
        '<div id = "progress_bar" class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="01" aria-valuemin="0" aria-valuemax="100" style="width:100%">' +
        '</div></div>';
    var progress = new frappe.ui.Dialog({
        title: "Loading...",
        fields: [
            {"fieldtype": "HTML", "fieldname": "progress"}]
    });
    frm.clear_table("items");
    progress.fields_dict.progress.$wrapper.html(progress_bar);
    progress.show();
    frappe.call({
        method: "gaisano.gaisano.doctype.server_information.server_information.force_update_item_costs",
        args: {},
        callback:function(r){
            progress.hide();
            frappe.msgprint("Updating Finished.", "Done.");
            var sync_html = document.querySelector('[data-fieldname="sync_html"]');
            sync_html.innerHTML = frm.doc.items_not_syncing;
        }
    });
});

frappe.ui.form.on("Server Information", "update_pos_items", function(frm){
        var progress_bar = '<span id="progress_bar_text">Updating POS Counters</span><div class="progress">' +
        '<div id = "progress_bar" class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="01" aria-valuemin="0" aria-valuemax="100" style="width:100%">' +
        '</div></div>';
    var progress = new frappe.ui.Dialog({
        title: "Loading...",
        fields: [
            {"fieldtype": "HTML", "fieldname": "progress"}]
    });
    progress.fields_dict.progress.$wrapper.html(progress_bar);
    progress.show();
    frappe.call({
        method: "gaisano.etask.cp_files",
        args: {
        },
        callback:function(r){
            progress.hide();
            frappe.msgprint("Updating Finished.", "Done.");
        }
    });
});

frappe.ui.form.on("Server Information", "delete_ntx_files", function(frm){
        var progress_bar = '<span id="progress_bar_text">Updating POS Server</span><div class="progress">' +
        '<div id = "progress_bar" class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="01" aria-valuemin="0" aria-valuemax="100" style="width:100%">' +
        '</div></div>';
    var progress = new frappe.ui.Dialog({
        title: "Loading...",
        fields: [
            {"fieldtype": "HTML", "fieldname": "progress"}]
    });
    progress.fields_dict.progress.$wrapper.html(progress_bar);
    progress.show();
    frappe.call({
        method: "gaisano.etask.rm_item_dbf",
        args: {
        },
        callback:function(r){
            progress.hide();
            frappe.msgprint("Updating Finished.", "Done.");
        }
    });
});

frappe.ui.form.on("Server Information", "update_items_in_table", function(frm){
    var progress_bar = '<span id="progress_bar_text">Updating Items</span><div class="progress">' +
        '<div id = "progress_bar" class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="01" aria-valuemin="0" aria-valuemax="100" style="width:100%">' +
        '</div></div>';
    var progress = new frappe.ui.Dialog({
        title: "Loading...",
        fields: [
            {"fieldtype": "HTML", "fieldname": "progress"}]
    });
    progress.fields_dict.progress.$wrapper.html(progress_bar);
    progress.show();
    frappe.call({
        method:"gaisano.gaisano.doctype.server_information.server_information.update_items_in_table",
        args:{},
        callback:function(r){
            progress.hide();
            frappe.msgprint(r.message);
            frm.reload_doc();
        }
    });
});

frappe.ui.form.on("Server Information", "copy_grocery_dbf_to_media_backup", function(frm){
    var progress_bar = '<span id="progress_bar_text">Copying Item.dbf to new system media/backup/grocery_dbf</span><div class="progress">' +
        '<div id = "progress_bar" class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="01" aria-valuemin="0" aria-valuemax="100" style="width:100%">' +
        '</div></div>';
    var progress = new frappe.ui.Dialog({
        title: "Loading...",
        fields: [
            {"fieldtype": "HTML", "fieldname": "progress"}]
    });
    progress.fields_dict.progress.$wrapper.html(progress_bar);
    progress.show();
    frappe.call({
        method:"gaisano.etask.copy_grocery_dbf_to_media_backup",
        args:{},
        callback:function(r){
            progress.hide();
            frappe.msgprint(r.message);
        }
    });
});

frappe.ui.form.on("Server Information","copy_old_system_prices_to_new", function(frm){
    var progress_bar = '<span id="progress_bar_text">Copying OLD SYSTEM Prices to NEW SYSTEM</span><div class="progress">' +
        '<div id = "progress_bar" class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="01" aria-valuemin="0" aria-valuemax="100" style="width:100%">' +
        '</div></div>';
    var progress = new frappe.ui.Dialog({
        title: "Loading...",
        fields: [
            {"fieldtype": "HTML", "fieldname": "progress"}]
    });
    progress.fields_dict.progress.$wrapper.html(progress_bar);
    progress.show();
    frappe.call({
        method:"gaisano.etask.copy_prices_to_new_system",
        args:{},
        callback:function(r){
            progress.hide();
            frappe.msgprint(r.message);
        }
    });
});

frappe.ui.form.on("Server Information", "copy_new_system_prices_to_old", function(frm){
    var progress_bar = '<span id="progress_bar_text">Copying NEW SYSTEM prices to OLD SYSTEM</span><div class="progress">' +
        '<div id = "progress_bar" class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="01" aria-valuemin="0" aria-valuemax="100" style="width:100%">' +
        '</div></div>';
    var progress = new frappe.ui.Dialog({
        title: "Loading...",
        fields: [
            {"fieldtype": "HTML", "fieldname": "progress"}]
    });
    progress.fields_dict.progress.$wrapper.html(progress_bar);
    progress.show();
    frappe.call({
        method:"gaisano.etask.copy_prices_to_old_system",
        args:{},
        callback:function(r){
            progress.hide();
            frappe.msgprint(r.message);
        }
    });
});

frappe.ui.form.on("Server Information", "mount_pos_server", function(frm){
    var pos_ip = frm.doc.pos_ip_address;
    var pos_drive = frm.doc.pos_server_drive;
    var mount_str="//"+pos_ip+"/"+pos_drive;
    var credentials_file = frm.doc.pos_credentials_file;
    var args = {
        "url":mount_str,
        "credentials_file":credentials_file,
        "type": "POS"
    };
    mount_drive(args, frm);
});

frappe.ui.form.on("Server Information", "mount_grocery_server", function(frm){
    var grocery_ip = frm.doc.grocery_ip_address;
    var grocery_drive = frm.doc.grocery_server_drive;
    var mount_str="//"+grocery_ip+"/"+grocery_drive;
    var credentials_file = frm.doc.grocery_credentials_file;
    var args = {
        "url":mount_str,
        "credentials_file":credentials_file,
        "type": "GROCERY"
    };
    mount_drive(args, frm);
});

function mount_drive(args, frm){
    frappe.call({
        method: "gaisano.etask.mount_drive",
        args:args,
        callback:function(r){
            if(r.message){
                frappe.msgprint(r.message);
                change_button_text(frm);
            }
        }
    });
}

function get_last_sync(frm){
	frappe.call({
		method : "gaisano.gaisano.doctype.server_information.server_information.get_last_sync",
        args: {},
        callback:function(r){
		    if(r.message){
		        if (r.message["status"]!=frm.doc.sync_status)
                {
                    frm.set_value("sync_status", r.message["status"]);
                    frm.save();
                }
            }
        }
	});
}

function get_server_ip(frm){
	frappe.call({
		method : "gaisano.gaisano.doctype.server_information.server_information.get_server_ip",
        args: {},
        callback:function(r){
		    if(r.message){
		        if (r.message!=frm.doc.server_ip)
                {
                    frm.set_value("server_ip", r.message);
                    frm.save()
                }
            }
        }
	});
}

function change_button_text(frm){
    frappe.call({
        method: "gaisano.etask.get_mount_status",
        args:{},
        callback:function(r){
            if(r.message){
                var status = r.message;
                var pos_button_string = (status['pos']?"DISCONNECT":"CONNECT")+" POS SERVER";
                var grocery_button_string = (status['grocery']?"DISCONNECT":"CONNECT")+" GROCERY SERVER";
                frm.set_df_property("mount_pos_server", "label", pos_button_string);
                frm.set_df_property("mount_grocery_server", "label", grocery_button_string);
                frm.set_df_property("update_pos_items", "hidden", status['pos']?0:1);
                frm.set_df_property("delete_ntx_files", "hidden", status['pos']?0:1);
                frm.set_df_property("copy_old_system_prices_to_new", "hidden", status['grocery']?0:1);
                if(!frappe.user.has_role("Support Team")){
                    frm.set_df_property("copy_old_system_prices_to_new", "hidden", 1);
                }
                frm.set_df_property("copy_new_system_prices_to_old", "hidden", status['grocery']?0:1);
                frm.set_df_property("copy_grocery_dbf_to_media_backup", "hidden", status['grocery']?0:1);
                console.log(pos_button_string);
                console.log(grocery_button_string);

            }
        }
    });
}


