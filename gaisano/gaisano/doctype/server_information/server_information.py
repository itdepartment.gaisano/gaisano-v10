# -*- coding: utf-8 -*-
# Copyright (c) 2015, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe, os, datetime
from frappe.model.document import Document
from frappe.frappeclient import FrappeClient
from gaisano.popupevents import send_emails
from gaisano.events import get_rounded_price
from gaisano.apisync import connect_to_server


class ServerInformation(Document):
	pass

@frappe.whitelist()
def force_bench_update():
	path = "/home/littlhera/-v7/apps/gaisano/"
	sh_file = "./update_gaisano.sh"
	#path = "/home/frappe/frappe-v7/apps/gaisano/"
	#sh_file = "./update_gaisano.sh"
	os.system("cd " + path + " && " + sh_file)
	return datetime.datetime.now()

@frappe.whitelist()
def get_server_ip():
		import socket
		s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		s.connect(("8.8.8.8", 80))
		this_ip = (s.getsockname()[0])
		s.close()
		return this_ip



@frappe.whitelist()
def get_last_sync():
	from gaisano.apisync import update_html_table
	## TODO: do not update "Last Sync".
	difference = ""
	status = None
	#update_html_table()
	try:
		replication = connect_to_server()
		# replication = FrappeClient("http://202.137.117.24", "tailerp_api@gaisano.com", 't@1l3rpAPI')
		#replication = FrappeClient("http://192.168.5.211", "tailerp_api@gaisano.com", 't@1l3rpAPI')
		last_item_replication = replication.get_api('gaisano.popupevents.get_last_item')
	except:
		#walay net :(
		print "Walay Net"
		status = {"status": "CDO Main Sever Cannot be Reached.", "last_sync": ""}
	else:
		print "========get from replication========="
		print last_item_replication
		last_item = frappe.db.sql(
			"""Select name, date_modified, modified from `tabItem Price History` where name = %s order by modified desc limit 1""",
			last_item_replication[0])
		print last_item_replication[0], last_item_replication[1], last_item_replication[2]
		if len(last_item) == 0:
			print "not in local"
			last_history = frappe.db.sql(
				"""Select name, modified from `tabItem Price History` order by modified desc limit 1""")
			if len(last_history) == 0:
				status = {"status": "Sync Not Started", "last_sync": "NEVER"}
			else:
				try:
					same_item = frappe.db.sql("""SELECT name, modified from `tabItem Price History` where item = %s""",
											  last_item_replication[3])
					if len(same_item) > 0:
						status = {"status": "Sync Not Working", "last_sync": str(same_item[0][1])}
					else:
						status = {"status": "Sync Not Working", "last_sync": str(last_history[0][1])}
				except:
					status={"status": "Sync Not Working", "last_sync": str(last_history[0][1])}
		else:
			"Naa xa sa local"
			for item in last_item:
				print item[0], item[1], item[2]
				try:
					difference = item[2] - datetime.datetime.strptime(last_item_replication[2], '%Y-%m-%d %H:%M:%S')
				except:
					difference = item[2] - datetime.datetime.strptime(last_item_replication[2], '%Y-%m-%d %H:%M:%S.%f')
				print difference
				if difference > (-datetime.timedelta(minutes=15)):
					sync_msg = frappe.db.get_value("Server Information", None, "items_not_syncing")
					if "Item Costs Last Update at" in sync_msg:
						status = {"status": "Syncing", "last_sync": str(item[2])}
					else:
						status = {"status": "Synced but with Errors", "last_sync": str(item[2])}
				else:
					status = {"status": "Well, this is weird.", "last_sync": ""}
	if frappe.db.get_value("Server Information", None, "sync_status")!= status['status']:
		frappe.db.set_value("Server Information", None, "sync_status", status['status'])
		return status

@frappe.whitelist()
def check_item_cost_sync():
	branch = frappe.db.get_value("Server Information", None, "branch")
	ip_address = frappe.db.get_value("Server Information", None, "server_ip")
	print branch
	if (branch == "CDO Main"):
		print "CDO MAIN or REPLICATION"
		pass
	else:
		#date_now = datetime.datetime.now() - datetime.timedelta(minutes=10)
		date_now = "2015-01-01"
		frappe.db.sql("""delete from `tabItems Not Syncing`""")
		frappe.db.commit()
		print str(date_now)
		try:
			replication = connect_to_server()
			# replication = FrappeClient("http://202.137.117.24", "tailerp_api@gaisano.com", 't@1l3rpAPI')
			#replication = FrappeClient("http://192.168.5.205", "tailerp_api@gaisano.com", 't@1l3rpAPI')
			#replication = FrappeClient("http://202.137.117.24:8080", "tailerp_api@gaisano.com", 't@1l3rpAPI')
			last_item_replication = replication.get_api('gaisano.itemsync.get_updated_items',
													params={"date_modified": str(date_now)})
			#last_item_replication = replication.get_api('gaisano.popupevents.get_all_items', params ={"date_modified": str(date_now)})
		except:
			# walay net :(
			#print "Walay Net"
			return {"status": "CDO Main Sever Cannot be Reached.", "last_sync": ""}
		else:
			not_matching = check_item_list(last_item_replication)
			if len(not_matching) > 0:
				from gaisano.apisync import update_server_info_table
				update_server_info_table(not_matching)
				frappe.db.set_value("Server Information", None, "last_sync", datetime.datetime.now())
			else:
				now = datetime.datetime.now()
				msg = "Item Costs Last Update at " + str(now)
				frappe.db.set_value("Server Information", None, "items_not_syncing", msg)

#TODO: Update crie.
@frappe.whitelist()
def force_update_item_costs():
	from gaisano.apisync import update_items

	frappe.db.sql("""delete from `tabItems Not Syncing`""")
	frappe.db.commit()

	update_items()

def create_item_list():
	data = []
	items = frappe.db.sql("""SELECT name, barcode_retial, item_id from `tabItem`""")
	for item in items:
		data.append(item)
	return data

def get_list_index(item_name, item_list):
	for i, item in enumerate(item_list):
		if item[0] == item_name:
			#print "index",i, item[0], "==", item_name
			return i

def check_item_list(item_list):
	not_matching = []
	local_item_list = create_item_list()
	#todo: check modified by
	for i, replication_item in enumerate(item_list):
		item_name = frappe.db.sql("""SELECT name from `tabItem` where item_id = %s""", replication_item[2])
		if item_name:
			print "AT: ", i, "|", (len(item_list)-i), "LEFT"
			local_item = frappe.get_doc("Item", item_name[0][0])
			for_delete = get_list_index(local_item.name, local_item_list)
			if type(for_delete) == int:
				local_item_list.pop(for_delete)
			if local_item.barcode_retial != replication_item[7]:
				print "DIFFERENT BARCODES", replication_item[1], local_item.item_name
				temp = {"item": replication_item[0], "barcode": replication_item[7],
						"cost_in_local": "DIFF. BARCODES",
						"cost_in_main": replication_item[1], "item_id": replication_item[2]}
				not_matching.append(temp)
			elif (local_item.name != replication_item[0]):
				print "DIFFERENT NAMES", replication_item[1], local_item.item_name
				temp = {"item": replication_item[0], "barcode": replication_item[7], "cost_in_local": "DIFF. ITEM NAME",
						"cost_in_main": replication_item[1], "item_id": replication_item[2]}
				not_matching.append(temp)
			else:
				try:
					#print "replica", replication_item[0], replication_item[1]
					local_item = frappe.get_doc("Item", replication_item[0])
					#print "local", local_item.name, local_item.item_cost
					if (local_item.item_cost != replication_item[1]) or (local_item.item_cost_with_freight != replication_item[4]):
						if (local_item.item_cost_with_freight != replication_item[4]) and (frappe.db.get_value("Server Information", None, "is_outbranch")=="No"):
							temp = {"item": local_item.name, "cost_in_local": "COST WITH FREIGHT NOT MATCHING",
								"cost_in_main": replication_item[4], "barcode":replication_item[7],"item_id":replication_item[2]}
						else:
							temp = {"item": local_item.name, "cost_in_local": "COST NOT MATCHING",
									"cost_in_main": replication_item[1], "barcode":replication_item[7],"item_id":replication_item[2]}
						not_matching.append(temp)
				except:
					temp = {"item": replication_item[0], "cost_in_local": "NOT FOUND",
							"cost_in_main": replication_item[1], "barcode":replication_item[7],"item_id":replication_item[2]}
					not_matching.append(temp)
		else:
			temp = {"item": replication_item[0], "cost_in_local": "NOT IN LOCAL",
					"cost_in_main": replication_item[1], "barcode": replication_item[7], "item_id": replication_item[2]}
			not_matching.append(temp)
	for item in local_item_list:
		temp = {"item": item[0], "cost_in_local": "NOT IN MAIN/WRONG BARCODE",
				"cost_in_main": "", "barcode": item[1], "item_id": (item[2] if ((item[2]!="") and (item[2]!=None)) else 0)}
		not_matching.append(temp)
	return not_matching

@frappe.whitelist()
def update_items_in_table():
	from gaisano.apisync import update_item_prices, update_item_details, get_item_doc  # , update_html_table
	items = frappe.db.sql("""SELECT item_id, barcode, description, sync_error from `tabItems Not Syncing`""")
	for i, item in enumerate(items):
		details_error, insert_error = "OK", "OK"
		if item[3] == "Price Decrease/For Manual Updating/Rebates":
			#Skip item for manual updating
			continue
		try:
			print i, "|", (len(items) - i), "left"
			print item[0], item[1], item[2]#, item[3]
			item_in_local = frappe.db.sql("""SELECT name from `tabItem` where item_id = %s""", item[0])
			item_in_local = item_in_local if len(item_in_local) > 0 else frappe.db.sql(
				"""SELECT name from `tabItem` where barcode_retial = %s""", item[1])
			if item_in_local:
				print "ITEM IN LOCAL."
				item_main = get_item_doc(item[0])
				item_branch = frappe.get_doc("Item", item_in_local[0][0])
				print "Item in Main : ", item_main.name, item_main.barcode_retial, item_main.item_cost
				print "Item in Local: ", item_branch.name, item_branch.barcode_retial, item_branch.item_cost
				update_item_prices(item_branch.name, item_main.item_cost, item_main.item_freight_peso,
								   item_main.item_cost_with_freight, item_main.item_cost_without_vat)
				details_error = update_item_details(item_branch.name, item[0])
				insert_error = "OK"
				if (item[3] == "DIFF. ITEM NAME") and (item_branch.name != item_main.name):
					frappe.rename_doc("Item", item_branch.name, item_main.name)
					print "RENAME"
			else:
				print "NOT IN LOCAL"
				from gaisano.apisync import get_item_from_main
				insert_error = get_item_from_main(item[0])
		except:
			print "###############ERROR###############"
			continue
		else:
			row_name = frappe.get_value("Items Not Syncing", {"item_id": item[0]}, "name")
			if (("ERROR" not in details_error) and ("ERROR" not in insert_error)):
				print "OKAY"
				frappe.delete_doc("Items Not Syncing", row_name)
			else:
				print "DETAILS:", details_error, "INSERT:", insert_error
				print "NOT OKAY. T________T"

	frappe.db.commit()
	#update_html_table()
	count = len(frappe.db.sql("""SELECT name from `tabItems Not Syncing`"""))
	message = "ALL ITEMS UPDATED!" if count == 0 else (str(count)+" ITEM/S NOT UPDATED.")
	return message
