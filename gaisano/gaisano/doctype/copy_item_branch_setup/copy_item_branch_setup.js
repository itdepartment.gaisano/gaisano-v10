// Copyright (c) 2016, Bai Mobile and Web Labs and contributors
// For license information, please see license.txt

frappe.ui.form.on('Copy Item Branch Setup', {
	refresh: function(frm) {
	},
	"copy_item_setup":function (frm) {
		if(frm.doc.__unsaved==1){
			frappe.msgprint("Please save the document.")
		}
		else{
			var progress_bar = '<span id="progress_bar_text">Updating...</span><div class="progress">' +
			'<div id = "progress_bar" class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="01" aria-valuemin="0" aria-valuemax="100" style="width:100%">' +
			'</div></div>';
			var progress = new frappe.ui.Dialog({
				title: "Loading...",
				fields: [
					{"fieldtype": "HTML", "fieldname": "progress"}]
			});
			progress.fields_dict.progress.$wrapper.html(progress_bar);
			progress.show();
			frappe.call({
				"method":"gaisano.gaisano.doctype.copy_item_branch_setup.copy_item_branch_setup.copy_item_setup_to_branch",
				"args":{
					'name':frm.doc.name,
					'from_branch':frm.doc.source_branch,
					'to_branch': frm.doc.to_branch,
					'copy_supplier_discounts': frm.doc.copy_supplier_discounts,
					'copy_branch_availability':frm.doc.copy_branch_availability
				},
				"callback":function(r){
					progress.hide();
					frappe.msgprint(r.message);
					frm.reload_doc();
				}
			})
		}
    }

});