// Copyright (c) 2016, Bai Mobile and Web Labs and contributors
// For license information, please see license.txt



var supplier_discount = ""
frappe.ui.form.on("Discounts for Auto Po", "create_po", function(frm, cdt, cdn){
	var row = locals[cdt][cdn];
	supplier_discount = row.supplier_discount;
	supplier_discount_name = row.supplier_discount_name;

	var progress_bar = '<span id="progress_bar_text">Creating New PO</span><div class="progress">' +
        '<div id = "progress_bar" class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="01" aria-valuemin="0" aria-valuemax="100" style="width:100%">' +
        '</div></div>';
	// frappe.boot.notification_settings.purchase_order_message;
	var progress = new frappe.ui.Dialog({
        title: "Loading...",
        fields: [
            {"fieldtype": "HTML", "fieldname": "progress"}]
		});

	progress.fields_dict.progress.$wrapper.html(progress_bar);
	progress.show();

	frappe.call({
		method: "gaisano.poevents.auto_po",
		args:{
			"supplier_discount": row.supplier_discount,
			"warehouse": frm.doc.auto_po_warehouse
		},
		callback: function (r) {
			po_name = r.message["po_name"];
			frappe.set_route("Form", "Purchase Order", po_name);
		}
	});
	progress.hide();


});

frappe.ui.form.on("Purchase Order", "refresh", function(frm){
	frm.set_value("supplier_discount", supplier_discount);
});
