// Copyright (c) 2016, Bai Mobile and Web Labs and contributors
// For license information, please see license.txt

frappe.ui.form.on('Item Checker', {
	refresh: function(frm) {
		document.querySelector('[data-fieldname="item_status"]').innerHTML = "<div style = 'padding:10px;background-color:#fffdf4;text-align:center'>WAITING FOR RETAIL AND CASE BARCODE.</div>";
	},
	"retail_barcode": function(frm){
		get_item_from_barcode(frm.doc.retail_barcode, "retail_item", frm);
	},
	"case_barcode": function(frm){
		get_item_from_barcode(frm.doc.case_barcode, "case_item", frm);
	},
	"retail_item": function(frm){
		check_item_matching(frm.doc.retail_item, frm.doc.retail_barcode, frm.doc.case_item, frm.doc.case_barcode);
	},
	"case_item": function(frm){
		check_item_matching(frm.doc.retail_item, frm.doc.retail_barcode, frm.doc.case_item, frm.doc.case_barcode);
	}
});

frappe.ui.form.on("Item Checker", "onload", function(frm){
	frappe.call({
		"method":"gaisano.gaisano.doctype.item_checker.item_checker.get_system_datetime",
		"args":{},
		"callback":function(r){
			//console.log(r.message);
			frm.set_value("ref_date", r.message);
		}
	});
});

frappe.ui.form.on("Item Checker", "get_details", function (frm) {
	if ((frm.doc.search_item == null) || (frm.doc.search_item == "")) {
		frappe.throw("Please Enter a barcode");
	}
	if (frm.doc.warehouse == null || (frm.doc.warehouse == "")) {
		frappe.throw("Please Enter a Warehouse");
	}

	frappe.call({
		"method":"gaisano.gaisano.doctype.item_checker.item_checker.get_item_details",
		"args":{"item":frm.doc.search_item,
				"warehouse": frm.doc.warehouse,
				"ref_date": frm.doc.ref_date},
		"callback":function(r){
			var item_details = document.querySelector('[data-fieldname="item_description"]');
			item_details.innerHTML = "<div style = 'padding:10px;border-style:dashed;background-color:#fffdf4;'>" + r.message + "</div>";
		}
	});
	frm.set_value("search_item", "");
	$("input[data-fieldname='search_item']").focus()
});

function get_item_from_barcode(barcode, barcode_type, frm){
	var html_field = "";
	if (barcode_type == "retail_item")
		html_field = '[data-fieldname="retail_item_desc"]';
	else
		html_field = '[data-fieldname="case_item_desc"]';
	frappe.call({
		"method":"gaisano.gaisano.doctype.item_checker.item_checker.get_item_from_barcode",
		"args":{"barcode":barcode, "barcode_type":barcode_type},
		"callback":function(r){
			var item_details = document.querySelector(html_field);
			item_details.innerHTML = "<div style = 'padding:10px;border-style:dashed;background-color:#fffdf4;text-align:center'>" + r.message[1] + "</div>";
			frm.set_value(barcode_type, r.message[0]);
		}
	});
}

function check_item_matching(retail_item, retail_barcode, case_item, case_barcode){
	console.log(retail_item, case_item);
	var item_details = document.querySelector('[data-fieldname="item_status"]');
	if(retail_item!=case_item){
		if((case_barcode)&&(retail_barcode))
			item_details.innerHTML = "<div style = 'padding:10px;background-color:#ff5858;color:white;text-align:center'>ITEMS DO NOT MATCH</div>";
		else
			item_details.innerHTML = "<div style = 'padding:10px;background-color:#fffdf4;text-align:center'>WAITING FOR RETAIL AND CASE BARCODE.</div>";
	}
	else{
		if((case_barcode)&&(retail_barcode))
			item_details.innerHTML = "<div style = 'padding:10px;background-color:#5e64ff;color:white;text-align:center'>Items Match!</div>";
		else
			item_details.innerHTML = "<div style = 'padding:10px;background-color:#fffdf4;text-align:center'>WAITING FOR RETAIL AND CASE BARCODE.</div>";
	}
}