# -*- coding: utf-8 -*-
# Copyright (c) 2015, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from gaisano.poevents import get_item_details_SE
import datetime

class ItemChecker(Document):
	pass

@frappe.whitelist()
def get_item_from_barcode(barcode, barcode_type):
	if barcode_type == "retail_item":
		item = frappe.db.sql("""SELECT name, item_name_dummy, type from `tabItem` where barcode_retial = %s""",barcode)
	else:
		item = frappe.db.sql(
			"""SELECT name, item_name_dummy, type, uom, pos_uom, item_cost_with_freight, item_price, item_price_retail_with_margin from `tabItem` where barcode_pack = %s""",
			barcode)
	if len(item) == 0 or barcode == "" or barcode is None:
		return "", "Not Found"
	else:
		html_str = item[0][1] + " | " + item[0][2]
		return item[0][0], html_str

@frappe.whitelist()
def get_item_details(item, warehouse, ref_date):

	item = frappe.db.sql("""select name from `tabItem` where barcode_retial = %s OR barcode_pack = %s""", (item, item), as_dict=True)

	if (item):
		item_details=get_item_details_SE(item[0]['name'], warehouse, ref_date)
		html_string = "<b>"+item_details['item_name']+"</b><br><b>INV</b>: "+str(item_details['inv'])+ \
					  "<br><b>Packing: </b>" + item_details['packing']+ \
					  "<br><b>UOM: </b>"+item_details['uom']+\
					  "<br><b>Retail UOM</b>: "+item_details['l_uom']+"<br><b>Wholesale Price:</b> PHP "+ str(item_details['rate'])+\
					  "<br><b>Retail Price</b>: PHP "+str(item_details['item_retail_price'])
		# +item_doc.uom+"<br><b>Retail UOM</b>: "+item_doc.pos_uom+"<br><b>Wholesale Price:</b> PHP "+str(item_details['retail_price'])+"<br><b>Retail Price</b>: PHP "+str(item_doc.item_price_retail_with_margin)
		print "ITEM DETAILS FROM SE STRING:", item_details
		print html_string
		#return "LOL"
		return html_string

	else:
		html_string = "<b>Barcode not found</b>"
		return html_string

@frappe.whitelist()
def get_system_datetime():
	return datetime.datetime.now()