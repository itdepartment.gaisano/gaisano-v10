# -*- coding: utf-8 -*-
# Copyright (c) 2015, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class ItemClassification(Document):
	pass


@frappe.whitelist()
def update_item_classification(old_classification, new_classification, subclass):

	rows = frappe.db.sql("""SELECT name, parent from `tabItem Class Each` where classification =%s""", old_classification)
	for row in rows:
		if (subclass == None) or (subclass == ""):
			frappe.db.sql("""UPDATE `tabItem Class Each` set classification = %s where name = %s""", (new_classification, row[0]))
		else:
			frappe.db.sql("""UPDATE `tabItem Class Each` set classification = %s, sub_class = %s where name = %s""", (new_classification, subclass, row[0]))
		frappe.db.commit()
		item_doc = frappe.get_doc("Item", row[1])
		try:
			item_doc.save()
			frappe.db.commit()
		except:
			print "ERROR!"
			continue
	return "Finished Updating Items."