import frappe

@frappe.whitelist()
def get_data_for_rename(from_=None, to_=None):
    if from_ and to_:
        return (frappe.db.sql("""SELECT * from `tabMerge Rename Item` where modified >=%s and modified <=%s and docstatus =1""",(from_, to_), as_dict = True))
    else:
        return frappe.db.sql("""SELECT * from `tabMerge Rename Item` where docstatus =1""", as_dict=True)


def test_output(from_ = None, to_=None):
    data = get_data_for_rename(from_, to_)
    for row in data:
        print row['name'], row['source_doctype'], row['old_name'], row['new_name'], row['is_merge'], row['modified']