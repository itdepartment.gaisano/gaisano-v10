import frappe
from poevents import get_balance
from datetime import datetime


@frappe.whitelist()
def get_items_from_main(from_, to_):
    # import time
    # time_now = time.time()
    data = []
    warehouse = "CDO Warehouse - GG"
    print from_, to_
    date_now = datetime.today()
    items = frappe.db.sql("""SELECT distinct(item.name), item.barcode_retial, item.item_name_dummy, item.modified,
                          item.item_price_retail_with_margin, item.type, ice.category, ice.classification, item.packing, item.item_price
                          from `tabItem` item join `tabItem Class Each` ice on item.name = ice.parent left join
                          `tabStock Ledger Entry` sle on sle.item_code = item.name where item.sync_to_streetby=1 and
                          ((item.modified >=%s and item.modified <=%s) or (sle.creation >=%s and sle.creation <=%s))""",
                          (from_, to_, from_, to_))
    for item in items:
        item_balance = get_balance(warehouse, item[0], date_now)
        temp = {"item_code": item[0], "barcode": item[1], "item_name": item[2], "qty": item_balance,
                "is_available": True if item_balance > 0 else False, 'modified': str(item[3]), 'srp': item[4],
                "disabled": (1 if item[5] in ['Disabled', 'Phased Out'] else 0), "category": item[6],
                "classification": item[7],
                "packing": item[8], "wsrp": item[9]}

        print temp['category'], temp['classification'], item[2]
        data.append(temp)
    return data
    # time_after = time.time()
    # time_diff = time_after - time_now
    # print time_now, time_after, time_diff
