import frappe
from array import array
import datetime, calendar
from dateutil.relativedelta import  relativedelta
import json

@frappe.whitelist()
def check_email_sent(name):
    status = frappe.db.sql("""SELECT status from `tabEmail Queue` where reference_name = %s""", name)
    print status
    return {"status": status}

@frappe.whitelist()
def auto_po(supplier_discount,warehouse):

    branch = frappe.db.sql("""SELECT branch from `tabWarehouse` where name = %s""", warehouse)
    branch = branch[0][0]

    branch_code = frappe.db.sql("""SELECT branch_code from `tabBranch` where name = %s""", branch)
    branch_code = branch_code[0][0]

    supplier_discount_info = frappe.db.sql("""SELECT disc_1, disc_2, disc_3, disc_4, lead_time, supplier_name from `tabSupplier Discounts` where name = %s """, supplier_discount)
    supplier = supplier_discount_info[0][5]

    contact_person = frappe.db.sql("""SELECT name, email_id, first_name from `tabContact` where supplier = %s""", supplier)
    if (contact_person):
        contact_name = contact_person[0][0]
        contact_email = contact_person[0][1]
        contact_display = contact_person[0][2]

        now = datetime.date.today()

        po = frappe.new_doc("Purchase Order")
        po.supplier_discount = supplier_discount
        po.branch_code = branch_code
        po.branch = branch
        po.delivered_to_warehouse = warehouse
        po.strictly_ship_via = warehouse
        po.contact_person = contact_name
        po.naming_series = "PO-.branch_code.-.YY.MM.DD"
        po.status = "To Receive and Bill"
        po.contact_email = contact_email
        po.memo = "Auto PO"
        po.workflow_state = "Submitted"
        po.contact_display = contact_display

        item = frappe.db.sql("""select parent,safety_stock,auto_po_pop from `tabItem Branch Discount` where branch = %s and supplier_discount = %s and auto_po_pop = 1""", (branch, supplier_discount), as_dict=1)

        lead_time = frappe.db.sql("""select lead_time from `tabSupplier Discounts` where name = %s""",(supplier_discount))
        lead_time = lead_time[0][0]

        for items in item:
            db_item = frappe.db.sql("""Select name, item_cost, uom, item_cost_with_freight, item_name, item_name_dummy, packing from `tabItem` where item_code = %s""",(items['parent']), as_dict = True)
            daily_offtake = int(items['safety_stock']) / lead_time
            safety_stock = items["safety_stock"]
            inv = get_balance(warehouse,items["parent"],now)
            qty = int(safety_stock) - int(inv)
            offtake_info = "Daily Offtake: " + str(daily_offtake) + "| Safety Stock: " + str(safety_stock) + "| Inv: " + str(inv) + "| Recommended order: " + str(qty)
            # print "Item: ", items["parent"], " Safety Stock: " , safety_stock, " Lead Time: ", lead_time, "Daily Offtake: ", daily_offtake
            if qty > 0:
                po.append('items', {
                            'item_code': db_item[0]['name'],
                            'item_description': db_item[0]['item_name_dummy'],
                            "schedule_date": now,
                            "offtake_info": offtake_info,
                            "qty": qty,
                            "uom": db_item[0]['uom'],
                            "conversion_factor": db_item[0]['packing']
                            })
        po.save()
        po.submit()

        return {"po_name": po.name}

    else:
        frappe.throw("Supplier Discount doesn't have an email")


def branch_change_naming_po_rr(from_date, to_date):
    copy = "%-copy"
    po = frappe.db.sql(
        """select name, server_branch_code, branch_code from `tabPurchase Order` where (transaction_date BETWEEN %s AND %s) AND (name LIKE %s)""",
        (from_date, to_date, copy))

    for pos in po:
        # code for string manipulation
        end = len(pos[0])
        start = 0
        for y in pos[0]:
            if y.isdigit():
                break
            start += 1
        new_string = pos[0][start:end]
        new_string = new_string.replace("-copy", "")
        # end of string manipulation

        new_naming_series = "PO-" + pos[1] + "/" + pos[2] + "-" + new_string
        print "==================="
        print pos[0]
        print new_naming_series

        po_item = frappe.db.sql("""select name from `tabPurchase Order Item` where parent = %s""", (pos[0]))

        for po_items in po_item:
            print po_items[0]

        frappe.db.sql("""UPDATE `tabPurchase Order Item` set parent = %s where parent = %s""", (new_naming_series, pos[0]))
        frappe.db.sql("""UPDATE `tabPurchase Order` set name = %s where name = %s""", (new_naming_series, pos[0]))
        frappe.db.sql("""UPDATE `tabPurchase Receipt Item` set purchase_order = %s where parent = %s""", (new_naming_series, pos[0]))
        frappe.db.sql("""UPDATE `tabPurchase Receipt` set purchase_order = %s where purchase_order = %s""", (new_naming_series, pos[0]))
        frappe.db.commit()

        rr = frappe.db.sql("""select name from `tabPurchase Receipt` WHERE purchase_order = %s""", (pos[0]))
        for rrs in rr:
            print rrs[0]
            rr_item = frappe.db.sql("""select name from `tabPurchase Receipt Item` where parent = %s""", (rrs[0]))
            for rr_items in rr_item:
                print rr_items[0]





#Execute the server_branch_code first
#Date should be from January 16 - June 1, 2018
def change_naming_po_rr(from_date, to_date):
    po = frappe.db.sql("""select name, server_branch_code, branch_code, transaction_date from `tabPurchase Order` where branch != server_branch and (transaction_date BETWEEN %s AND %s)""", (from_date, to_date))
    po_count = 0
    po_item_count = 0

    for pos in po:
        po_item = frappe.db.sql("""select name from `tabPurchase Order Item` where parent = %s""", (pos[0]))
        po_count += 1
        print "-------------------------"

        print pos[0]
        #code for string manipulation
        end = len(pos[0])
        start = 0
        for y in pos[0]:
            if y.isdigit():
                break
            start += 1
        new_string = pos[0][start:end]
        #end of string manipulation

        new_naming_series = "PO-" + pos[1] + "/" + pos[2] + "-" + new_string
        print new_naming_series

        for po_items in po_item:
            po_item_count += 1
            frappe.db.sql("""UPDATE `tabPurchase Order Item` set parent = %s where parent = %s""",
                          (new_naming_series, pos[0]))

            print "PO-Item: " + po_items[0]
        frappe.db.sql("""UPDATE `tabPurchase Order` set name = %s where name = %s""", (new_naming_series, pos[0]))
        frappe.db.commit()
    print "Number of PO: ",  po_count
    print "Number of PO-Item: ", po_item_count


def server_branch_code(from_date, to_date):
    server_branch = frappe.db.sql("""select value from `tabSingles` where field = 'branch' and doctype = 'Server Information'""")
    print server_branch[0][0]

    server_branch_code = frappe.db.sql("""select branch_code from `tabBranch` where name = %s""", (server_branch[0][0]))

    print server_branch_code[0][0]

    po = frappe.db.sql("""select name, transaction_date from `tabPurchase Order` where (server_branch IS NULL OR server_branch_code IS NULL) and (transaction_date BETWEEN %s AND %s)""", (from_date, to_date))

    for pos in po:
        frappe.db.sql("""UPDATE `tabPurchase Order` set server_branch = %s, server_branch_code = %s where name = %s""", (server_branch[0][0],server_branch_code[0][0],pos[0]))
        print pos[0]
    frappe.db.commit()

    print "===server_branch and server_branch_code has entry now==="

#Be careful on what branch to use
#Date should be before January 16, 2018
@frappe.whitelist()
def delete_po(branch, from_date, to_date):
    po_count = 0
    rr_count = 0
    po = frappe.db.sql("""select name, transaction_date from `tabPurchase Order` where branch = %s and (transaction_date BETWEEN %s AND %s)""", (branch, from_date, to_date))

    for pos in po:
        po_count += 1
        print "--------------------------------------"
        print pos[0], pos[1]
        rr = frappe.db.sql("""select name from `tabPurchase Receipt` WHERE purchase_order = %s""", (pos[0]))
        for rrs in rr:
            rr_count += 1
            print rrs

            frappe.db.sql("""delete from `tabPurchase Receipt Item` where parent = %s""", (rrs[0]))
            frappe.db.sql("""delete from `tabPurchase Receipt` where purchase_order = %s""", (pos[0]))
            frappe.db.commit()
        frappe.db.sql("""delete from `tabPurchase Order Item` where parent = %s""", (pos[0]))
        frappe.db.sql("""delete from `tabPurchase Order` WHERE name = %s""", (pos[0]))
    frappe.db.commit()

    print "===Done==="

#This script will just give list of possible PO RR to be delete. For double check purposes only
@frappe.whitelist()
def list_of_po_rr(branch, from_date, to_date):
    po_count = 0
    po_item_count = 0
    rr_count = 0
    rr_item_count = 0
    po = frappe.db.sql("""select name, transaction_date from `tabPurchase Order` where branch = %s and (transaction_date BETWEEN %s AND %s)""",(branch, from_date, to_date))

    for pos in po:
        po_count += 1
        print "--------------------------------------"
        print pos[0], pos[1]

        po_item = frappe.db.sql("""select name from `tabPurchase Order Item` Item WHERE parent = %s""", (pos[0]))
        for po_items in po_item:
            po_item_count += 1
            print po_items

        rr = frappe.db.sql("""select name from `tabPurchase Receipt` WHERE purchase_order = %s""", (pos[0]))
        for rrs in rr:
            rr_count += 1
            print rrs
            rr_item = frappe.db.sql("""select name from `tabPurchase Receipt Item` WHERE parent = %s""", (rrs[0]))

            for rr_items in rr_item:
                print rr_items
                rr_item_count += 1

    print "po: ", po_count
    print "po_item: ", po_item_count
    print "rr: ", rr_count
    print "rr_item", rr_item_count

def po_validate(doc, method):
    print doc.transaction_date, "###########################>>>"
    branch = frappe.get_doc("Branch", doc.branch)
    branch_code = branch.branch_code if (branch.branch_code != "") and (branch.branch_code != None) else branch
    doc.branch_code = branch_code
    rows = frappe.db.sql('''SELECT lead_time from `tabSupplier Discounts` where name = %s''', doc.supplier_discount)

    if not (check_discount_in_branch(doc.supplier_discount, doc.branch)):
        frappe.throw("Supplier Discount is not allowed/available in your branch!")

    for row in rows:
        po_date = datetime.datetime.strptime(doc.transaction_date, "%Y-%m-%d")
        lead_time = row[0]
        delivery_date = po_date + datetime.timedelta(days=int(lead_time))
        cancellation_date = delivery_date + datetime.timedelta(days=1)
        doc.expected_delivery = delivery_date.strftime("%Y-%m-%d")
        doc.cancellation_date = cancellation_date.strftime("%Y-%m-%d")

    # set title
    if doc.supplier_discount:
        doc.title = doc.supplier_discount
        doc.discount_1, doc.discount_2, doc.discount_3, doc.discount_4, doc.supplier = get_discounts(doc.supplier_discount)
        print get_discounts(doc.supplier_discount)
        email = doc.contact_email
        supplier_discount_doc = frappe.get_doc('Supplier Discounts', doc.supplier_discount)
        try:
            doc.supplier_discount_name = supplier_discount_doc.supplier_discount_name
            if(supplier_discount_doc.contact_email !=None) and ( supplier_discount_doc.contact_email != ""):
                doc.contact_email = supplier_discount_doc.contact_email
            else:
                doc.contact_email = email
        except:
            doc.contact_email = email
    else:
        doc.title = doc.supplier
    # calculate total less supplier discount
    doc.is_save = 1
    disc1 = float(doc.discount_1) / 100.00
    disc2 = float(doc.discount_2) / 100.00
    disc3 = float(doc.discount_3) / 100.00
    disc4 = float(doc.discount_4) / 100.00
    disc5 = float(doc.additional_supplier_discount) / 100.00
    total_freight = 0
    total_case = 0

    #po_items(doc)

    if disc1 != 0 and disc2 == 0 and disc3 == 0 and disc4 == 0:
        discounted = float(doc.grand_total) - float(doc.grand_total * disc1)
        disc_amt_1 = float(doc.grand_total * disc1)
        doc.calc_disc_1 = '{:20,.2f}'.format(disc_amt_1)
        doc.calc_disc_2 = 0
        doc.calcl_disc_3 = 0
        doc.calc_disc_4 = 0
        less_disc = discounted
        trade_disc = float(doc.trade_discount)
        if (doc.additional_supplier_discount == 0):
            doc.grand_total_less_discount = less_disc - trade_disc
        else:
            add_disc = float(discounted) - float(discounted * disc5)
            disc_amt_5 = float(float(discounted * disc5))
            doc.calc_disc_5 = '{:20,.2f}'.format(disc_amt_5)
            doc.grand_total_less_discount = add_disc - trade_disc
    elif disc1 != 0 and disc2 != 0 and disc3 == 0 and disc4 == 0:
        discounted = float(doc.grand_total) - float(doc.grand_total * disc1)
        disc_amt_1 = float(doc.grand_total * disc1)
        doc.calc_disc_1 = '{:20,.2f}'.format(disc_amt_1)
        second_discount = float(discounted) - float(discounted * disc2)
        disc_amt_2 = float(float(discounted * disc2))
        doc.calc_disc_2 = '{:20,.2f}'.format(disc_amt_2)
        doc.calcl_disc_3 = 0
        doc.calc_disc_4 = 0
        less_disc = second_discount
        trade_disc = float(doc.trade_discount)

        if (doc.additional_supplier_discount == 0):
            doc.grand_total_less_discount = less_disc - trade_disc
        else:
            add_disc = float(second_discount) - float(second_discount * disc5)
            disc_amt_5 = float(float(second_discount * disc5))
            doc.calc_disc_5 = '{:20,.2f}'.format(disc_amt_5)
            doc.grand_total_less_discount = add_disc - trade_disc
    elif disc1 != 0 and disc2 != 0 and disc3 != 0 and disc4 == 0:
        discounted = float(doc.grand_total) - float(doc.grand_total * disc1)
        disc_amt_1 = float(doc.grand_total * disc1)
        doc.calc_disc_1 = '{:20,.2f}'.format(disc_amt_1)
        second_discount = float(discounted) - float(discounted * disc2)
        disc_amt_2 = float(float(discounted * disc2))
        doc.calc_disc_2 = '{:20,.2f}'.format(disc_amt_2)
        final_total = float(second_discount) - float(second_discount * disc3)
        disc_amt_3 = float(second_discount * disc3)
        doc.calcl_disc_3 = '{:20,.2f}'.format(disc_amt_3)
        doc.calc_disc_4 = 0
        less_disc = final_total
        trade_disc = float(doc.trade_discount)
        if (doc.additional_supplier_discount == 0):
            doc.grand_total_less_discount = less_disc - trade_disc
        else:
            add_disc = float(final_total) - float(final_total * disc5)
            disc_amt_5 = float(float(final_total * disc5))
            doc.calc_disc_5 = '{:20,.2f}'.format(disc_amt_5)
            doc.grand_total_less_discount = add_disc - trade_disc
    elif disc1 != 0 and disc2 != 0 and disc3 != 0 and disc4 != 0:
        discounted = float(doc.grand_total) - float(doc.grand_total * disc1)
        disc_amt_1 = float(doc.grand_total * disc1)
        doc.calc_disc_1 = '{:20,.2f}'.format(disc_amt_1)
        second_discount = float(discounted) - float(discounted * disc2)
        disc_amt_2 = float(float(discounted * disc2))
        doc.calc_disc_2 = '{:20,.2f}'.format(disc_amt_2)
        final_total = float(second_discount) - float(second_discount * disc3)
        disc_amt_3 = float(second_discount * disc3)
        doc.calcl_disc_3 = '{:20,.2f}'.format(disc_amt_3)
        final_total_4 = float(final_total) - float(final_total * disc4)
        disc_amt_4 = float(final_total * disc4)
        doc.calc_disc_4 = '{:20,.2f}'.format(disc_amt_4)
        less_disc = final_total_4
        trade_disc = float(doc.trade_discount)
        if (doc.additional_supplier_discount == 0):
            doc.grand_total_less_discount = less_disc - trade_disc
        else:
            add_disc = float(final_total_4) - float(final_total_4 * disc5)
            disc_amt_5 = float(float(final_total_4 * disc5))
            doc.calc_disc_5 = '{:20,.2f}'.format(disc_amt_5)
            doc.grand_total_less_discount = add_disc - trade_disc
    else:
        doc.calc_disc_1 = 0
        doc.calc_disc_2 = 0
        doc.calcl_disc_3 = 0
        doc.calc_disc_4 = 0
        less_disc = doc.grand_total
        trade_disc = float(doc.trade_discount)
        if (doc.additional_supplier_discount == 0):
            doc.grand_total_less_discount = less_disc - trade_disc
        else:
            #print "============ Only Additional Discount ================="
            disc_amt = float(doc.grand_total) - float(doc.grand_total * disc5)
            disc_amt_5 = float(doc.grand_total * disc5)
            doc.calc_disc_5 = '{:20,.2f}'.format(disc_amt_5)
            doc.grand_total_less_discount = disc_amt - trade_disc
            #print "Grand total:" +str(doc.grand_total)
            #print "Total with discount: "+ str(doc.grand_total_less_discount)
        doc.grand_total_less_discount = less_disc - trade_disc


    get_freight(doc)
    for row in doc.get("items"):

        if (row.conversion_factor != 0):
            row.stock_qty = row.qty / row.conversion_factor

        barcode = frappe.db.sql("""Select barcode_retial from tabItem where name = %s""", (row.item_code))

        row.warehouse = doc.delivered_to_warehouse
        row.freight_amount = row.freight_charge * row.qty
        total_freight += row.freight_amount
        total_case += row.qty

        packing = frappe.db.sql("""Select packing from `tabItem`
                      where name = %s""", (row.item_code))
        for p in packing:
            pkg_list = int(p[0])
            row.packing = pkg_list
        nv = frappe.get_doc("Item", row.item_code)
        row.non_vat = nv.item_cost_without_vat
        row.non_vat = round(float(row.rate / (1.12)),2)
        # row.non_vat = (row.rate - (row.rate * 0.107137433))
        # row.dummy_qty = row.stock_qty

        for ret_barcode in barcode:
            row.barcode_retial = str(ret_barcode[0])

            if disc1 != 0 and disc2 == 0 and disc3 == 0 and disc4 == 0:
                discounted = float(row.rate) - float(row.rate * disc1)
                row.net_cost = discounted
                row.net_cost_amount = row.net_cost * row.qty
            elif disc1 != 0 and disc2 != 0 and disc3 == 0 and disc4 == 0:
                discounted = float(row.rate) - float(row.rate * disc1)
                second_discount = float(discounted) - float(discounted * disc2)
                row.net_cost = second_discount
                row.net_cost_amount = row.net_cost * row.qty
            elif disc1 != 0 and disc2 != 0 and disc3 != 0 and disc4 == 0:
                discounted = float(row.rate) - float(row.rate * disc1)
                second_discount = float(discounted) - float(discounted * disc2)
                final_total = float(second_discount) - float(second_discount * disc3)
                row.net_cost = final_total
                row.net_cost_amount = row.net_cost * row.qty
            elif disc1 != 0 and disc2 != 0 and disc3 != 0 and disc4 != 0:
                discounted = float(row.rate) - float(row.rate * disc1)
                second_discount = float(discounted) - float(discounted * disc2)
                final_total = float(second_discount) - float(second_discount * disc3)
                super_total = float(final_total) - float(final_total * disc4)
                row.net_cost = super_total
                row.net_cost_amount = row.net_cost * row.qty

            else:
                row.net_cost = row.rate*(float(1-disc5))
                row.net_cost_amount = row.net_cost * row.qty

    doc.total_freight_charge = total_freight
    #print doc.total_freight_charge
    doc.total_case = total_case


    #items = doc.items
    for i, item in enumerate(doc.items):
        if item.name != None:
            item_id = branch_code + '-' + item.name
            doc.items[i].item_id = item_id
            print item_id

    #renzo and bevan's code
    for x, item in enumerate(doc.items):
        current_item = doc.items[x].item_code
        for y, item in enumerate(doc.items):
            if x != y and current_item == doc.items[y].item_code:
                frappe.throw("Duplicate Item: " + current_item + ". Please check row " + str(x+1) + " and " + str(y+1) + ".")

def get_discounts(supplier_discount):
    discount_doc = frappe.get_doc("Supplier Discounts",supplier_discount)
    return discount_doc.disc_1, discount_doc.disc_2, discount_doc.disc_3, discount_doc.disc_4, discount_doc.supplier_name

def get_freight(doc):
    for item in doc.get("items"):
        item_code = item.item_code
        itm = frappe.db.sql("""Select name from tabItem where item_code = %s""", (item_code))
        for i in itm:
            current_item = frappe.get_doc("Item", i[0])
            print current_item.item_cost_with_freight
            print item.qty
            difference = abs(current_item.item_cost_with_freight - current_item.item_cost)
            if difference != current_item.item_cost:
                item.freight_charge = difference
            else:
                item.freight_charge = 0


#TODO: add condition for BO.
@frappe.whitelist()
def get_supplier_disc_detail(disc_grp):
    count = frappe.db.sql("""SELECT count(*) FROM `tabSupplier Discounts` WHERE name = %s""",
                          (disc_grp))
    sup = frappe.db.sql("""Select supplier_name from `tabSupplier Discounts`
              where name = %s""",(disc_grp))
    if not count[0][0]:
        print "No Discount"
    else:
        disc_1 = frappe.db.sql(
            """SELECT disc_1 FROM `tabSupplier Discounts` WHERE name = %s""",
            (disc_grp))

        disc_2 = frappe.db.sql(
            """SELECT disc_2 FROM `tabSupplier Discounts` WHERE name = %s""",
            (disc_grp))

        disc_3 = frappe.db.sql(
            """SELECT disc_3 FROM `tabSupplier Discounts` WHERE name = %s""",
            (disc_grp))

        disc_4 = frappe.db.sql(
            """SELECT disc_4 FROM `tabSupplier Discounts` WHERE name = %s""",
            (disc_grp))

        discount_1 = float(disc_1[0][0])
        discount_2 = float(disc_2[0][0])
        discount_3 = float(disc_3[0][0])
        discount_4 = float(disc_4[0][0])

    return {"discount_1": discount_1, "discount_2": discount_2,
            "discount_3": discount_3, "discount_4": discount_4, "sup": sup[0][0]}


@frappe.whitelist()
def get_supplier_item_grp_disc(name):
    count = frappe.db.sql("""SELECT count(*) FROM `tabSupplier Discounts` WHERE supplier_name = %s""",
                          (name))
    print count
    if not count[0][0]:
        return {"has_discount": ("0")}
    else:
        return {"has_discount": ("1")}


@frappe.whitelist()
def get_items_under_supplier_discount(sup_disc, supplier, branch, warehouse, trans_date):
    not_included = "The following items are PHASED OUT and were not included in the PO:\n\n"
    items_array_code = []
    items_array_name = []
    items_array_cost = []
    items_array_uom = []
    items_array_isl = []
    items_array_cisl = []
    items_array_inv = []
    items_array_months_data = []
    items_array_shelf_inv = []
    items_array_promo = []
    items_array_w_cisl = []
    items_array_new_item = []
    items_array_item_desc = []

    items_dict_code = {}
    items_dict_name = {}
    items_dict_cost = {}
    items_dict_uom = {}
    items_dict_months_data = {}
    items_dict_shelf_inv = {}
    items_dict_promo = {}
    items_dict_w_cisl= {}
    items_dict_new_item = {}
    items_dict_item_desc = {}

    if not (check_discount_in_branch(sup_disc,branch)):
        frappe.throw("Supplier Discount is not allowed/available in your branch!")

    # doc = frappe.get_doc("Supplier Discounts", sup_disc)
    doc = frappe.db.sql("""select `tabSupplier Discount Items`.items, `tabItem`.type, `tabItem`.all_branches from `tabSupplier Discount Items` LEFT JOIN `tabItem` on `tabSupplier Discount Items`.items = `tabItem`.name where `tabSupplier Discount Items`.parent = %s AND `tabItem`.type != "Phased Out" AND `tabItem`.type != "Disabled" ORDER BY `tabItem`.type DESC, `tabSupplier Discount Items`.item_description ASC""", sup_disc)
    #doc = frappe.db.sql("""Select items from `tabSupplier Discount Items` where parent = %s order by items asc""",
    #                    (sup_disc))
    # for item in doc.get("items"):
    discount_doc = frappe.get_doc("Supplier Discounts", sup_disc)
    print "-----------SUPPLIER DISCOUNT ---------"
    print discount_doc.freight_cost_paid_by_supplier
    for item in doc:
        item_code = str(item[0])
        all_branches = item[2]
        validate_branch = frappe.db.sql("""Select count(*) from `tabItem Branch` where branch = %s and parent = %s""",
                                        (branch, item_code))
        val_branch = (validate_branch[0][0])
        print "+========validate, all branches==========+"
        print val_branch, all_branches
        if (val_branch <1)and (all_branches!=1):

            frappe.msgprint("Item "+item_code+" is not available in your branch and was excluded from the PO.")
            continue

        db_item = frappe.db.sql("""Select name, item_cost, uom, item_cost_with_freight, item_name, item_name_dummy from `tabItem` where item_code = %s""", (item_code))
        for d in db_item:
            item_desc = str(d[5])
            item_name = str(d[4])
            item_temp = str(d[0])
            this_item = frappe.get_doc("Item", item_temp)
            if (this_item.type == "Disabled") or (this_item.type == "Phased Out") or (this_item.type == "Company Bundling" and branch == "CDO Main"):
                print "Item" + item_temp + " is Phased Out/Disabled/Company Bundling"
                not_included += item_temp + "\n"
                continue
            if discount_doc.freight_cost_paid_by_supplier == 0:
                item_cost = str(d[3])
            else:
                item_cost = str(d[1])
            item_uom = str(d[2])
            offtake_temp = get_offtake(sup_disc, branch, item_temp, warehouse, trans_date)
            isl = str(offtake_temp['isl'])
            cisl = str(offtake_temp['cisl'])
            inv = str(offtake_temp['inv'])
            months_data = str(offtake_temp['months_data'])
            shelf_inv = get_item_shelf_inv(item_temp)
            promo = offtake_temp['promo']
            w_cisl = offtake_temp['w_cisl']
            new_item = is_new_item(item_temp)

            items_dict_code = {"item": item_code}
            items_dict_name = {"item": item_name}
            items_dict_cost = {"item": item_cost}
            items_dict_uom = {"item": item_uom}
            items_dict_isl = {"item": round(float(isl), 0)}
            items_dict_cisl = {"item": round(float(cisl), 0)}
            items_dict_months_data = {"item": months_data}
            items_dict_shelf_inv = {"item":shelf_inv}
            items_dict_promo = {"item":promo}
            items_dict_w_cisl = {"item":w_cisl}
            items_dict_new_item = {"item":new_item}
            items_dict_item_desc = {"item":item_desc}

            print "SHELF INV "+str(items_dict_shelf_inv)
            if (inv == 'None'):
                inv = 0
                items_dict_inv = {"item": round(float(inv), 0)}
            else:
                items_dict_inv = {"item": round(float(inv), 0)}

            items_array_code.append(items_dict_code)
            items_array_name.append(items_dict_name)
            items_array_cost.append(items_dict_cost)
            items_array_uom.append(items_dict_uom)
            items_array_isl.append(items_dict_isl)
            items_array_cisl.append(items_dict_cisl)
            items_array_inv.append(items_dict_inv)
            items_array_months_data.append(items_dict_months_data)
            items_array_shelf_inv.append(items_dict_shelf_inv)
            items_array_promo.append(items_dict_promo)
            items_array_w_cisl.append(items_dict_w_cisl)
            items_array_new_item.append(items_dict_new_item)
            items_array_item_desc.append(items_dict_item_desc)

            print items_array_shelf_inv

    #frappe.msgprint(not_included, "Message", "Orange")
    return {"sup_itm_code": items_array_code, "sup_itm_name": items_array_name,
            "sup_itm_cost": items_array_cost, "sup_itm_uom": items_array_uom,
            "itm_isl": items_array_isl, "itm_cisl": items_array_cisl, "itm_inv":items_array_inv,
            "itm_months_data":items_array_months_data, "itm_shelf_inv":items_array_shelf_inv,
            "itm_promo":items_array_promo, "itm_w_cisl":items_array_w_cisl, "itm_new_item": items_array_new_item,
            "itm_item_desc": items_array_item_desc}

@frappe.whitelist()
def get_supplier_lead_time(name):
    doc = frappe.get_doc("Supplier Discounts", name)
    lead_time = doc.lead_time
    return {"lead_time": lead_time}


@frappe.whitelist()
def get_packing(name):
    packing = frappe.db.sql("""Select max(conversion_factor) from `tabUOM Conversion Detail`
              where parent = %s""", (name))
    pkg_list = str(packing[0][0])

    return {"packing": pkg_list}

@frappe.whitelist()
def get_item_details_SE(item_code= None, warehouse=None, date=None, branch=None):
    barcode = ""
    inventory = ""
    item = frappe.db.sql("""Select item_name, item_code, default_warehouse, stock_uom, all_branches, packing, barcode_retial, item_price_retail_with_margin from `tabItem`
              where name = %s""", (item_code))
    print "=========SLE============"

    print item
    try:
        item_name = str(item[0][0])
        itm_code = str(item[0][1])
        itm_wr = str(item[0][2])
        uom = str(item[0][3])
        packing = str(item[0][5])
        barcode = str(item[0][6])
        item_retail_price = str(item[0][7])
        print "ITEM RETAIL PRICE:"+item_retail_price

        least_uom = frappe.db.sql("""Select uom, conversion_factor from `tabUOM Conversion Detail` where parent = %s
                  order by conversion_factor DESC limit 1""", (item_code))

        l_uom = least_uom[0][0]
        con_fac = least_uom[0][1]

        cost = frappe.db.sql("""Select price_list_rate from `tabItem Price`
                              where buying = 1 AND item_code = %s limit 1""", (item_code))
        try:
            item_cost = str(cost[0][0])
        except:
            item_cost = 0

        inv = get_balance(warehouse, item_code, date)
        if (inv == 'None'):
            inv = 0
        inventory = "PCK:  " + str(packing) + " INV: " + str(inv)

        return {"item_name": item_name, "item_code": itm_code, "description": itm_code,
                "rate": item_cost, "warehouse": itm_wr, "uom": uom, "inv": inv, "packing": packing, "inventory": inventory,
                "con_fac": con_fac, "l_uom": l_uom, "barcode":barcode, "item_retail_price":item_retail_price}

    except:
        print "Error"

@frappe.whitelist()
def get_item_details(item_code, branch, supplier, warehouse, date):
    phased_out = 0
    item_cost_from_master = 0

    item_discount = get_supplier_discount(item_code)
    item_doc = frappe.get_doc("Item",item_code)
    print "renzo"
    print item_discount
    #TODO: check if this works (for items that belong in more than 1 supplier discount)
    if supplier not in item_discount:
        frappe.throw("Item "+item_doc.item_name_dummy+"("+item_discount[0]+") does not belong in this Supplier Discount.",
                        "Warning")

    item = frappe.db.sql("""Select item_name, item_code, default_warehouse, stock_uom, all_branches, item_cost,
                        item_cost_with_freight, all_branches, item_name_dummy from `tabItem` where name = %s""", (item_code))
    discount_doc = frappe.get_doc("Supplier Discounts", supplier)

    print item
    all_branches = item[0][7]
    item_name = str(item[0][0])
    itm_code = str(item[0][1])
    item_desc = item[0][8]
    if (frappe.get_doc("Item", itm_code).type == "Phased Out") or (frappe.get_doc("Item", itm_code).type == "Company Bundling" and branch == "CDO Main"):
        ##frappe.msgprint("Item "+itm_code + "is phased out.")
        phased_out = 1
        frappe.throw("ITEM "+ item_desc+ " is Phased Out/Company Bundling. Do NOT PO.")

    itm_wr = str(item[0][2])
    uom = str(item[0][3])
    all_branch = str(item[0][4])
    if (item[0][5]!=None):
        if discount_doc.freight_cost_paid_by_supplier == 0:
            item_cost_from_master = item[0][6]
        else:
            item_cost_from_master = item[0][5]
    else:
        item_cost_from_master = 0
    frappe.msgprint(all_branches)
    validate_branch = frappe.db.sql("""Select count(*) from `tabItem Branch` where branch = %s and parent = %s""",
                                        (branch, item_code))
    val_branch = validate_branch[0][0]
    print "+========validate, all branches==========+"
    print val_branch, all_branches
    if val_branch < 1 and all_branches!=1:
        print "im here in not allowed"

        return {"msg": 1}

    else:
        cost = frappe.db.sql("""Select price_list_rate from `tabItem Price`
                              where buying = 1 AND item_name = %s limit 1""", (item_name))

        try:
            item_cost = str(item[0][6])
            #item_cost = str(cost[0][0])
        except:
            item_cost = 0
        offtake_temp = get_offtake(supplier, branch, item_code, warehouse, date)
        isl = str(offtake_temp['isl'])
        cisl = str(offtake_temp['cisl'])
        inv = str(offtake_temp['inv'])
        months_data = str(offtake_temp['months_data'])
        shelf_inv = get_item_shelf_inv(item_code)
        promo = offtake_temp['promo']
        w_cisl = offtake_temp['w_cisl']
        new_item = is_new_item(item_code)

        if (inv == 'None'):
            inv = 0
        if phased_out == 0:
            return {"item_name": item_name, "item_code": item_code, "description": item_code,
                "rate": item_cost_from_master, "warehouse": itm_wr, "uom": uom, "isl": round(float(isl), 0),
                "cisl": round(float(cisl), 0), "inv": inv, "months_data": months_data, "shelf_inv": shelf_inv,
                "promo":promo, "w_cisl":w_cisl, "new_item":new_item, "item_desc":item_desc}



def get_offtake(supplier, branch, item_code, warehouse, trans_date):
    isl = 0
    cisl = 0

    con_f = ""
    con_factor = ""
    factor = ""
    s1_sum, s1_qty = 0, 0
    s2_sum, s2_qty = 0, 0
    s3_sum, s3_qty = 0, 0
    withdrawals_offtake, no_days= 0, 0

    new_avg = 0
    uom_conv = 0
    inv = 0

    m1, m2, m3 = 0, 0, 0
    months_data = "N/A"
    # get 90 day average based on previous month
    #now = datetime.datetime.now().replace(day = 1)
    now = datetime.datetime.now()
    now -= datetime.timedelta(days=1)#start yesterday


    frequency = frappe.db.sql("""Select offtake_calculation_frequency from `tabBranch` where branch = %s""", (branch))
    freq = str(frequency[0][0])
    fac_calc = frappe.db.sql("""Select (lead_time + cycle_time + buffer) from `tabSupplier Discounts` where name = %s""",
                             (supplier)) #factor calculation
    con_factor = frappe.db.sql("""Select packing from `tabItem` where item_code = %s""", (item_code))

    for f in fac_calc:
        print f
        factor = str(f[0])


    date = now - datetime.timedelta(days=90)

    #if con_factor == None:
    #    con_f = 1
    #    uom_conv = 1
    for f in con_factor:
        con_f = str(f[0])
        uom_conv = 1
    #print "==========+CONF+==========="
    #print "conf "+con_f

    if str(con_f) == 'None':
            uom_conv = 1
    else:
        try:
            uom_conv= float(con_f)
        except:
            print con_f
            con_f = 1
            uom_conv = 1

        #ave qty: per piece/smallest UOM
        #count : # of records/days
    wholesale_sql2 = frappe.db.sql(
            """SELECT avg(`tabStock Entry Detail`.qty), sum(`tabStock Entry Detail`.qty) from `tabStock Entry Detail` LEFT JOIN `tabStock Entry`
            ON `tabStock Entry Detail`.parent = `tabStock Entry`.name WHERE `tabStock Entry`.posting_date >= %s
            and `tabStock Entry`.posting_date <= %s and `tabStock Entry Detail`.qty !=0
            and `tabStock Entry Detail`.item_code = %s
            AND `tabStock Entry`.branch = %s AND `tabStock Entry`.type='Wholesale' AND `tabStock Entry`.docstatus = 1""",
            (date, now, item_code, branch))
    #print "========================compute======================"
    case_offtake = get_offtake_from_case_barcode(item_code, date, now, branch)
    sales = frappe.db.sql("""Select avg(qty), sum(qty), uom from `tabUpload POS`
                                                         where trans_date >= %s and trans_date <= %s and qty !=0 and
                                                         barcode = (Select barcode_retial from `tabItem` where item_code = %s)
                                                        and branch = %s""",
                              (date, now, item_code, branch))
    s1 = frappe.db.sql("""Select count(qty), sum(qty), uom from `tabUpload POS`
                                                         where trans_date >= %s and trans_date <= %s and qty !=0 and
                                                         barcode = (Select barcode_retial from `tabItem` where item_code = %s)
                                                        and branch = %s""",
                           (date, now, item_code, branch))
    s2 = frappe.db.sql("""SELECT count(`tabStock Entry Detail`.qty), sum(`tabStock Entry Detail`.qty), `tabStock Entry Detail`.stock_uom, `tabStock Entry Detail`.uom from `tabStock Entry Detail` LEFT JOIN `tabStock Entry`
                            ON `tabStock Entry Detail`.parent = `tabStock Entry`.name WHERE `tabStock Entry`.posting_date >= %s
                            and `tabStock Entry`.posting_date <= %s and `tabStock Entry Detail`.qty >0
                            and `tabStock Entry Detail`.item_code = %s
                            AND `tabStock Entry`.branch = %s AND `tabStock Entry`.type = 'Wholesale' AND `tabStock Entry`.docstatus = 1""",
                           (date, now, item_code, branch))
    #TODO: Uncomment if Stock Transfers has been fixed/Consolidated.

    withdrawals_temp = frappe.db.sql("""SELECT count(`tabStock Entry Detail`.qty), sum(`tabStock Entry Detail`.qty), `tabStock Entry Detail`.stock_uom, `tabStock Entry Detail`.uom from `tabStock Entry Detail` LEFT JOIN `tabStock Entry`
                                ON `tabStock Entry Detail`.parent = `tabStock Entry`.name WHERE `tabStock Entry`.posting_date >= %s
                                and `tabStock Entry`.posting_date <= %s and `tabStock Entry Detail`.qty >0
                                and `tabStock Entry Detail`.item_code = %s
                                AND `tabStock Entry`.branch = %s AND `tabStock Entry`.type = 'Withdrawals' AND `tabStock Entry`.docstatus = 1""",
                       (date, now, item_code, branch))
    s3 = frappe.db.sql("""SELECT count(`tabStock Entry Detail`.qty), sum(`tabStock Entry Detail`.qty), `tabStock Entry Detail`.stock_uom, `tabStock Entry Detail`.uom   from `tabStock Entry Detail` LEFT JOIN `tabStock Entry`
                            ON `tabStock Entry Detail`.parent = `tabStock Entry`.name WHERE `tabStock Entry`.posting_date >= %s
                            and `tabStock Entry`.posting_date <= %s and `tabStock Entry Detail`.qty >0
                            and `tabStock Entry Detail`.item_code = %s
                            AND `tabStock Entry`.branch = %s AND `tabStock Entry`.type ='Stock Transfer' AND `tabStock Entry Detail`.s_warehouse = %s AND
                            `tabStock Entry`.docstatus = 1 AND `tabStock Entry`.change_cost_for_wet_market = %s""",
                            (date, now, item_code, branch, warehouse, "STOCK TRANSFER TO OTHER DEPARTMENTS (USE SRP)"))
    #Return PROMO:
    promo_qty = has_promo(item_code, branch, warehouse, date, now)
    promo_items = get_promo_items(branch, item_code, supplier, date, now, warehouse)
    promo_items_string = ",".join(promo_items)

    #print sales[0][0]
    #print s1
    #print s2
    #print s3

    if (str(s1[0][1]) == 'None') or str(s1[0][0]) == 'None':
        s1_sum, s1_qty = 0, 0
    else:
        s1_sum = s1[0][1]
        s1_qty = s1[0][0]


    if (str(s2[0][1]) == 'None') or str(s2[0][0]) == 'None':
        s2_sum, s2_qty = 0, 0
    else:
        s2_sum = (s2[0][1]*uom_conv)
        s2_qty =  s2[0][0]

    if (str(s3[0][1]) == 'None') or str(s3[0][0]) == 'None':
        s3_sum, s3_qty = 0, 0
    else:
        s3_sum = (s3[0][1]*uom_conv)
    #    s3_qty = s3[0][0]
    case_offtake = (case_offtake*uom_conv) if case_offtake!=None else 0
    sum = s1_sum + s2_sum  + case_offtake# + s3_sum
    new_avg = sum / 90 #divisor
    s = str(sales[0][0])

    if (s != 'None') and (s!=''):
        if (con_f == 'None') or (con_f == ''):
            con_f = 1
        if (factor == 'None') or (factor == ''):
            factor = 1
        try:
            isl = round((float(s) / (float(con_f)) * (float(factor)/2)), 0)
            cisl = round((float(s) / (float(con_f)) * float(factor)), 0)
            print "S is " + s + "| con_f is " + str(con_f) + "| factor is " + str(factor)
        except:

            isl = round((float(s) / (float(con_f)) * (1 / 2)), 0)
            cisl = round((float(s) / (float(con_f)) * float(1)), 0)

    isl2 = round((float(new_avg) * (float(factor)/2) / (float(con_f))), 0)
    cisl2 = round((float(new_avg) * float(factor) / (float(con_f))), 0)

    isl_new = 0
    cisl_new = 0

    if (new_avg == 0) and (cisl>0):
       isl_new = isl
       cisl_new = cisl
    else:
        isl_new = isl2
        cisl_new = cisl2
    m1 = get_m1(branch,item_code, supplier, now, warehouse)
    m2 = get_m2(branch,item_code, supplier, now, warehouse)
    m3 = get_m3(branch,item_code, supplier, now, warehouse)

    temp = float (cisl_new)
    print "NEW CISL: " + str(temp)
    #old_offtake = get_old_system_offtake(now, item_code, branch, con_f, factor)

    #print "OLD CISL:"+ str(old_offtake)
    #if old_offtake != None:
    #    if not((old_offtake == 0 ) and (abs(old_offtake - temp) > 5)):
    #        if str(old_offtake)!=None:
    #            if abs(old_offtake - temp) > 3:
    #                print "=======USE OLD OFFTAKE======="
    #                cisl_new = old_offtake


    if len(withdrawals_temp) > 0:
        for data in withdrawals_temp:
            withdrawals_offtake = data[1]
            no_days = data[0]
    withdrawals_offtake = withdrawals_offtake if (withdrawals_offtake!=None) else 0
    withdrawals_offtake *= uom_conv #withdrawals in PCS.

    #print "CISL Based on withdrawals: -----------------**"
    #print "WHOLESALE: ", s2_sum, "WITHDRAWALS"
    w_cisl = ((withdrawals_offtake+s2_sum)/90)*(float(factor))/uom_conv
    #print w_cisl
    #print "----------------*****------------------"


    months_data = str(int(m1/uom_conv))+" | "+str(int(m2/uom_conv))+ " | "+str(int(m3/uom_conv))+ "| PROMO QTY: "+str(promo_qty)+ " | "
    months_data += "PROMO ITEMS:"+promo_items_string
    inv = get_balance(warehouse, item_code, trans_date)
    return {"isl": isl_new, "cisl": cisl_new, "inv": inv, "months_data":months_data,
            "promo":(0 if ((promo_qty < 1) and len(promo_items)==0) else 1), "w_cisl":int(w_cisl)}
        #return {"isl": isl, "cisl": cisl, "inv": inv} #un-comment if CISL new is WRONG.


def get_balance(warehouse, item_code, trans_date):
    balance = 0
    recon = frappe.db.sql("""select posting_date, qty_after_transaction from `tabStock Ledger Entry`
                where warehouse = %s and
                item_code = %s and
                posting_date <= %s
                ORDER BY posting_date DESC, posting_time DESC, name DESC limit 1""",(warehouse, item_code, trans_date))
    balance = (recon[0][1] if len(recon)>0 else 0)
                #and voucher_type = 'Stock Reconciliation' ORDER BY posting_date DESC limit 1""",
                #          (warehouse, item_code, trans_date))

    #for r in recon:
    #    rem_date = str(r[0])
    #    qty = str(r[1])

    #    rem_total = frappe.db.sql("""Select sum(actual_qty) from `tabStock Ledger Entry`
    #            where warehouse = %s and
    #            item_code = %s and
    #            posting_date >= %s and
    #            posting_date <= %s and
    #            voucher_type != 'Stock Reconciliation'""", (warehouse, item_code, rem_date, trans_date))
    #    if ((str(rem_total[0][0])) == 'None'):

    #        balance = round(float(qty))

    #    else:
    #        rem_sum = str(rem_total[0][0])
    #        balance = round(float(rem_sum) + float(qty), 0)

    return balance

def get_m1(branch, item_code, supplier, to_date, warehouse):
    total = 0
    now = to_date
    now -= datetime.timedelta(days=60)
    date = now - datetime.timedelta(days=30)
    total = get_qty_sold(branch, item_code, supplier, date, now, warehouse)
    return total

def get_m2(branch, item_code, supplier, to_date, warehouse):
    total = 0

    now = to_date
    now -= datetime.timedelta(days=30)
    date = now - datetime.timedelta(days=30)
    total = get_qty_sold(branch, item_code, supplier, date, now, warehouse)
    return total

def get_m3(branch, item_code, supplier, to_date, warehouse):
    total = 0

    now = to_date
    date = now - datetime.timedelta(days=30)
    total = get_qty_sold(branch, item_code, supplier, date, now, warehouse)
    return total


def get_qty_sold(branch, item_code, supplier, date, now, warehouse):

    con_f = ""
    s1_sum, s1_qty = 0, 0
    s2_sum, s2_qty = 0, 0
    s3_sum, s3_qty = 0, 0
    new_avg = 0
    uom_conv = 0

    sum = 0

    frequency = frappe.db.sql("""Select offtake_calculation_frequency from `tabBranch` where branch = %s""", (branch))
    freq = str(frequency[0][0])
    fac_calc = frappe.db.sql("""Select (lead_time + cycle_time + buffer) from `tabSupplier Discounts` where name = %s""",
                             (supplier))  # factor calculation
    for f in fac_calc:

        factor = str(f[0])

        con_factor = frappe.db.sql(
            """Select packing from `tabItem` where item_code = %s""", (item_code))

        for f in con_factor:
            con_f = str(f[0])

        if str(con_f) == 'None':
            uom_conv = 1
        else:
            try:
                uom_conv = float(con_f)
            except:
                print con_f
                con_f = 1
                uom_conv = 1


        s1 = frappe.db.sql("""Select count(qty), sum(qty), uom from `tabUpload POS`
                                                              where trans_date >= %s and trans_date <= %s and qty !=0 and
                                                              barcode = (Select barcode_retial from `tabItem` where item_code = %s)
                                                             and branch = %s""",
                           (date, now, item_code, branch))
        s2 = frappe.db.sql("""SELECT count(`tabStock Entry Detail`.qty), sum(`tabStock Entry Detail`.qty), `tabStock Entry Detail`.stock_uom, `tabStock Entry Detail`.uom from `tabStock Entry Detail` LEFT JOIN `tabStock Entry`
                                 ON `tabStock Entry Detail`.parent = `tabStock Entry`.name WHERE `tabStock Entry`.posting_date >= %s
                                 and `tabStock Entry`.posting_date <= %s and `tabStock Entry Detail`.qty >0
                                 and `tabStock Entry Detail`.item_code = %s
                                 AND `tabStock Entry`.branch = %s AND `tabStock Entry`.type = 'Wholesale' AND `tabStock Entry`.docstatus = 1""",
                           (date, now, item_code, branch))
        case_offtake = get_offtake_from_case_barcode(item_code, date, now, branch)
        #s3 = frappe.db.sql("""SELECT count(`tabStock Entry Detail`.qty), sum(`tabStock Entry Detail`.qty), `tabStock Entry Detail`.stock_uom, `tabStock Entry Detail`.uom   from `tabStock Entry Detail` LEFT JOIN `tabStock Entry`
        #                    ON `tabStock Entry Detail`.parent = `tabStock Entry`.name WHERE `tabStock Entry`.posting_date >= %s
        #                    and `tabStock Entry`.posting_date <= %s and `tabStock Entry Detail`.qty >0
        #                    and `tabStock Entry Detail`.item_code = %s
        #                    AND `tabStock Entry`.branch = %s AND  `tabStock Entry`.type ='Stock Transfer' AND `tabStock Entry Detail`.s_warehouse = %s
        #                    AND `tabStock Entry`.docstatus = 1""",
        #                    (date, now, item_code, branch, warehouse))


        if str(s1[0][1]) == 'None':
            s1_sum, s1_qty = 0, 0
        else:
            s1_sum = s1[0][1]

        if str(s2[0][1]) == 'None':
            s2_sum, s2_qty = 0, 0
        else:
            s2_sum = (s2[0][1] * uom_conv)

        case_offtake = 0 if case_offtake == None else (case_offtake*uom_conv)

        #if str(s3[0][1]) == 'None':
        #    s3_sum, s3_qty = 0, 0
        #else:
        #    s3_sum = (s3[0][1] * uom_conv)

        # if (s1_qty+s2_qty+s3_qty)!=0:
        # new_avg = (s1_sum+s2_sum+s3_sum)/(s1_qty+s2_qty+s3_qty)
        sum = s1_sum + s2_sum + case_offtake #+ s3_sum


    return sum

def get_old_system_offtake(now, item_code, branch, packing, cycletime):
    print "===========OFFTAKE OLD==========="
    #print "============THIS MONTH==========="
    #print now.month, calendar.month_name[now.month]
    #print "===========LAST MONTH==========="
    last_month = now - relativedelta(months=1)
    #print last_month.month, calendar.month_name[last_month.month]
    #print "itemcode"
    print item_code
    barcode = ""
    barcodes = frappe.db.sql("""Select barcode, wholesale from `tabUpload Offtake` where item = %s""", item_code)
    offtake = None
    for row in barcodes:
        barcode = row[0]
    print "barcode"
    print barcode


    rows = frappe.db.sql("""SELECT m1_qty, m2_qty, m3_qty, m3 from `tabUpload Offtake` where barcode = %s and branch = %s""", (barcode, branch))

    if len(rows) == 0:
        offtake = None
    for data in rows:
        print "===============m3=============="
        if data[3] != calendar.month_name[last_month.month]:
            print "Data is old/not updated. Do not compute old offtake"
            offtake = None
            break
        else:
            print "OK."
        #print data, data[0], data[1], data[2]
        if (data[0]==None) or (data[1]== None) or (data[2]==None):
            offtake = None
            continue
        else:
            sum = (int(data[0]) + int(data[1]) + int(data[2]))
            offtake = ((sum)*(float(cycletime)))/90 #multiply to cycletime so that offtake isn't 0.
            #offtake = ((data[0] + data[1] + data[2]) / 90)/packing*cycletime
            print offtake

    return offtake

@frappe.whitelist()
def reset_item_price(items, branch):
    items = json.loads(items)
    newlist = sorted(items, key=lambda k: k['idx'])
    #print items
    for item in items:
        print "=============PO ITEM============="
        print item
        cost =frappe.db.sql("""SELECT item_cost_with_freight, item_code from `tabItem` where item_code = %s""",item['item_code'])
        try:
            if cost[0][0] != None:
                item['rate'] = cost[0][0]
        except:
            continue
    return items

def get_item_shelf_inv(item):
    item_doc = frappe.get_doc("Item", item)
    return item_doc.shelf_inventory

def get_supplier_discount(item):
    disc = []
    discounts = frappe.db.sql("""SELECT parent from `tabSupplier Discount Items` where items = %s""", item)
    if (len(discounts) > 0):
        for i in discounts:
            #print i[0]
            disc.append(i[0])
        return disc
    else:
        return "Has NO Supplier Discount"

def has_promo(item_code, branch, s_warehouse, date, now):
    print "=================CHECKING FOR PROMO ================"
    promo = frappe.db.sql("""SELECT count(`tabStock Entry Detail`.qty), sum(`tabStock Entry Detail`.qty), `tabStock Entry Detail`.stock_uom, `tabStock Entry Detail`.uom   from `tabStock Entry Detail` LEFT JOIN `tabStock Entry`
                        ON `tabStock Entry Detail`.parent = `tabStock Entry`.name WHERE `tabStock Entry`.posting_date >= %s
                        and `tabStock Entry`.posting_date <= %s and `tabStock Entry Detail`.qty >0
                        and `tabStock Entry Detail`.item_code = %s
                        AND `tabStock Entry`.branch = %s AND `tabStock Entry Detail`.s_warehouse = %s
                        and `tabStock Entry Detail`.t_warehouse LIKE %s
                        AND `tabStock Entry`.docstatus = 1""",
                       (date, now, item_code, branch, s_warehouse, 'Promo%'))
    if len(promo)>0:
        for item in promo:
            print "PROMO SUM QTY", item[1]
            return item[1]
    else:
        return 0

def get_promo_items(branch, item_code, supplier, from_date, to_date, warehouse):
    print "------------PROMO ITEMS---------------"
    promo_items =[]
    items = frappe.db.sql("""SELECT name,item_name_dummy from `tabItem` where (type = 'Promo Pack' or type = 'Company Bundling') and regular_item = %s""",item_code)
    for item in items:
        qty_sold = get_qty_sold(branch, item[0], supplier, from_date, to_date, warehouse)
        print "ITEM:", item[1], "QTY:", qty_sold
        inv = get_balance(warehouse, item[0],to_date)
        promo_items.append(item[1]+"(OFFTK:"+str(qty_sold)+"| INV: "+str(inv)+")")
    return promo_items

def is_new_item(item_name):
    records = frappe.db.sql("""select creation, now(), name from `tabItem` where DATEDIFF(NOW(),creation)<90 and name = %s""",item_name)
    if len(records)>0:
        return 1
    else:
        return 0

def get_offtake_from_case_barcode(item, from_date, to_date, branch):
    print item, from_date, to_date, branch
    item_doc = frappe.get_doc("Item", item)
    print item_doc.name, item_doc.barcode_pack
    print "--------QTY FROM CASE BARCODE:---------"
    if item_doc.barcode_retial == item_doc.barcode_pack:
        return 0
    else:
        offtake = frappe.db.sql("""Select sum(qty) from `tabUpload POS` where barcode = %s and
    								trans_date >= %s and trans_date <= %s and branch = %s""",
                          (item_doc.barcode_pack, from_date, to_date, branch))

        print offtake
        qty_sold = offtake[0][0] if offtake[0][0]!= None else 0
        print "QTY SOLD --------------> ", qty_sold
        return qty_sold

@frappe.whitelist()
def test_create_pos_for_item():
    dates = ["2018-04-01","2018-04-02", "2018-04-03", "2018-04-04", "2018-04-05", "2018-04-06", "2018-04-07", "2018-04-08", "2018-04-10", "2018-04-11"]
    for date in dates:
        pos_doc = {
            "doctype":"Upload POS",
            "trans_date":date,
            "barcode": "4800361406024",
            "branch": "CDO Main",
            "qty": 50,
            "upload_date":date,
            "filename": "temp.txt",
            "amount": "71717.5",
            "price": "1,434.35",
            "cost": "1,366.02"
        }
        #upload_date, filename, price, amount, cost
        doc_for_insert = frappe.get_doc(pos_doc)
        doc_for_insert.insert()

@frappe.whitelist()
def create_item_id(from_date, to_date):
    po_name = frappe.db.sql("""SELECT name,branch_code from `tabPurchase Order` where creation BETWEEN %s and %s""",(from_date, to_date))
    for po_names in po_name:
        naming = frappe.db.sql("""SELECT name from `tabPurchase Order Item` WHERE parent = %s AND item_id IS NULL""",(po_names[0]))
        print po_names[0]
        for namings in naming:
            print namings[0]
            item_id = po_names[1] + "-" + namings[0]
            frappe.db.sql("""UPDATE `tabPurchase Order Item` set item_id = %s where parent = %s""",(item_id, po_names[0]))
            frappe.db.commit()

@frappe.whitelist()
def get_item_cost_per_uom(item_code, uom, supplier_discount):
    item = frappe.get_doc("Item", item_code)
    discount = frappe.get_doc("Supplier Discounts", supplier_discount)
    if discount.freight_cost_paid_by_supplier == 1:
        item_cost = item.item_cost
    else:
        item_cost = item.item_cost_with_freight

    conv_factor = frappe.db.sql("""Select conversion_factor from `tabUOM Conversion Detail` where parent = %s and
                                uom = %s""", (item_code, uom))
    if len(conv_factor)==0:
        frappe.throw("Please create a UOM Conversion factor for "+uom+" in "+item_code+".")
    else:
        for factor in conv_factor:
            converted_cost = item_cost/(int(factor[0]))
        return converted_cost

def update_po_supplier():
    suppliers = get_supplier_list()
    pos= frappe.db.sql("""SELECT name, supplier_discount_name from `tabPurchase Order` where supplier is NULL """)
    for po in pos:
        this_supplier = None
        for supplier in suppliers:
            if supplier in po[1]:
                this_supplier = supplier
                break
        print po[0], "|", po[1], "| SUPPLIER: ", str(this_supplier)
        if this_supplier is not None:
            frappe.db.sql("""UPDATE `tabPurchase Order` set supplier = %s where name = %s and supplier is NULL""",(this_supplier, po[0]))
            frappe.db.commit()

def get_supplier_list():
    data = []
    for row in frappe.db.sql("""SELECT name from `tabSupplier`"""):
        data.append(row[0])
    return data

@frappe.whitelist()
def check_supplier_discount_for_auto_po(doc, method):
    form_supplier_discount = doc.supplier_discount
    rows = frappe.db.sql("""select supplier_discount from `tabDiscounts for Auto Po`""",as_dict = True)
    for row in rows:
        if form_supplier_discount == row['supplier_discount']:
            print form_supplier_discount
            frappe.db.sql("""delete from `tabDiscounts for Auto Po` where supplier_discount = %s""",(row['supplier_discount']))
            frappe.db.commit()

@frappe.whitelist()
def check_discount_in_branch(discount, branch):
    disc_doc = frappe.get_doc("Supplier Discounts", discount)
    branch_list = frappe.db.sql("""SELECT branch from `tabItem Branch` where parent = %s and branch = %s""", (discount, branch))
    if len(branch_list)<1 and disc_doc.all_branches!=1 and branch!="Temporary":
        return False
    else:
        return True