import frappe
from array import array
import datetime
from erpnext.controllers.stock_controller import StockController
from popupevents import test_duplicate_rr



@frappe.whitelist()
def get_items_recon(sup_disc):
    items = []

    doc = frappe.db.sql("""Select supplier.items from `tabSupplier Discount Items` supplier LEFT join `tabItem` item on supplier.items = item.name where supplier.parent = %s order by item.type DESC, supplier.items asc""",
                        (sup_disc))

    for item in doc:
        item_code = str(item[0])
        db_item = frappe.db.sql("""Select name, item_cost, uom, item_name_dummy from tabItem where item_code = %s""", (item_code))
        for d in db_item:
            item_name = str(d[0])
            item_cost = str(d[1])
            item_uom = str(d[2])
            item_description= str(d[3])
            items.append({"item_code":item_code, "item_name":item_name, "uom":item_uom,
                          "item_description":item_description, "item_cost":item_cost})
            #items_dict_code = {"item": item_code}
            #items_dict_name = {"item": item_name}
            #items_dict_cost = {"item": item_cost}
            #items_dict_uom = {"item": item_uom}

            #items_array_code.append(items_dict_code)
            #items_array_name.append(items_dict_name)
            #items_array_cost.append(items_dict_cost)
            #items_array_uom.append(items_dict_uom)

    return items


"""
Force submit: Used to submit RR
"""
@frappe.whitelist()
def force_submit(sr):
    sr_doc = frappe.get_doc("Stock Reconciliation", sr)
    frappe.db.sql("""Update `tabStock Reconciliation Item` set docstatus = 1, modified = NOW() where parent = %s;""", sr)
    frappe.db.sql("""Update `tabStock Reconciliation` set docstatus = 1, modified = NOW() where name = %s""", sr)
    frappe.db.commit()

    try:
        update_stock_ledger(sr_doc)
    except:
        return "slkdhfds"

def update_stock_ledger(self):
    """	find difference between current and expected entries
        and create stock ledger entries based on the difference"""
    from erpnext.stock.stock_ledger import get_previous_sle

    for row in self.items:
        previous_sle = get_previous_sle({
            "item_code": row.item_code,
            "warehouse": row.warehouse,
            "posting_date": self.posting_date,
            "posting_time": self.posting_time
        })
        if previous_sle:
            if row.qty in ("", None):
                row.qty = previous_sle.get("qty_after_transaction", 0)

            if row.valuation_rate in ("", None):
                row.valuation_rate = previous_sle.get("valuation_rate", 0)

        if row.qty and not row.valuation_rate:
            frappe.throw("Valuation Rate required for Item in row {0}".format(row.idx))

        if ((previous_sle and row.qty == previous_sle.get("qty_after_transaction")
             and row.valuation_rate == previous_sle.get("valuation_rate"))
            or (not previous_sle and not row.qty)):
            continue

        self.insert_entries(row)


def insert_entries(self, row):
    """Insert Stock Ledger Entries"""
    args = frappe._dict({
        "doctype": "Stock Ledger Entry",
        "item_code": row.item_code,
        "warehouse": row.warehouse,
        "posting_date": self.posting_date,
        "posting_time": self.posting_time,
        "voucher_type": self.doctype,
        "voucher_no": self.name,
        "company": self.company,
        "stock_uom": frappe.db.get_value("Item", row.item_code, "stock_uom"),
        "is_cancelled": "No",
        "qty_after_transaction": row.qty,
        "valuation_rate": row.valuation_rate
    })
    self.make_sl_entries([args])


def update_sle_srs():
    today = datetime.date.today()
    december = (today - datetime.timedelta(days=180))
    items = []
    #get latest SR's and their items + details. if Item has been checked, add item to the list. Item will no be checked again.
    latest_sr = frappe.db.sql("""Select sr.name, sr.actual_qty, sr.qty_after_transaction, sr.posting_date, sr.posting_time, sr.warehouse, sr.item_code
                    from `tabStock Ledger Entry` as sr inner join `tabWarehouse` wh on sr.warehouse = wh.name where
                    sr.voucher_no like %s and sr.warehouse = %s and sr.posting_date >=%s ORDER BY name desc """,
                              ('SR-' + '%', "CDO Warehouse - GG", str(december) ))
    for sr in latest_sr:
        sr_name = sr[0]
        sr_actual_qty = sr[1]
        sr_qty_after_transaction = sr[2]
        sr_posting_date = sr[3]
        sr_posting_time = sr[4]
        sr_warehouse = sr[5]
        item = sr[6]

        #print sr_name, sr_actual_qty, sr_qty_after_transaction, sr_posting_date, sr_posting_time, item
        if item not in items:
            items.append(check_sles_after_sr(sr_qty_after_transaction, sr_posting_date, sr_posting_time, sr_warehouse, item))


def check_sles_after_sr(sr_qty_after_transaction, sr_posting_date, sr_posting_time, sr_warehouse, item):
    sles = frappe.db.sql("""SELECT name, posting_date, posting_time, actual_qty, qty_after_transaction, voucher_no from `tabStock Ledger Entry` where
                        item_code = %s and posting_date >= %s and warehouse = %s order by posting_date ASC, posting_time ASC"""
                         , (item, sr_posting_date, sr_warehouse))
    current_qty = int(sr_qty_after_transaction)
    #print "Current QTY AFter RECON:", current_qty
    for sle in sles:
        sle_name, sle_posting_date, sle_posting_time = sle[0], sle[1], sle[2]
        sle_actual_qty, sle_qty_after_transaction = int(sle[3]), int(sle[4])
        voucher_no = sle[5]
        if sle_posting_date == sr_posting_date:
            if sle_posting_time > sr_posting_time:
                #print "QTY after transaction Should be", (current_qty + sle_actual_qty)
                current_qty = update_sle(sle_name, (current_qty + sle_actual_qty), sle_qty_after_transaction, voucher_no)
        elif sle_posting_date > sr_posting_date:
            #print "Actual QTY ", sle_actual_qty, "QTY after Transaction", sle_qty_after_transaction, item
            #print "QTY after transaction Should be", (current_qty + sle_actual_qty)
            current_qty = update_sle(sle_name, (current_qty + sle_actual_qty), sle_qty_after_transaction, voucher_no)
    return item

def update_sle(name, qty_after_transaction, sle_qty_after_transaction, voucher_no):
    if qty_after_transaction != sle_qty_after_transaction:
        if "RR" in voucher_no:
            print "Update this RR: ", voucher_no #if RR, correct. If not. Kill yourself. T_T
            test_duplicate_rr(voucher_no)
        #frappe.db.sql("""UPDATE `tabStock Ledger Entry` set qty_after_transaction = %s where name = %s""",(qty_after_transaction, name))
        #frappe.db.commit()
    else:
        print "OKAY~No need to worry"
    return qty_after_transaction
