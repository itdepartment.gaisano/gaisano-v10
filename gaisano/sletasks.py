import frappe, datetime

def get_docs_no_sles():
    #RR's
    for rr in frappe.db.sql("""SELECT name, docstatus, status from `tabPurchase Receipt` where docstatus = 1"""):
        count = len(frappe.db.sql("""Select * from `tabStock Ledger Entry` where voucher_no = %s""", rr[0]))
        print count, rr[0]
        if count == 0:
            print rr[0] + " "+str(rr[1]) + " "+ rr[2]+ " NO SLE!"
    for se in frappe.db.sql("""SELECT name, docstatus, workflow_state from `tabStock Entry` where docstatus = 1"""):
        count = len(frappe.db.sql("""Select count(name) from `tabStock Ledger Entry` where voucher_no = %s""", se[0]))
        if count == 0:
            print se[0] +" " +str(se[1]) +" "+ se[2] +" NO SLE!"


def delete_sle_not_in_branch(from_date, to_date):
    branch = frappe.db.get_value("Server Information", None, "branch")
    rows = frappe.db.sql("""select sle.name, sle.warehouse, sle.item_code, w.branch, sle.posting_date from `tabStock Ledger Entry` sle
                    inner join `tabWarehouse` w on sle.warehouse = w.name where w.branch != %s and
                    sle.creation >= %s and sle.creation <= %s""",(branch, from_date, to_date))
    frappe.db.sql("""DELETE sle from `tabStock Ledger Entry` sle inner join `tabWarehouse` w on sle.warehouse = w.name
                    where w.branch != %s and sle.creation >= %s and sle.creation <= %s""",(branch, from_date, to_date))
    frappe.db.commit()

    print "NO OF SLE ROWS DELETED:", len(rows)

def delete_sle_for_main(from_date, to_date):
    branch = frappe.db.get_value("Server Information", None, "branch")
    rows = frappe.db.sql("""SELECT count(sle.name) from `tabStock Ledger Entry` sle inner join `tabStock Entry` se on se.name = sle.voucher_no
                          inner join `tabWarehouse` w on sle.warehouse = w.name where w.branch !=%s and sle.creation >=%s
                          and sle.creation <=%s and se.branch = %s""", (branch, from_date, to_date, branch))
    frappe.db.sql("""DELETE sle from `tabStock Ledger Entry` sle inner join `tabStock Entry` se on se.name = sle.voucher_no
                          inner join `tabWarehouse` w on sle.warehouse = w.name where w.branch !=%s and sle.creation >=%s
                          and sle.creation <=%s and se.branch = %s""", (branch, from_date, to_date, branch))
    print "DONE DELETING FOR ", from_date, " to ", to_date
    print "NO OF SLE ROWS DELETED:", rows[0][0]

def delete_sle_hourly():
    from_date = datetime.date.today()
    to_date = from_date + datetime.timedelta(days=1)
    branch = frappe.db.get_value("Server Information", None, "branch")
    # if MAIN server, script should not delete SLE's from SYM sync. Delete SLE's from SE's only.
    # if BRANCH server,  script should delete ALL SLE's from warehouses not belonging to the branch.
    print from_date, "-", to_date, "| BRANCH:", branch

    if branch == "CDO Main":
        print "DELETE FOR MAIN"
        delete_sle_for_main(from_date, to_date)
    else:
        print "DELETE FOR BRANCH"
        delete_sle_not_in_branch(from_date,to_date)


def delete_sle_range(from_date, to_date):
    branch = frappe.db.get_value("Server Information", None, "branch")
    # if MAIN server, script should not delete SLE's from SYM sync. Delete SLE's from SE's only.
    # if BRANCH server,  script should delete ALL SLE's from warehouses not belonging to the branch.
    print from_date, "-", to_date, "| BRANCH:", branch

    if branch == "CDO Main":
        print "DELETE FOR MAIN"
        delete_sle_for_main(from_date, to_date)
    else:
        print "DELETE FOR BRANCH"
        delete_sle_not_in_branch(from_date, to_date)

# HOW TO CHECK FOR RUNNING TIME (ACTUAL)
    #  import time
    # time_now = time.time()
    # time_after = time.time()
    # time_diff = time_after - time_now
    # print time_now, time_after, time_diff
