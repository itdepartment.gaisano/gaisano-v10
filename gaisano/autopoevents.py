import frappe
import datetime
import frappe.utils

def set_item_safety_stock():
    items = frappe.db.sql("""SELECT name, packing, barcode_retial, item_price, item_cost from `tabItem` where type='Active'""")
    branch = frappe.db.get_value("Server Information", None, "branch")
    warehouse = frappe.db.get_value("Auto PO", "Auto PO", 'auto_po_warehouse')
    now = datetime.datetime.now()
    posting_time = now.strftime("%H:%M:%S.%f")
    now -= datetime.timedelta(days=1)
    date = now - datetime.timedelta(days=90)
    print branch, warehouse, posting_time, now, date

    for item in items:
        discounts = frappe.db.sql("""SELECT supplier_discount from `tabItem Branch Discount` where parent = %s and
                                    branch = %s""", (item[0], branch))
        for row in discounts:
            if row[0]:
                try:
                    supplier_discount = frappe.get_doc("Supplier Discounts", row[0])
                    lead_time = int(supplier_discount.lead_time)
                    inv = get_balance(item[0], warehouse, now, posting_time)
                    s1 = frappe.db.sql("""Select count(qty), sum(qty) from `tabUpload POS`
                                                                         where trans_date >= %s and trans_date <= %s and qty !=0 and
                                                                         barcode = %s
                                                                        and branch = %s""",
                                       (date, now, item[2], branch))
                    s2 = frappe.db.sql("""SELECT count(`tabStock Entry Detail`.qty), sum(`tabStock Entry Detail`.qty), `tabStock Entry Detail`.stock_uom, `tabStock Entry Detail`.uom from `tabStock Entry Detail` LEFT JOIN `tabStock Entry`
                                                ON `tabStock Entry Detail`.parent = `tabStock Entry`.name WHERE `tabStock Entry`.posting_date >= %s
                                                and `tabStock Entry`.posting_date <= %s and `tabStock Entry Detail`.qty >0
                                                and `tabStock Entry Detail`.item_code = %s
                                                AND `tabStock Entry`.branch = %s AND `tabStock Entry`.type = 'Wholesale' AND `tabStock Entry`.docstatus = 1""",
                                       (date, now, item[0], branch))
                    s3 = frappe.db.sql("""SELECT count(`tabStock Entry Detail`.qty), sum(`tabStock Entry Detail`.qty), `tabStock Entry Detail`.stock_uom, `tabStock Entry Detail`.uom   from `tabStock Entry Detail` LEFT JOIN `tabStock Entry`
                                            ON `tabStock Entry Detail`.parent = `tabStock Entry`.name WHERE `tabStock Entry`.posting_date >= %s
                                            and `tabStock Entry`.posting_date <= %s and `tabStock Entry Detail`.qty >0
                                            and `tabStock Entry Detail`.item_code = %s
                                            AND `tabStock Entry`.branch = %s AND `tabStock Entry`.type ='Stock Transfer' AND `tabStock Entry Detail`.s_warehouse = %s AND
                                            `tabStock Entry`.docstatus = 1""",
                                       (date, now, item[0], branch, warehouse))
                    if str(s1[0][1]) == 'None':
                        s1_sum, s1_qty = 0, 0
                    else:
                        s1_sum = s1[0][1]

                    if str(s2[0][1]) == 'None':
                        s2_sum, s2_qty = 0, 0
                    else:
                        s2_sum = (s2[0][1] * item[1])

                    if str(s3[0][1]) == 'None':
                        s3_sum, s3_qty = 0, 0
                    else:
                        s3_sum = (s3[0][1] * item[1])
                    sum = s1_sum + s2_sum + s3_sum
                    print item[0], s1_sum, s2_sum, s3_sum
                    item_packing = sum / item[1]
                    offtake = float(item_packing) / 90
                    safety_stock = lead_time * round(offtake)
                    if safety_stock >= 1 and safety_stock - inv > 0:
                        frappe.db.sql("""update `tabItem Branch Discount` set safety_stock = %s, auto_po_pop = 1 where parent = %s and branch = %s """,(safety_stock, item[0], branch))
                    else:
                        frappe.db.sql("""update `tabItem Branch Discount` set safety_stock = %s, auto_po_pop = 0 where parent = %s and branch = %s """,(safety_stock, item[0], branch))

                except:
                    continue
            else:
                print item[0], "no discount -----------------------------------------------####"
    frappe.db.commit()
    return "finished"

def get_items_below_safety_stock():
    data = []
    branch = frappe.db.get_value("Server Information", None, "branch")
    from poevents import get_balance
    date = datetime.date.today()
    supplier_discounts = frappe.db.sql("""select name from `tabSupplier Discounts`""")
    for supplier_discount in supplier_discounts:
        print supplier_discount[0]
        items = frappe.db.sql("""select parent, supplier_discount, supplier_discount_name, safety_stock from `tabItem Branch Discount` where branch = %s and supplier_discount = %s and round(safety_stock, 0) != 0 and safety_stock is not null """,(branch, supplier_discount))
        for item in items:
            try:
                item_doc = frappe.get_doc("Item", item[0])
                item_warehouse = frappe.db.get_value("Auto PO", "Auto PO", 'auto_po_warehouse')
                print item_warehouse

            except:
                continue
            else:
                if "Display" in frappe.db.get_value("Auto PO", "Auto PO", 'auto_po_warehouse'):
                    continue
                else:
                    if (len(data) > 0):
                        if (data[len(data)-1] == item[1]):
                            continue
                    safety_stock = round(float(item[3]))
                    inv = get_balance(item_warehouse, item[0], date)
                    print item[0], safety_stock - inv
                    if safety_stock >= 1 and safety_stock - inv > 0:
                        data.append({"supplier_discount":supplier_discount[0], "supplier_discount_name":item[2]})
                        break


    return data


def get_discounts_for_po(): #for daily task
    warehouse = frappe.db.get_value("Auto PO", "Auto PO", "auto_po_warehouse")
    if warehouse == "" or warehouse is None:
        print "No warehouse"
    else:
        msg = set_item_safety_stock()
        print msg
        frappe.db.sql("""DELETE from `tabDiscounts for Auto Po`""")
        data = get_items_below_safety_stock()

        for r in data :
            item_dict = {
                "doctype": "Discounts for Auto Po",
                "parent": "Auto PO",
                "parenttype": "Auto PO",
                "parentfield": "for_auto_po",
                "supplier_discount": r['supplier_discount'],
                "supplier_discount_name": r['supplier_discount_name']
            }
            table_row = frappe.get_doc(item_dict)
            table_row.insert()
            check_po_expected_delivery()

####FOR ITEM STOCK LEVEL UPDATE
def update_item_stock_level(local_item_name):
	branch = frappe.db.get_value("Server Information", None, "branch")
	print local_item_name, branch
	discounts = frappe.db.sql("""SELECT supplier_discount from `tabItem Branch Discount` where parent = %s and
				branch = %s""", (local_item_name, branch))
	for discount in discounts:
		supplier_discount = frappe.get_doc("Supplier Discounts", discount[0])
		lead_time = int(supplier_discount.lead_time)
		safety_stock = lead_time * 2
		query = '''UPDATE `tabItem` set safety_stock = {0} where name = "{1}"'''.format(safety_stock, local_item_name)
		frappe.db.sql(query)
		frappe.db.commit()

#delete supplier_discounts in `tabDiscounts for Auto Po`
def check_po_expected_delivery():
    today = datetime.date.today()
    pos = frappe.db.sql("""select name, expected_delivery, supplier_discount from `tabPurchase Order` where expected_delivery >= %s and docstatus = 1""", today)
    for po in pos:
        supplier_discount = po[2]
        discount_pos = frappe.db.sql("""select name, supplier_discount, supplier_discount_name  from `tabDiscounts for Auto Po` where supplier_discount = %s """, supplier_discount)
        for discount_po in discount_pos:
            print discount_po[0]
            auto_po_name = discount_po[0]
            frappe.db.sql("""DELETE from `tabDiscounts for Auto Po` where name = %s""", auto_po_name)
            frappe.db.commit()

def get_balance(item_code,warehouse, posting_date, posting_time=None):
    from erpnext.stock.stock_ledger import get_previous_sle
    from frappe.utils import flt, cstr, nowdate, nowtime
    if not posting_date: posting_date = nowdate()
    if not posting_time: posting_time = nowtime()

    last_entry = get_previous_sle({
        "item_code": item_code,
        "warehouse": warehouse,
        "posting_date": posting_date,
        "posting_time": posting_time})
    try:
        return last_entry.qty_after_transaction
    except:
        return 0




